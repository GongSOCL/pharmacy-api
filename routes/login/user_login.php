<?php
use Hyperf\HttpServer\Router\Router;
use App\Controller\Auth\AuthController;

Router::addGroup('/api/auth', function () {
    Router::post('/login', [AuthController::class, 'login']);
    Router::post('/get-role', [AuthController::class, 'getRole']);
});
