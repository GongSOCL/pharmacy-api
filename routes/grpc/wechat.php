<?php
declare(strict_types=1);

use App\Handler\Wechat\WechatHandler;
use Grpc\Pharmacy\Wechat\WechatSvcRegister;

WechatSvcRegister::register(WechatHandler::class);
