<?php
declare(strict_types=1);

use App\Handler\Resource\ResourceHandler;
use Grpc\Pharmacy\Resource\ResourceSvcRegister;

ResourceSvcRegister::register(ResourceHandler::class);