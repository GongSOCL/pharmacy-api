<?php
declare(strict_types=1);

use Hyperf\HttpServer\Router\Router;
use App\Middleware\AuthMiddleware;
use App\Controller\Store\StoreController;

Router::addGroup('/api/store', function () {
    // 扫医生码药店列表
    Router::get('/scan-store-list', [StoreController::class, 'scanStoreList']);
    // 药店列表
    Router::get('/store-list', [StoreController::class, 'storeList']);
    // 附近药店列表
    Router::get('/near-store-list', [StoreController::class, 'nearStoreList']);

    // 药店详情
    Router::get('/store-detail', [StoreController::class, 'storeDetail']);
    // 获取药店gps坐标
    Router::get('/{id:\d+}/gps', [StoreController::class, 'getGpsInfo']);

    // 获取药店当前所在城市或所属科室
    Router::get('/current-info', [StoreController::class, 'getCurrentGeoPosAndDepart']);
    // 根据当前所在城市或科室获取店铺列表
    Router::get('/geo-store-list', [StoreController::class, 'geoStoreList']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
