<?php
declare(strict_types=1);

use Hyperf\HttpServer\Router\Router;
use App\Middleware\AuthMiddleware;
use App\Controller\Store\StoreController;

Router::addGroup('/api/store/gps', function () {
    // 城市列表
    Router::get('/city-list', [StoreController::class, 'cityList']);
    // 连锁总店列表
    Router::get('/main-store-list', [StoreController::class, 'mainStoreListByCity']);
    // 店铺列表
    Router::get('/store-list', [StoreController::class, 'branchStoreListByMain']);
    // 省市列表
    Router::get('/province-list', [StoreController::class, 'provinceList']);
    // 省市城市列表
    Router::get('/province-city-list', [StoreController::class, 'provinceCityList']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
