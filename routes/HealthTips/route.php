<?php
declare(strict_types=1);

use Hyperf\HttpServer\Router\Router;
use App\Middleware\AuthMiddleware;
use App\Controller\HealthTips\HealthTipsController;

Router::addGroup('/api/tips', function () {

    # 头部类型
    Router::get('/res-mold', [HealthTipsController::class, 'getResourcesMold']);

    # 健康小知识
    Router::get('/health-tips/{MoldId:\d+}', [HealthTipsController::class, 'getHealthTips']);

    # 适应症
    Router::get('/indication/{MoldId:\d+}', [HealthTipsController::class, 'getIndication']);

    # 资源
    Router::get('/res/{ResId:\d+}', [HealthTipsController::class, 'getResourceByID']);

    # 说明书
    Router::get('/drug/instructions/{DrugId:\d+}', [HealthTipsController::class, 'getInstructionsByID']);

    # 服务指南
    Router::get('/drug/guide/{ResId:\d+}', [HealthTipsController::class, 'getServiceGuide']);

    # 用户反馈
    Router::get('/drug/question/{ResId:\d+}', [HealthTipsController::class, 'getQuestionAnswer']);

    # 点赞
    Router::put('/drug/set-fab/{QId:\d+}', [HealthTipsController::class, 'setQuestionAnswerFab']);

    # 首页
    Router::get('/dy-index', [HealthTipsController::class, 'getDYIndex']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
