<?php
declare(strict_types=1);

use App\Controller\Questionnaire\QuestionnaireController;
use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup("/api/questionnaire", function () {
    //活动问卷-培训中心
    Router::get('/activity-question', [QuestionnaireController::class, 'activityQuestionnaire']);

    Router::post('/start', [QuestionnaireController::class, 'start']);
    //获取问卷题目
    Router::get('/question/{id:\d+}', [QuestionnaireController::class, 'get']);
    //回答问卷试题
    Router::post('/answer/{id:\d+}', [QuestionnaireController::class, 'answer']);
    //提交问卷
    Router::put('/submit', [QuestionnaireController::class, 'submit']);
}, [
    'middleware' => [AuthMiddleware::class]
]);
