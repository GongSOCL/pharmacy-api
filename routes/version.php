<?php
declare(strict_types=1);

use App\Controller\Common\VersionController;
use Hyperf\HttpServer\Router\Router;

Router::get('/api/version', [VersionController::class, 'info']);
