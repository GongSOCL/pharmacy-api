<?php
declare(strict_types=1);

use Hyperf\HttpServer\Router\Router;
use App\Middleware\AuthMiddleware;
use App\Controller\Clerk\ClerkController;

Router::addGroup('/api/clerk', function () {
    // 店员申请
    Router::post('/apply', [ClerkController::class, 'apply']);
    // 我的店员列表
    Router::get('/my-clerk-list', [ClerkController::class, 'myClerkList']);
    // 我的店员详情汇总
    Router::get('/my-clerk-detail', [ClerkController::class, 'myClerkSaleInfo']);
    // 我的店员详情销售列表
    Router::get('/my-clerk-sale-list', [ClerkController::class, 'myClerkSaleList']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
