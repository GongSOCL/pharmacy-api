<?php
declare(strict_types=1);

use Hyperf\HttpServer\Router\Router;
use App\Middleware\AuthMiddleware;
use App\Controller\Clerk\ClerkController;

Router::addGroup('/api/clerk/sale', function () {
    // 购买通知列表
    Router::get('/notify-list', [ClerkController::class, 'notifyList']);
    // 取消购买通知
    Router::get('/cancel-notify', [ClerkController::class, 'cancelNotify']);
    // 接受购买通知
    //Router::get('/receive-notify', [ClerkController::class, 'receiveNotify']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
