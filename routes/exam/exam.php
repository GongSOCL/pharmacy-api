<?php
declare(strict_types=1);

use App\Controller\Exam\ExamController;
use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup("/api/exam", function () {
    //开始考试
    Router::get('', [ExamController::class, 'list']);
    //详情
    Router::get('/{id:\d+}', [ExamController::class, 'info']);
    //开始考试
    Router::post('/{id:\d+}/start', [ExamController::class, 'start']);
    //获取试题
    Router::get('/{id:\d+}/question', [ExamController::class, 'getQuestion']);
    //回答题目
    Router::post('/{id:\d+}/answer', [ExamController::class, 'answer']);
    //交卷
    Router::post('/{id:\d+}/submit', [ExamController::class, 'finish']);
    //考试回顾
    Router::get('/{id:\d+}/review', [ExamController::class, 'review']);
}, [
    'middleware' => [AuthMiddleware::class]
]);
