<?php
declare(strict_types=1);

use App\Controller\Agent\StoreController;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/api/agent/stores', function () {
    //药店列表
    Router::get('', [StoreController::class, 'list']);
    //药店邀请二维码
    Router::get('/{id:\d+}/qrcode', [StoreController::class, 'getQrLink']);
    //获取分享链接
    Router::get('/{id:\d+}/invite', [StoreController::class, 'getInviteLink']);
});
