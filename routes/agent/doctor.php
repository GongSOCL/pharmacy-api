<?php
declare(strict_types=1);

use App\Controller\Agent\DoctorController;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/api/agent/doctors', function () {
    //医生列表
    Router::get('', [DoctorController::class, 'list']);
    //获取医生二维码
    Router::get('/{id:\d+}/qrcode', [DoctorController::class, 'getQrLink']);
});
