<?php
declare(strict_types=1);

use App\Controller\Order\OrderController;
use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/api/orders', function () {
    //添加订单
    Router::post('', [OrderController::class, 'submit']);
    //店员订单列表
    Router::get('', [OrderController::class, 'list']);
    //患者订单列表
    Router::get('/patient/list', [OrderController::class, 'patientList']);
    //店员销售订单列表
    Router::get('/clerk/sale/list', [OrderController::class, 'searchClerkList']);
}, [
    'middleware' => [
        AuthMiddleware::class
    ]
]);
