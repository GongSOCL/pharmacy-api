<?php
declare(strict_types=1);

use App\Controller\Common\ProductController;
use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/api/products', function () {
    //产品列表
    Router::get('', [ProductController::class, 'list']);
    //产品sku列表
    Router::get('/{id:\d+}/skus', [ProductController::class, 'listSku']);
}, [
    'middleware' => [
        AuthMiddleware::class
    ]
]);
