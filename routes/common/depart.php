<?php
declare(strict_types=1);

use App\Controller\Common\DepartController;
use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/api/departs', function () {
    //产品列表
    Router::get('', [DepartController::class, 'departList']);
}, [
    'middleware' => [
        AuthMiddleware::class
    ]
]);
