<?php
declare(strict_types=1);

use App\Controller\Common\SmsController;
use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/api/send-code', function () {
    //发送验证码
    Router::post('', [SmsController::class, 'sendCode']);
}, [
    'middleware' => [
        AuthMiddleware::class
    ]
]);

Router::addGroup('/api/verify-code', function () {
    //验证验证码
    Router::post('', [SmsController::class, 'verifyCode']);
}, [
    'middleware' => [
        AuthMiddleware::class
    ]
]);
