<?php
declare(strict_types=1);

use App\Controller\Common\UploadController;
use App\Middleware\AppHeaderCheckMiddleware;
use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/api/upload', function () {
    //记录上传文件
    Router::post('', [UploadController::class, 'record']);
    //获取上传配置
    Router::get('/token', [UploadController::class, 'token']);
}, [
    'middlewares' => [
        AppHeaderCheckMiddleware::class,
        AuthMiddleware::class
    ]
]);
