<?php
declare(strict_types=1);

use App\Controller\Common\PointController;
use App\Middleware\AppHeaderCheckMiddleware;
use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/api/points/rank-top', function () {
    //人员服务积分排行
    Router::get('/service/user', [PointController::class, 'servicePointRankUserTop']);
    //店铺服务积分排行
    Router::get('/service/store', [PointController::class, 'servicePointRankStoreTop']);
    //人员订单消费积分排行
    Router::get('/order/user', [PointController::class, 'orderMoneyPointRankUserTop']);
    //店铺订单消费积分排行
    Router::get('/order/store', [PointController::class, 'orderMoneyPointRankStoreTop']);
    //城市订单消费积分排行
    Router::get('/order/city', [PointController::class, 'orderMoneyPointRankCityTop']);
    //城市服务积分排行
    Router::get('/service/city', [PointController::class, 'servicePointRankCityTop']);
}, [
    'middleware' => [
        AppHeaderCheckMiddleware::class,
        AuthMiddleware::class
    ]
]);
