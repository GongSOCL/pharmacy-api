<?php
declare(strict_types=1);

use App\Middleware\AppHeaderCheckMiddleware;
use Hyperf\HttpServer\Router\Router;
use App\Middleware\AuthMiddleware;
use App\Controller\User\UserPointsController;

Router::addGroup('/api/user/points', function () {
    // 扫医生码药店列表
    Router::get('', [UserPointsController::class, 'userPoints']);
    // 药店列表
    Router::get('/detail', [UserPointsController::class, 'userPointsDetail']);
    //兑换商品列表
    Router::get('/goods', [UserPointsController::class, 'goodsList']);
    //通用详情
    Router::get('/goods/{id:\d+}', [UserPointsController::class, 'goodsDetail']);
    //优惠券商品详情
    Router::get('/goods/coupon/{id:\d+}', [UserPointsController::class, 'couponGoodsDetail']);
    //兑换商品
    Router::post('/goods/{id:\d+}/exchange', [UserPointsController::class, 'exchange']);
    //获取待确认收货列表
    Router::get('/goods/receipts', [UserPointsController::class, 'listReceipt']);
    // 实物确认收货
    Router::put('/goods/receipts/{id:\d+}/confirm', [UserPointsController::class, 'confirmReceipt']);
}, [
    'middleware' => [
        AuthMiddleware::class,
        AppHeaderCheckMiddleware::class
    ]
]);
