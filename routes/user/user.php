<?php
declare(strict_types=1);

use Hyperf\HttpServer\Router\Router;
use App\Controller\User\UserController;
use App\Middleware\AuthMiddleware;
use App\Middleware\AppHeaderCheckMiddleware;

Router::addGroup('/api/user', function () {
    // 用户信息
    Router::get('/info', [UserController::class, 'userInfo']);
    // 用户信息修改
    Router::post('/update-info', [UserController::class, 'updateUserInfo']);
    // 切换设置角色
    Router::post('/set-role', [UserController::class, 'setRole']);

    //上传身份证
    Router::post('/idcard/upload', [UserController::class, 'upload']);
    //提交身份证信息
    Router::post('/idcard', [UserController::class, 'idcard']);
    //获取个人中心用户聚合信息
    Router::get('/aggregation', [UserController::class, 'aggregation']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
