<?php
declare(strict_types=1);
use Hyperf\HttpServer\Router\Router;
use App\Middleware\AuthMiddleware;
use App\Controller\Patient\PatientController;

Router::addGroup('/api/patient', function () {
    Router::post('/scan-qr', [PatientController::class, 'scanQr']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
