<?php
declare(strict_types=1);

use Hyperf\HttpServer\Router\Router;
use App\Controller\User\UserKeywordController;
use App\Middleware\AuthMiddleware;

// 用户关键字列表
Router::addGroup('/api/keyword', function () {
    Router::get('/list', [UserKeywordController::class, 'keywordList']);
    Router::get('/delete', [UserKeywordController::class, 'deleteKeyword']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
