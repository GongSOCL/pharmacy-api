<?php
declare(strict_types=1);

use Hyperf\HttpServer\Router\Router;
use App\Middleware\AuthMiddleware;
use App\Controller\DynamicInformation\DynamicInformationController;

Router::addGroup('/api/dynamic', function () {
    # list
    Router::get('/information', [DynamicInformationController::class, 'getDynamicInformationList']);
    Router::get('/information/{id:\d+}', [DynamicInformationController::class, 'getDynamicInformationDetailById']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
