<?php
declare(strict_types=1);

use Hyperf\HttpServer\Router\Router;
use App\Middleware\AuthMiddleware;
use App\Controller\Topic\TopicController;
use App\Controller\Topic\ImagesController;

Router::addGroup('/api/topics', function () {

    # 话题首页
    Router::get('', [TopicController::class, 'getTopics']);

    # 话题详情
    Router::get('/topic/{id:\d+}', [TopicController::class, 'detail']);

    # 话题图片列表
    Router::get('/{id}/list/img', [TopicController::class, 'getTopicImagesList']);

    # 点赞
    Router::put('/{id}/like/img', [ImagesController::class, 'setTopicLikeImages']);

    # 分享记录数
    Router::put('/{id}/share', [ImagesController::class, 'setTopicShareNum']);

    # 参与话题
    Router::post('/{id}/participate', [TopicController::class, 'participate']);


    # 图片详情
    Router::get('/personal/{record_id:\d+}', [ImagesController::class, 'detail']);

    //个人中心列表
    Router::get('/Images', [ImagesController::class, 'list']);


    //发布
    Router::post('/{id:\d+}/publish', [ImagesController::class, 'publish']);
    //批量删除
    Router::post('/batch-delete', [ImagesController::class, 'del']);

    //活动中心
    Router::get('/activity', [ImagesController::class, 'activityList']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
