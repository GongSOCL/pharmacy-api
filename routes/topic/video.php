<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/admin-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
use App\Controller\Topic\VideoController;
use App\Middleware\AuthMiddleware;
use App\Middleware\PermissionMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/api/topic/videos', function () {
    //列表
    Router::get('', [VideoController::class, 'list']);
    //详情
    Router::get('/{id:\d+}', [VideoController::class, 'info']);
    //审核通过
    Router::put('/{id:\d+}/audit-success', [VideoController::class, 'auditSuccess']);
    //审核失败
    Router::put('/{id:\d+}/audit-fail', [VideoController::class, 'auditFail']);
    //标签列表
    Router::get('/labels', [VideoController::class, 'labels']);
    //删除视频
    Router::delete('/{id:\d+}', [VideoController::class, 'delete']);
}, [
    'middleware' => [
        AuthMiddleware::class,
        PermissionMiddleware::class
    ],
]);
