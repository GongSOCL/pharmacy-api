<?php

declare(strict_types=1);

use Hyperf\HttpServer\Router\Router;
use App\Middleware\AuthMiddleware;
use App\Controller\Coupon\UserCouponController;

Router::addGroup('/api/user/coupon', function () {
    // 患者优惠券列表
    Router::get('/my-list', [UserCouponController::class, 'myCouponList']);
    // 患者可使用优惠券列表
    Router::get('/use-list', [UserCouponController::class, 'useCouponList']);
    // 患者优惠券列表可使用优惠券数量
    Router::get('/user-count', [UserCouponController::class, 'getUserCouponNum']);
    // 患者店铺下单可使用优惠券数量
    Router::get('/use-count', [UserCouponController::class, 'getCanUseCouponNum']);
    // 患者领取免费优惠券
    Router::post('/get-coupon', [UserCouponController::class, 'getCoupon']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
