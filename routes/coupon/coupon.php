<?php
declare(strict_types=1);

use Hyperf\HttpServer\Router\Router;
use App\Middleware\AuthMiddleware;
use App\Controller\Coupon\CouponController;

Router::addGroup('/api/coupon', function () {
    // 优惠券适用范围列表
    Router::get('/scope-list', [CouponController::class, 'getCouponScopeList']);
    // 店铺拥有优惠券列表
    Router::get('/store-coupon-list', [CouponController::class, 'storeCouponList']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
