<?php
declare(strict_types=1);

use App\Controller\Msg\MsgController;
use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/api/messages', function () {
    //列表
    Router::get('', [MsgController::class, 'list']);
    //详情
    Router::get('/{id:\d+}', [MsgController::class, 'detail']);
}, [
    'middleware' => [
        AuthMiddleware::class
    ]
]);
