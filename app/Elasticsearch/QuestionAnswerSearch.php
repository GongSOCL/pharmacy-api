<?php
declare(strict_types=1);
namespace App\Elasticsearch;

class QuestionAnswerSearch
{
    use EsTrait;

    const TYPE = '_doc';

    const INDEX_PREFIX = 'pharmacy-question-answer';

    public static function searchWithHighLight($resourceId, $keywords = ''): array
    {
        $query = self::buildSearchQuery($resourceId, $keywords);
        $client = self::getClient();
        $resp = $client->search([
            'index' => self::getIndex(),
            'type' => self::TYPE,
            'body' => $query
        ]);

        $total = isset($resp['hits']['total']) ? (int) $resp['hits']['total'] : 0;
        $data = [];
        if ($total > 0) {
            $data = self::formatQuestionAnswer($resp['hits']['hits']);
        }
        return [$total, $data];
    }

    private static function getIndex(): string
    {
        $env = env('APP_ENV', 'local');
        return sprintf('%s-%s', self::INDEX_PREFIX, $env);
    }

    private static function buildSearchQuery($resourceId, $keywords = '')
    {
        $bool = [
            'must' => [
                [
                    "term" => ["resource_id" => ["value" => $resourceId]]
                ]
            ]
        ];
        if ($keywords) {
            $bool['must'][] = [
                'multi_match' => [
                    "query" =>  $keywords,
                    "fields" => ['quest', 'answer']
                ]

            ];
        }

        $highlight = [
            'fields' => [
                "quest" => new \stdClass(),
                "answer" => new \stdClass()
            ],
            "pre_tags" => ["<span style='color: #5870FE'>"],
            "post_tags" => ["</span>"]
        ];
        return [
            'query' => ['bool' => $bool],
            'highlight' => $highlight,
            'size' => 1000
        ];
    }

    private static function formatQuestionAnswer(array $hits): array
    {
        $data = [];
        foreach ($hits as $hitItem) {
            $item = [
                'id' => $hitItem['_source']['id'],
                'quest' => $hitItem['_source']['quest'],
                'answer' => $hitItem['_source']['answer']
            ];
            if (isset($hitItem['highlight']) && $hitItem['highlight']) {
                foreach ($hitItem['highlight'] as $field => $highlightItem) {
                    $item[$field] = $highlightItem[0];
                }
            }
            $data[] = $item;
        }
        return $data;
    }
}