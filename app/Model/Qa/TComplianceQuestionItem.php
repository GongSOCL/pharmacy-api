<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Constants\Status;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $question_yyid 题库的编号 对应t_question的yyid
 * @property string $name 题目的内容
 * @property int $sort 题目的排序  排序越小越靠前
 * @property int $type 题目类型 1表示单选题 2表示多选题  3表示填写题
 * @property string $input_type 输入框时的规则json  type min max  line
 * @property string $explain_content 答题解析
 * @property int $status 0 表示删除 1 表示正常
 * @property string $created_time
 * @property string $modify_time
 */
class TComplianceQuestionItem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_question_items';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'qa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'sort' => 'integer', 'type' => 'integer', 'status' => 'integer'];

    //type  1表示单选题 2表示多选题  3表示填写题
    const CHECK_TYPE_RADIO = 1;     //单选
    const CHECK_TYPE_CHECKBOX =2;   //多选
    const CHECK_TYPE_INPUT = 3;     //文本

    /**
     * @var \Hyperf\Utils\HigherOrderTapProxy|mixed|void
     */

    /**
     * @return \Hyperf\Database\Model\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(TComplianceQuestionAnswer::class, 'question_item_yyid', 'yyid')
            ->where('status', Status::REGULAR);
    }


    public function isRadioCheck(): bool
    {
        return $this->type == self::CHECK_TYPE_RADIO;
    }

    public function isCheckboxCheck(): bool
    {
        return $this->type == self::CHECK_TYPE_CHECKBOX;
    }

    public function isContentCheck(): bool
    {
        return $this->type == self::CHECK_TYPE_INPUT;
    }

    public function isWithOptions(): bool
    {
        return in_array($this->type, [
            self::CHECK_TYPE_RADIO,
            self::CHECK_TYPE_CHECKBOX
        ]);
    }

    /**
     * 获取题型描述
     * @return string
     */
    public function getTypeDesc(): string
    {
        switch ($this->type) {
            case self::CHECK_TYPE_RADIO:
                return "单选";
            case self::CHECK_TYPE_CHECKBOX:
                return "多选";
            case self::CHECK_TYPE_INPUT:
                return "填空";
            default:
                return "未知";
        }
    }
}
