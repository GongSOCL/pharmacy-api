<?php

declare (strict_types=1);
namespace App\Model\Qa;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $name 题库名称
 * @property int $single_count 内涵题目数量
 * @property int $multi_count
 * @property int $status 状态 0删除   1有效
 * @property string $created_time
 * @property string $modify_time
 */
class TComplianceQuestion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_question';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'qa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'single_count' => 'integer',
        'multi_count' => 'integer',
        'status' => 'integer'
    ];
}
