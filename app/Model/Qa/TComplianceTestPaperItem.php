<?php

declare (strict_types=1);
namespace App\Model\Qa;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $test_paper_yyid 对应t_test_paper_yyid
 * @property string $question_yyid 题库对应yyid
 * @property string $item_yyid 题目对应yyid
 * @property int $status 0-删除，1-有效
 * @property string $created_time
 * @property string $modify_time
 */
class TComplianceTestPaperItem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_test_paper_items';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'qa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];
}
