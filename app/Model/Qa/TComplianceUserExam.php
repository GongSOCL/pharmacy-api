<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Constants\DataStatus;
use App\Constants\Status;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $user_yyid 用户yyid
 * @property string $openid
 * @property string $exam_yyid 考试yyid
 * @property string $pass_time 通过时间
 * @property int $status
 * @property string $created_time
 * @property string $modify_time
 */
class TComplianceUserExam extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_user_exam';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'qa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';

    public function isPassed(): bool
    {
        return $this->status == Status::REGULAR;
    }
}
