<?php

declare (strict_types=1);
namespace App\Model\Qa;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property int $exam_id 考试id
 * @property int $user_id 参加用户id
 * @property string $union_id 微信union_id
 * @property string $open_id 微信open_id
 * @property int $question_id 题目id
 * @property string $option_ids 考试选项id, json
 * @property string $content 考试内容
 * @property int $is_pass 当前试题是否通过
 * @property string $created_time
 * @property string $modify_time
 * @property int $platform 1优药 2优能汇
 */
class TComplianceExamAnswersSnapshot extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_exam_answers_snapshot';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'qa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'exam_id' => 'integer',
        'user_id' => 'integer',
        'question_id' => 'integer',
        'is_pass' => 'integer',
        'platform' => 'integer'
    ];

    public function getOptionIds()
    {
        return explode(',', $this->option_ids);
    }
}
