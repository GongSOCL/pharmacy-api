<?php

declare (strict_types=1);
namespace App\Model\Qa;

use Hyperf\DbConnection\Model\Model;
/**
 * @property int $id 
 * @property string $yyid 
 * @property string $user_yyid t_users用户yyid
 * @property string $idcard_front_location 身份证正面存储地址
 * @property string $idcard_back_location 身份证反面存储地址
 * @property string $realname 姓名
 * @property string $id_code 身份证号
 * @property string $gender 性别
 * @property string $nationality 民族
 * @property string $birth_date 生日
 * @property string $address 地址
 * @property string $due_date 身份证到期时间
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property string $mobile 联系方式
 * @property int $platform 1优药 2优佳医 4优能汇
 * @property int $uid 用户id 优能汇使用总表user的id
 */
class TUserIdentity extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_user_identity';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'qa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime', 'platform' => 'integer', 'uid' => 'integer'];
}