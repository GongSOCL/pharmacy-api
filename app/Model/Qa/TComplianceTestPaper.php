<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Constants\Status;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $name 试卷名称
 * @property int $test_paper_type 试卷类型，0-题库出卷，1-试题出卷
 * @property int $question_type 出题类型，0-顺序出题，1-随机出题
 * @property int $sort 排序越小越靠前
 * @property int $single_count 单选题统计
 * @property int $multi_count 多选题统计
 * @property int $status 0 表示删除 1 表示正常
 * @property string $created_time
 * @property string $modify_time
 */
class TComplianceTestPaper extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_test_paper';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'qa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'test_paper_type' => 'integer',
        'question_type' => 'integer',
        'sort' => 'integer',
        'single_count' => 'integer',
        'multi_count' => 'integer',
        'status' => 'integer'
    ];


    const TEST_PAPER_TYPE_QUESTION = 0;     //题库出题
    const TEST_PAPER_TYPE_ITEM = 1;         //试题出题


    /**
     * 试卷反向关联问卷模型
     * @return \Hyperf\Database\Model\Relations\HasOne
     */
    public function questionnairePaper()
    {
        return $this->hasOne(TQuestionnairePaper::class, 'id', 'questionnaire_paper_id');
    }

    /**
     * 试卷关联题库
     * @return \Hyperf\Database\Model\Relations\HasMany
     */
    public function testPaperItems()
    {
        return self::hasMany(TComplianceTestPaperItem::class, 'test_paper_yyid', 'yyid')
            ->where("status", Status::REGULAR);
    }

    /**
     * 是否题库出题
     * @return bool
     */
    public function isTestPaperQuestion()
    {
        return $this->test_paper_type == self::TEST_PAPER_TYPE_QUESTION;
    }
}
