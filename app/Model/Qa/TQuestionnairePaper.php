<?php

declare (strict_types=1);
namespace App\Model\Qa;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $yyid 问卷yyid
 * @property string $name 问卷名称
 * @property int $paper_type 问卷类型: 1普通问卷 2逻辑问卷
 * @property int $show_type 展示方式：1逐题问答 2整卷问答
 * @property int $wrong_tips 是否答错提示
 * @property string $publish_time 问卷发布时间
 * @property string $expire_time 问卷结束时间时间
 * @property int $total_quota 总发放作答数
 * @property int $used_quote 已分配数量
 * @property int $finished_quota 已经完成配额
 * @property int $status 问卷状态: 1新建 2已发布 3已删除
 * @property string $platform 发布平台
 * @property int $disable_auto_publish 是否忽略自动发布, 目前针对otc平台， 是否不发布
 * @property int $start_bg_id 首页背景图upload_id
 * @property int $tail_bg_id 尾页背景图upload_id
 * @property int $answer_bg_id 答题页背景图upload_id
 * @property string $intro 问卷简介
 * @property string $cash 可获得现金
 * @property int $points 可获得积分
 * @property int $is_invite_answer 是否邀请作答: 1是 0否
 * @property int $invite_expire_hour 邀请失效时间
 * @property string $created_time
 * @property string $modify_time
 */
class TQuestionnairePaper extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_questionnaire_paper';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'qa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'paper_type' => 'integer',
        'show_type' => 'integer',
        'wrong_tips' => 'integer',
        'total_quota' => 'integer',
        'used_quote' => 'integer',
        'finished_quota' => 'integer',
        'status' => 'integer',
        'disable_auto_publish' => 'integer',
        'start_bg_id' => 'integer',
        'tail_bg_id' => 'integer',
        'answer_bg_id' => 'integer',
        'points' => 'integer',
        'is_invite_answer' => 'integer',
        'invite_expire_hour' => 'integer'
    ];
}
