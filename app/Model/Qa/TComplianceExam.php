<?php

declare (strict_types=1);
namespace App\Model\Qa;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $exam_start 考试开始时间
 * @property string $exam_end 考试结束时间
 * @property string $name 考试名称
 * @property string $introduce 考试介绍
 * @property int $publish_answer 考试结束立即公布答案，0-否，1-是
 * @property int $wrong_remind 是否答错提示,0-否，1-是
 * @property string $test_paper_yyid 考试试卷yyid，对应t_test_paper的yyid
 * @property string $agreement_yyid
 * @property int $pass_condition 考试通过分数
 * @property int $status 0-删除，1-有效
 * @property string $created_time
 * @property string $modify_time
 * @property int $pass_type 1题目数量 2分数
 * @property int $exam_time 考试时间
 * @property int $exam_num 考试次数
 * @property int $is_review 是否回顾 1允许 0 禁止
 * @property int $plan_days 绑定计划 结束时间限制
 * @property int $exam_type 1默认普通考试 2计划-自动固定入职 3计划-自动 4计划-手动
 */
class TComplianceExam extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_exam';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'qa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'publish_answer' => 'integer',
        'wrong_remind' => 'integer',
        'pass_condition' => 'integer',
        'status' => 'integer',
        'pass_type' => 'integer',
        'exam_time' => 'integer',
        'exam_num' => 'integer',
        'is_review' => 'integer',
        'plan_days' => 'integer',
        'exam_type' => 'integer'
    ];
}
