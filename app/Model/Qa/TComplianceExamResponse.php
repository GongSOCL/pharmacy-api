<?php

declare (strict_types=1);
namespace App\Model\Qa;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $user_yyid 用户的编号有的时候填写
 * @property string $openid 必填项
 * @property string $exam_user_yyid 对应t_compliance_user_exam的yyid
 * @property string $exam_yyid 考试的编号 对应t_exam的yyid
 * @property string $question_item_yyid 问卷题目的编号 对应t_question_item的yyid
 * @property string $answers 用户的选择yyid,对应t_question_answer的yyid，多选用，分隔
 * @property int $status 0-删除，1-有效
 * @property string $created_time
 * @property string $modify_time
 * @property int $platform 1优药 2优能汇
 * @property int $uid 用户id
 */
class TComplianceExamResponse extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_exam_response';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'qa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer', 'platform' => 'integer', 'uid' => 'integer'];

    public function getAnswerYYIDS(): array
    {
        return explode(',', $this->answers);
    }
}
