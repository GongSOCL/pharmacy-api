<?php

declare (strict_types=1);
namespace App\Model\Qa;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $product_yyid 产品编号   对应 t_drug_product 的yyid
 * @property string $series_yyid 系列的编号    对应 t_drug_series 的 yyid
 * @property string $title 标题
 * @property string $sub_title 简介
 * @property string $author
 * @property string $content 内容
 * @property string $reference
 * @property int $rank 排序
 * @property int $status 是否隐藏 1 显示 2隐藏
 * @property int $is_recom 是否推荐 1 是 2 否
 * @property string $created_time
 * @property string $modify_time
 */
class TDrugDocument extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_drug_document';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'qa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'rank' => 'integer', 'status' => 'integer', 'is_recom' => 'integer'];
}
