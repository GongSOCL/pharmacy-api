<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;
/**
 * @property int $id
 * @property int $resource_id 资源id
 * @property int $resource_type 资源类型1-普通资源 2-问卷资源
 * @property int $user_id 用户id
 * @property int $part 来源模块 1-首页 2-培训中心
 * @property \Carbon\Carbon $created_at 创建时间
 */
class UserResourceClick extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_resource_click';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'resource_id' => 'integer',
        'resource_type' => 'integer',
        'user_id' => 'integer',
        'part' => 'integer',
        'created_at' => 'datetime'
    ];

    const RESOURCE_TYPE_NORMAL = 1; # 普通资源
    const RESOURCE_TYPE_QUESTIONNAIRE = 2;  # 问卷
    const PART_1 = 1;   # 首页
    const PART_2 = 2;   # 培训中心
}