<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use App\Model\Model;

/**
 * @property int $id
 * @property int $yy_doctor_id 医生id
 * @property string $first_scan 患者首次扫医生码时间
 * @property string $magnification_end 倍率规则生效结束时间
 * @property int $magnification 积分倍率
 * @property int $is_delete 删除标记（0：未删除 1：已删除）
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class PatientDoctorMagnificationTimeline extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_patient_doctor_magnification_timeline';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'patient_user_id' => 'integer',
        'yy_doctor_id' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
