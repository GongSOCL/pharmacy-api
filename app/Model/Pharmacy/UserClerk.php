<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 自增ID
 * @property int $user_id 用户ID
 * @property int $agent_id 邀请的代表ID
 * @property int $store_id 药店ID
 * @property int $role 角色1：普通店员  2：店长
 * @property string $phone 手机号
 * @property string $real_name 真是姓名
 * @property int $sex 性别0：未知 1：男 2：女
 * @property int $position 职位：1：药师  2：非药师
 * @property int $status 审核状态0：未审核  1：通过  2：拒绝
 * @property int $register_time 注册时间
 * @property int $is_delete 删除标记0：未删除 1：已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class UserClerk extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_clerk';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'agent_id' => 'integer',
        'store_id' => 'integer',
        'role' => 'integer',
        'sex' => 'integer',
        'position' => 'integer',
        'status' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
