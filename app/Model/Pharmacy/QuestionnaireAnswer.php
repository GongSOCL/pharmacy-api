<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 自增ID
 * @property int $store_id 药店ID
 * @property int $user_id 店员用户ID（来自user）
 * @property int $paper_id 问卷ID
 * @property int $type 类型（1：问卷  2：考试）
 * @property string $score 考试分数（问卷无，考试有）
 * @property int $is_pass 考试是否通过（0：未通过 1：通过）
 * @property int $is_delete 删除（0：未删除 1：删除）
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class QuestionnaireAnswer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'questionnaire_answer';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'integer',
        'user_id' => 'integer',
        'paper_id' => 'integer',
        'type' => 'integer',
        'is_pass' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    const QUEST_TYPE = 1;
    const EXAM_TYPE = 2;
}
