<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property int $uid 考试用户id
 * @property int $test_id 试卷id
 * @property int $exam_id 考试id
 * @property int $item_id 对应题目id
 * @property int $status 0-删除，1-有效
 * @property int $is_deleted 0未删除1已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class ExamRandItem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'exam_rand_item';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'uid' => 'integer',
        'test_id' => 'integer',
        'exam_id' => 'integer',
        'item_id' => 'integer',
        'status' => 'integer',
        'is_deleted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
