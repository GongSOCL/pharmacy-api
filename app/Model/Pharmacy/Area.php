<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 自增ID
 * @property string $name 大区
 * @property string $number 编号
 * @property int $is_deleted 0未删除 1已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class Area extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'area';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'is_deleted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
