<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 自增ID
 * @property int $user_id 申请人ID
 * @property int $city_id 城市
 * @property int $main_store_id 连锁总店ID
 * @property int $store_id 药店ID
 * @property string $real_name 真实姓名
 * @property int $sex 性别0：未知 1：男 2：女
 * @property string $phone 手机号
 * @property int $role 角色1：普通店员  2：店长
 * @property int $position 职位：1：药师  2：非药师
 * @property int $status 审核状态0：初始状态 1：申请中  2：通过  3：拒绝
 * @property int $is_delete 删除标记（0：未删除 1：删除）
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class ClerkApply extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clerk_apply';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'city_id' => 'integer',
        'main_store_id' => 'integer',
        'store_id' => 'integer',
        'sex' => 'integer',
        'role' => 'integer',
        'position' => 'integer',
        'status' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
