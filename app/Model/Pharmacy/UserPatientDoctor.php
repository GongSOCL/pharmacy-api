<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 自增ID
 * @property int $yy_doctor_id 医生Id
 * @property int $patient_user_id 患者对应的用户Id
 * @property int $depart_id 医生科室id
 * @property int $is_delete 删除标记
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class UserPatientDoctor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_patient_doctor';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'yy_doctor_id' => 'integer',
        'patient_user_id' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
