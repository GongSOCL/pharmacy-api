<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use App\Model\Model;

/**
 * @property int $id
 * @property int $event_id 积分事件id
 * @property int $product_id 药品id
 * @property int $sku_id sku id
 * @property int $base_cnt 基础盒数
 * @property string $money_point 消费积分
 * @property string $service_point 服务积分
 * @property string $rule_desc 积分规则描述
 * @property int $num_total 总共能获得次数
 * @property int $num_per_day 单日能获得次数
 * @property int $is_delete 删除标记（0：未删除 1：已删除）
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property int $group_id 积分分组活动id
 */
class PharmacyPointOrderDrugRule extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pharmacy_point_order_drug_rule';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer', 'event_id' => 'integer',
        'product_id' => 'integer',
        'sku_id' => 'integer',
        'base_cnt' => 'integer',
        'num_total' => 'integer',
        'num_per_day' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
