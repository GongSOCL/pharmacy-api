<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use App\Model\Model;

/**
 * @property int $id
 * @property int $user_id 兑换用户id
 * @property int $role_id 兑换用户角色id, user_role.id
 * @property int $goods_id 兑换商品id
 * @property string $service_point 兑换消耗服务积分
 * @property string $money_point 兑换消费消费积分
 * @property int $num 兑换份数
 * @property int $point_order_id 积分系统订单id
 * @property int $state 兑换状态: 1新建 2已完成兑换 3兑换已取消
 * @property int $is_delete 删除标记（0：未删除 1：已删除）
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class PointExchange extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_point_exchange';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'role_id' => 'integer',
        'goods_id' => 'integer',
        'num' => 'integer',
        'point_order_id' => 'integer',
        'state' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];


    //新建状态
    const STATE_NEW = 1;
    //已兑换完成
    const STATE_EXCHANGE_OK = 2;
    //兑换已取消
    const STATE_CANCEL = 3;
    //兑换产品已发放
    const STATE_SEND_OK = 4;
    //兑换产品已确认收货[实物]
    const STATE_RECEIPT_CONFIRMD = 5;
    // 兑换失败回滚[兑换过程中产生]
    const STATE_FAIL_ROLLBACK = 6;

    public function isSended()
    {
        return $this->state == self::STATE_SEND_OK;
    }
}
