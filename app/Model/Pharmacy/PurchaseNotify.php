<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use App\Constants\Notify;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 自增ID
 * @property int $store_id 药店ID
 * @property int $patient_user_id 患者ID(来自user)
 * @property int $clerk_user_id 店员ID(来自user)
 * @property int $status 状态：0 未处理  1：已处理  2：已取消
 * @property int $is_delete 删除标记0：未删除 1：已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class PurchaseNotify extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'purchase_notify';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'integer',
        'patient_user_id' => 'integer',
        'clerk_user_id' => 'integer',
        'status' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function isReceived(): bool
    {
        return $this->status == Notify::NOTIFY_RECEIVED;
    }

    public function isPending(): bool
    {
        return $this->status == Notify::NOTIFY_PENDING;
    }
}
