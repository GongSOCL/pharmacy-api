<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property int $order_point_id 订单积分id
 * @property int $point_type 积分类型: 1消费积分 2服务积分
 * @property float $point_value 积分值
 * @property int $is_delete 删除标记（0：未删除 1：已删除）
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class OrderPointDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_point_detail';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_point_id' => 'integer',
        'point_type' => 'integer',
        'point_value' => 'float',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
