<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\Database\Model\Relations\BelongsTo;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property int $topic_id 话题id
 * @property int $video_id 视频上传id
 * @property int $user_id 参与人员id
 * @property int $state 记录状态: 1草稿 2待审核 3审核成功 4审核失败
 * @property string $comment
 * @property string $reject_reason 审核拒绝理由
 * @property int $resource_id 关联资源id
 * @property int $status 状态: 0已删除 1正常
 * @property int $like_count 点赞数量
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 记录最后更新时间
 */
class TopicParticipateRecord extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'topic_participate_record';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    const STATE_DRAFT = 1;

    const STATE_AUDITING = 2;

    const STATE_AUDIT_SUCCESS = 3;

    const STATE_AUDIT_FAILED = 4;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'topic_id' => 'integer',
        'video_id' => 'integer',
        'user_id' => 'integer',
        'state' => 'integer',
        'resource_id' => 'integer',
        'status' => 'integer',
        'like_count' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function isPublished(): bool
    {
        return in_array($this->state, [
            self::STATE_AUDITING,
            self::STATE_AUDIT_SUCCESS,
            self::STATE_AUDIT_FAILED
        ]);
    }

    /**
     * @return BelongsTo
     */
    public function topic(): BelongsTo
    {
        return $this->belongsTo(Topic::class, 'topic_id', 'id');
    }

    public function isDraft(): bool
    {
        return $this->state == self::STATE_DRAFT;
    }

    public function isAuditPassed(): bool
    {
        return $this->state == self::STATE_AUDIT_SUCCESS;
    }
}
