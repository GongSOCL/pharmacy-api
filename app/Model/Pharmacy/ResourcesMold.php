<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $name 名称
 * @property int $created_admin 创建者id
 * @property int $is_deleted 0未删除1已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class ResourcesMold extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'resources_mold';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'created_admin' => 'integer',
        'is_deleted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
