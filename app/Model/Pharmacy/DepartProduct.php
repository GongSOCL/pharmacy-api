<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 自增ID
 * @property int $depart_id 科室ID（来自优药）
 * @property int $yy_product_id 产品ID（来自优药）
 * @property int $is_delete 删除标记0：未删除 1：已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class DepartProduct extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'depart_product';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'depart_id' => 'integer',
        'yy_product_id' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
