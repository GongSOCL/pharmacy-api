<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 自动ID
 * @property string $union_id 微信unionId
 * @property string $open_id 微信openId
 * @property string $nick_name 昵称
 * @property string $real_name 真实姓名
 * @property string $phone 手机号
 * @property int $sex 性别0：未知 1：男 2：女
 * @property int $store_role 当前角色0：没有 1：普通店员 2：店长
 * @property int $role 当前角色：1：患者  2：店员  3：医生
 * @property int $status 状态0：正常 1：暂停 2：关闭
 * @property string $header_url 头像
 * @property int $is_delete 删除0：未删除 1：已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class User extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sex' => 'integer',
        'store_role' => 'integer',
        'role' => 'integer',
        'status' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime', 'updated_at' => 'datetime'
    ];

    public function clerk()
    {
        return $this->hasOne(UserClerk::class, 'user_id', 'id');
    }

    public function roles()
    {
        return $this->hasMany(UserRole::class, 'user_id', 'id');
    }

    public function isValid()
    {
        if ($this->status != 0 || $this->is_delete != 0) {
            return false;
        }
        return true;
    }
}
