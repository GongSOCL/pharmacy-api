<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property int $record_id 话题id 关联topic_participate_record
 * @property int $user_id 用户id
 * @property int $status 1点赞2点赞已取消
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class RecordLike extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'record_like';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'record_id' => 'integer',
        'user_id' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    const STATUS_LIKE = 1;

    const STATUS_CANCEL = 2;
}
