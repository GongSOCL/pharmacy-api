<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use App\Constants\Role;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 自增ID
 * @property int $user_id 用户ID
 * @property int $role 角色1：患者 2：店员 3：医生
 * @property int $is_delete 删除标记0：未删除 1：已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class UserRole extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_role';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'role' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function isClerk(): bool
    {
        return $this->role == Role::ROLE_CLERK;
    }
}
