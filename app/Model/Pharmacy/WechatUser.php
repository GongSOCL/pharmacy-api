<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 自增ID
 * @property int $user_id 用户ID
 * @property string $nick_name 昵称
 * @property string $union_id 微信union_id
 * @property string $open_id 微信open_id
 * @property string $header_url 头像
 * @property int $gender 性别
 * @property string $country 国家
 * @property string $province 省份
 * @property string $city 城市
 * @property int $subscribe 是否关注0否1是
 * @property string $subscribe_time 关注时间
 * @property string $subscribe_scene 关注场景
 * @property string $qr_scene 扫码场景
 * @property string $qr_scene_str 扫码场景
 * @property string $remark 备注
 * @property int $is_delete 删除标记0：未删除 1：已删除
 * @property \Carbon\Carbon $created_at 创建日期
 * @property \Carbon\Carbon $updated_at 更新日期
 */
class WechatUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wechat_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'gender' => 'integer',
        'subscribe' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
