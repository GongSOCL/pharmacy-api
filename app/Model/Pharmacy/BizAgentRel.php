<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 自增ID
 * @property int $biz_id 业务ID
 * @property int $biz_type 业务ID类型（1：患者扫药店码 2：问卷 3：陈列 4：导购注册...）
 * @property int $agent_id 服务药店代表
 * @property int $is_delete 删除标记0：未删除 1：已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class BizAgentRel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'biz_agent_rel';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'biz_id' => 'integer',
        'biz_type' => 'integer',
        'agent_id' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
