<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 自增ID
 * @property string $title 描述
 * @property int $yy_product_id 药品ID
 * @property int $policy_type 类型：1：满减  2：折扣
 * @property string $policy_val 优惠策略
 * @property int $deadline_type 截止类型：1：倒计时  2：具体日期
 * @property string $deadline_val 截止值
 * @property string $json_policy 扩展
 * @property int $total_num 总数量
 * @property int $receive_num 已领数量
 * @property int $is_limit 是否有限数量（0：无限 1：有限）
 * @property int $is_exchange 是否兑换（0：不兑换  1：兑换）
 * @property int $exchange_goods_id 绑定对应的商品ID（待定）
 * @property string $remark 备注
 * @property int $scope 适用范围（0：所有  1：连锁店   2：药店）
 * @property int $status 状态（0：未上架  1：已上架）
 * @property int $is_delete 删除标记（0：未删除 1：已删除）
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class Coupon extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coupon';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'yy_product_id' => 'integer',
        'policy_type' => 'integer',
        'deadline_type' => 'integer',
        'total_num' => 'integer',
        'receive_num' => 'integer',
        'is_limit' => 'integer',
        'is_exchange' => 'integer',
        'exchange_goods_id' => 'integer',
        'scope' => 'integer',
        'status' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
