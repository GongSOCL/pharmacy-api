<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use App\Model\Model;

/**
 * @property int $id
 * @property int $order_user_id order_user表人员id
 * @property string $operate 操作日志记录
 * @property int $is_delete 删除标记（0：未删除 1：已删除）
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class OrderUserOperateLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_user_operate_log';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_user_id' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
