<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;
/**
 * @property int $id
 * @property string $title 标题
 * @property string $cover_url 封面
 * @property string $concent 详情
 * @property int $lable_id 标签id
 * @property int $status 状态: 0已删除 1正常
 * @property int $created_admin 创建人id
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property int $is_deleted 0未删除1已删除
 */
class DynamicInformation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dynamic_information';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'lable_id' => 'integer',
        'status' => 'integer',
        'created_admin' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'is_deleted' => 'integer'
    ];
}