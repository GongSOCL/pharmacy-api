<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use App\Model\Model;

/**
 * @property int $id
 * @property string $version app版本号
 * @property int $is_checking 1-审核中 2-已通过
 * @property int $created_id
 * @property int $status 1有效2无效
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class Version extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'version';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'version', 'is_checking', 'created_id', 'status', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'is_checking' => 'integer',
        'created_id' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
