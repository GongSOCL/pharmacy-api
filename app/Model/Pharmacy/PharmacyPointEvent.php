<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use App\Model\Model;

/**
 * @property int $id
 * @property string $event_key 积分事件key
 * @property string $event_desc 事件描述信息
 * @property string $money_point 消费积分
 * @property string $service_point 服务积分
 * @property int $is_special 是否特殊事件
 * @property int $num_total 总共能获得次数
 * @property int $num_per_day 单日能获得次数
 * @property int $is_delete 删除标记（0：未删除 1：已删除）
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property int $group_id 积分分组活动id
 */
class PharmacyPointEvent extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pharmacy_point_event';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'is_special' => 'integer',
        'num_total' => 'integer',
        'num_per_day' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];


    const PATIENT_SCAN_DOCTOR_PATIENT_POINT='PATIENT_SCAN_DOCTOR_PATIENT_POINT';    // 患者首次扫医生码患者得积分
    const PATIENT_SCAN_DOCTOR_DOCTOR_POINT='PATIENT_SCAN_DOCTOR_DOCTOR_POINT';      // 患者首次扫医生医生得积分
    const PATIENT_ORDER_DOCTOR_FIRST_POINT='PATIENT_ORDER_DOCTOR_FIRST_POINT';      // 患者下单医生首次得金币
    const PATIENT_ORDER_PATIENT_POINT = 'PATIENT_ORDER_PATIENT_POINT';              // 患者下单得消费积分
    const PATIENT_ORDER_CLERK_POINT = 'PATIENT_ORDER_CLERK_POINT';                // 店员接单成功得消费积分
    const PHARMACY_CLERK_READ_ARTICLE = 'PHARMACY_CLERK_READ_ARTICLE';
    const PHARMACY_CLERK_VERIFY = 'PHARMACY_CLERK_VERIFY';
    const PHARMACY_CLERK_TOPIC_PART = 'PHARMACY_CLERK_TOPIC_PART';
}
