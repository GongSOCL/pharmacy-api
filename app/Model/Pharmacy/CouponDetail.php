<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 自增ID
 * @property int $coupon_id 优惠券ID
 * @property string $coupon_num 优惠券编码
 * @property int $patient_user_id 患者用户ID（来自user）
 * @property int $get_time 领取时间
 * @property int $use_time 使用时间
 * @property int $deadline_time 截止日期
 * @property int $is_use 是否使用（0：未使用 1：已使用）
 * @property int $is_delete 删除标记（0：未删除 1：已删除）
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class CouponDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coupon_detail';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'coupon_id' => 'integer',
        'patient_user_id' => 'integer',
        'get_time' => 'integer',
        'use_time' => 'integer',
        'deadline_time' => 'integer',
        'is_use' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
