<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use App\Model\Model;
/**
 * @property int $id 
 * @property int $user_id 店员id
 * @property string $point_time 积分发放时间
 * @property int $year 积分年度yyyy
 * @property int $month 积分发放月度mm, 1-12
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class UserArticleMonthReadPoint extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_article_month_read_point';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'user_id' => 'integer', 'year' => 'integer', 'month' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}