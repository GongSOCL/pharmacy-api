<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use App\Model\Model;

/**
 * @property int $id
 * @property string $token 分享token
 * @property int $schema_id schema码图片上传id
 * @property int $biz_type 业务类型: 1医生schema码 2药店schema码
 * @property int $biz_id 具体业务id, 如医生id, 代表id, 药店id
 * @property int $is_delete 删除标记（0：未删除 1：已删除）
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property int $size 图片尺寸
 */
class ShareTokenSchema extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'share_token_schema';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'token_type' => 'integer',
        'schema_id' => 'integer',
        'biz_type' => 'integer',
        'biz_id' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    const BIZ_TYPE_DOCTOR_SCHEMA = 1;
    const BIZ_TYPE_STORE_SCHEMA = 2;
    const BIZ_TYPE_CLERK_INVITE = 3;
}
