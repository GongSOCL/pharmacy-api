<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use App\Model\Qa\TDrugProduct;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $title 标题
 * @property int $type 1-图文 2-视频
 * @property int $use 1-店员 2-患者
 * @property int $drug_id 药品id
 * @property int $mold_id 资源类型id
 * @property string $img 资源图片
 * @property string $video_link 视频链接地址
 * @property string $content 富文本内容
 * @property int $is_pdf 是否为pdf
 * @property string $pdf_url 如果是pdf pdf地址
 * @property int $created_admin 创建者id
 * @property int $is_deleted 0未删除1已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class Resource extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'resources';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'type' => 'integer',
        'use' => 'integer',
        'drug_id' => 'integer',
        'mold_id' => 'integer',
        'is_pdf' => 'integer',
        'created_admin' => 'integer',
        'is_deleted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function drugInfoByID()
    {
        return $this->hasOne(
            TDrugProduct::class,
            'id',
            'drug_id'
        )->select('id', 'name');
    }

    public function PDFInfoByID()
    {
        return $this->hasMany(PdfDetail::class, 'resources_id', 'id')->select('img');
    }
}
