<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use App\Model\Model;

/**
 * @property int $id 自增ID
 * @property int $notify_id 购买通知ID
 * @property string $order_num 订单编码
 * @property int $yy_doctor_id 医生ID（来自优药）
 * @property int $clerk_user_id 店员用户ID(来源user)
 * @property int $patient_user_id 患者用户ID(来源user)
 * @property int $store_id 药店ID
 * @property string $amount 总金额
 * @property int $num 总数量
 * @property int $status 状态0：未支付 1：已支付
 * @property float $longitude 经度
 * @property float $latitude 维度
 * @property string $remark 备注
 * @property int $is_delete 删除标记0：未删除 1：已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class Order extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'notify_id' => 'integer',
        'yy_doctor_id' => 'integer',
        'clerk_user_id' => 'integer',
        'patient_user_id' => 'integer',
        'store_id' => 'integer',
        'num' => 'integer',
        'status' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    const STATUS_PAIED = 1;
}
