<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use App\Model\Model;

/**
 * @property int $id
 * @property int $order_id 订单id
 * @property int $user_id 人员id
 * @property string $order_date 订单日期, 冗余过来用来按时间聚合
 * @property int $user_type 人员类型: 1服务药店代表 2订单关联医生 3关联医生所在群组关联代表 4店员 5店长
 * @property int $state 操作状态: 1新建  其它状态根据人员 类型来决定,如医生积分、通知 代表通知
 * @property int $is_delete 删除标记（0：未删除 1：已删除）
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class OrderUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'user_id' => 'integer',
        'user_type' => 'integer',
        'state' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
