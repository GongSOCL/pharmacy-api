<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use App\Constants\Role;
use App\Model\Model;

/**
 * @property int $id
 * @property string $name 商品名称
 * @property string $point 所需积分
 * @property int $point_type 所需积分类型: 1消费积分 2服务积分
 * @property int $cover_id 积分商品封面上传id
 * @property int $goods_type 商品类型:1现金 2实物 3优惠券
 * @property int $external_goods_id 外部商品id,如优惠券关联外部券id
 * @property string $start 商品生效开始时间
 * @property string $end 商品生效结束时间
 * @property string $money 现金券可兑换金额,goods_type=1时指定
 * @property int $total 商品总数量
 * @property int $exchanged 已兑换数量
 * @property string $point_goods_yyid 积分商品yyid
 * @property int $exchange_user_type 兑换人员类型: 1患者 2店员
 * @property int $is_delete 删除标记（0：未删除 1：已删除）
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property int $status 商品上架状态: 0下架 1上架
 * @property int $group_id 积分活动分组id
 */
class PointGood extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_point_goods';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'point_type' => 'integer',
        'cover_id' => 'integer',
        'goods_type' => 'integer',
        'external_goods_id' => 'integer',
        'total' => 'integer',
        'rest' => 'integer',
        'exchange_user_type' => 'integer',
        'is_delete' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    //消耗服务积分
    const POINT_TYPE_SERVICE_POINT = 2;
    //消耗现金积分
    const POINT_TYPE_MONEY_POINT = 1;

    //现金类型商品
    const GOODS_TYPE_CASH = 1;
    //实物类型商品
    const GOODS_TYPE_REAL = 2;
    //优惠券类型商品
    const GOODS_TYPE_COUPON = 3;
    //虚拟物品
    const GOODS_TYPE_VIRTUAL = 4;

    //可被店员兑换
    const EXCHANGE_BY_CLERK = 2;
    //可被患者兑换
    const EXCHANGE_BY_PATIENT = 1;

    public function isExpired(): bool
    {
        return $this->end && $this->end <= date('Y-m-d H:i:s');
    }

    public function quoteExceed(): bool
    {
        return $this->total > 0 && $this->exchanged >= $this->total;
    }

    public function getServicePoint()
    {
        return $this->point_type == self::POINT_TYPE_SERVICE_POINT ? $this->point : 0;
    }

    public function getMoneyPoint()
    {
        return $this->point_type == self::POINT_TYPE_MONEY_POINT ? $this->point : 0;
    }

    public function isCouponGoods(): bool
    {
        return $this->goods_type == self::GOODS_TYPE_COUPON;
    }

    public function isValidToExchange($role): bool
    {
        return ($this->exchange_user_type == self::EXCHANGE_BY_PATIENT && $role == Role::ROLE_PATIENT) ||
            ($this->exchange_user_type == self::EXCHANGE_BY_CLERK && $role == Role::ROLE_CLERK);
    }

    public function isRealTypeGoods()
    {
        return $this->goods_type == self::GOODS_TYPE_REAL;
    }
}
