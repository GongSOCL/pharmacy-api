<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $name 话题名称
 * @property int $cover_id 话题封面上传id
 * @property int $detail_id 话题详情页图片上传id
 * @property int $is_publish 是否已发布
 * @property int $total 总参与数
 * @property int $status 话题状态: 0已删除 1正常
 * @property int $admin_id 创建人id
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 记录最后更新时间
 * @property int $group_id 积分活动分组id
 * @property int $top_n_service_point topN额外服务积分奖励
 * @property int $top_n_money_point topN额外消费积分奖励
 * @property int $service_point 完成审核服务积分数
 * @property int $money_point 完成审核发放消费积分数
 */
class Topic extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'topic';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cover_id' => 'integer',
        'detail_id' => 'integer',
        'is_publish' => 'integer',
        'total' => 'integer',
        'status' => 'integer',
        'admin_id' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function isPublished(): bool
    {
        return $this->is_publish == 1;
    }
}
