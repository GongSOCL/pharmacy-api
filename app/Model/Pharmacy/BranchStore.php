<?php

declare (strict_types=1);
namespace App\Model\Pharmacy;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 自增ID
 * @property int $yy_store_id 药店ID（来自优药药店ID）
 * @property int $area_id 大区id
 * @property int $pro_id 省id
 * @property int $city_id 城市id
 * @property int $main_store_id 主店id
 * @property string $store_name_alias 门店短名称
 * @property string $store_name 药店名称
 * @property string $location 地址
 * @property string $phone 联系电话
 * @property string $model 在售品规
 * @property string $number 编号
 * @property int $status 有效状态
 * @property int $is_deleted 0未删除1已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class BranchStore extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'branch_store';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'yy_store_id' => 'integer',
        'area_id' => 'integer',
        'pro_id' => 'integer',
        'city_id' => 'integer',
        'main_store_id' => 'integer',
        'is_deleted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function products()
    {
        return $this->hasMany(BranchStoreProduct::class, 'store_id', 'id');
    }

    public function policies()
    {
        return $this->hasMany(BranchStorePolicy::class, 'store_id', 'id');
    }
}
