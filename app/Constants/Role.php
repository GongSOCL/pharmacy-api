<?php
declare(strict_types=1);

namespace App\Constants;

use Youyao\Framework\AbstractConstants;

class Role extends AbstractConstants
{
    /**
     * @Info("患者")
     */
    const ROLE_PATIENT = 1;

    /**
     * @Info("店员")
     */
    const ROLE_CLERK = 2;
}
