<?php
declare(strict_types=1);

namespace App\Constants;

class Store
{
    /**
     * @Info("正常")
     */
    const STATUS_NORM = 1;
    /**
     * @Info("无效")
     */
    const STATUS_INVALID = 2;
}
