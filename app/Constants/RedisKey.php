<?php
declare(strict_types=1);

namespace App\Constants;

use Youyao\Framework\AbstractConstants;

class RedisKey extends AbstractConstants
{
    /**
     * @Info("店员手机验证Key")
     */
    const CLERK_PHONE_VERIFY_KEY = 'pharmacy:sms:clerk:';
}
