<?php
declare(strict_types=1);

namespace App\Constants;

use Youyao\Framework\AbstractConstants;

class CouponConstant extends AbstractConstants
{
    /**
     * @Info("所有")
     */
    const COUPON_SCOPE_ALL = 0;
    /**
     * @Info("连锁店")
     */
    const COUPON_SCOPE_MAIN = 1;
    /**
     * @Info("药店")
     */
    const COUPON_SCOPE_STORE = 2;
    /**
     * @Info("未使用")
     */
    const COUPON_USE_NO = 0;
    /**
     * @Info("已使用")
     */
    const COUPON_USE_YES = 1;
    /**
     * @Info("不兑换")
     */
    const COUPON_EXCHANGE_NO = 0;
    /**
     * @Info("兑换")
     */
    const COUPON_EXCHANGE_YES = 1;
    /**
     * @Info("倒计时")
     */
    const COUPON_DEADLINE_TYPE_DOWN = 1;
    /**
     * @Info("具体时间")
     */
    const COUPON_DEADLINE_TYPE_SPEC = 2;
    /**
     * @Info("无限")
     */
    const COUPON_LIMIT_NO = 0;
    /**
     * @Info("有限")
     */
    const COUPON_LIMIT_YES = 1;

    /**
     * @Info("未上架")
     */
    const COUPON_STATUS_NO = 0;
    /**
     * @Info("已上架")
     */
    const COUPON_STATUS_YES = 1;
}
