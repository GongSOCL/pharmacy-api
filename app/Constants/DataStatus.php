<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class DataStatus extends AbstractConstants
{
    /**
     * @Info("页面展示数量")
     */
    const PAGE_SIZE = 12;

    const DEFAULT_AES_IV = "LtRdHdS:x7Kyg?^n";
    const DEFAULT_AES_METHOD = "aes-256-cbc";

    /**
     * @Info("testing环境")
     */
    const ENV_TESTING = 'testing';
    /**
     * @Info("staging环境")
     */
    const ENV_STAGING = 'staging';
    /**
     * @Info("prod环境")
     */
    const ENV_PROD = 'prod';
}
