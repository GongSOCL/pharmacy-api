<?php

declare(strict_types=1);
namespace App\Constants;

use Youyao\Framework\AbstractConstants;

class BizAgentType extends AbstractConstants
{
    /**
     * @Info("患者扫药店码")
     */
    const BIZ_TYPE_SCAN = 1;
    /**
     * @Info("问卷")
     */
    const BIZ_TYPE_QUEST = 2;
    /**
     * @Info("陈列")
     */
    const BIZ_TYPE_DISPLAY = 3;
    /**
     * @Info("导购注册")
     */
    const BIZ_TYPE_CLERK = 4;
}
