<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/service-core.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Constants;

use Hyperf\Constants\Annotation\Constants;
use Youyao\Framework\AbstractConstants;

/**
 * @Constants
 */
class Delete extends AbstractConstants
{

    /**
     * @Info("未删除")
     */
    const UNDELETED = 0;

    /**
     * @Info("已删除")
     */
    const DELETED = 1;
}
