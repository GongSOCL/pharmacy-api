<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/doctor-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Constants;

use Hyperf\Constants\Annotation\Constants;
use Youyao\Framework\AbstractConstants;

/**
 * @Constants
 */
class Auth extends AbstractConstants
{
    /**
     * @info("token missing")
     */
    const TOKEN_MISSING = 7001;

    /**
     * @info("illegal user_id")
     */
    const ILLEGAL_USER_ID = 7002;

    /**
     * @info("Authentication failed")
     */
    const AUTHENTICATION_FAILED = 7003;

    /**
     * @info("user forbidden")
     */
    const USER_FORBIDDEN = 7007;

    /**
     * @info("different session token")
     */
    const DIFFERENT_SESSION_TOKEN = 7004;

    /**
     * @info("session time overdue")
     */
    const SESSION_TIME_OVERDUE = 7005;

    /**
     * @info("Authentication failed after delete")
     */
    const AUTHENTICATION_FAILED_AFTER_DELETE = 7006;

    /**
     * @info("Authentication failed after delete")
     */
    const ILLEGAL_REQUEST = 7007;
}
