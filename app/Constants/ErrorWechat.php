<?php

declare(strict_types=1);

namespace App\Constants;

use Hyperf\Constants\Annotation\Constants;
use Youyao\Framework\AbstractConstants;

/**
 * 错误码: 5 位,前 2 位为模块，后 3 位为业务
 * 模块错误码定义:
 * 1290X  -  微信模块.
 * @Constants
 */
class ErrorWechat extends AbstractConstants
{
    const FAIL_TO_GET_ACCESSTOKEN = 12901;

    /**
     * @Info("创建或更新wechatuser失败")
     */
    const FAIL_TO_CREATE_WECHATUSER = 12903;

    /**
     * @Info("code失效")
     */
    const USELESS_CODE = 12904;

    /**
     * @Info("微信服务异常")
     */
    const WECHAT_BUSINESS_EXCEPTIN = 12950;

    /**
     * @Info("注册用户角色不存在")
     */
    const ROLE_NOT_EXIST = 12951;
    /**
     * @Info("注册用户参数异常")
     */
    const REG_USER_PARAMS_EXP = 12952;
}
