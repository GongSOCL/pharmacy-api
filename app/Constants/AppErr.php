<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/doctor-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Constants;

use Hyperf\Constants\Annotation\Constants;
use Youyao\Framework\AbstractConstants;

/**
 * @Constants
 */
class AppErr extends AbstractConstants
{
    /**
     * @Info("请求参数异常！")
     */
    const REQ_PARAM_EXP = 12000;

    /**
     * @Info("医生api错误！")
     */
    const SERVERERR = 12500;

    /**
     * @Info("通用逻辑错误！")
     */
    const BUSINESSERR = 12501;

    /**
     * @Info("不存在扫码信息！")
     */
    const SCAN_INFO_NOT_EXIST = 12502;

    /**
     * @Info("发送验证码失败！")
     */
    const FAIL_TO_SEND_SMSCODE = 12503;

    /**
     * @Info("上传失败！")
     */
    const FAIL_TO_UPLOAD = 12504;

    /**
     * @Info("上传小票新增消息失败！")
     */
    const FAIL_TO_UPLOAD_TICKET_INFO = 12505;

    /**
     * @Info("新增优佳医预设报名信息失败！")
     */
    const  FAIL_TO_ADD_YOUJIAYI_SIGNINFO = 12506;

    /**
     * @Info("收藏夹重名！")
     */
    const  FAV_ALREADY_EXISTS = 12507;

    /**
     * @Info("不存在该商品！")
     */
    const  GOOD_NOT_EXISTS = 12508;

    /**
     * @Info("获取百度token失败！")
     */
    const  FAIL_TO_GET_ACCESS_TOKEN = 12509;

    /**
     * @Info("内容包含敏感词审核未通过！")
     */
    const  FAIL_TO_ADUIT_CONTENT = 12510;

    /**
     * @Info("库存不足！")
     */
    const  GOOD_IS_NOT_ENOUGH = 12511;

    /**
    * @Info("版本不存在！")
    */
    const WORING_VERSION = 12512;
    /**
     * @Info("注册用户角色不存在")
     */
    const ROLE_NOT_EXIST = 12951;
    /**
     * @Info("注册用户参数异常")
     */
    const REG_USER_PARAMS_EXP = 12952;
    /**
     * @Info("您已扫描")
     */
    const NOTIFY_EXISTED = 12953;
    /**
     * @Info("购买通知取消失败")
     */
    const NOTIFY_CANCEL_ERR = 12954;
    /**
     * @Info("购买通知接受失败")
     */
    const NOTIFY_RECEIVE_ERR = 12955;
    /**
     * @Info("还未申请店员或申请还在审核中")
     */
    const CLERK_NOT_PASS = 12956;
    /**
     * @Info("店员申请还在审核中")
     */
    const CLERK_APPLY_EXISTED = 12957;

    /**
     * @Info("业务异常")
     */
    const BUSINESS_ERROR = 521023;

    /**
     * @Info("抢单失败，请回到订单列表刷新重试")
     */
    const CLERK_RECEIVE_FAIL = 12958;

    /**
     * @Info("地图定位失败")
     */
    const MAP_REVERSE_COORD_ERROR = 12959;

    /**
     * @Info("店员不允许通过店铺二维码下单")
     */
    const CLERK_DENY_SCAN_PATIENT_QR = 13000;
}
