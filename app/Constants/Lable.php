<?php
declare(strict_types=1);

namespace App\Constants;

use Youyao\Framework\AbstractConstants;

class Lable extends AbstractConstants
{
    const LABLE = [1=>'考试榜单', 2=>'话题榜单', 3=>'线下会议'];
}
