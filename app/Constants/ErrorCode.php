<?php

declare(strict_types=1);

namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 * @method string getMessage(int $errorCode) static 通过反射获取错误信息
 */
class ErrorCode extends AbstractConstants
{
    /**
     * @message("Success")
     */
    const ERROR_CODE_SUCCESS = 0;

    /**
     * @message("Parameters error")
     */
    const ERROR_CODE_ILLEGAL_REQUEST = 4001;

    /**
     * @message("Parameters error")
     */
    const ERROR_CODE_PARAMETERS_ERROR = 4002;

    /**
     * @message("Authentication failed")
     */
    const ERROR_CODE_AUTHENTICATION_FAILED = 4003;

    /**
     * @message("Query failed")
     */
    const ERROR_CODE_QUERY_FAILED = 4004;

    /**
     * @message("Operation failed")
     */
    const ERROR_CODE_OPERATION_FAILED = 4005;

    /**
     * @Message("Server Error！")
     */
    const SERVER_ERROR = 500;

    /**
     * @message("validate error")
     */
    const VALIDATION_ERROR = 100000;

    /**
     * @message("upload error")
     */
    const UPLOAD_ERROR = 200000;

    /**
     * @message("代表用户不存在")
     */
    const AGENT_USER_NOT_EXIST = 522001;
    /**
     * @message("医生用户不存在")
     */
    const DOCTOR_USER_NOT_EXIST = 522002;
    /**
     * @message("用户不存在")
     */
    const USER_NOT_EXIST = 522003;
    /**
     * @message("药品不存在")
     */
    const DRUG_NOT_EXIST = 522004;
    /**
     * @message("药店不存在")
     */
    const DRUGSTORE_NOT_EXIST = 522005;
    /**
     * @message("业务异常")
     */
    const BUSINESS_ERROR = 521023;
    /**
     * @message("缺少上传照片信息")
     */
    const USER_NOT_EXISTS = 522006;
    /**
     * @message("该用户不存在或已注销")
     */
    const UPLOAD_INFO_MISS = 522007;
    /**
     * @message("已上传照片，请勿重复上传")
     */
    const IDCARD_ALREADY_UPLOADED = 522007;
}
