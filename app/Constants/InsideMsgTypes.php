<?php

namespace App\Constants;

use Hyperf\Constants\Annotation\Constants;
use Youyao\Framework\AbstractConstants;

/**
 * @Constants
 */
class InsideMsgTypes extends AbstractConstants
{
    /**
     * @Info("完成订单店员通知")
     */
    const MSG_TYPE_CLERK_ORDER_FINISHED = 1;

    /**
     * @Info("完成订单店长通知")
     */
    const MSG_TYPE_MANAGER_ORDER_FINISHED = 2;

    /**
     * @Info("完成订单患者通知")
     */
    const MSG_TYPE_PATIENT_ORDER_FINISHED = 3;

    /**
     * @Info("pharmacy小程序")
     */
    const PLATFORM_PHARMACY_MINIAPP = 7;

    /**
     * @Info("pharmacy小程序用户")
     */
    const USER_TYPE_PHARMACY_USER = 5;

    /**
     * @Info("店员阅读文章获得积分消息")
     */
    const MSG_TYPE_CLERK_READ_ARTICLE_GAIN_POINT = 6;
}
