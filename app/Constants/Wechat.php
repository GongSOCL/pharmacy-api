<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/service-core.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Constants;

use Hyperf\Constants\Annotation\Constants;
use Youyao\Framework\AbstractConstants;

/**
 * @Constants
 */
class Wechat extends AbstractConstants
{
    /**
     * @Info("订阅")
     */
    const SUBSCRIBE = 1;

    /**
     * @Info("取消订阅")
     */
    const UN_SUBSCRIBE = 0;
}
