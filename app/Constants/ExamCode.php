<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/service-core.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Constants;

use Hyperf\Constants\Annotation\Constants;
use Youyao\Framework\AbstractConstants;

/**
 * @Constants
 */
class ExamCode extends AbstractConstants
{

    /**
     * @Info("优药")
     */
    const YOUYAO = 1;

    /**
     * @Info("优能汇")
     */
    const PHARMACY = 2;

    /**
     * @Info("通过")
     */
    const REGULAR = 1;

    /**
     * @Info("未通过")
     */
    const NOTPASS = 0;

    /**
     * @Info("可参加通过")
     */
    const CANPART = 2;

    /**
     * @Info("未开始")
     */
    const NOTSTARTED = 3;

    /**
     * @Info("补考")
     */
    const MAKE_UP_EXAM = 4;

    /**
     * @Info("过期")
     */
    const OVERDUE = 5;
}
