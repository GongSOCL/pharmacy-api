<?php


namespace App\Constants;

class Policy
{
    /**
     * 满减
     */
    const POLICY_TYPE_DEC = 1;
    /**
     * 折扣
     */
    const POLICY_TYPE_DIS = 2;

    /**
     * 倒计时
     */
    const DEADLINE_TYPE_DOWN = 1;
    /**
     * 具体日期
     */
    const DEADLINE_TYPE_SPECIFIC = 2;
}
