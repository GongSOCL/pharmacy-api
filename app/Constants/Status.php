<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/service-core.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Constants;

use Hyperf\Constants\Annotation\Constants;
use Youyao\Framework\AbstractConstants;

/**
 * @Constants
 */
class Status extends AbstractConstants
{

    /**
     * @Info("正常")
     */
    const REGULAR = 1;

    /**
     * @Info("禁用")
     */
    const FORBIDDEN = 2;

    /**
     * @Info("是否删除 - 未删")
     */
    const NOTDELETED = 0;
    /**
     * @Info("是否删除 - 已删")
     */
    const DELETED = 1;
}
