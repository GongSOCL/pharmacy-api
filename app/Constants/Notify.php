<?php
declare(strict_types=1);

namespace App\Constants;

class Notify
{
    /**
     * @Info ("待接单")
     */
    const NOTIFY_PENDING = 0;
    /**
     * @Info ("已接单")
     */
    const NOTIFY_RECEIVED = 1;
    /**
     * @Info ("未接单")
     */
    const NOTIFY_CANCEL = 2;
}
