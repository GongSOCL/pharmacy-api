<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/service-core.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Constants;

use Hyperf\Constants\Annotation\Constants;
use Youyao\Framework\AbstractConstants;

/**
 * @Constants
 */
class Upload extends AbstractConstants
{

    //身份证照片上传文件公共路径
    const UPLOAD_URI_IDCARD = BASE_PATH . '/storage/upload/idcard/';
}
