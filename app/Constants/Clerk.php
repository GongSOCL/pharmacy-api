<?php


namespace App\Constants;

class Clerk
{
    /**
     * @Info("初始状态")
     */
    const STATUS_INIT = 0;
    /**
     * @Info("审核中")
     */
    const STATUS_AUDITING = 1;
    /**
     * @Info("通过")
     */
    const STATUS_PASS = 2;
    /**
     * @Info("拒绝")
     */
    const STATUS_REFUSE = 3;

    /**
     * @Info("普通店员")
     */
    const ROLE_STAFF = 1;
    /**
     * @Info("店长")
     */
    const ROLE_MANAGER = 2;
    /**
     * @Info("医师")
     */
    const POS_PHYSICIAN = 1;
    /**
     * @Info("非医师")
     */
    const POS_NON_PHYSICIAN = 2;
}
