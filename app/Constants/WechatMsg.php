<?php
declare(strict_types=1);
namespace App\Constants;

use Hyperf\Constants\Annotation\Constants;
use Youyao\Framework\AbstractConstants;

/**
 * @Constants
 */
class WechatMsg extends AbstractConstants
{
    /**
     * @Info("pharmacy小程序别名")
     */
    const WECHAT_CHANNEL_PHARMACY_ALIAS = 'pharmacy_miniapp';

    /**
     * @Info("优药公众号别名")
     */
    const WECHAT_CHANNEL_WECHAT = 'wxapi';

    /**
     * @Info("优佳医公众号别名")
     */
    const WECHAT_CHANNEL_YOUJIAYI = 'youjiayi';

    /**
     * @Info("wxapi项目进度提醒模板别名")
     */
    const AGENT_PHARMACY_AGENT_NOTIFY = 'pharmacy_agent_notify';
}
