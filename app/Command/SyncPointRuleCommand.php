<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\Point\PointService;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Psr\Container\ContainerInterface;

/**
 * @Command
 */
class SyncPointRuleCommand extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('point:sync-rules');
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('同步所有积分规则到积分系统');
    }

    public function handle()
    {
        PointService::loadPointRules();
        $this->line('积分规则同步完成!', 'info');
    }
}
