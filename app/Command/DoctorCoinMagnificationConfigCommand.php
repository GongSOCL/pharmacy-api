<?php

declare(strict_types=1);

namespace App\Command;

use App\Constants\AppErr;
use App\Exception\BusinessException;
use App\Service\Doctor\DoctorService;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * @Command
 */
class DoctorCoinMagnificationConfigCommand extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('doctor:coin-magnification-update');
    }

    public function configure()
    {
        parent::configure();
        $this->addOption('days', '', InputOption::VALUE_REQUIRED, '时间范围(天)')
            ->addOption('magnification', '', InputOption::VALUE_REQUIRED, '积分系数');
        $this->setDescription('更新医生积分翻倍规则');
    }

    public function handle()
    {
        $days = (int) $this->input->getOption('days');
        $magnification = (int) $this->input->getOption('magnification');
        if ($days < 1) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '时间范围必须大于0');
        }
        if ($magnification < 1) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '积分倍数必须大于0');
        }
        DoctorService::updateDoctorMagnification($days, $magnification);
        $this->line('积分翻倍规则配置更新完成', 'info');
    }
}
