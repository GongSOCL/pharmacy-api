<?php

declare(strict_types=1);

namespace App\Command;

use App\Constants\BizAgentType;
use App\Helper\Helper;
use App\Repository\PurchaseNotifyRepository;
use App\Repository\QuestionnaireAnswerRepository;
use App\Repository\Topic\TopicParticipateRecordRepository;
use App\Repository\UserClerkRepository;
use App\Service\Common\BizAgentService;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Hyperf\Utils\Parallel;
use Psr\Container\ContainerInterface;

/**
 * @Command
 */
class RefreshBizAgentRelCommand extends HyperfCommand
{


    const TYPE_PREFIX = [
        1=>'purchase',
        2=>'quest',
        3=>'topic',
        4=>'clerk',
    ];
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('biz-agent:refresh');
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('刷新药店业务关联的代表数据');
    }

    public function handle()
    {
        // 刷扫码记录表
        $func = $this->getHandleFunc(BizAgentType::BIZ_TYPE_SCAN);
        PurchaseNotifyRepository::handlePurchaseRelAgentChunk($func);
        // 刷店员注册表
        $func = $this->getHandleFunc(BizAgentType::BIZ_TYPE_CLERK);
        UserClerkRepository::handleClerkRelAgentChunk($func);
        // 刷topic
        $func = $this->getHandleFunc(BizAgentType::BIZ_TYPE_DISPLAY);
        TopicParticipateRecordRepository::handleTopicRelAgentChunk($func);
        // 刷问卷
        $func = $this->getHandleFunc(BizAgentType::BIZ_TYPE_QUEST);
        QuestionnaireAnswerRepository::handleQuestRelAgentChunk($func);
        $this->line('刷新药店业务关联的代表完毕!', 'info');
    }

    private function getHandleFunc($type, $params = [])
    {
        return function ($chunkList) use ($type) {
            foreach ($chunkList as $row) {
                BizAgentService::recordAgent(
                    $row['id'],
                    $row['store_id'],
                    $type
                );
                $this->line($type.':'.$row['id'].'处理完毕!', 'info');
            }
        };
    }
}
