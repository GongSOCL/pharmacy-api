<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\Store\StoreService;
use Grpc\Upload\StorageChannel;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * @Command
 */
class GenerateStoreCommonSchemaCommand extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('schema:store-common-generate');
    }

    public function configure()
    {
        parent::configure();
        $this->addOption('size', '', InputOption::VALUE_OPTIONAL, '图片尺寸,整数128-1280, 默认不传430');
        $this->addOption('channel', '', InputOption::VALUE_OPTIONAL, '存储channel: 1七牛 2 minio');
        $this->setDescription('生成店铺通用码');
    }

    public function handle()
    {
        $size = (int) $this->input->getOption('size');
        if (!$size) {
            $size = 430;
        }
        $channel = (int) $this->input->getOption('channel');
        if ($channel == 2) {
            $channel = StorageChannel::CHANNEL_MINIO;
        } else {
            $channel = StorageChannel::CHANNEL_QINIU;
        }
        $link = StoreService::generateStoreCommonSchema($size, $channel);
        $this->line('店铺通用码生成成功', 'info');
        $this->table(["通用码链接"], [
            [
                $link
            ]
        ]);
    }
}
