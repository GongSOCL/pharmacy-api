<?php

declare(strict_types=1);

namespace App\Command;

use App\Model\Pharmacy\DoctorProduct;
use App\Model\Pharmacy\OrderDetail;
use App\Model\Pharmacy\OrderDoctor;
use App\Model\Pharmacy\OrderUser;
use Carbon\Carbon;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Hyperf\Utils\Collection;
use Psr\Container\ContainerInterface;

/**
 * @Command
 */
class RefreshOrderDoctorCommand extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('order-doctor:refresh');
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('刷新订单医生服务产品表');
    }

    public function handle()
    {

        $list = OrderUser::query()
            ->from('order_user as ou')
            ->join(
                'order_detail as od',
                'ou.order_id',
                '=',
                'od.order_id'
            )
            ->select(['ou.order_id', 'ou.user_id', 'od.yy_product_id'])
            ->distinct()
            ->where('ou.user_type', 2)
            ->get();
        $doctorProductsGroup = DoctorProduct::query()
            ->select(['yy_doctor_id', 'yy_product_id'])
            ->whereIn('yy_doctor_id', $list->pluck('user_id')->unique())
            ->where('is_delete', 0)
            ->get()->groupBy('yy_doctor_id');
        $list->each(function ($row) use ($doctorProductsGroup) {
            /** @var Collection $doctorProducts */
            $doctorProducts = $doctorProductsGroup[$row['user_id']]?? new Collection();
            if ($doctorProducts->count() > 0 &&
                $doctorProducts->where(
                    'yy_product_id',
                    $row['yy_product_id']
                )->count()) {
                $exist = OrderDoctor::query()->where([
                    'order_id'=>$row['order_id'],
                    'yy_product_id'=>$row['yy_product_id'],
                ])->exists();
                if (!$exist) {
                    $orderDoctor = new OrderDoctor();
                    $orderDoctor->order_id = $row['order_id'];
                    $orderDoctor->yy_doctor_id= $row['user_id'];
                    $orderDoctor->yy_product_id= $row['yy_product_id'];
                    $orderDoctor->created_at= Carbon::now();
                    $orderDoctor->updated_at= Carbon::now();
                    return $orderDoctor->save();
                }
            }
        });
    }
}
