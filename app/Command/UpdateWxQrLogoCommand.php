<?php

declare(strict_types=1);

namespace App\Command;

use App\Helper\UploadHelper;
use App\Model\Pharmacy\ShareTokenSchema;
use App\Repository\ShareTokenSchemaRepository;
use App\Service\Common\SchemaService;
use App\Service\Common\UploadRpcService;
use Carbon\Carbon;
use Grpc\Upload\UploadFileItem;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Hyperf\Utils\Exception\ParallelExecutionException;
use Hyperf\Utils\Parallel;
use Psr\Container\ContainerInterface;

/**
 * @Command
 */
class UpdateWxQrLogoCommand extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('wxQrLogo:update');
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('修改微信二维码Logo');
    }

    public function handle()
    {
        //TODO
        $schemaIds = ShareTokenSchema::query()
            ->get()->pluck('schema_id')->toArray();
        $resp = UploadRpcService::getByIds(array_unique($schemaIds));
        $imageList = [];
        /** @var UploadFileItem $up */
        foreach ($resp->getList() as $up) {
            $imageList[$up->getId()] = $up->getUrl();
        }
        foreach ($imageList as $schemaId => $imageUrl) {
            $share = ShareTokenSchemaRepository::getInfoBySchemaId($schemaId);
            if (empty($share)) {
                continue;
            }
            $logo = $qr = null;
            $parallel =  new Parallel(2);
            $parallel->add(function () use (&$logo, $share) {
                $logo = SchemaService::createLogo($share);
            });
            $parallel->add(function () use (&$qr, $imageUrl) {
                $qr = file_get_contents($imageUrl);
            });
            try {
                $parallel->wait();
            } catch (ParallelExecutionException $e) {
            }
            $o = tmpfile();
            $meta = stream_get_meta_data($o);
            file_put_contents($meta['uri'], SchemaService::qrcodeWithLogo($qr, $logo));
            //上传文件
            $uploader = UploadHelper::newInstance('schema');
            [$upId, $upName, $upUrl] = $uploader->upload($meta['uri'], sprintf("%s.png", uniqid()));
            fclose($o);
            $oldSchemaId = $share->schema_id;
            $share->schema_id = $upId;
            $share->updated_at = Carbon::now();
            $share->save();
            $this->line($share->id.':'.json_encode(['o'=>$oldSchemaId, 'n'=>$upId]), 'info');
        }
        $this->line('all of wx logo update complete!', 'info');
    }
}
