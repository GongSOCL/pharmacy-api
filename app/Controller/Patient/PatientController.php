<?php
declare(strict_types=1);

namespace App\Controller\Patient;

use App\Helper\Helper;
use App\Request\Patient\ScanQrRequest;
use App\Service\Order\PurchaseNotifyService;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Youyao\Framework\AbstractAction;

class PatientController extends AbstractAction
{
    /**
     * 
     * @var PurchaseNotifyService purchaseService
     */
    private $purchaseService = null;
    public function __construct()
    {
        $this->purchaseService = ApplicationContext::getContainer()
            ->get(PurchaseNotifyService::class);
    }

    /**
     *
     * @OA\Post (
     *      path="/patient/scan-qr",
     *      tags={"患者扫药店码"},
     *      summary="推送消费通知",
     *      description="推送消费通知",
     *      operationId="PatientScanQr",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="药店二维码信息",
     *         in="query",
     *         name="token_id",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Parameter(
     *         description="经度",
     *         in="query",
     *         name="longitude",
     *         required=false,
     *         @OA\Schema(type="float")
     *      ),
     *      @OA\Parameter(
     *         description="维度",
     *         in="query",
     *         name="latitude",
     *         required=false,
     *         @OA\Schema(type="float")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                     )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param ScanQrRequest $request
     * @return array
     */
    public function scanQr(ScanQrRequest $request)
    {
        $userInfo = Helper::getLoginUser();
        $token = $request->input('token_id');
        $lon = $request->input('longitude', 0.0);
        $lat = $request->input('latitude', 0.0);
        $info = $this->purchaseService->notifyClerk($userInfo->id, $token, $lon, $lat);
        return $this->success(['info'=>$info]);
    }
}
