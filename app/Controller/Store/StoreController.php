<?php
declare(strict_types=1);
namespace App\Controller\Store;

use App\Request\Store\BranchStoreListRequest;
use App\Request\Store\GeoStoreListRequest;
use App\Request\Store\GetCurrentGeoPosAndDepartRequest;
use App\Request\Store\MainStoreListRequest;
use App\Request\Store\NearStoreListRequest;
use App\Request\Store\ProvinceCityListRequest;
use App\Request\Store\ScanStoreListRequest;
use App\Request\Store\StoreDetailRequest;
use App\Service\Store\StoreGpsService;
use App\Service\Store\StoreService;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Youyao\Framework\AbstractAction;

class StoreController extends AbstractAction
{
    private $storeService = null;
    private $storeGpsService = null;
    public function __construct()
    {
        $this->storeService = ApplicationContext::getContainer()
            ->get(StoreService::class);
        $this->storeGpsService = ApplicationContext::getContainer()
            ->get(StoreGpsService::class);
    }

    /**
     *
     * @OA\Get (
     *      path="/store/gps/province-list",
     *      tags={"药店"},
     *      summary="获取省市列表",
     *      description="获取省市列表",
     *      operationId="StoreGpsProvinceList",
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="省市列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="省市ID"),
     *                                  @OA\Property(property="province", type="string", description="省市名称")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param RequestInterface $request
     * @return array
     */
    public function provinceList(RequestInterface $request)
    {
        $res = $this->storeService->provinceList();
        return  $this->success($res);
    }
    /**
     *
     * @OA\Get (
     *      path="/store/gps/province-city-list",
     *      tags={"药店"},
     *      summary="获取省市城市列表",
     *      description="获取省市城市列表",
     *      operationId="StoreGpsProvinceCityList",
     *      @OA\Parameter(
     *         description="省市ID",
     *         in="query",
     *         name="province_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="省市城市列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="城市ID"),
     *                                  @OA\Property(property="city_name", type="string", description="城市名称")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param ProvinceCityListRequest $request
     * @return array
     */
    public function provinceCityList(ProvinceCityListRequest $request)
    {
        $provinceId = $request->input('province_id');
        $res = $this->storeService->provinceCityList($provinceId);
        return  $this->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/store/gps/city-list",
     *      tags={"药店"},
     *      summary="获取城市列表",
     *      description="获取城市列表",
     *      operationId="StoreGpsCityList",
     *      @OA\Parameter(
     *         description="当前页码",
     *         in="query",
     *         name="page",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="当前页大小",
     *         in="query",
     *         name="page_size",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="城市列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="城市ID"),
     *                                  @OA\Property(property="city_name", type="string", description="城市名称")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param RequestInterface $request
     * @return array
     */
    public function cityList(RequestInterface $request)
    {
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', 20);
        $res = $this->storeService->cityList((int)$page, (int)$pageSize);
        return $this->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/store/gps/main-store-list",
     *      tags={"药店"},
     *      summary="连锁店列表",
     *      description="连锁店列表",
     *      operationId="StoreGpsMianStoreList",
     *      @OA\Parameter(
     *         description="城市ID",
     *         in="query",
     *         name="city_id",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Parameter(
     *         description="当前页码",
     *         in="query",
     *         name="page",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="当前页大小",
     *         in="query",
     *         name="page_size",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="搜索关键字",
     *         in="query",
     *         name="keywords",
     *         required=false,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="连锁店列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="连锁店ID"),
     *                                  @OA\Property(property="city_id", type="string", description="城市ID"),
     *                                  @OA\Property(property="store_name", type="string", description="连锁店名称")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param MainStoreListRequest $request
     * @return array
     */
    public function mainStoreListByCity(MainStoreListRequest $request)
    {
        $cityId = $request->input('city_id');
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', 20);
        $keywords = (string) $request->query('keywords', '');
        $res = $this->storeService->mainStoreListByCity($cityId, (int)$page, (int)$pageSize, $keywords);
        return $this->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/store/gps/store-list",
     *      tags={"药店"},
     *      summary="药店列表",
     *      description="药店列表",
     *      operationId="StoreGpsStoreList",
     *      @OA\Parameter(
     *         description="连锁店ID",
     *         in="query",
     *         name="main_store_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="当前页码",
     *         in="query",
     *         name="page",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="当前页大小",
     *         in="query",
     *         name="page_size",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="搜索关键字",
     *         in="query",
     *         name="keywords",
     *         required=false,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="连锁店列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="药店ID"),
     *                                  @OA\Property(property="store_name", type="string", description="药店名称"),
     *                                  @OA\Property(property="store_name_alias", type="string", description="药店别名"),
     *                                  @OA\Property(property="location", type="string", description="药店地址")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param BranchStoreListRequest $request
     * @return array
     */
    public function branchStoreListByMain(BranchStoreListRequest $request)
    {
        $mainStoreId = $request->input('main_store_id');
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', 20);
        $keywords = (string) $request->query('keywords', '');
        $res = $this->storeService->storeListByMainStore($mainStoreId, (int)$page, (int)$pageSize, $keywords);
        return $this->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/store/current-info",
     *      tags={"药店"},
     *      summary="获取当前城市或科室信息",
     *      description="获取当前城市或科室信息",
     *      operationId="StoreGpsCurrentInfo",
     *      @OA\Parameter(
     *         description="经度",
     *         in="query",
     *         name="longitude",
     *         required=true,
     *         @OA\Schema(type="float")
     *      ),
     *      @OA\Parameter(
     *         description="维度",
     *         in="query",
     *         name="latitude",
     *         required=true,
     *         @OA\Schema(type="float")
     *      ),
     *      @OA\Parameter(
     *         description="二维码TokenId",
     *         in="query",
     *         name="token_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="is_scan", type="integer", description="是否扫码0：未扫码 1：已扫码"),
     *                          @OA\Property(
     *                              property="city",
     *                              type="object",
     *                              description="当前城市信息",
     *                              @OA\Property(property="id", type="integer", description="城市ID"),
     *                              @OA\Property(property="name", type="string", description="城市名称"),
     *                          ),
     *                          @OA\Property(
     *                              property="depart",
     *                              type="object",
     *                              description="当前城市信息",
     *                              type="object",
     *                              @OA\Property(property="id", type="integer", description="科室ID"),
     *                              @OA\Property(property="name", type="string", description="科室"),
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param GetCurrentGeoPosAndDepartRequest $request
     * @return array
     */
    public function getCurrentGeoPosAndDepart(GetCurrentGeoPosAndDepartRequest $request)
    {
        $longitude = $request->input('longitude');
        $latitude = $request->input('latitude');
        $tokenId = $request->input('token_id');
        $res = $this->storeGpsService->getCurrentGeoPosAndDepart($longitude, $latitude, $tokenId);
        return $this->success($res);
    }


    /**
     *
     * @OA\Get (
     *      path="/store/geo-store-list",
     *      tags={"药店"},
     *      summary="当前定位城市的附近药店列表",
     *      description="当前定位城市的附近药店列表",
     *      operationId="StoreGeoStoreList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="城市ID",
     *         in="query",
     *         name="city_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="科室ID",
     *         in="query",
     *         name="depart_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="是否扫码0：未扫码 1：已扫码",
     *         in="query",
     *         name="is_scan",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="经度",
     *         in="query",
     *         name="longitude",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="纬度",
     *         in="query",
     *         name="latitude",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *     @OA\Parameter(
     *         description="当前分页",
     *         in="query",
     *         name="page",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="分页大小",
     *         in="query",
     *         name="page_size",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="药店列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="药店ID"),
     *                                  @OA\Property(property="store_name", type="string", description="药店名称"),
     *                                  @OA\Property(property="store_name_alias", type="string", description="药店别名"),
     *                                  @OA\Property(property="location", type="string", description="药店地址"),
     *                                  @OA\Property(property="phone", type="string", description="手机号"),
     *                                  @OA\Property(property="is_policy", type="integer", description="是否有优惠券"),
     *                                  @OA\Property(property="dist_km", type="float", description="距离（千米为单位）"),
     *                                  @OA\Property(
     *                                      property="products",
     *                                      type="array",
     *                                      description="商品集合",
     *                                      @OA\Items(
     *                                          type="object",
     *                                          @OA\Property(property="store_id", type="integer", description="药店ID"),
     *                                          @OA\Property(
     *                                              property="yy_product_id",
     *                                              type="integer",
     *                                              description="商品ID"
     *                                          ),
     *                                          @OA\Property(
     *                                              property="product_name",
     *                                              type="string",
     *                                              description="商品名称"
     *                                          ),
     *                                      )
     *                                  )
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param GeoStoreListRequest $request
     * @return array
     */
    public function geoStoreList(GeoStoreListRequest $request)
    {
        $cityId = $request->input('city_id');
        $isScan = $request->input('is_scan');
        $departId = $request->input('depart_id', 0);
        $longitude = $request->input('longitude');
        $latitude = $request->input('latitude');
        $page = (int)$request->input('page', 1);
        $pageSize = (int)$request->input('page_size', 20);
        $res = $this->storeGpsService->geoStoreList(
            $cityId,
            $departId,
            (float)$longitude,
            (float)$latitude,
            $isScan,
            $page,
            $pageSize
        );
        return $this->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/store/scan-store-list",
     *      tags={"药店"},
     *      summary="患者扫医生码药店列表(废弃)",
     *      description="患者扫医生码药店列表(废弃)",
     *      operationId="StoreScanStoreList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="二维码信息",
     *         in="query",
     *         name="token_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="搜索关键字",
     *         in="query",
     *         name="keyword",
     *         required=false,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Parameter(
     *         description="当前分页",
     *         in="query",
     *         name="page",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="分页大小",
     *         in="query",
     *         name="page_size",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="药店列表",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="page", type="integer", description="当前页数"),
     *                              @OA\Property(property="page_size", type="integer", description="分页数量"),
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="药店ID"),
     *                                  @OA\Property(property="store_name", type="string", description="药店名称"),
     *                                  @OA\Property(property="store_name_alias", type="string", description="药店别名"),
     *                                  @OA\Property(property="location", type="string", description="药店地址"),
     *                                  @OA\Property(property="phone", type="string", description="手机号"),
     *                                  @OA\Property(property="is_policy", type="integer", description="是否有优惠券"),
     *                                  @OA\Property(
     *                                      property="products",
     *                                      type="array",
     *                                      description="商品集合",
     *                                      @OA\Items(
     *                                          type="object",
     *                                          @OA\Property(property="store_id", type="integer", description="药店ID"),
     *                                          @OA\Property(
     *                                              property="yy_product_id",
     *                                              type="integer",
     *                                              description="商品ID"
     *                                          ),
     *                                          @OA\Property(
     *                                              property="product_name",
     *                                              type="string",
     *                                              description="商品名称"
     *                                          ),
     *                                      )
     *                                  )
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param ScanStoreListRequest $request
     * @return array
     */
    public function scanStoreList(ScanStoreListRequest $request)
    {
        $token = $request->input('token_id');
        $keyword = $request->input('keyword', '');
        $page = (int)$request->input('page', 1);
        $pageSize = (int)$request->input('page_size', 20);
        $res = $this->storeService->scanDoctorQrStoreList($token, $keyword, $page, $pageSize);
        return $this->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/store/store-list",
     *      tags={"药店"},
     *      summary="患者切换城市或搜索药店列表",
     *      description="患者切换城市或搜索药店列表",
     *      operationId="StoreStoreList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="城市ID",
     *         in="query",
     *         name="city_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="科室ID",
     *         in="query",
     *         name="depart_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="搜索关键字",
     *         in="query",
     *         name="keyword",
     *         required=false,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Parameter(
     *         description="当前分页",
     *         in="query",
     *         name="page",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="分页大小",
     *         in="query",
     *         name="page_size",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="药店列表",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="page", type="integer", description="当前页数"),
     *                              @OA\Property(property="page_size", type="integer", description="分页数量"),
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="药店ID"),
     *                                  @OA\Property(property="store_name", type="string", description="药店名称"),
     *                                  @OA\Property(property="store_name_alias", type="string", description="药店别名"),
     *                                  @OA\Property(property="location", type="string", description="药店地址"),
     *                                  @OA\Property(property="phone", type="string", description="手机号"),
     *                                  @OA\Property(property="is_policy", type="integer", description="是否有优惠券"),
     *                                  @OA\Property(
     *                                      property="products",
     *                                      type="array",
     *                                      description="商品集合",
     *                                      @OA\Items(
     *                                          type="object",
     *                                          @OA\Property(property="store_id", type="integer", description="药店ID"),
     *                                          @OA\Property(
     *                                              property="yy_product_id",
     *                                              type="integer",
     *                                              description="商品ID"
     *                                          ),
     *                                          @OA\Property(
     *                                              property="product_name",
     *                                              type="string",
     *                                              description="商品名称"
     *                                          ),
     *                                      )
     *                                  )
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param RequestInterface $request
     * @return array
     */
    public function storeList(RequestInterface $request)
    {
        $cityId = $request->input('city_id', 0);
        $departId = $request->input('depart_id', 0);
        $keyword = $request->input('keyword', '');
        $pageSize = (int)$request->input('page_size', 20);
        $res = $this->storeService->searchStoreList($cityId, $departId, $keyword, $pageSize);
        return $this->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/store/near-store-list",
     *      tags={"药店"},
     *      summary="附近药店列表药店列表",
     *      description="附近药店列表药店列表",
     *      operationId="StoreNearStoreList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="二维信息(可不传)",
     *         in="query",
     *         name="token_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="经度",
     *         in="query",
     *         name="longitude",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="纬度",
     *         in="query",
     *         name="latitude",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Parameter(
     *         description="距离",
     *         in="query",
     *         name="range",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="药店列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="药店ID"),
     *                                  @OA\Property(property="store_name", type="string", description="药店名称"),
     *                                  @OA\Property(property="store_name_alias", type="string", description="药店别名"),
     *                                  @OA\Property(property="location", type="string", description="药店地址"),
     *                                  @OA\Property(property="phone", type="string", description="手机号"),
     *                                  @OA\Property(property="is_policy", type="integer", description="是否有优惠券"),
     *                                  @OA\Property(
     *                                      property="products",
     *                                      type="array",
     *                                      description="商品集合",
     *                                      @OA\Items(
     *                                          type="object",
     *                                          @OA\Property(property="store_id", type="integer", description="药店ID"),
     *                                          @OA\Property(
     *                                              property="yy_product_id",
     *                                              type="integer",
     *                                              description="商品ID"
     *                                          ),
     *                                          @OA\Property(
     *                                              property="product_name",
     *                                              type="string",
     *                                              description="商品名称"
     *                                          ),
     *                                      )
     *                                  )
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param NearStoreListRequest $request
     * @return array
     */
    public function nearStoreList(NearStoreListRequest $request)
    {
        $longitude = $request->input('longitude');
        $latitude = $request->input('latitude');
        $range = $request->input('range');
        $res = $this->storeGpsService->nearStoreList($longitude, $latitude, $range);
        return $this->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/store/store-detail",
     *      tags={"药店"},
     *      summary="药店详情",
     *      description="药店详情",
     *      operationId="StoreDetail",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="药店ID",
     *         in="query",
     *         name="store_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="药店ID"),
     *                          @OA\Property(property="store_name", type="string", description="药店名称"),
     *                          @OA\Property(property="store_name_alias", type="string", description="药店别名"),
     *                          @OA\Property(property="location", type="string", description="药店地址"),
     *                          @OA\Property(property="phone", type="string", description="手机号"),
     *                          @OA\Property(property="is_policy", type="integer", description="是否有优惠券"),
     *                          @OA\Property(
     *                                      property="products",
     *                                      type="array",
     *                                      description="商品集合",
     *                                      @OA\Items(
     *                                          type="object",
     *                                          @OA\Property(property="store_id", type="integer", description="药店ID"),
     *                                          @OA\Property(
     *                                              property="yy_product_id",
     *                                              type="integer",
     *                                              description="商品ID"
     *                                          ),
     *                                          @OA\Property(
     *                                              property="product_name",
     *                                              type="string",
     *                                              description="商品名称"
     *                                          ),
     *                                      )
     *                            )
     *
     *                      )
     *               )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param StoreDetailRequest $request
     * @return array
     */
    public function storeDetail(StoreDetailRequest $request)
    {
        $storeId = $request->input('store_id');
        $res = $this->storeService->getStoreDetail($storeId);
        return $this->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/store/{id}/gps",
     *      tags={"药店"},
     *      summary="获取药店最新一个gps",
     *      description="获取药店最新一个gps",
     *      operationId="StoreNewiestGps",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="药店ID",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="longitude", type="string", description="药店经度"),
     *                          @OA\Property(property="latituded", type="string", description="药店纬度")
     *                      )
     *               )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return array
     */
    public function getGpsInfo($id): array
    {
        [$lat, $long] = $this->storeService->getStoreGpsInfo((int) $id);
        return $this->success([
            'longitude' => $long,
            'latituded' => $lat
        ]);
    }
}
