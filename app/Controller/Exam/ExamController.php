<?php
declare(strict_types=1);

namespace App\Controller\Exam;

use App\Request\Exam\AnswerExamRequest;
use App\Request\Exam\FinishExamRequest;
use App\Request\Exam\GetQuestion;
use App\Request\Exam\ReviewRequest;
use App\Service\Exam\ExamService;
use Psr\Http\Message\ResponseInterface;
use Youyao\Framework\AbstractAction;

class ExamController extends AbstractAction
{
    /**
     *
     * @OA\Get (
     *      path="api/exams",
     *      tags={"考试"},
     *      summary="考试列表",
     *      description="考试列表",
     *      operationId="AppExamList",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                               property="list",
     *                              type="array",
     *                              description="考试列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="考试id"),
     *                                  @OA\Property(property="name", type="string", description="考试名称"),
     *                                  @OA\Property(
     *                                      property="is_time_unlimited",
     *                                      type="integer",
     *                                      description="是否不限时: 0否 1是"),
     *                                  @OA\Property(
     *                                      property="start",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="考试开始时间"),
     *                                  @OA\Property(
     *                                      property="end",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="考试结束时间"),
     *                                  @OA\Property(
     *                                      property="exam_time",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="考试时长"),
     *                                  @OA\Property(
     *                                      property="img",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="图片"),

     *                                  @OA\Property(property="exam_status", type="array", description="考试状态",
     *                                        @OA\Items(
     *                                          type="object",
     *                                          @OA\Property(property="score", type="integer", description="得分"),
     *                                          @OA\Property(
     *                                              property="is_pass",
     *                                              type="string",
     *                                              description="考试状态 0未通过 1通过 2可以参加考试 3未开始 4补考")
     *                                        )
     *                              ),
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )

     */
    public function list(): array
    {
        $data = ExamService::list();

        return $this->success([
            'list' => $data
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="api/exam/{id}",
     *      tags={"考试"},
     *      summary="考试详情",
     *      description="考试详情",
     *      operationId="AppExamInfo",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="考试id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="考试详情",
     *                              @OA\Property(property="name", type="string", description="考试名称"),
     *                              @OA\Property(property="desc", type="string", description="考试说明"),
     *                              @OA\Property(
     *                                  property="is_time_unlimited",
     *                                  type="integer",
     *                                  description="是否不限时: 0否 1是"),
     *                              @OA\Property(
     *                                  property="start",
     *                                  type="string",
     *                                  example="2021-10-01",
     *                                  description="考试开始时间"),
     *                              @OA\Property(
     *                                  property="end",
     *                                  type="string",
     *                                  example="2021-10-01",
     *                                  description="考试结束时间"),
     *                              @OA\Property(
     *                                  property="exam_time",
     *                                  type="string",
     *                                  example="2021-10-01",
     *                                  description="考试时长"),
     *                              @OA\Property(
     *                                  property="is_pass",
     *                                  type="integer",
     *                                  example="2021-10-01",
     *                                  description="0未通过 1通过 2可以参加考试 3未开始 4补考 5当前考试正巧过期 返回考试列表"),
     *                              @OA\Property(
     *                                  property="fraction",
     *                                  type="integer",
     *                                  example="2021-10-01",
     *                                  description="得分"),
     *                              @OA\Property(
     *                                  property="exam_num",
     *                                  type="integer",
     *                                  example="2021-10-01",
     *                                  description="考试次数 0无限")
     *                          )
     *                      )
     *
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id

     */
    public function info($id)
    {
        $id = (int)$id;
        $info = ExamService::getExamInfo($id);

        return $this->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Post  (
     *      path="api/exam/{id}/start",
     *      tags={"考试"},
     *      summary="开始考试",
     *      description="开始考试",
     *      operationId="AppExamStart",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="考试id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="next_question_id", type="integer", description="第一题题目id"),
     *                          @OA\Property(property="total", type="integer", description="总题目id"),
     *                          @OA\Property(property="request_id", type="string", description="请求id,后面答题需要带上"),
     *                          @OA\Property(property="pre_item_id", type="string", description="上一题id"),
     *                          @OA\Property(
     *                              property="use_timen",
     *                              type="string",
     *                              description="当值是数字时为该场考试的时长/剩余时间【单位：秒】。
     * -2 考试时间到【交卷】,-1 考试时长无限时"),
     *                          @OA\Property(property="is_sub", type="string", description="no 正常, yes强制提交考卷"),
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     */
    public function start($id)
    {
        $id = (int) $id;

        [$nextQuestionId, $total, $requestId, $useTimen, $preItemId, $is_sub] = ExamService::startExam($id);

        return $this->success([
            'pre_item_id' => $preItemId,
            'next_question_id' => $nextQuestionId,
            'total' => $total,
            'request_id' => $requestId,
            'use_timen' => $useTimen,
            'is_sub' => $is_sub
        ]);
    }

    /**
     *
     * @OA\Get  (
     *      path="api/exam/{id}/question",
     *      tags={"考试"},
     *      summary="获取考试试题",
     *      description="获取考试试题",
     *      operationId="AppExamQuestionGet",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="考试id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="question_id",
     *          in="query",
     *          description="试题id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="request_id",
     *          in="query",
     *          description="请求id, start接口返回的",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="item_info", type="string", description="题目已选择答案"),
     *                          @OA\Property(property="s_num", type="integer", description="题目序号"),
     *                          @OA\Property(
     *                              property="is_sub",
     *                              type="integer",
     *                              description="是否最后一题 yes最后一题【按钮改交卷】 no不是"),
     *                          @OA\Property(property="pre_item_id", type="string", description="上一题"),
     *                          @OA\Property(property="question_id", type="integer", description="题目id"),
     *                          @OA\Property(property="name", type="string", description="考试题目"),
     *                          @OA\Property(
     *                              property="type",
     *                              type="object",
     *                              description="题目类型",
     *                              @OA\Property(property="id", type="integer", description="类型id: 1单选 2多选 3输入"),
     *                              @OA\Property(property="desc", type="string", description="类型描述: 如单选\多选\输入"),
     *                          ),
     *                          @OA\Property(
     *                              property="options",
     *                              type="array",
     *                              description="题目选项",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="选项id"),
     *                                  @OA\Property(property="name", type="string", description="选项标题")
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     */
    public function getQuestion($id, GetQuestion $getQuestion)
    {
        $id = (int)$id;
        $questionId = (int)$getQuestion->query('question_id');
        $requestId = (string)$getQuestion->query('request_id');
        $info = ExamService::getQuestion(
            $id,
            $questionId,
            $requestId
        );

        return $this->success($info);
    }

    /**
     *
     * @OA\Post   (
     *      path="api/exam/{id}/answer",
     *      tags={"考试"},
     *      summary="考试回答题目",
     *      description="考试回答题目",
     *      operationId="AppExamQuestionAnswer",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="考试id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                     type="object",
     *                     @OA\Property(property="question_id", type="integer", description="试题id"),
     *                     @OA\Property(property="request_id", type="string", description="请求id"),
     *                     @OA\Property(
     *                          property="option_ids",
     *                          type="array",
     *                          description="选中答案选项id",
     *                          @OA\Items(type="integer")
     *                     ),
     *                     @OA\Property(
     *                          property="content",
     *                          type="string",
     *                          description="文本类型题目输入内容",
     *                     ),
     *                     required={"question_id", "request_id"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="result",
     *                              type="array",
     *                              description="答题结果",
     *                              @OA\Items(
     *                                  @OA\Property(
     *                                      property="is_question_pass",
     *                                      type="integer",
     *                                      description="该题目是否通过: 0否 1是"),
     *                                  @OA\Property(
     *                                      property="is_last",
     *                                      type="integer",
     *                                      description="是否最后一题: 0否 1是"),
     *                                  @OA\Property(
     *                                      property="next_question_id",
     *                                      type="integer",
     *                                       description="下一题题目id"),
     *                                  @OA\Property(
     *                                      property="is_explain",
     *                                      type="integer",
     *                                       description="是否答案解析: 0否 1是"),
     *                                  @OA\Property(property="explain", type="string", description="答案解析"),
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id

     */
    public function answer($id, AnswerExamRequest $request)
    {
        $id = (int)$id;
        $info = ExamService::answerExam(
            $id,
            (int)$request->post('question_id'),
            (string)$request->post('request_id'),
            $request->post('option_ids', []),
            (string)$request->post('content')
        );
//
        return $this->success([
            'result' => $info
        ]);
    }

    /**
     *
     * @OA\Post   (
     *      path="api/exam/{id}/submit",
     *      tags={"考试"},
     *      summary="考试交卷",
     *      description="考试交卷",
     *      operationId="AppExamQuestionSubmit",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="考试id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                     type="object",
     *                     @OA\Property(
     *                          property="request_id",
     *                          type="string",
     *                          description="请求id"
     *                      ),
     *                     @OA\Property(
     *                          property="is_sub",
     *                          type="string",
     *                          description="是否强制交卷"
     *                      ),
     *                     required={"request_id", "is_sub"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="result",
     *                              type="object",
     *                              description="答题结果",
     *                                  @OA\Property(
     *                                      property="with_agreement",
     *                                      type="integer",
     *                                      description="是否包含协议: 0否 1是"),
     *                                  @OA\Property(
     *                                      property="is_exam_pass",
     *                                      type="integer",
     *                                      description="考试是否通过: 0否 1是"),
     *                                  @OA\Property(property="fraction", type="string", description="分数"),
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @param FinishExamRequest $request

     */
    public function finish($id, FinishExamRequest $request)
    {
        $id = (int)$id;
        $info = ExamService::finishExam(
            $id,
            (string)$request->post('request_id'),
            (string) $request->post('is_sub')
        );

        return $this->success([
            'result' => $info
        ]);
    }



    /**
     *
     * @OA\Get  (
     *      path="api/exam/{id}/review",
     *      tags={"考试"},
     *      summary="考试回顾",
     *      description="考试回顾",
     *      operationId="AppExamQuestionSubmit",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="考试id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="request_id",
     *          in="query",
     *          description="答题请求id,刚刚完成考试后需要带上",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="回顾列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="题目id"),
     *                                  @OA\Property(property="name", type="string", description="题目名称"),
     *                                  @OA\Property(
     *                                      property="type",
     *                                      type="object",
     *                                      description="题目类型",
     *                                      @OA\Property(
     *                                          property="id",
     *                                          type="integer",
     *                                          description="类型id: 1单选 2多选 3输入"),
     *                                      @OA\Property(
     *                                          property="desc",
     *                                          type="string",
     *                                          description="类型描述: 如单选\多选\输入"),
     *                                  ),
     *                                  @OA\Property(
     *                                      property="options",
     *                                      type="array",
     *                                      description="题目选项",
     *                                      @OA\Items(
     *                                          @OA\Property(property="id", type="integer", description="选项id"),
     *                                          @OA\Property(property="name", type="string", description="选项内容"),
     *                                          @OA\Property(
     *                                              property="is_choosed",
     *                                              type="integer",
     *                                              description="是否勾选"),
     *                                          @OA\Property(
     *                                              property="is_right",
     *                                              type="integer",
     *                                              description="答案是否正确")
     *                                      )
     *                                  ),
     *                                  @OA\Property(
     *                                      property="is_right",
     *                                      type="integer",
     *                                      description="当前题目是否答对了"),
     *                                  @OA\Property(property="explain", type="string", description="答案解析"),
     *                                  @OA\Property(property="content", type="string", description="文本类型输入内容"),
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     */
    public function review($id, ReviewRequest $request)
    {
        $id =(int)$id;
        $requestId = (string)$request->query('request_id', '');
        $reviews = ExamService::review($id, $requestId);

        return $this->success([
            'list' => $reviews
        ]);
    }
}
