<?php
declare(strict_types=1);
namespace App\Controller\Clerk;

use App\Helper\Helper;
use App\Request\Clerk\ApplyRequest;
use App\Request\Clerk\CancelNotifyRequest;
use App\Request\Clerk\MyClerkSaleListRequest;
use App\Request\Clerk\NotifyListRequest;
use App\Service\Order\OrderService;
use App\Service\Order\PurchaseNotifyService;
use App\Service\User\UserClerkService;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Youyao\Framework\AbstractAction;

class ClerkController extends AbstractAction
{

    private $purchaseService = null;
    private $clerkService = null;
    public function __construct()
    {
        $this->purchaseService = ApplicationContext::getContainer()
            ->get(PurchaseNotifyService::class);
        $this->clerkService = ApplicationContext::getContainer()
            ->get(UserClerkService::class);
    }

    /**
     *
     * @OA\Get (
     *      path="/clerk/sale/notify-list",
     *      tags={"店员"},
     *      summary="获取购买消息列表",
     *      description="获取购买消息列表",
     *      operationId="ClerkSaleNotifyList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="药店ID",
     *         in="query",
     *         name="store_id",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *     @OA\Parameter(
     *         description="tab状态值(0 待接单  2：取消)",
     *         in="query",
     *         name="status",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="当前页",
     *         in="query",
     *         name="page",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="每页返回数量",
     *         in="query",
     *         name="page_size",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="total", type="integer", description="总数量"),
     *                          @OA\Property(property="page", type="integer", description="当前页数"),
     *                          @OA\Property(property="page_size", type="integer", description="分页数量"),
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="购买消息列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="推送消息ID"),
     *                                  @OA\Property(property="patient_user_id", type="string", description="患者ID"),
     *                                  @OA\Property(property="status", type="string", description="状态"),
     *                                  @OA\Property(property="created_at", type="string", description="创建时间"),
     *                                  @OA\Property(property="patient_name", type="string", description="患者名称"),
     *                                  @OA\Property(property="store_id", type="string", description="药店ID"),
     *                                  @OA\Property(property="store_name", type="string", description="药店名称")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param NotifyListRequest $request
     * @return array
     */
    public function notifyList(NotifyListRequest $request)
    {
        $userInfo = Helper::getLoginUser();
        $storeId = (int)$request->input('store_id');
        $status = (int)$request->input('status');
        $pageSize = (int)$request->input('page_size', 20);
        $res = $this->purchaseService->getNotifyListByStoreId($storeId, $userInfo->id, $status, $pageSize);
        return $this->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/clerk/sale/cancel-notify",
     *      tags={"店员"},
     *      summary="店员取消购买消息",
     *      description="店员取消购买消息",
     *      operationId="ClerkSaleCancelNotify",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="消息ID",
     *         in="query",
     *         name="notify_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                     )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param CancelNotifyRequest $request
     * @return array
     */
    public function cancelNotify(CancelNotifyRequest $request)
    {
        $userInfo = Helper::getLoginUser();
        $notifyId = $request->input('notify_id');
        $this->purchaseService->cancelNotify($notifyId, $userInfo);
        return $this->success();
    }

    public function receiveNotify(RequestInterface $request)
    {
        $userInfo = Helper::getLoginUser();
        $notifyId = $request->input('notify_id');
        $this->purchaseService->receiveNotify($notifyId, $userInfo);
        return $this->success();
    }

    /**
     *
     * @OA\Post (
     *      path="/clerk/apply",
     *      tags={"店员"},
     *      summary="店员申请认证",
     *      description="店员申请认证",
     *      operationId="ClerkApply",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="真实姓名",
     *         in="query",
     *         name="real_name",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="药店ID",
     *         in="query",
     *         name="branch_store_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="手机号",
     *         in="query",
     *         name="phone",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="店员角色",
     *         in="query",
     *         name="role",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="职位",
     *         in="query",
     *         name="position",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                     )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param ApplyRequest $request
     * @return array
     */
    public function apply(ApplyRequest $request)
    {
        $userInfo = Helper::getLoginUser();
        $realName = $request->input('real_name');
        $storeId = $request->input('branch_store_id');
        $phone = $request->input('phone');
        $role = $request->input('role');
        $position = $request->input('position');
        $this->clerkService->directPassClerk(
            $userInfo->id,
            $storeId,
            $realName,
            $phone,
            (int)$role,
            (int)$position
        );
        return $this->success();
    }

    /**
     *
     * @OA\Get (
     *      path="/clerk/my-clerk-list",
     *      tags={"店员"},
     *      summary="我的店员",
     *      description="我的店员",
     *      operationId="MyClerkList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="年月（格式2022/10）",
     *         in="query",
     *         name="year_month",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *     @OA\Parameter(
     *         description="商品ID",
     *         in="query",
     *         name="product_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="商品规格ID",
     *         in="query",
     *         name="specs_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="total", type="integer", description="总数量"),
     *                          @OA\Property(property="page", type="integer", description="当前页数"),
     *                          @OA\Property(property="page_size", type="integer", description="分页数量"),
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="店员列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="user_id", type="integer", description="店员ID"),
     *                                  @OA\Property(property="real_name", type="string", description="名称"),
     *                                  @OA\Property(property="header_url", type="string", description="头像"),
     *                                  @OA\Property(property="num", type="integer", description="订单数"),
     *                                  @OA\Property(property="amount", type="float", description="消费金额")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param RequestInterface $request
     * @return array
     */
    public function myClerkList(RequestInterface $request)
    {
        $yearMonth = $request->query('year_month', '');
        $productId = $request->query('product_id', 0);
        $specsId = $request->query('specs_id', 0);
        $res = $this->clerkService->myClerkList($yearMonth, $productId, $specsId);
        return $this->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/clerk/my-clerk-detail",
     *      tags={"店员"},
     *      summary="我的店员详情汇总",
     *      description="我的店员详情汇总",
     *      operationId="MyClerkDetail",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="店员Id（列表中的user_id）",
     *         in="query",
     *         name="clerk_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="年月（格式2022/10）",
     *         in="query",
     *         name="year_month",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *     @OA\Parameter(
     *         description="商品ID",
     *         in="query",
     *         name="product_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="商品规格ID",
     *         in="query",
     *         name="specs_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="store_info",
     *                              type="object",
     *                              description="店员信息",
     *                              @OA\Property(property="store_name", type="string", description="药店名称"),
     *                              @OA\Property(property="store_name_alias", type="string", description="药店别名"),
     *                              @OA\Property(property="location", type="string", description="药店地址"),
     *                              @OA\Property(property="phone", type="string", description="店员手机号")
     *                          ),
     *                          @OA\Property(
     *                              property="sum",
     *                              type="object",
     *                              description="销售汇总",
     *                              @OA\Property(property="num", type="integer", description="订单总数"),
     *                              @OA\Property(property="amount", type="float", description="订单总额"),
     *                              @OA\Property(property="last_sale_time", type="string", description="最近售卖日期")
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param RequestInterface $request
     * @return array
     */
    public function myClerkSaleInfo(RequestInterface $request)
    {
        $clerkId = $request->query('clerk_id', 0);
        $yearMonth = $request->query('year_month', '');
        $productId = $request->query('product_id', 0);
        $specsId = $request->query('specs_id', 0);
        $res = $this->clerkService->myClerkSaleInfo(
            $clerkId,
            $yearMonth,
            $productId,
            $specsId
        );
        return $this->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/clerk/my-clerk-sale-list",
     *      tags={"店员"},
     *      summary="店员销售订单列表",
     *      description="店员销售订单列表",
     *      operationId="MyClerkSaleList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="店员ID",
     *         in="query",
     *         name="clerk_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="年月（格式2022/10）",
     *         in="query",
     *         name="year_month",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *     @OA\Parameter(
     *         description="商品ID",
     *         in="query",
     *         name="product_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="商品规格ID",
     *         in="query",
     *         name="specs_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="当前页",
     *         in="query",
     *         name="page",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="每页显示数量",
     *         in="query",
     *         name="page_size",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="完成订单列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="订单id"),
     *                                  @OA\Property(property="no", type="string", description="订单编号"),
     *                                  @OA\Property(property="date", type="string", description="订单完成时间"),
     *                                  @OA\Property(property="patient", type="string", description="患者姓名"),
     *                                  @OA\Property(property="store", type="string", description="药店名称"),
     *                                  @OA\Property(
     *                                      property="amount",
     *                                      type="number",
     *                                      format="float",
     *                                      description="订单总金额"),
     *                                  @OA\Property(
     *                                      property="score",
     *                                      type="number",
     *                                      format="float",
     *                                      description="积分"),
     *                                  @OA\Property(
     *                                      property="pics",
     *                                      type="array",
     *                                      description="小票图片",
     *                                      @OA\Items(
     *                                          @OA\Property(property="id", type="integer", description="上传图片id"),
     *                                          @OA\Property(property="name", type="string", description="上传图片名称"),
     *                                          @OA\Property(property="url", type="string", description="上传图片url"),
     *                                          @OA\Property(
     *                                              property="type",
     *                                              type="integer",
     *                                              description="上传图片channel: 1 qiniu 3 oss")
     *                                      )
     *                                  ),
     *                                  @OA\Property(
     *                                      property="coupons",
     *                                      type="array",
     *                                      description="优惠券",
     *                                      @OA\Items(
     *                                          @OA\Property(
     *                                              property="policy_type",
     *                                              type="integer",
     *                                              description="优惠类型"),
     *                                          @OA\Property(
     *                                              property="policy_val",
     *                                              type="string",
     *                                              description="优惠方式")
     *                                      )
     *                                  ),
     *                                  @OA\Property(
     *                                      property="drugs",
     *                                      type="array",
     *                                      description="药品列表",
     *                                      @OA\Items(
     *                                          @OA\Property(property="id", type="integer", description="药品id"),
     *                                          @OA\Property(property="name", type="string", description="药品名称"),
     *                                          @OA\Property(property="sku", type="string", description="规格"),
     *                                          @OA\Property(property="total", type="integer", description="购买数量"),
     *                                          @OA\Property(
     *                                              property="score",
     *                                              type="number",
     *                                              format="float",
     *                                              description="积分"),
     *                                          @OA\Property(
     *                                              property="cash",
     *                                              type="number",
     *                                              format="float",
     *                                              description="金额")
     *                                      )
     *                                  )
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @param MyClerkSaleListRequest $request
     * @return array
     */
    public function myClerkSaleList(MyClerkSaleListRequest $request)
    {
        $clerkId = $request->query('clerk_id', 0);
        $yearMonth = $request->query('year_month', '');
        $productId = $request->query('product_id', 0);
        $specsId = $request->query('specs_id', 0);
        $page = (int)$request->input('page', 1);
        $pageSize = (int)$request->input('page_size', 10);
        [$page, $data]  = OrderService::clerkOrderList(
            $clerkId,
            $yearMonth,
            $productId,
            $specsId,
            $page,
            $pageSize
        );
        return $this->success([
            'list' => $data,
            'page' => $page
        ]);
    }
}
