<?php
declare(strict_types=1);
namespace App\Controller\Order;

use App\Request\Order\ListFinishedOrdersRequest;
use App\Request\Order\ListSearchClerkOrdersRequest;
use App\Request\Order\SubmitOrderRequest;
use App\Service\Order\OrderService;
use Youyao\Framework\AbstractAction;

class OrderController extends AbstractAction
{
    /**
     *
     * @OA\Post(
     *      path="/orders",
     *      tags={"订单管理"},
     *      summary="提交订单",
     *      description="提交订单",
     *      operationId="SubmitOrder",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="notify_id", type="integer", description="通知id"),
     *                  @OA\Property(property="longitude", type="number", description="经度"),
     *                  @OA\Property(property="latitude", type="number", description="维度"),
     *                  @OA\Property(
     *                      property="pics",
     *                      type="array",
     *                      description="上传小票图片id",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(
     *                      property="drugs",
     *                      type="array",
     *                      description="购买产品列表",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="product_id", type="integer", description="药品id"),
     *                          @OA\Property(property="sku_id", type="integer", description="药品sku id"),
     *                          @OA\Property(property="total", type="integer", description="购买数量"),
     *                          @OA\Property(property="cash", type="number", format="float", description="总金额")
     *                      )
     *                  ),
     *                  @OA\Property(
     *                      property="coupon_nums",
     *                      type="array",
     *                      description="选用优惠券编码集合",
     *                      @OA\Items(type="string")
     *                  ),
     *                  required={
     *                      "notify_id",
     *                      "pics",
     *                      "drugs"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="新加订单记录id")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @param SubmitOrderRequest $request
     * @return array
     */
    public function submit(SubmitOrderRequest $request): array
    {
        $notifyId =  (int) $request->post('notify_id');
        $pics = $request->post('pics');
        $drugs = $request->post('drugs');
        $couponNums = $request->post('coupon_nums', []);
        $lon = $request->post('longitude', 0.0);
        $lat = $request->post('latitude', 0.0);
        $order = OrderService::submit(
            $notifyId,
            $pics,
            $drugs,
            $couponNums,
            $lon,
            $lat
        );
        return $this->success([
            'id' => $order->id
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/orders",
     *      tags={"订单管理"},
     *      summary="已完成订单列表",
     *      description="已完成订单列表",
     *      operationId="GetFinishOrderList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="当前页",
     *         in="query",
     *         name="current",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="每页显示数量",
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="完成订单列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="订单id"),
     *                                  @OA\Property(property="no", type="string", description="订单编号"),
     *                                  @OA\Property(property="date", type="string", description="订单完成时间"),
     *                                  @OA\Property(property="patient", type="string", description="患者姓名"),
     *                                  @OA\Property(property="store", type="string", description="药店名称"),
     *                                  @OA\Property(
     *                                      property="amount",
     *                                      type="number",
     *                                      format="float",
     *                                      description="订单总金额"),
     *                                  @OA\Property(property="score", type="number", format="float", description="积分"),
     *                                  @OA\Property(
     *                                      property="pics",
     *                                      type="array",
     *                                      description="小票图片",
     *                                      @OA\Items(
     *                                          @OA\Property(property="id", type="integer", description="上传图片id"),
     *                                          @OA\Property(property="name", type="string", description="上传图片名称"),
     *                                          @OA\Property(property="url", type="string", description="上传图片url"),
     *                                          @OA\Property(
     *                                              property="type",
     *                                              type="integer",
     *                                              description="上传图片channel: 1 qiniu 3 oss")
     *                                      )
     *                                  ),
     *                                  @OA\Property(
     *                                      property="coupons",
     *                                      type="array",
     *                                      description="优惠券",
     *                                      @OA\Items(
     *                                          @OA\Property(
     *                                              property="policy_type",
     *                                              type="integer",
     *                                              description="优惠类型"),
     *                                          @OA\Property(
     *                                              property="policy_val",
     *                                              type="string",
     *                                              description="优惠方式")
     *                                      )
     *                                  ),
     *                                  @OA\Property(
     *                                      property="drugs",
     *                                      type="array",
     *                                      description="药品列表",
     *                                      @OA\Items(
     *                                          @OA\Property(property="id", type="integer", description="药品id"),
     *                                          @OA\Property(property="name", type="string", description="药品名称"),
     *                                          @OA\Property(property="sku", type="string", description="规格"),
     *                                          @OA\Property(property="total", type="integer", description="购买数量"),
     *                                          @OA\Property(
     *                                              property="score",
     *                                              type="number",
     *                                              format="float",
     *                                              description="积分"),
     *                                          @OA\Property(
     *                                              property="cash",
     *                                              type="number",
     *                                              format="float",
     *                                              description="金额")
     *                                      )
     *                                  )
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @param ListFinishedOrdersRequest $request
     * @return array
     */
    public function list(ListFinishedOrdersRequest $request): array
    {
        [$page, $data] = OrderService::listFinishOrder(
            (int) $request->query('current', 1),
            (int) $request->query('limit', 10)
        );
        return $this->success([
            'list' => $data,
            'page' => $page
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/orders/clerk/sale/list",
     *      tags={"订单管理"},
     *      summary="店员销售订单列表",
     *      description="店员销售订单列表",
     *      operationId="GetClerkSaleOrderList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="年月",
     *         in="query",
     *         name="year_month",
     *         required=false,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Parameter(
     *         description="产品ID",
     *         in="query",
     *         name="product_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="规格ID",
     *         in="query",
     *         name="specs_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="当前页",
     *         in="query",
     *         name="current",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="每页显示数量",
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="完成订单列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="订单id"),
     *                                  @OA\Property(property="no", type="string", description="订单编号"),
     *                                  @OA\Property(property="date", type="string", description="订单完成时间"),
     *                                  @OA\Property(property="patient", type="string", description="患者姓名"),
     *                                  @OA\Property(property="store", type="string", description="药店名称"),
     *                                  @OA\Property(
     *                                      property="amount",
     *                                      type="number",
     *                                      format="float",
     *                                      description="订单总金额"),
     *                                  @OA\Property(
     *                                      property="score",
     *                                      type="number",
     *                                      format="float",
     *                                      description="积分"),
     *                                  @OA\Property(
     *                                      property="pics",
     *                                      type="array",
     *                                      description="小票图片",
     *                                      @OA\Items(
     *                                          @OA\Property(property="id", type="integer", description="上传图片id"),
     *                                          @OA\Property(property="name", type="string", description="上传图片名称"),
     *                                          @OA\Property(property="url", type="string", description="上传图片url"),
     *                                          @OA\Property(
     *                                              property="type",
     *                                              type="integer", description="上传图片channel: 1 qiniu 3 oss")
     *                                      )
     *                                  ),
     *                                  @OA\Property(
     *                                      property="coupons",
     *                                      type="array",
     *                                      description="优惠券",
     *                                      @OA\Items(
     *                                          @OA\Property(
     *                                              property="policy_type",
     *                                              type="integer", description="优惠类型"),
     *                                          @OA\Property(
     *                                              property="policy_val",
     *                                              type="string", description="优惠方式")
     *                                      )
     *                                  ),
     *                                  @OA\Property(
     *                                      property="drugs",
     *                                      type="array",
     *                                      description="药品列表",
     *                                      @OA\Items(
     *                                          @OA\Property(property="id", type="integer", description="药品id"),
     *                                          @OA\Property(property="name", type="string", description="药品名称"),
     *                                          @OA\Property(property="sku", type="string", description="规格"),
     *                                          @OA\Property(property="total", type="integer", description="购买数量"),
     *                                          @OA\Property(
     *                                              property="score",
     *                                              type="number", format="float", description="积分"),
     *                                          @OA\Property(
     *                                              property="cash",
     *                                              type="number", format="float", description="金额")
     *                                      )
     *                                  )
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @param ListSearchClerkOrdersRequest $request
     * @return array
     */
    public function searchClerkList(ListSearchClerkOrdersRequest $request):array
    {
        [$page, $list,  $sumInfo ] = OrderService::clerkOrderTotalAndOrderList(
            $request->query('year_month', ''),
            $request->query('product_id', 0),
            $request->query('specs_id', 0),
            (int) $request->query('current', 1),
            (int) $request->query('limit', 10)
        );
        return $this->success([
            'list' =>$list,
            'page' => $page,
            'sum_info'=>$sumInfo,
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/orders/patient/list",
     *      tags={"订单管理"},
     *      summary="患者订单列表",
     *      description="患者订单列表",
     *      operationId="GetPatientOrderList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="当前页",
     *         in="query",
     *         name="current",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="每页显示数量",
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="完成订单列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="订单id"),
     *                                  @OA\Property(property="no", type="string", description="订单编号"),
     *                                  @OA\Property(property="date", type="string", description="订单完成时间"),
     *                                  @OA\Property(property="patient", type="string", description="患者姓名"),
     *                                  @OA\Property(property="store", type="string", description="药店名称"),
     *                                  @OA\Property(
     *                                      property="amount",
     *                                      type="number", format="float", description="订单总金额"),
     *                                  @OA\Property(
     *                                      property="score",
     *                                      type="number", format="float", description="积分"),
     *                                  @OA\Property(
     *                                      property="pics",
     *                                      type="array",
     *                                      description="小票图片",
     *                                      @OA\Items(
     *                                          @OA\Property(property="id", type="integer", description="上传图片id"),
     *                                          @OA\Property(property="name", type="string", description="上传图片名称"),
     *                                          @OA\Property(property="url", type="string", description="上传图片url"),
     *                                          @OA\Property(
     *                                              property="type",
     *                                              type="integer",
     *                                              description="上传图片channel: 1 qiniu 3 oss")
     *                                      )
     *                                  ),
     *                                  @OA\Property(
     *                                      property="coupons",
     *                                      type="array",
     *                                      description="优惠券",
     *                                      @OA\Items(
     *                                          @OA\Property(
     *                                              property="policy_type",
     *                                              type="integer",
     *                                              description="优惠类型"),
     *                                          @OA\Property(
     *                                              property="policy_val",
     *                                              type="string",
     *                                              description="优惠方式")
     *                                      )
     *                                  ),
     *                                  @OA\Property(
     *                                      property="drugs",
     *                                      type="array",
     *                                      description="药品列表",
     *                                      @OA\Items(
     *                                          @OA\Property(property="id", type="integer", description="药品id"),
     *                                          @OA\Property(property="name", type="string", description="药品名称"),
     *                                          @OA\Property(property="sku", type="string", description="规格"),
     *                                          @OA\Property(property="total", type="integer", description="购买数量"),
     *                                          @OA\Property(
     *                                              property="score",
     *                                              type="number",
     *                                              format="float",
     *                                              description="积分"),
     *                                          @OA\Property(
     *                                              property="cash",
     *                                              type="number",
     *                                              format="float",
     *                                              description="金额")
     *                                      )
     *                                  )
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @param ListFinishedOrdersRequest $request
     * @return array
     */
    public function patientList(ListFinishedOrdersRequest $request):array
    {
        [$page, $data] = OrderService::patientOrderList(
            (int) $request->query('current', 1),
            (int) $request->query('limit', 10)
        );
        return $this->success([
            'list' => $data,
            'page' => $page
        ]);
    }
}
