<?php
declare(strict_types=1);
namespace App\Controller\Msg;

use App\Request\Msg\ListInsideMsgRequest;
use App\Service\Msg\InsideMsgService;
use Youyao\Framework\AbstractAction;

class MsgController extends AbstractAction
{
    /**
     *
     * @OA\Get (
     *      path="/app/messages",
     *      tags={"站内消息"},
     *      summary="消息列表",
     *      description="消息列表",
     *      operationId="/app/message/list",
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(ref="#/components/parameters/app-role"),
     *     @OA\Parameter(
     *          name="current",
     *          in="query",
     *          description="当前页码",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          description="每页显示数量，默认10",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="total", type="integer", description="消息列表总数"),
     *                          @OA\Property(property="unread_total", type="integer", description="未读消息数量"),
     *                          @OA\Property(property="page", type="integer", description="页码"),
     *                          @OA\Property(property="page_size", type="integer", description="页面数量"),
     *                          @OA\Property(
     *                              property="list",
     *                              type="object",
     *                              @OA\Property(property="id", type="integer", description="id"),
     *                              @OA\Property(property="title", type="string", description="标题"),
     *                              @OA\Property(property="sub_title", type="string", description="副标题"),
     *                              @OA\Property(property="type", type="integer", description="消息的类型"),
     *                              @OA\Property(property="link", type="string", description="链接"),
     *                              @OA\Property(property="paper_yyid", type="string", description="问卷yyid"),
     *                              @OA\Property(property="created_at", type="string", description="时间"),
     *                              @OA\Property(property="is_read", type="integer", description="是否已读0未读1已读")
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    public function list(ListInsideMsgRequest $request): array
    {
        $data = InsideMsgService::listMsg(
            (int) $request->query('current', 1),
            (int) $request->query('limit', 10)
        );
        return $this->success($data);
    }


    /**
     *
     * @OA\Get (
     *      path="/app/messages/{id}",
     *      tags={"app-站内消息"},
     *      summary="消息详情",
     *      description="消息详情",
     *      operationId="/app/message/detail",
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(ref="#/components/parameters/app-role"),
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="消息id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="id"),
     *                          @OA\Property(property="title", type="string", description="标题"),
     *                          @OA\Property(property="sub_title", type="string", description="副标题"),
     *                          @OA\Property(property="type", type="integer", description="消息的类型 "),
     *                          @OA\Property(property="body", type="string", description="消息内容"),
     *                          @OA\Property(property="link", type="string", description="链接"),
     *                          @OA\Property(property="paper_yyid", type="string", description="问卷yyid"),
     *                          @OA\Property(property="created_at", type="string", description="时间")
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    public function detail($id): array
    {
        $data = InsideMsgService::getMsgDetail((int) $id);
        return $this->success($data);
    }
}
