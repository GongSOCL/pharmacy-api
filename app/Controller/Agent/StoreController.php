<?php
declare(strict_types=1);
namespace App\Controller\Agent;

use App\Request\Agent\AgentTokenRequest;
use App\Request\Agent\ListAgentStoresRequest;
use App\Service\Store\StoreService;
use Youyao\Framework\AbstractAction;

class StoreController extends AbstractAction
{
    /**
     *
     * @OA\Get (
     *      path="/agent/stores",
     *      tags={"代表管理"},
     *      summary="获取药店列表",
     *      description="获取药店列表",
     *      operationId="GetAgentDrugStores",
     *      @OA\Parameter(
     *         description="代表token",
     *         in="query",
     *         name="token",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *     @OA\Parameter(
     *         description="搜索关键字",
     *         in="query",
     *         name="keywords",
     *         required=false,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="药店列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="药店id"),
     *                                  @OA\Property(property="name", type="string", description="药店名称")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param ListAgentStoresRequest $request
     * @return array
     */
    public function list(ListAgentStoresRequest $request): array
    {
        $list = StoreService::listAgentStores(
            (string) $request->query('token'),
            (string) $request->query('keywords', '')
        );
        return $this->success([
            'list' => $list
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/agent/stores/{id}/qrcode",
     *      tags={"代表管理"},
     *      summary="获取药店二维码链接",
     *      description="获取药店二维码链接",
     *      operationId="GetAgentDrugStoreQrcodeLink",
     *      @OA\Parameter(
     *         description="代表token",
     *         in="query",
     *         name="token",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Parameter(
     *         description="药店id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="link", type="string", description="药店小程序码链接")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return array
     */
    public function getQrLink($id, AgentTokenRequest $request): array
    {
        $link = StoreService::genStoreQrcode((int) $id, (string) $request->query('token'));
        return $this->success([
            'link' => $link
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/agent/stores/{id}/invite",
     *      tags={"代表管理"},
     *      summary="获取店员邀请链接",
     *      description="获取店员邀请链接",
     *      operationId="GetAgentDrugStoreInviteLink",
     *      @OA\Parameter(
     *         description="代表token",
     *         in="query",
     *         name="token",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Parameter(
     *         description="药店id",
     *         in="query",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="link", type="string", description="邀请小程序码链接")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return array
     */
    public function getInviteLink($id, AgentTokenRequest $request): array
    {
        $link = StoreService::getInviteClerkLink(
            (string) $request->query('token'),
            (int) $id
        );
        return $this->success([
            'link' => $link
        ]);
    }
}
