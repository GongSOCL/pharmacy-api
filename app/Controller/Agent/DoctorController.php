<?php
declare(strict_types=1);
namespace App\Controller\Agent;

use App\Request\Agent\GetAgentDoctorQrcodeRequest;
use App\Request\Agent\ListServeDoctorsRequest;
use App\Service\Agent\AgentService;
use Youyao\Framework\AbstractAction;

class DoctorController extends AbstractAction
{
    /**
     *
     * @OA\Get (
     *      path="/agent/doctors",
     *      tags={"代表管理"},
     *      summary="获取代表医生列表",
     *      description="获取代表医生列表",
     *      operationId="GetAgentDoctors",
     *      @OA\Parameter(
     *         description="代表token",
     *         in="query",
     *         name="token",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *     @OA\Parameter(
     *         description="当前页",
     *         in="query",
     *         name="current",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="每页返回数量",
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          ),
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="医生列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="医生id"),
     *                                  @OA\Property(property="name", type="string", description="医生名称"),
     *                                  @OA\Property(property="headimg", type="string", description="医生头像"),
     *                                  @OA\Property(property="hospital_name", type="string", description="医院名称"),
     *                                  @OA\Property(property="depart_name", type="string", description="科室名称")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @param GetAgentDoctorQrcodeRequest $request
     * @return array
     */
    public function list(ListServeDoctorsRequest $request): array
    {
        [$page, $data] = AgentService::listAgentDoctors(
            (string) $request->query('token'),
            (int) $request->query('current', 1),
            (int) $request->query('limit', 10)
        );
        return $this->success([
            'list' => $data,
            'page' => $page
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/agent/doctors/{id}/qrcode",
     *      tags={"代表管理"},
     *      summary="获取代表所属医生二维码",
     *      description="获取代表所属医生二维码",
     *      operationId="GetAgentDoctorQrcode",
     *      @OA\Parameter(
     *         description="代表token",
     *         in="query",
     *         name="token",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *     @OA\Parameter(
     *         description="医生id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="link", type="string", description="医生邀请小程序码链接")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @param GetAgentDoctorQrcodeRequest $request
     * @return array
     */
    public function getQrLink($id, GetAgentDoctorQrcodeRequest $request): array
    {
        $link = AgentService::generateDoctorQrLink(
            (string) $request->query('token'),
            (int) $id
        );
        return $this->success([
            'link' => $link
        ]);
    }
}
