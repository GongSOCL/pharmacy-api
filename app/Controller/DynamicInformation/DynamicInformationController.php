<?php
declare(strict_types=1);
namespace App\Controller\DynamicInformation;

use App\Request\DynamicInformation\DynamicInformationRequest;
use App\Service\DynamicInformation\DynamicInformationService;
use Youyao\Framework\AbstractAction;

class DynamicInformationController extends AbstractAction
{

    /**
     *
     * @OA\Get(
     *      path="/api/dynamic/information",
     *      tags={"动态资讯"},
     *      summary="动态资讯",
     *      description="动态资讯",
     *      operationId="getDynamicInformationList",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="page",
     *          in="path",
     *          description="页码",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="object",
     *                          @OA\Property(property="id", type="integer", description="ID"),
     *                          @OA\Property(property="title", type="string", description="标题"),
     *                          @OA\Property(property="cover_url", type="string", description="封面"),
     *                          @OA\Property(property="created_at", type="string", description="日期"),
     *                          @OA\Property(property="lable_id", type="string", description="标签id")
     *                       ),
     *                      @OA\Property(property="total", type="string", description="总数"),
     *                      @OA\Property(property="lastPage", type="string", description="最后页码"),
     *                      @OA\Property(property="page", type="string", description="当前页码"),
     *                      @OA\Property(property="size", type="string", description="每页数量"),
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     */
    public function getDynamicInformationList(DynamicInformationRequest $request): array
    {
        $data = DynamicInformationService::getList((int)$request->query('page'), $pageSize = 10);

        return $this->success($data);
    }

    /**
     *
     * @OA\Get(
     *      path="/api/dynamic/information/{id}",
     *      tags={"动态资讯"},
     *      summary="资讯详情",
     *      description="资讯详情",
     *      operationId="getDynamicInformationDetailById",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="page",
     *          in="path",
     *          description="页码",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="ID"),
     *                          @OA\Property(property="title", type="string", description="标题"),
     *                          @OA\Property(property="lable_id", type="string", description="标签id"),
     *                          @OA\Property(property="created_at", type="string", description="日期")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     */
    public function getDynamicInformationDetailById($id): array
    {
        $data = DynamicInformationService::getDetailById($id);

        return $this->success($data);
    }
}
