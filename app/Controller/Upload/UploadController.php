<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\Upload;

use App\Request\Upload\RecordUploadRequests;
use App\Request\Upload\UploadTokenRequests;
use App\Service\Upload\UploadService;
use Grpc\Upload\UPLOAD_SCOPE;
use Psr\Http\Message\ResponseInterface;
use Youyao\Framework\AbstractAction;

class UploadController extends AbstractAction
{

    /**
     * @param string $url
     * @param object $file
     * @param $id
     * @return bool|string
     */
    public function upload(string $url, object $file, $id)
    {
        $extension = $file->getExtension();
        $dir = $url . $id . '/';
        $fileName = $id . '_' . time() . rand(10000000, 99999999) . '.' . $extension;
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $path = $dir.$fileName;
        $file->moveTo($path);
        if (!$file->isMoved()) {
            return false;
        }
        return $fileName;
    }
}
