<?php
declare(strict_types=1);
namespace App\Controller\User;

use App\Helper\Helper;
use App\Repository\UserRepository;
use App\Request\User\IdcardRequest;
use App\Request\User\IdcardUploadRequest;
use App\Request\User\UpdateUserInfoRequest;
use App\Service\User\UserRoleService;
use App\Service\User\UserService;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Context;
use Youyao\Framework\AbstractAction;

class UserController extends AbstractAction
{
    /**
     *
     * @var UserService $userService
     */
    private $userService = null;
    private $userRoleService = null;
    public function __construct()
    {
        $this->userService = ApplicationContext::getContainer()
            ->get(UserService::class);
        $this->userRoleService = ApplicationContext::getContainer()
            ->get(UserRoleService::class);
    }

    /**
     *
     * @OA\Get (
     *      path="/user/info",
     *      tags={"用户"},
     *      summary="用户信息",
     *      description="用户信息",
     *      operationId="UserInfo",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="string", description="用户ID"),
     *                          @OA\Property(property="union_id", type="string", description="用户union_id"),
     *                          @OA\Property(property="open_id", type="string", description="用户open_id"),
     *                          @OA\Property(property="nick_name", type="string", description="昵称"),
     *                          @OA\Property(property="real_name", type="string", description="真实姓名"),
     *                          @OA\Property(property="header_url", type="string", description="头像"),
     *                          @OA\Property(property="role", type="string", description="用户当前角色(1：患者  2：店员)"),
     *                          @OA\Property(property="store_role", type="string", description="店员当前角色"),
     *                          @OA\Property(property="is_upload_idcard", type="integer", description="身份证是否过期1正常2过期"),
     *                          @OA\Property(
     *                              property="roles",
     *                              type="array",
     *                              description="角色列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="user_id", type="string", description="用户ID"),
     *                                  @OA\Property(property="role", type="string", description="角色枚举(1：患者  2：店员)")
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="clerk",
     *                              type="object",
     *                              description="店员信息",
     *                              @OA\Property(property="user_id", type="string", description="用户ID"),
     *                              @OA\Property(property="store_id", type="string", description="店员所在店"),
     *                              @OA\Property(
     *                                  property="role",
     *                                  type="string",
     *                                  description="店员角色(1：普通店员  2：店长)"),
     *                              @OA\Property(property="phone", type="string", description="手机号"),
     *                              @OA\Property(
     *                                  property="position",
     *                                  type="string",
     *                                  description="店员角色(1：药师  2：非药师)"),
     *                              @OA\Property(
     *                                  property="status",
     *                                  type="string",
     *                                  description="状态(0：未申请 1：申请中  2：通过  3：拒绝  4:取消)")
     *                          )
     *                     )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * 获取用户信息
     * @param RequestInterface $request
     * @return array
     */
    public function userInfo(RequestInterface $request)
    {
        $userInfo = Helper::getLoginUser();
        $role = Context::get('role');
        $res = $this->userService->getUserInfoByRole($userInfo->id, $role);
        return $this->success($res);
    }

    /**
     *
     * @OA\Post (
     *      path="/user/update-info",
     *      tags={"用户"},
     *      summary="用户信息更新",
     *      description="用户信息更新",
     *      operationId="UserUpdateInfo",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="昵称",
     *         in="query",
     *         name="nick_name",
     *         required=false,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Parameter(
     *         description="头像",
     *         in="query",
     *         name="header_url",
     *         required=false,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                     )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param UpdateUserInfoRequest $request
     * @return array
     */
    public function updateUserInfo(UpdateUserInfoRequest $request)
    {
        $nickName = (string) $request->input('nick_name', '');
        $headerUrl = (string) $request->input('header_url', '');
        $this->userService->updateUserInfo($nickName, $headerUrl);
        return $this->success();
    }

    /**
     *
     * @OA\Post (
     *      path="/user/set-role",
     *      tags={"用户"},
     *      summary="用户角色设置",
     *      description="用户角色设置",
     *      operationId="UserSetRole",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="二维码信息",
     *         in="query",
     *         name="token_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                     )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * 设置角色
     * @param RequestInterface $request
     * @return array
     * @throws \Exception
     */
    public function setRole(RequestInterface $request)
    {
        $userInfo = Helper::getLoginUser();
        $role = Context::get('role');
        $tokenId = $request->input('token_id');
        $this->userRoleService->setRole($userInfo, $role, $tokenId);
        return $this->success();
    }


    /**
     *
     * @OA\Post (
     *      path="api/user/idcard/upload",
     *      tags={"用户"},
     *      summary="上传身份证",
     *      description="上传身份证",
     *      operationId="upload",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *     @OA\Parameter(
     *          name="photo",
     *          in="query",
     *          description="图片",
     *          required=true,
     *          @OA\Schema(
     *              type="file"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="file_name", type="string", description="文件名称"),
     *                          @OA\Property(property="user_info", type="object",
     *                              @OA\Property(property="name", type="string", description="用户真实姓名（当上传证件为反面时该参数不存在）"),
     *                              @OA\Property(property="sex", type="string", description="用户性别（当上传证件为反面时该参数不存在）"),
     *                              @OA\Property(property="ethnicity", type="string", description="用户民族（当上传证件为反面时该参数不存在）"),
     *                              @OA\Property(
     *                                  property="birthDate",
     *                                  type="string",
     *                                  description="用户出生日期（当上传证件为反面时该参数不存在）"),
     *                              @OA\Property(property="address", type="string", description="用户家庭住址（当上传证件为反面时该参数不存在）"),
     *                              @OA\Property(
     *                                  property="idNumber",
     *                                  type="string",
     *                                   description="用户身份证号码（当上传证件为反面时该参数不存在）"),
     *                              @OA\Property(
     *                                  property="issueAuthority",
     *                                  type="string",
     *                                  description="用户所属户籍机构（当上传证件为正面时该参数不存在）"),
     *                              @OA\Property(
     *                                  property="validPeriod",
     *                                  type="string",
     *                                  description="用户证件有效期（当上传证件为正面时该参数不存在）"),
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */

    /**
     * @param IdcardUploadRequest $request
     * @return mixed
     */
    public function upload(IdcardUploadRequest $request)
    {
        $file = $request->file('photo');

        $fileName = $this->userService->uploadIdcard($file);

        return $this->success($fileName);
    }



    /**
     *
     * @OA\Post (
     *      path="api/user/idcard",
     *      tags={"用户"},
     *      summary="身份证信息提交",
     *      description="身份证信息提交",
     *      operationId="idcard",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *     @OA\Parameter(
     *          name="idcard_front",
     *          in="query",
     *          description="身份证正面",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="idcard_back",
     *          in="query",
     *          description="身份证反面",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *              name="name",
     *              in="query",
     *              description="用户真实姓名",
     *              required=true,
     *              @OA\Schema(
     *                  type="string"
     *              )
     *          ),
     *          @OA\Parameter(
     *              name="sex",
     *              in="query",
     *              description="用户性别",
     *              required=true,
     *              @OA\Schema(
     *                  type="string"
     *              )
     *          ),
     *          @OA\Parameter(
     *              name="ethnicity",
     *              in="query",
     *              description="用户民族",
     *              required=true,
     *              @OA\Schema(
     *                  type="string"
     *              )
     *          ),
     *          @OA\Parameter(
     *              name="birthDate",
     *              in="query",
     *              description="用户出生日期",
     *              required=true,
     *              @OA\Schema(
     *                  type="string"
     *              )
     *          ),
     *          @OA\Parameter(
     *              name="address",
     *              in="query",
     *              description="用户家庭住址",
     *              required=true,
     *              @OA\Schema(
     *                  type="string"
     *              )
     *          ),
     *          @OA\Parameter(
     *              name="idNumber",
     *              in="query",
     *              description="用户身份证号码",
     *              required=true,
     *              @OA\Schema(
     *                  type="string"
     *              )
     *          ),
     *          @OA\Parameter(
     *              name="issueAuthority",
     *              in="query",
     *              description="用户所属户籍机构",
     *              required=true,
     *              @OA\Schema(
     *                  type="string"
     *              )
     *          ),
     *          @OA\Parameter(
     *              name="validPeriod",
     *              in="query",
     *              description="用户证件有效期",
     *              required=true,
     *              @OA\Schema(
     *                  type="string"
     *              )
     *          ),
     *          @OA\Parameter(
     *              name="mobile",
     *              in="query",
     *              description="手机号",
     *              required=true,
     *              @OA\Schema(
     *                  type="string"
     *              )
     *          ),
     *          @OA\Parameter(
     *              name="is_upload_idcard",
     *              in="query",
     *              description="身份证是否过期1正常2过期",
     *              required=true,
     *              @OA\Schema(
     *                  type="string"
     *              )
     *          ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="yyid", type="string", description="用户yyid")
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */

    public function idcard(IdcardRequest $request)
    {
        $front  = $request->input('idcard_front');
        $back   = $request->input('idcard_back');
        $name   = $request->input('name');
        $sex    = $request->input('sex');
        $ethnicity  = $request->input('ethnicity');
        $birthDate  = $request->input('birthDate');
        $address    = $request->input('address');
        $idNumber   = $request->input('idNumber');
        $issueAuthority = $request->input('issueAuthority');
        $validPeriod    = $request->input('validPeriod');
        $mobile         = $request->input('mobile');
        $is_upload_idcard         = $request->input('is_upload_idcard');

        $result = $this->userService->idcardSubmit(
            $front,
            $back,
            $name,
            $sex,
            $ethnicity,
            $birthDate,
            $address,
            $idNumber,
            $issueAuthority,
            $validPeriod,
            $mobile,
            $is_upload_idcard
        );

        return $this->response->success($result);
    }

    /**
     *
     * @OA\Get (
     *      path="/user/aggregation",
     *      tags={"用户"},
     *      summary="用户个人中心聚合接口",
     *      description="用户个人中心聚合接口",
     *      operationId="UserAggregation",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="msg_unread_total",
     *                              type="integer",
     *                              description="未读消息数量"
     *                          ),
     *                          @OA\Property(
     *                              property="receipt_total",
     *                              type="integer",
     *                              description="店员待收货总订单数"
     *                          ),
     *                          @OA\Property(
     *                              property="coupon_total",
     *                              type="integer",
     *                              description="患者可使用的优惠券数量"
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return array
     */
    public function aggregation(): array
    {
        $data = UserService::getAggregationInfo();
        return $this->success($data);
    }
}
