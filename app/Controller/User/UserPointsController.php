<?php
declare(strict_types=1);

namespace App\Controller\User;

use App\Model\Pharmacy\PointGood;
use App\Request\App\Point\GetUserPointRequest;
use App\Request\Point\ExchangeGoodsRequest;
use App\Request\Point\GetGoodsListRequest;
use App\Request\Point\ListReceiptToConfirmRequest;
use App\Request\Point\UserPointsDetailRequest;
use App\Service\Point\PointService;
use Grpc\Point\ExchangeGoodsReq;
use Hyperf\Utils\Str;
use Youyao\Framework\AbstractAction;

class UserPointsController extends AbstractAction
{

    /**
     *
     * @OA\Get(
     *      path="/user/points",
     *      tags={"积分"},
     *      summary="患者或店员积分",
     *      description="患者或店员积分",
     *      operationId="GetUserPoints",
     *     @OA\Parameter(
     *         description="积分活动分组id,不传或传0时使用平台默认积分活动分组id",
     *         in="query",
     *         name="group_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="integer", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                                  property="money_point",
     *                                  type="number",
     *                                  format="float",
     *                                  description="消费积分"
     *                          ),
     *                         @OA\Property(
     *                                  property="service_point",
     *                                  type="number",
     *                                  format="float",
     *                                  description="服务积分"
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @return array
     */
    public function userPoints(GetUserPointRequest $request)
    {
        $res  = PointService::getUserAssets((int) $request->query('group_id', 0));
        return $this->success($res);
    }

    /**
     *
     * @OA\Get(
     *      path="/user/points/detail",
     *      tags={"积分"},
     *      summary="积分明细",
     *      description="积分明细",
     *      operationId="GetUserPointsDetail",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="页码",
     *         in="query",
     *         name="page",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="页码大小",
     *         in="query",
     *         name="page_size",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="积分类型: 1服务积分 2消费积分 默认1",
     *         in="query",
     *         name="point_type",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="类型: 0全部 1发放 2使用 默认0",
     *         in="query",
     *         name="type",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="积分活动分组id,不传或传0时使用平台默认积分活动分组id",
     *         in="query",
     *         name="group_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="integer", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="total", type="integer", description="总数"),
     *                          @OA\Property(property="page", type="integer", description="页码"),
     *                          @OA\Property(property="page_size", type="integer", description="页面数量"),
     *                          @OA\Property(
     *                              property="list",
     *                              type="object",
     *                              @OA\Property(property="desc", type="string", description="明细描述"),
     *                              @OA\Property(
     *                                  property="points",
     *                                  type="string",
     *                                  example="+100",
     *                                  description="积分变更数量"),
     *                              @OA\Property(
     *                                  property="status",
     *                                  type="integer",
     *                                  description="状态 1-申请中 2-已到帐 3-已兑换"),
     *                              @OA\Property(property="date", type="string", description="创建时间")
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @param UserPointsDetailRequest $request
     * @return array
     * @throws \Exception
     */
    public function userPointsDetail(UserPointsDetailRequest $request)
    {
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', 10);
        $pointType = $request->input('point_type', 1);
        $type = $request->input('type', 0);
        $res = PointService::getUserPointsDetail($page, $pageSize, $pointType, $type, (int) $request->query('group_id', 0));
        return $this->success($res);
    }


    /**
     *
     * @OA\Get(
     *      path="/user/points/goods",
     *      tags={"积分"},
     *      summary="积分商品列表",
     *      description="积分商品列表",
     *      operationId="PointGoodsList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="当前页,默认1",
     *         in="query",
     *         name="current",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="每页数量,默认10",
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="兑换积分类型: 1消费积分 2服务积分 默认1 患者目前固定是1消息积分类型",
     *         in="query",
     *         name="type",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="积分活动分组id,不传或传0时使用平台默认积分活动分组id",
     *         in="query",
     *         name="group_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="integer", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          ),
     *                          @OA\Property(
     *                              property="list",
     *                              description="商品列表",
     *                              type="array",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="商品id"),
     *                                  @OA\Property(property="name", type="string", description="商品名称"),
     *                                  @OA\Property(property="cover", type="string", description="商品封面url"),
     *                                  @OA\Property(
     *                                      property="type",
     *                                      type="integer",
     *                                      description="商品类型: 1现金券 2实物 3优惠券"),
     *                                  @OA\Property(
     *                                      property="rest",
     *                                      type="integer",
     *                                      description="商品剩余数量 -1 无限制 0 已兑完 大于0 可兑换"),
     *                                  @OA\Property(
     *                                      property="service_point",
     *                                      type="integer",
     *                                      description="兑换一份所需服务积分"),
     *                                  @OA\Property(
     *                                      property="money_point",
     *                                      type="integer",
     *                                      description="兑换一份所需消费积分")
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @return array
     * @throws \Exception
     */
    public function goodsList(GetGoodsListRequest $request): array
    {
        $pointType = (int) $request->query('type', 1) == 1 ? PointGood::POINT_TYPE_SERVICE_POINT :
                PointGood::POINT_TYPE_MONEY_POINT;

        [$page, $data] =  PointService::getGoodsList(
            $pointType,
            (int) $request->query('current', 1),
            (int) $request->query('limit', 10),
            (int) $request->query('group_id', 0)
        );
        return $this->success([
            'list' => $data,
            'page' => $page
        ]);
    }

    /**
     *
     * @OA\Get(
     *      path="/user/points/goods/{id}",
     *      tags={"积分"},
     *      summary="通用积分商品详情",
     *      description="通用积分商品详情",
     *      operationId="CommonPointGoodsDetail",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="商品id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="integer", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              @OA\Property(property="id", type="integer", description="商品id"),
     *                              @OA\Property(property="name", type="string", description="商品名称"),
     *                              @OA\Property(property="cover", type="string", description="商品封面url"),
     *                              @OA\Property(property="desc", type="string", description="商品描述"),
     *                              @OA\Property(
     *                                  property="rest",
     *                                  type="integer",
     *                                  description="商品剩余数量 -1 无限制 0 已兑完 大于0 可兑换"),
     *                              @OA\Property(
     *                                  property="service_point",
     *                                  type="integer",
     *                                  description="兑换一份所需服务积分"),
     *                              @OA\Property(
     *                                  property="money_point",
     *                                  type="integer",
     *                                  description="兑换一份所需消费积分")
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @return array
     * @throws \Exception
     */
    public function goodsDetail($id): array
    {
        $info = PointService::getGoodsDetail($id);
        return $this->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Get(
     *      path="/user/points/goods/coupon/{id}",
     *      tags={"积分"},
     *      summary="优惠券商品详情",
     *      description="优惠券商品详情",
     *      operationId="CouponPointGoodsDetail",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="优惠券商品id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="integer", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              @OA\Property(property="id", type="integer", description="商品id"),
     *                              @OA\Property(property="name", type="string", description="商品名称"),
     *                              @OA\Property(property="coupon_id", type="integer", description="优惠券id"),
     *                              @OA\Property(property="cover", type="string", description="商品封面url"),
     *                              @OA\Property(
     *                                  property="rest",
     *                                  type="integer",
     *                                  description="商品剩余数量 -1 无限制 0 已兑完 大于0 可兑换"),
     *                              @OA\Property(
     *                                  property="service_point",
     *                                  type="integer",
     *                                  description="兑换一份所需服务积分"),
     *                              @OA\Property(
     *                                  property="money_point",
     *                                  type="integer",
     *                                  description="兑换一份所需消费积分"),
     *                              @OA\Property(property="drugs", type="string", description="使用范围"),
     *                              @OA\Property(property="store", type="string", description="适用门店,返回1个"),
     *                              @OA\Property(property="store_total", type="integer", description="适用门店总数量"),
     *                              @OA\Property(
     *                                  property="expire_type",
     *                                  type="integer",
     *                                  description="截止日期类型: 1固定时间 2领券后多少天"),
     *                              @OA\Property(
     *                                  property="exipre_at",
     *                                  type="string",
     *                                  description="截止日期: expire_type=1时返回"),
     *                              @OA\Property(
     *                                  property="exipre_day",
     *                                  type="string",
     *                                  description="过期天数: expire_type=2时返回")
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @return array
     * @throws \Exception
     */
    public function couponGoodsDetail($id): array
    {
        $info = PointService::getCouponGoodsInfo((int) $id);

        return $this->success([
           'info' => $info
        ]);
    }

    /**
     *
     * @OA\Post (
     *      path="/user/points/goods/{id}/exchange",
     *      tags={"积分"},
     *      summary="优惠券商品详情",
     *      description="优惠券商品详情",
     *      operationId="ExchangePointGoods",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="商品id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="num", type="integer", description="兑换份数"),
     *                  required={"num"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="integer", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @return array
     * @throws \Exception
     */
    public function exchange($id, ExchangeGoodsRequest $request): array
    {
        PointService::exchangeGods(
            (int) $id,
            (int) $request->post('num')
        );
        return $this->success([]);
    }

    /**
     *
     * @OA\Get(
     *      path="/user/points/goods/receipts",
     *      tags={"积分"},
     *      summary="待确认收货列表",
     *      description="待确认收货列表",
     *      operationId="PointGoodsReceiptList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="当前页,默认1",
     *         in="query",
     *         name="current",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="每页数量,默认10",
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="integer", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          ),
     *                          @OA\Property(
     *                              property="list",
     *                              description="待确认收货列表",
     *                              type="array",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="兑换id"),
     *                                  @OA\Property(property="name", type="string", description="商品名称"),
     *                                  @OA\Property(property="cover", type="string", description="商品封面url"),
     *                                  @OA\Property(property="num", type="integer", description="商品兑换数量"),
     *                                  @OA\Property(property="time", type="string", description="商品兑换时间,格式是yyyy-mm-dd HH:ii:ss")
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @return array
     * @throws \Exception
     */
    public function listReceipt(ListReceiptToConfirmRequest $request)
    {
        [$page, $list] = PointService::listExchangeRealGoodsReceiptsToConfirm(
            (int) $request->query('current', 1),
            (int) $request->query('limit', 10)
        );
        return $this->success([
            'page' => $page,
            'list' => $list
        ]);
    }


    /**
     *
     * @OA\Put (
     *      path="/user/points/goods/receipts/{id:\d+}/confirm",
     *      tags={"积分"},
     *      summary="确认收货",
     *      description="确认收货",
     *      operationId="ExchangePointGoodsReceiptConfirm",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="待收货兑换id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="integer", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @return array
     * @throws \Exception
     */
    public function confirmReceipt($id)
    {
        PointService::goodsReceiptConfirmed((int) $id);
        return $this->success([]);
    }
}
