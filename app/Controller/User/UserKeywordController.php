<?php
declare(strict_types=1);

namespace App\Controller\User;

use App\Service\User\UserKeywordService;
use Youyao\Framework\AbstractAction;

class UserKeywordController extends AbstractAction
{
    private $service;
    public function __construct(UserKeywordService $service)
    {
        $this->service = $service;
    }

    /**
     *
     * @OA\Get (
     *      path="/keyword/list",
     *      tags={"关键字"},
     *      summary="关键字列表",
     *      description="关键字列表",
     *      operationId="KeywordList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="产品列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="keyword", type="string", description="关键字")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return array
     */
    public function keywordList()
    {
        $res = $this->service->getKeywordList();
        return $this->success(['list'=>$res]);
    }

    /**
     *
     * @OA\Get (
     *      path="/keyword/delete",
     *      tags={"关键字"},
     *      summary="关键字删除",
     *      description="关键字删除",
     *      operationId="KeywordDelete",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                     )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * 关键字删除
     * @return array
     */
    public function deleteKeyword()
    {
        $this->service->deleteKeyword();
        return $this->success();
    }
}
