<?php
declare(strict_types=1);
namespace App\Controller\Topic;

use App\Request\Images\BatchDeleteRequest;
use App\Request\Images\DetailImagesRequest;
use App\Request\Images\ListImagesRequest;
use App\Service\Topic\ImagesService;
use Youyao\Framework\AbstractAction;

class ImagesController extends AbstractAction
{
    /**
     *
     * @OA\Get (
     *      path="/api/topics/Images",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="个人中心图片列表",
     *      description="个人中心图片列表",
     *      operationId="TopicUserVideoList",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *         description="状态: 1草稿 2已发布 3已审核通过 4已审核拒绝",
     *         in="query",
     *         name="status",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="当前页,默认1",
     *         in="query",
     *         name="current",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="返回条数,默认10",
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="图片列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="记录id"),
     *                                  @OA\Property(property="upload_id", type="integer", description="当前图片/视频id"),
     *                                  @OA\Property(property="tp", type="integer", description="1图片 2视频"),
     *                                  @OA\Property(property="url", type="string", description="图片url地址")
     *                              )
     *                           ),
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     */
    public function list(ListImagesRequest $request): array
    {

        [$page, $data] = ImagesService::listUserImages(
            (int) $request->query('status', 0),
            (int) $request->query('current', 1),
            (int) $request->query('limit', 10)
        );

        return $this->success([
            'list' => $data,
            'page' => $page
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/api/topics/personal/{record_id:\d+}",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="记录-图片详情",
     *      description="记录-图片详情",
     *      operationId="detail",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="记录id",
     *         in="path",
     *         name="record_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="话题id",
     *         in="query",
     *         name="topic_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="来源：1个人中心2话题",
     *         in="query",
     *         name="source",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="当前列表类型【最新/最热】，当来源=2时必传 ",
     *         in="query",
     *         name="order",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="当前列表类型【1草稿 2已发布 3已审核通过 4已审核拒绝】，当来源=1时必传 ",
     *         in="query",
     *         name="state",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="划动动作（1上，2下，3首次列表进入）",
     *         in="query",
     *         name="action",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="图片详情",
     *                              @OA\Property(property="id", type="integer", description="记录详情id"),
     *                              @OA\Property(property="pre", type="integer", description="若为真则再次上划提示:无"),
     *                              @OA\Property(property="next", type="integer", description="若为真则再次下划提示:无"),
     *                              @OA\Property(property="tp", type="integer", description="1图片 2视频"),
     *                              @OA\Property(
     *                              property="url",
     *                              type="array",
     *                              description="列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="string", description="地址")
     *                              )
     *                           ),
     *                              @OA\Property(property="comment", type="string", description="图片文案"),
     *                              @OA\Property(
     *                                  property="state",
     *                                  type="integer",
     *                                  description="图片状态: 1草稿 2待审核 3审核通过 4审核拒绝"),
     *                              @OA\Property(property="like_count", type="integer", description="点赞数"),
     *                              @OA\Property(property="fab", type="integer", description="是否点赞 1点赞0为点赞"),
     *                              @OA\Property(property="share_count", type="integer", description="分享数"),
     *                              @OA\Property(property="topic_id", type="integer", description="所属话题id"),
     *                              @OA\Property(property="topic_name", type="string", description="所属话题"),
     *                              @OA\Property(property="reject_reason", type="string", description="拒绝原因")
     *                           )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     */
    public function detail($record_id, DetailImagesRequest $request): array
    {

        $action = $request->query('action', '');
        $order = $request->query('order', '');
        $source = $request->query('source', 1);
        $state = $request->query('state', '');
        $topic_id = $request->query('topic_id', '');

        $info = ImagesService::info((int) $record_id, $action, $order, $source, $state, $topic_id);

        return $this->success([
           'info' => $info
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/api/topics/activity",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="活动中心",
     *      description="活动中心",
     *      operationId="activityList",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="记录id"),
     *                                  @OA\Property(property="title", type="string", description="title"),
     *                                  @OA\Property(property="img", type="string", description="图片"),
     *                                  @OA\Property(property="quest_id", type="integer", description="问卷id"),
     *                                  @OA\Property(property="type", type="integer", description="1问卷(已删除) 2话题 3考试"),
     *                              )
     *                           )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    public function activityList(): array
    {
        $info = ImagesService::activityList();

        return $this->success(['list'=>$info[0]]);
    }



    /**
     *
     * @OA\Put  (
     *      path="/api/topics/{id}/like/img",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="点赞",
     *      description="点赞",
     *      operationId="setTopicLikeImages",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="记录id",
     *         in="path",
     *         name="scope",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="Boolean",
     *                          description="点赞结果",
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     */
    public function setTopicLikeImages($id): array
    {
        ImagesService::setTopicLikeImages((int) $id);

        return $this->success(true);
    }


    /**
     *
     * @OA\Put (
     *      path="/api/topics/{id}/share",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="记录分享数",
     *      description="记录分享数",
     *      operationId="setTopicShareNum",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="记录id",
     *         in="path",
     *         name="scope",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="Boolean",
     *                          description="分享数记录结果",
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     */
    public function setTopicShareNum($id): array
    {
        ImagesService::setTopicShareNum((int) $id);

        return $this->success(true);
    }

    /**
     *
     * @OA\Post(
     *      path="/api/topics/{id}/publish",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="发布草稿图片",
     *      description="发布草稿图片",
     *      operationId="PublishVideo",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *         description="记录id",
     *         in="path",
     *         name="scope",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     */
    public function publish($id): array
    {
        ImagesService::publishImage((int) $id);

        return $this->success([]);
    }

    /**
     *
     * @OA\Post(
     *      path="/api/topics/batch-delete",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="批量删除图片",
     *      description="批量删除图片",
     *      operationId="del",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(
     *                      property="ids",
     *                      type="array",
     *                      description="上传记录id",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  required={"ids"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     */
    public function del(BatchDeleteRequest $request): array
    {
        $videoIds = $request->post('ids');

        ImagesService::delByIds($videoIds);

        return $this->success([]);
    }
}
