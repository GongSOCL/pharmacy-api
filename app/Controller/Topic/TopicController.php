<?php
declare(strict_types=1);
namespace App\Controller\Topic;

use App\Repository\Topic\TopicRepository;
use App\Request\Topic\ListTopicImagesRequest;
use App\Request\Topic\ParticipateTopicRequest;
use App\Service\Topic\TopicService;
use Youyao\Framework\AbstractAction;

class TopicController extends AbstractAction
{
    /**
     *
     * @OA\Get (
     *      path="/api/topics",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="话题首页",
     *      description="话题首页",
     *      operationId="getTopics",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                              description="话题list",
     *                              @OA\Property(property="title", type="string", description="话题名称"),
     *                              @OA\Property(property="id", type="integer", description="话题id"),
     *                              @OA\Property(property="cover_url", type="integer", description="封面图片"),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     */
    public function getTopics(): array
    {
        $info = TopicRepository::getTopicsList();

        return $this->success($info);
    }

    /**
     *
     * @OA\Get (
     *      path="/api/topics/topic/{id}",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="话题详情",
     *      description="话题详情",
     *      operationId="TopicDetail",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="话题id",
     *         in="path",
     *         name="scope",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="话题详情",
     *                              @OA\Property(property="name", type="string", description="话题名称"),
     *                              @OA\Property(property="total", type="integer", description="话题参与人数"),
     *                              @OA\Property(property="detail_pic", type="string", description="话题详情pic")
     *                           )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     */
    public function detail($id): array
    {
        $info = TopicService::detail((int) $id);
        return $this->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/api/topics/{id}/list/img",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="话题图片列表",
     *      description="话题图片列表",
     *      operationId="getTopicImagesList",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="话题id",
     *         in="path",
     *         name="scope",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="排序类型: 1最新 2最热,默认1",
     *         in="query",
     *         name="order",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="当前页,默认1",
     *         in="query",
     *         name="current",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="返回条数,默认10",
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="话题详情",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="记录id"),
     *                                  @OA\Property(property="upload_id", type="integer", description="图片/视频id"),
     *                                  @OA\Property(property="tp", type="integer", description="1图片/2视频"),
     *                                  @OA\Property(property="url", type="string", description="图片url地址"),
     *                                  @OA\Property(
     *                                      property="is_fab",
     *                                      type="integer",
     *                                      description="是否自己发布点赞: 1是 0否"),
     *                                  @OA\Property(
     *                                      property="is_mine",
     *                                      type="integer",
     *                                      description="是否自己发布的视频: 1是 0否"),
     *                                  @OA\Property(property="total", type="integer", description="点赞数")
     *                              )
     *                           ),
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     */
    public function getTopicImagesList($id, ListTopicImagesRequest $request): array
    {

        [$page, $data] = TopicService::listTopicImages(
            (int) $id,
            $request->query('order', 1) == 1,
            (int) $request->query('current', 1),
            (int) $request->query('limit', 10)
        );
        return $this->success([
           'list' => $data,
            'page' => $page
        ]);
    }


    /**
     *
     * @OA\Post(
     *      path="/api/topics/{id}/participate",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="参与话题",
     *      description="参与话题",
     *      operationId="TopicParticipate",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *         description="话题id",
     *         in="path",
     *         name="scope",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(
     *                          property="upload_image_ids",
     *                          type="array",
     *                          description="图片id",
     *                          @OA\Items(type="integer")
     *                     ),
     *                  @OA\Property(
     *                          property="upload_video_ids",
     *                          type="array",
     *                          description="视频id",
     *                          @OA\Items(type="integer")
     *                     ),
     *                  @OA\Property(property="comment", type="string", description="文案"),
     *                  @OA\Property(property="tp", type="integer", description="1图片 2视频"),
     *                  @OA\Property(property="is_published", type="integer", description="是否发布: 0否存草稿 1是"),
     *                  required={"comment", "is_published", "tp"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="发布视频记录id")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     */
    public function participate($id, ParticipateTopicRequest $request): array
    {
        $part = TopicService::participate(
            (int) $id,
            $request->post('tp', ''),
            $request->post('upload_image_ids', []),
            $request->post('upload_video_ids', []),
            (string) $request->post('comment'),
            $request->post('is_published') == 1
        );

        return $this->success([
            'id' => $part->id
        ]);
    }
}
