<?php
declare(strict_types=1);

namespace App\Controller\Coupon;

use App\Request\Coupon\GetCouponScopeListRequest;
use App\Request\Coupon\StoreCouponListRequest;
use App\Service\Coupon\CouponService;
use Hyperf\HttpServer\Contract\RequestInterface;
use Youyao\Framework\AbstractAction;

class CouponController extends AbstractAction
{

    private $service;
    public function __construct(CouponService $service)
    {
        $this->service = $service;
    }

    /**
     *
     * @OA\Get (
     *      path="/coupon/scope-list",
     *      tags={"优惠券"},
     *      summary="优惠券适用范围列表",
     *      description="优惠券适用范围列表",
     *      operationId="CouponScopeList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="优惠券ID",
     *         in="query",
     *         name="coupon_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="当前分页",
     *         in="query",
     *         name="page",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="分页大小",
     *         in="query",
     *         name="page_size",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="药店列表",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="page", type="integer", description="当前页数"),
     *                              @OA\Property(property="page_size", type="integer", description="分页数量"),
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="药店ID"),
     *                                  @OA\Property(property="store_name", type="string", description="药店名称"),
     *                                  @OA\Property(property="store_name_alias", type="string", description="药店别名"),
     *                                  @OA\Property(property="location", type="string", description="药店地址"),
     *                                  @OA\Property(property="phone", type="string", description="手机号")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * 礼券适用范围
     * @param GetCouponScopeListRequest $request
     * @return array
     *
     */
    public function getCouponScopeList(GetCouponScopeListRequest $request)
    {
        $couponId = $request->input('coupon_id');
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', 10);
        $res = $this->service->getCouponScopeList((int)$couponId, (int)$page, (int)$pageSize);
        return $this->success($res);
    }


    /**
     *
     * @OA\Get (
     *      path="/coupon/store-coupon-list",
     *      tags={"优惠券"},
     *      summary="药店可领取优惠券列表",
     *      description="药店可领取优惠券列表",
     *      operationId="StoreCouponList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="药店ID",
     *         in="query",
     *         name="store_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="药店列表",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="page", type="integer", description="当前页数"),
     *                              @OA\Property(property="page_size", type="integer", description="分页数量"),
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="优惠券ID"),
     *                                  @OA\Property(property="title", type="string", description="优惠券描述"),
     *                                  @OA\Property(
     *                                          property="policy_type",
     *                                          type="integer",
     *                                          description="折扣类型1：满减  2：折扣"),
     *                                  @OA\Property(
     *                                          property="policy_val",
     *                                          type="string", description="优惠策略"),
     *                                  @OA\Property(
     *                                          property="deadline_type",
     *                                          type="integer",
     *                                          description="截止类型1：倒计时  2：具体日期"),
     *                                  @OA\Property(property="deadline_val", type="string", description="截止日期"),
     *                                  @OA\Property(property="yy_product_id", type="integer", description="产品ID"),
     *                                  @OA\Property(property="product_name", type="string", description="产品名称"),
     *                                  @OA\Property(
     *                                          property="is_get",
     *                                          type="integer",
     *                                          description="是否已领取标记0：未领取 1：已领取")
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="store",
     *                              type="object",
     *                              description="药店信息",
     *                              @OA\Property(property="id", type="integer", description="药店ID"),
     *                              @OA\Property(property="store_name", type="string", description="药店名称"),
     *                              @OA\Property(property="store_name_alias", type="string", description="药店别名"),
     *                              @OA\Property(property="location", type="string", description="药店地址"),
     *                              @OA\Property(property="phone", type="string", description="手机号")
     *                          ),
     *                          @OA\Property(
     *                                          property="is_get",
     *                                          type="integer",
     *                                          description="优惠券列表是否都已领取标记0：未领取 1：已领取")
     *
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param StoreCouponListRequest $request
     * @return array
     */
    public function storeCouponList(StoreCouponListRequest $request)
    {
        $storeId = $request->input('store_id');
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', 10);
        $res = $this->service->storeCouponList($storeId);
        return $this->success($res);
    }
}
