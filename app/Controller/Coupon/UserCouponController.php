<?php
declare(strict_types=1);

namespace App\Controller\Coupon;

use App\Service\Coupon\UserCouponService;
use Hyperf\HttpServer\Contract\RequestInterface;
use Youyao\Framework\AbstractAction;

/**
 * 患者用户礼券接口
 * Class UserCouponController
 * @package App\Controller\User
 */
class UserCouponController extends AbstractAction
{
    private $service;
    public function __construct(UserCouponService $service)
    {
        $this->service = $service;
    }

    /**
     *
     * @OA\Get (
     *      path="/user/coupon/my-list",
     *      tags={"优惠券"},
     *      summary="患者优惠券列表",
     *      description="患者优惠券列表",
     *      operationId="MyUserCouponList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="优惠券标签0：未使用 1：已使用 2：已过期",
     *         in="query",
     *         name="type",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="当前分页",
     *         in="query",
     *         name="page",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="分页大小",
     *         in="query",
     *         name="page_size",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="药店列表",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="page", type="integer", description="当前页数"),
     *                              @OA\Property(property="page_size", type="integer", description="分页数量"),
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="优惠券领取ID"),
     *                                  @OA\Property(property="coupon_id", type="integer", description="优惠券ID"),
     *                                  @OA\Property(property="coupon_num", type="string", description="优惠券编码"),
     *                                  @OA\Property(
     *                                      property="is_use",
     *                                      type="integer",
     *                                      description="是否使用0：未使用 1：已使用"),
     *                                  @OA\Property(
     *                                      property="scope",
     *                                      type="integer",
     *                                      description="适用范围0：所有  1：连锁店   2：药店"),
     *                                  @OA\Property(
     *                                      property="product_name",
     *                                      type="string",
     *                                      description="产品名称"),
     *                                  @OA\Property(
     *                                      property="policy_type",
     *                                      type="string",
     *                                      description="折扣类型1：满减  2：折扣"),
     *                                  @OA\Property(property="policy_val", type="string", description="优惠策略"),
     *                                  @OA\Property(property="end_time", type="string", description="截止日期")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * 我的礼券
     * @param RequestInterface $request
     * @return array
     */
    public function myCouponList(RequestInterface $request)
    {
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', 10);
        $type = $request->input('type', 1);
        $res = $this->service->getUserCouponList((int)$type, (int)$page, (int)$pageSize);
        return $this->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/user/coupon/use-list",
     *      tags={"优惠券"},
     *      summary="患者订单可使用的优惠券列表",
     *      description="患者订单可使用的优惠券列表",
     *      operationId="StoreStoreList",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="待接单ID",
     *         in="query",
     *         name="notify_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="当前分页",
     *         in="query",
     *         name="page",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="分页大小",
     *         in="query",
     *         name="page_size",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="药店列表",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="page", type="integer", description="当前页数"),
     *                              @OA\Property(property="page_size", type="integer", description="分页数量"),
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="优惠券领取ID"),
     *                                  @OA\Property(property="coupon_id", type="integer", description="优惠券ID"),
     *                                  @OA\Property(property="coupon_num", type="string", description="优惠券编码"),
     *                                  @OA\Property(
     *                                      property="is_use",
     *                                      type="integer",
     *                                       description="是否使用0：未使用 1：已使用"),
     *                                  @OA\Property(
     *                                      property="scope",
     *                                      type="integer",
     *                                      description="适用范围0：所有  1：连锁店   2：药店"),
     *                                  @OA\Property(
     *                                      property="product_name",
     *                                      type="string",
     *                                      description="产品名称"),
     *                                  @OA\Property(
     *                                      property="policy_type",
     *                                      type="string",
     *                                      description="折扣类型1：满减  2：折扣"),
     *                                  @OA\Property(property="policy_val", type="string", description="优惠策略"),
     *                                  @OA\Property(property="end_time", type="string", description="截止日期")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param RequestInterface $request
     * @return array
     */
    public function useCouponList(RequestInterface $request)
    {
        $notifyId = $request->input('notify_id');
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', 10);
        $res = $this->service->getUserUseCouponList((int)$notifyId, (int)$page, (int)$pageSize);
        return $this->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/user/coupon/user-count",
     *      tags={"优惠券"},
     *      summary="患者我的可使用的优惠券数量",
     *      description="患者我的可使用的优惠券数量",
     *      operationId="MyUserCouponCount",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="num", type="integer", description="数量"),
     *                     )
     *                 )
     *           )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param RequestInterface $request
     * @return array
     */
    public function getUserCouponNum(RequestInterface $request)
    {
        $res = $this->service->getUserCouponNum();
        return $this->success($res);
    }
    /**
     *
     * @OA\Get (
     *      path="/user/coupon/use-count",
     *      tags={"优惠券"},
     *      summary="患者药店可使用的优惠券数量",
     *      description="患者药店可使用的优惠券数量",
     *      operationId="CanUseCouponCount",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="待接单ID",
     *         in="query",
     *         name="notify_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="num", type="integer", description="数量"),
     *                     )
     *                 )
     *           )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param RequestInterface $request
     * @return array
     */
    public function getCanUseCouponNum(RequestInterface $request)
    {
        $notifyId = $request->input('notify_id');
        $res = $this->service->getCanUserCouponNum((int)$notifyId);
        return $this->success($res);
    }


    /**
     *
     * @OA\Post(
     *      path="/user/coupon/get-coupon",
     *      tags={"优惠券"},
     *      summary="优惠券免费领取",
     *      description="优惠券免费领取",
     *      operationId="UserGetCoupon",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(
     *                      property="coupon_ids",
     *                      type="array",
     *                      description="礼券Id的数组",
     *                      @OA\Items(type="integer")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * 领取礼券
     * @param RequestInterface $request
     * @return array
     * @throws \Exception
     */
    public function getCoupon(RequestInterface $request)
    {
        $couponIds = $request->input('coupon_ids');
        $this->service->gainCoupon($couponIds);
        return $this->success();
    }

    /**
     * 兑换礼券
     * @param RequestInterface $request
     * @return array
     * @throws \Throwable
     */
    public function exchangeCoupon(RequestInterface $request)
    {
        $goodsId = $request->input('goods_id');
        $res = $this->service->exchangeCoupon($goodsId);
        return $this->success();
    }
}
