<?php
declare(strict_types=1);
namespace App\Controller\Common;

use App\Service\Common\ProductService;
use Youyao\Framework\AbstractAction;

class ProductController extends AbstractAction
{
    /**
     *
     * @OA\Get (
     *      path="/products",
     *      tags={"产品"},
     *      summary="获取产品列表",
     *      description="获取产品列表",
     *      operationId="GetProducts",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="产品列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="产品id"),
     *                                  @OA\Property(property="name", type="string", description="产品名称")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return array
     */
    public function list(): array
    {
        $list = ProductService::getProducts();
        return $this->success([
            'list' => $list
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/products/{id}/skus",
     *      tags={"产品"},
     *      summary="获取产品规格列表",
     *      description="获取产品规则列表",
     *      operationId="GetProductSkus",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="产品id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="产品规格列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="产品规格id"),
     *                                  @OA\Property(property="spec", type="string", description="产品规格")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return array
     */
    public function listSku($id): array
    {
        $list = ProductService::listProductSkus((int) $id);
        return $this->success([
            'list' => $list
        ]);
    }
}
