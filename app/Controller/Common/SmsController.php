<?php
declare(strict_types=1);

namespace App\Controller\Common;

use App\Request\Sms\SendCodeRequest;
use App\Request\Sms\VerifyCodeRequest;
use App\Service\Common\SmsService;
use Hyperf\HttpServer\Contract\RequestInterface;
use Youyao\Framework\AbstractAction;

class SmsController extends AbstractAction
{

    private $smsService;
    public function __construct(SmsService $service)
    {
        $this->smsService = $service;
    }

    /**
     *
     * @OA\Post (
     *      path="/send-code",
     *      tags={"短信"},
     *      summary="发送验证码",
     *      description="发送验证码",
     *      operationId="SendCode",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="手机号",
     *         in="query",
     *         name="phone",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="verify_code", type="string", description="验证码")
     *                     )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param SendCodeRequest $request
     * @return array
     * @throws \Exception
     */
    public function sendCode(SendCodeRequest $request)
    {
        $phone = $request->input('phone');
        $res = $this->smsService->sendCode($phone);
        return $this->success($res);
    }

    /**
     *
     * @OA\Post (
     *      path="/verify-code",
     *      tags={"短信"},
     *      summary="验证手机验证码",
     *      description="验证手机验证码",
     *      operationId="VerifyCode",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="手机号",
     *         in="query",
     *         name="phone",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Parameter(
     *         description="验证码",
     *         in="query",
     *         name="verify_code",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="verify", type="integer", description="发送成功标记")
     *                     )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param VerifyCodeRequest $request
     * @return array
     */
    public function verifyCode(VerifyCodeRequest $request)
    {
        $phone = $request->input('phone');
        $verifyCode = $request->input('verify_code');
        $res = $this->smsService->verify($phone, $verifyCode);
        return $this->success($res);
    }
}
