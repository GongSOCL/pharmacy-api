<?php
declare(strict_types=1);
namespace App\Controller\Common;

use App\Request\Upload\RecordUploadRequests;
use App\Request\Upload\UploadTokenRequests;
use App\Service\Common\UploadService;
use Grpc\Upload\UPLOAD_SCOPE;
use Youyao\Framework\AbstractAction;

class UploadController extends AbstractAction
{
    /**
     *
     * @OA\Get (
     *      path="/api/upload/token",
     *      tags={"公用"},
     *      summary="获取上传token",
     *      description="获取上传凭证",
     *      operationId="AppUploadToken",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="bucket类型 1: static bucket 2 user bucket",
     *         in="query",
     *         name="type",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Parameter(
     *         description="务类型描述，单个英文词",
     *         in="query",
     *         name="business",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Parameter(
     *         description="上传通道: 1七牛 3oss",
     *         in="query",
     *         name="channel",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                           @OA\Property(
     *                             property="config",
     *                             type="object",
     *                             description="上传配置",
     *                              @OA\Property(
     *                                      property="path",
     *                                      type="string",
     *                                      description="上传路径前缀,需要拼接文件名作为上传key"),
     *                              @OA\Property(
     *                                      property="token",
     *                                      type="token",
     *                                      description="七牛通道对应上传token, oss通道对应经-oss-security-token"),
     *                              @OA\Property(
     *                                      property="expire_time",
     *                                      type="string",
     *                                      description="token过期时间,默认一时间过期"),
     *                              @OA\Property(
     *                                      property="bucket",
     *                                      type="string",
     *                                      description="上传bucket名"),
     *                              @OA\Property(
     *                                      property="base_uri",
     *                                      type="string",
     *                                      description="cdn地址,拼接上传key为文件的访问url"),
     *                              @OA\Property(
     *                                      property="upload_uri",
     *                                      type="string",
     *                                      description="上传文件地址,sdk上传地址"),
     *                              @OA\Property(
     *                                      property="key",
     *                                      type="string",
     *                                      description="上传key, 仅oss-OSSAccessKeyId"),
     *                              @OA\Property(
     *                                      property="signature",
     *                                      type="string",
     *                                       description="上传签名, oss-Signature"),
     *                              @OA\Property(property="policy", type="string", description="上传策略, oss-policy"),
     *                              @OA\Property(property="channel", type="integer", description="上传通道id: 1七牛 3oss")
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param UploadTokenRequests $requests
     */
    public function token(UploadTokenRequests $requests): array
    {
        $config = UploadService::getConfig(
            (int) $requests->query('type'),
            UPLOAD_SCOPE::SCOPE_PHARMACY,
            $requests->query('business'),
            (int) $requests->query('channel', 1)
        );

        //获取七牛上传token
        return $this->success([
            'config' => $config
        ]);
    }

    /**
     *
     * @OA\Post (
     *      path="/api/upload",
     *      tags={"公用"},
     *      summary="记录上传七牛文件",
     *      description="记录上传七牛文件",
     *      operationId="AppUploadAdd",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="channel", type="integer", description="上传通道id: 1七牛 3oss"),
     *                  @OA\Property(property="bucket", type="string", description="bucket名称,config返回的bucket字段"),
     *                  @OA\Property(property="key", type="string",description="上传七牛后返回key"),
     *                  @OA\Property(property="name", type="string", description="上传文件名称"),
     *                  @OA\Property(property="url", type="string", description="上传后文件的访问全路径url"),
     *                  required={"bucket", "key", "url", "name"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                           @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="文件信息",
     *                               @OA\Property(property="id", type="integer", description="文件id"),
     *                               @OA\Property(property="name", type="string", description="文件名"),
     *                               @OA\Property(property="url", type="string", description="文件url"),
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    public function record(RecordUploadRequests $requests): array
    {
        $res = UploadService::addUpload(
            $requests->post('bucket'),
            $requests->post('key'),
            $requests->post('name'),
            $requests->post('url'),
            (int) $requests->post('channel', 1)
        );

        return $this->success([
            'info' => $res
        ]);
    }
}
