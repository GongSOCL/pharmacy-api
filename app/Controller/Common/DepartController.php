<?php
declare(strict_types=1);

namespace App\Controller\Common;

use App\Service\Depart\DepartService;
use Youyao\Framework\AbstractAction;

class DepartController extends AbstractAction
{
    private $service;
    public function __construct(DepartService $departService)
    {
        $this->service = $departService;
    }

    /**
     *
     * @OA\Get (
     *      path="/departs",
     *      tags={"科室"},
     *      summary="获取科室列表",
     *      description="获取科室列表",
     *      operationId="GetDeparts",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="产品列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="科室id"),
     *                                  @OA\Property(property="name", type="string", description="科室名称")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return mixed
     */
    public function departList()
    {
        $res = $this->service->departList();
        return $this->success(['list'=>$res]);
    }
}
