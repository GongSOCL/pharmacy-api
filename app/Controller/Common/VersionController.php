<?php
declare(strict_types=1);
namespace App\Controller\Common;

use App\Request\Common\VersionRequest;
use App\Request\Upload\UploadTokenRequests;
use App\Service\Common\VersionService;
use Youyao\Framework\AbstractAction;

class VersionController extends AbstractAction
{
    /**
     *
     * @OA\Get (
     *      path="/api/version",
     *      tags={"版本"},
     *      summary="获取版本信息",
     *      description="获取版本信息",
     *      operationId="AppVersionInfo",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="版本编号",
     *         in="query",
     *         name="version",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="is_checking", type="string", description="1-审核中 2-已通过")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param UploadTokenRequests $requests
     */
    public function info(VersionRequest $request)
    {
        $version = VersionService::getVersionInfo($request->query('version'));

        return $this->success([
            'is_checking' => $version->is_checking
        ]);
    }
}
