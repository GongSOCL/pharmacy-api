<?php
declare(strict_types=1);
namespace App\Controller\Common;

use App\Request\Point\StoreMoneyPointRankRequest;
use App\Request\Point\StoreServicePointRankRequest;
use App\Request\Point\UserMoneyPointRankRequest;
use App\Request\Point\UserServicePointRankRequest;
use App\Service\Point\PointService;
use App\Service\User\UserClerkService;
use App\Service\User\UserService;
use Youyao\Framework\AbstractAction;

class PointController extends AbstractAction
{

    /**
     *
     * @OA\Get(
     *      path="/points/rank-top/service/user",
     *      tags={"积分"},
     *      summary="服务积分人员排行",
     *      description="服务积分人员排行",
     *      operationId="ServicePointUserRankTop",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="排序类型: 1日 2月 3季 4 年",
     *         in="query",
     *         name="type",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="top n 返回数量, 默认10",
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="integer", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              description="人员积分列表",
     *                              type="array",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="店员id"),
     *                                  @OA\Property(property="name", type="string", description="店员名称"),
     *                                  @OA\Property(property="store", type="string", description="药店名称"),
     *                                  @OA\Property(property="point", type="integer", description="积分数"),
     *                                  @OA\Property(property="cover", type="string", description="头像url")
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="mine",
     *                              type="object",
     *                              description="当前用户排行信息",
     *                              @OA\Property(property="id", type="integer", description="店员id"),
     *                              @OA\Property(property="name", type="string", description="店员名称"),
     *                              @OA\Property(property="store", type="string", description="药店名称"),
     *                              @OA\Property(property="point", type="integer", description="积分数"),
     *                              @OA\Property(
     *                                  property="rank",
     *                                  type="integer",
     *                                  description="当前用户排名:如果为0表示当前用户没有积分,没进排行")
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @return array
     * @throws \Exception
     */
    public function servicePointRankUserTop(UserServicePointRankRequest $request): array
    {
        [$list, $mine] =  UserClerkService::getUserServicePointRank(
            (int) $request->query('type'),
            (int) $request->query('limit', 10)
        );
        return $this->success([
            'list' => $list,
            'mine' => $mine ? $mine : (new \stdClass())
        ]);
    }

    /**
     *
     * @OA\Get(
     *      path="/points/rank-top/service/store",
     *      tags={"积分"},
     *      summary="服务积分分店排行",
     *      description="服务积分分店排行",
     *      operationId="ServicePointStoreRankTop",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="排序类型: 1日 2月 3季 4 年",
     *         in="query",
     *         name="type",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="top n 返回数量, 默认10",
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="integer", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              description="药店积分列表",
     *                              type="array",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="药店id"),
     *                                  @OA\Property(property="name", type="string", description="药店名称"),
     *                                  @OA\Property(property="point", type="integer", description="积分数"),
     *                                  @OA\Property(property="city", type="string", description="城市名称")
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="mine",
     *                              type="object",
     *                              description="当前用户所在药店排行信息,可能为空",
     *                              @OA\Property(property="id", type="integer", description="店员id"),
     *                              @OA\Property(property="name", type="string", description="店员名称"),
     *                              @OA\Property(property="point", type="integer", description="积分数"),
     *                              @OA\Property(
     *                                  property="rank",
     *                                  type="integer",
     *                                  description="当前药店排名:如果为0表示当前药店没有积分信息,没进排行"),
     *                              @OA\Property(property="city", type="string", description="城市名称")
     *                          ),
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @return array
     * @throws \Exception
     */
    public function servicePointRankStoreTop(StoreServicePointRankRequest $request): array
    {
        [$list, $mine] = UserClerkService::getStoreServicePointRank(
            (int) $request->query('type'),
            (int) $request->query('limit', 10)
        );
        return $this->success([
            'list' => $list,
            'mine' => $mine
        ]);
    }

    /**
     *
     * @OA\Get(
     *      path="/points/rank-top/order/user",
     *      tags={"积分"},
     *      summary="用户订单消费积分排行",
     *      description="用户订单消费积分排行",
     *      operationId="UserOrderMoneyPointRankTop",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="排序类型: 1日 2月 3季 4 年",
     *         in="query",
     *         name="type",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="top n 返回数量, 默认10",
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="药品id",
     *         in="query",
     *         name="product_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="integer", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              description="人员积分列表",
     *                              type="array",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="店员id"),
     *                                  @OA\Property(property="name", type="string", description="店员名称"),
     *                                  @OA\Property(property="store", type="string", description="药店名称"),
     *                                  @OA\Property(property="point", type="integer", description="积分数"),
     *                                  @OA\Property(property="cover", type="string", description="头像url")
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="mine",
     *                              type="object",
     *                              description="当前用户排行信息,可能为空，用户没有积分排行信息",
     *                              @OA\Property(property="id", type="integer", description="店员id"),
     *                              @OA\Property(property="name", type="string", description="店员名称"),
     *                              @OA\Property(property="store", type="string", description="药店名称"),
     *                              @OA\Property(property="point", type="integer", description="积分数"),
     *                              @OA\Property(
     *                                      property="rank",
     *                                      type="integer",
     *                                      description="当前用户排名:如果为0表示当前用户没有积分,没进排行")
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @return array
     * @throws \Exception
     */
    public function orderMoneyPointRankUserTop(UserMoneyPointRankRequest $request): array
    {
        [$list, $mine] = UserClerkService::getUserMoneyPointRank(
            (int) $request->query('type'),
            (int) $request->query('product_id'),
            (int) $request->query('limit', 10)
        );
        return $this->success([
            'list' => $list,
            'mine' => $mine ? $mine : (new \stdClass())
        ]);
    }

    /**
     *
     * @OA\Get(
     *      path="/points/rank-top/order/store",
     *      tags={"积分"},
     *      summary="店铺订单消费积分排行",
     *      description="店铺订单消费积分排行",
     *      operationId="StoreOrderMoneyPointRankTop",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="排序类型: 1日 2月 3季 4 年",
     *         in="query",
     *         name="type",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="top n 返回数量, 默认10",
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="药品id",
     *         in="query",
     *         name="product_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="integer", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              description="店铺积分列表",
     *                              type="array",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="药店id"),
     *                                  @OA\Property(property="name", type="string", description="药店名称"),
     *                                  @OA\Property(property="point", type="integer", description="积分数"),
     *                                  @OA\Property(property="city", type="string", description="城市名称")
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="mine",
     *                              type="object",
     *                              description="当前用户所属店铺排行信息,可能为空，店铺没有积分排行信息",
     *                              @OA\Property(property="id", type="integer", description="药店id"),
     *                              @OA\Property(property="name", type="string", description="药店名称"),
     *                              @OA\Property(property="point", type="integer", description="积分数"),
     *                              @OA\Property(property="rank", type="integer", description="排名数"),
     *                              @OA\Property(property="city", type="string", description="城市名称")
     *                          ),
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @return array
     * @throws \Exception
     */
    public function orderMoneyPointRankStoreTop(StoreMoneyPointRankRequest $request): array
    {
        [$list, $mine] = UserClerkService::getStoreMoneyPointRank(
            (int) $request->query('type'),
            (int) $request->query('product_id'),
            (int) $request->query('limit', 10)
        );
        return $this->success([
            'list' => $list,
            'mine' => $mine ? $mine : new \stdClass()
        ]);
    }

    /**
     *
     * @OA\Get(
     *      path="/points/rank-top/service/city",
     *      tags={"积分"},
     *      summary="服务积分区域排行",
     *      description="服务积分区域排行",
     *      operationId="ServicePointCityRankTop",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="排序类型: 1日 2月 3季 4 年",
     *         in="query",
     *         name="type",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="top n 返回数量, 默认10",
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="integer", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              description="区域积分列表",
     *                              type="array",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="城市id"),
     *                                  @OA\Property(property="name", type="string", description="城市名称"),
     *                                  @OA\Property(property="point", type="integer", description="积分数")
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="mine",
     *                              type="object",
     *                              description="当前用户排行信息",
     *                              @OA\Property(property="id", type="integer", description="城市id"),
     *                              @OA\Property(property="name", type="string", description="城市名称"),
     *                              @OA\Property(property="point", type="integer", description="城市积分数"),
     *                              @OA\Property(
     *                                  property="rank",
     *                                  type="integer",
     *                                  description="当前用户所属城市排名:如果为0表示当前城市没有积分,没进排行")
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @return array
     * @throws \Exception
     */
    public function servicePointRankCityTop(UserServicePointRankRequest $request): array
    {
        [$list, $mine] = UserClerkService::getCityServicePointRank(
            (int) $request->query('type'),
            (int) $request->query('limit', 10)
        );
        return $this->success([
            'list' => $list,
            'mine' => $mine ? $mine : (new \stdClass())
        ]);
    }

    /**
     *
     * @OA\Get(
     *      path="/points/rank-top/order/city",
     *      tags={"积分"},
     *      summary="城市订单消费积分排行",
     *      description="城市订单消费积分排行",
     *      operationId="CityOrderMoneyPointRankTop",
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="排序类型: 1日 2月 3季 4 年",
     *         in="query",
     *         name="type",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="top n 返回数量, 默认10",
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="药品id",
     *         in="query",
     *         name="product_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="integer", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              description="区域积分列表",
     *                              type="array",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="城市id"),
     *                                  @OA\Property(property="name", type="string", description="城市名称"),
     *                                  @OA\Property(property="point", type="integer", description="积分数")
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="mine",
     *                              type="object",
     *                              description="当前用户所属店铺所在排行信息,可能为空，店铺没有积分排行信息",
     *                              @OA\Property(property="id", type="integer", description="城市id"),
     *                              @OA\Property(property="name", type="string", description="城市名称"),
     *                              @OA\Property(property="point", type="integer", description="积分数"),
     *                              @OA\Property(property="rank", type="integer", description="排名数")
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @return array
     * @throws \Exception
     */
    public function orderMoneyPointRankCityTop(StoreMoneyPointRankRequest $request): array
    {
        [$list, $mine] = UserClerkService::getCityMoneyPointRankInfo(
            (int) $request->query('type'),
            (int) $request->query('product_id'),
            (int) $request->query('limit', 10)
        );
        return $this->success([
            'list' => $list,
            'mine' => $mine ? $mine : new \stdClass()
        ]);
    }
}
