<?php
declare(strict_types=1);

namespace App\Controller\Questionnaire;

use App\Request\Questionnaire\ActivityQuestionnaireRequest;
use App\Request\Questionnaire\AnswerQuestionnaireQuestionRequest;
use App\Request\Questionnaire\GetQuestionnaireQuestionRequest;
use App\Request\Questionnaire\StartQuestionnaireRequest;
use App\Request\Questionnaire\SubmitQuestionnaireRequest;
use App\Service\Questionnaire\QuestionnaireService;
use Youyao\Framework\AbstractAction;

class QuestionnaireController extends AbstractAction
{
    /**
     *
     * @OA\Post(
     *      path="/api/questionnaire/activity-question",
     *      tags={"问卷"},
     *      summary="活动问卷-培训中心",
     *      description="活动问卷",
     *      operationId="questionnaireStart",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="page",
     *          in="path",
     *          description="页码",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="quest_id",
     *                              type="object",
     *                          @OA\Property(property="quest_id", type="integer", description="问卷ID"),
     *                          @OA\Property(property="resource_id", type="integer", description="资源ID"),
     *                          @OA\Property(property="type", type="integer", description="1资源 2问卷"),
     *                          @OA\Property(property="title", type="string", description="标题"),
     *                          @OA\Property(property="img", type="string", description="图片")
     *                       ),
     *                      @OA\Property(property="total", type="string", description="总数"),
     *                      @OA\Property(property="lastPage", type="string", description="最后页码"),
     *                      @OA\Property(property="page", type="string", description="当前页码"),
     *                      @OA\Property(property="size", type="string", description="每页数量"),
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     */
    public function activityQuestionnaire(ActivityQuestionnaireRequest $request): array
    {
        $data = QuestionnaireService::activityQuestionnaire((int)$request->query('page'), $pageSize = 10);

        return $this->success($data);
    }

    /**
     *
     * @OA\Post(
     *      path="/api/questionnaire/start",
     *      tags={"问卷"},
     *      summary="开始问卷",
     *      description="问卷",
     *      operationId="questionnaireStart",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="paper_id", type="integer", description="问卷id"),
     *                  required={"paper_id"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="total", type="integer", description="试卷题目总数"),
     *                          @OA\Property(
     *                              property="next_question_id",
     *                              type="integer", description="下一题目id,没有下一题目时返回0"),
     *                          @OA\Property(
     *                              property="request_id",
     *                              type="string", description="请求id,后续get和answer需要带上"),
     *                          @OA\Property(property="intro", type="string", description="问卷简介"),
     *                          @OA\Property(property="start_bg", type="string", description="开始页背景图url"),
     *                          @OA\Property(property="backend_bg", type="string", description="结束页背景图url"),
     *                          @OA\Property(property="answer_bg", type="string", description="答题页面背景图url")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     */
    public function start(StartQuestionnaireRequest $request): array
    {
        $data = QuestionnaireService::start(
            (int)$request->post('paper_id')
        );
        return $this->success($data);
    }

    /**
     *
     * @OA\Get(
     *      path="/api/questionnaire/question/{id}",
     *      tags={"问卷"},
     *      summary="获取问卷试题",
     *      description="问卷",
     *      operationId="questionnaireGetQuestion",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="request_id",
     *          in="query",
     *          description="请求id",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="paper_id",
     *          in="query",
     *          description="问卷标识",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="试题id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="name", type="string", description="试题名称"),
     *                          @OA\Property(
     *                              property="type",
     *                              type="integer",
     *                              description="题目类型 1表示单选题 2表示多选题  3表示填写题"),
     *                          @OA\Property(
     *                              property="options",
     *                              type="array",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="选项id"),
     *                                  @OA\Property(property="name", type="string", description="选项名称"),
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="next_question_id",
     *                              type="integer",
     *                              description="下一题目id,没有下一题时返回0"),
     *                          @OA\Property(
     *                              property="is_skip",
     *                              type="integer",
     *                              description="是否允许跳过不答: 0不允许 1允许")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     */
    public function get($id, GetQuestionnaireQuestionRequest $request): array
    {
        $data = QuestionnaireService::getQuestionnaireQuestion(
            (int)$request->query('paper_id'),
            (string)$request->query('request_id'),
            (int)$id
        );
        return $this->success($data);
    }

    /**
     *
     * @OA\Post(
     *      path="/api/questionnaire/answer/{id}",
     *      tags={"问卷"},
     *      summary="回答问题",
     *      description="问答问卷",
     *      operationId="questionnaireAnswerQuestion",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="试题id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="paper_id", type="integer", description="问卷id"),
     *                  @OA\Property(
     *                      property="options",
     *                      type="array",
     *                      description="选择的选项id",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="content", type="string", description="如果题目类型是填空题是填写内容,否则为空"),
     *                  @OA\Property(property="request_id", type="string", description="请求id"),
     *                  required={"paper_id", "request_id"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="need_check",
     *                              type="integer",
     *                              description="是否需要校验对错: 1是 0否"),
     *                          @OA\Property(property="is_right", type="integer", description="回答是否正确: 1是 0否"),
     *                          @OA\Property(
     *                               property="right_options",
     *                              type="array",
     *                              description="正确选择的yyid",
     *                              @OA\Items(type="string")
     *                          ),
     *                          @OA\Property(property="explain", type="string", description="答案解析"),
     *                          @OA\Property(
     *                              property="next_question_id",
     *                              type="integer",
     *                              description="下一题目id,没有下一题目返回0"),
     *                          @OA\Property(
     *                              property="with_resource",
     *                              type="integer",
     *                              description="是否关联资源: 0否 1是"),
     *                          @OA\Property(property="resource_id", type="integer", description="资源id")
     *                      ),
     *
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    public function answer($id, AnswerQuestionnaireQuestionRequest $request): array
    {
        $data = QuestionnaireService::answerQuestion(
            (int)$request->post('paper_id'),
            (string)$request->post('request_id'),
            (int)$id,
            $request->post('options', []),
            (string)$request->post('content')
        );
        return $this->success($data);
    }

    /**
     *
     * @OA\Put(
     *      path="/api/questionnaire/submit",
     *      tags={"问卷"},
     *      summary="提交问卷",
     *      description="提交问卷",
     *      operationId="questionnaireSubmit",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="paper_id", type="integer", description="问卷id"),
     *                  @OA\Property(property="request_id", type="string", description="请求id"),
     *                  @OA\Property(
     *                      property="task_token",
     *                      type="string",
     *                      description="积分任务token,如果是任务跳转过来需要带上"),
     *                  required={"paper_id", "request_id"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    public function submit(SubmitQuestionnaireRequest $request): array
    {
        QuestionnaireService::submit(
            (int)$request->post('paper_id'),
            (string)$request->post('request_id'),
            (string) $request->post('task_token', '')
        );

        return $this->success([]);
    }
}
