<?php
declare(strict_types=1);
namespace App\Controller\Auth;

use App\Constants\Role;
use App\Request\Agent\GetAgentDoctorQrcodeRequest;
use App\Request\Auth\GetRoleRequest;
use App\Request\Auth\LoginRequest;
use App\Service\Auth\AuthService;
use Hyperf\HttpServer\Contract\RequestInterface;
use Youyao\Framework\AbstractAction;

class AuthController extends AbstractAction
{
    private $authService = null;
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }


    /**
     *
     * @OA\Post (
     *      path="/auth/login",
     *      tags={"登录"},
     *      summary="获取token",
     *      description="获取token",
     *      operationId="AuthLogin",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(
     *         description="微信code",
     *         in="query",
     *         name="code",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="token", type="string", description="token")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param LoginRequest $request
     * @return array
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function login(LoginRequest $request)
    {
        $code = $request->input('code');
        $res = $this->authService->login($code);
        return $this->success($res);
    }

    /**
     *
     * @OA\Post (
     *      path="/auth/get-role",
     *      tags={"登录"},
     *      summary="获取用户当前角色",
     *      description="获取用户当前角色",
     *      operationId="AuthGetRole",
     *      @OA\Parameter(
     *         description="微信code",
     *         in="query",
     *         name="code",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="role", type="string", description="role:0未注册 1:当前角色患者 2：当前角色店员")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param GetRoleRequest $request
     * @return array
     */
    public function getRole(GetRoleRequest $request)
    {
        $code = $request->input('code');
        $res = $this->authService->getRole($code);
        return $this->success($res);
    }
}
