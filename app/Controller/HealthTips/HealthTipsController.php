<?php
declare(strict_types=1);
namespace App\Controller\HealthTips;

use App\Request\App\HealthTips\ListQuestionAnswerRequest;
use App\Service\HealthTips\HealthTipsService;
use Youyao\Framework\AbstractAction;

class HealthTipsController extends AbstractAction
{

    /**
     *
     * @OA\Get (
     *      path="/tips/res-mold",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="首页头部分类",
     *      description="首页头部分类",
     *      operationId="getResourcesMold",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          description="列表",
     *                          @OA\Property(property="id",     type="integer", description="id"),
     *                          @OA\Property(property="name",  type="string", description="文件名称"),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    public function getResourcesMold()
    {
        $data = HealthTipsService::getResourcesMold();

        return $this->success($data);
    }


    /**
     *
     * @OA\Get (
     *      path="/tips/health-tips/{MoldId:\d+}",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="健康小知识",
     *      description="健康小知识",
     *      operationId="getHealthTips",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="MoldId",
     *          in="path",
     *          description="分类id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          description="列表",
     *                          @OA\Property(property="id",     type="integer", description="id"),
     *                          @OA\Property(property="title",  type="string", description="标题"),
     *                          @OA\Property(property="img",  type="string", description="图片"),
     *                          @OA\Property(property="resource_id",  type="integer", description="资源ID"),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    /**
     * 小知识
     * @return array
     */
    public function getHealthTips($MoldId)
    {
        $data = HealthTipsService::getHealthTips($MoldId);

        return $this->success($data);
    }


    /**
     *
     * @OA\Get (
     *      path="/tips/indication/{MoldId:\d+}",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="适应症",
     *      description="适应症",
     *      operationId="getIndication",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="MoldId",
     *          in="path",
     *          description="分类id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          description="列表",
     *                          @OA\Property(property="id",     type="integer", description="id"),
     *                          @OA\Property(property="title",  type="string", description="标题"),
     *                          @OA\Property(property="img",  type="string", description="图片"),
     *                          @OA\Property(property="resource_id",  type="integer", description="资源ID"),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    /**
     * 适应症
     * @return array
     */
    public function getIndication($MoldId)
    {
        $data = HealthTipsService::getIndication($MoldId);

        return $this->success($data);
    }

    /**
     *
     * @OA\Get (
     *      path="/tips/res/{ResId:\d+}",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="资源",
     *      description="资源",
     *      operationId="getResourceByID",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="ResId",
     *          in="path",
     *          description="资源id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          description="列表",
     *                          @OA\Property(property="id",     type="integer", description="id"),
     *                          @OA\Property(property="title",  type="string", description="标题"),
     *                          @OA\Property(property="type",  type="integer", description="1-图文 2-视频 9-pdf"),
     *                          @OA\Property(property="drug_id",  type="integer", description="药品id"),
     *                          @OA\Property(property="mold_id",  type="integer", description="资源类型id"),
     *                          @OA\Property(property="img",  type="string", description="图片"),
     *                          @OA\Property(property="video_link",  type="string", description="视频"),
     *                          @OA\Property(property="content",  type="string", description="内容"),
     *                          @OA\Property(property="pdf_url",  type="string", description="pdf_url 当type=9时"),
     *                          @OA\Property(
     *                              property="is_pdf",
     *                              type="integer",
     *                              description="是否pdf 【当type=9时 该值=1】"),
     *                          @OA\Property(
     *                              property="drug_i_nfo_by_i_d",
     *                              type="object",
     *                              description="药品信息",
     *                              @OA\Property(property="id",  type="integer", description="id"),
     *                              @OA\Property(property="name",  type="string", description="名称"),
     *                          ),
     *                          @OA\Property(
     *                              property="p_d_f_info_by_i_d",
     *                              type="object",
     *                              description="pdf轮播图【当type=9时】",
     *                              @OA\Property(property="img",  type="string", description="名称"),
     *                          ),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    /**
     * 资源
     * @return array
     */
    public function getResourceByID($ResId)
    {
        $partId = 1;
        $resourceType = 1;
        $data = HealthTipsService::getResourceByID($ResId, $partId, $resourceType);

        return $this->success($data);
    }

    /**
     *
     * @OA\Get (
     *      path="/tips/drug/instructions/{DrugId:\d+}",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="产品说明书",
     *      description="产品说明书",
     *      operationId="getInstructionsByID",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="DrugId",
     *          in="path",
     *          description="药品id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="product_name", type="string", description="产品名称"),
     *                          @OA\Property(
     *                              property="manuals",
     *                              type="array",
     *                              description="产品说明书",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="name", type="string", description="项目名称"),
     *                                  @OA\Property(property="content", type="string", description="项目内容")
     *                              )
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    /**
     * 产品说明书
     * @return array
     */
    public function getInstructionsByID($DrugId)
    {
        $data = HealthTipsService::getInstructionsByID($DrugId);

        return $this->success($data);
    }


    /**
     *
     * @OA\Get (
     *      path="/tips/drug/guide/{ResId:\d+}",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="服务指南",
     *      description="服务指南",
     *      operationId="getServiceGuide",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="ResId",
     *          in="path",
     *          description="药品id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          description="列表",
     *                          @OA\Property(property="id",  type="integer", description="id"),
     *                          @OA\Property(property="content",  type="string", description="内容")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    /**
     * 服务指南
     * @return array
     */
    public function getServiceGuide($ResId)
    {
        $data = HealthTipsService::getServiceGuide($ResId);

        return $this->success($data);
    }


    /**
     *
     * @OA\Get (
     *      path="/tips/drug/question/{ResId:\d+}",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="用户反馈",
     *      description="用户反馈",
     *      operationId="getQuestionAnswer",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="ResId",
     *          in="path",
     *          description="标签id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="keywords",
     *          in="query",
     *          description="搜索关键字",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          description="列表",
     *                          @OA\Property(property="id",  type="integer", description="id"),
     *                          @OA\Property(property="quest",  type="string", description="问题"),
     *                          @OA\Property(property="answer",  type="string", description="答案"),
     *                          @OA\Property(property="self_fab",  type="bool", description="自己是否点赞"),
     *                          @OA\Property(property="fab",  type="string", description="点赞")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    /**
     * 用户反馈
     * @return array
     */
    public function getQuestionAnswer($ResId, ListQuestionAnswerRequest $request)
    {
        $data = HealthTipsService::getQuestionAnswer(
            (int) $ResId,
            (string) $request->query('keywords', '')
        );

        return $this->success($data);
    }


    /**
     *
     * @OA\Put (
     *      path="/tips/drug/set-fab/{QId:\d+}",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="常见问题点赞",
     *      description="常见问题点赞",
     *      operationId="setQuestionAnswerFab",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="QId",
     *          in="path",
     *          description="问题id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(property="data", type="bool", description="点赞是否成功")
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    /**
     * 点赞
     * @return array
     */
    public function setQuestionAnswerFab($QId)
    {
        $data = HealthTipsService::setQuestionAnswerFab($QId);

        return $this->success($data);
    }

    /**
     *
     * @OA\Get (
     *      path="/tips/dy-index",
     *      tags={"健康小贴士-话题管理-PK站"},
     *      summary="店员首页疾病产品",
     *      description="店员首页疾病产品",
     *      operationId="getDYIndex",
     *      @OA\Parameter(ref="#/components/parameters/app-role"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="indication",
     *                          type="object",
     *                          description="列表",
     *                          @OA\Property(property="id",  type="integer", description="id"),
     *                          @OA\Property(property="title",  type="string", description="title"),
     *                          @OA\Property(property="img",  type="string", description="图片"),
     *                          @OA\Property(property="resource_id",  type="integer", description="资源id")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    /**
     * 点赞
     * @return array
     *
     */
    public function getDYIndex($QId)
    {
        $data = HealthTipsService::getDYIndex($QId);

        return $this->success($data);
    }
}
