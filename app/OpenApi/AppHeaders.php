<?php

/**
 * @OA\Parameter(
 *     name="app-role",
 *     in="header",
 *     description="用户角色: 1患者 2店员,默认1店员",
 *     required=false,
 *     @OA\Schema(
 *          type="integer"
 *      )
 * ),
 * @OA\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="登陆接口返回token,格式为'Bearer <token>'",
 *     required=true,
 *     @OA\Schema(
 *          type="string"
 *      )
 * ),
 */
