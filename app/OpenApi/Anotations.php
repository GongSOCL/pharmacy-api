<?php

/**
 * @OA\OpenApi(
 *     @OA\Info(
 *         version="1.0.0",
 *         title="OTA小程序 API ",
 *         description="OTA小程序 api 接口文档",
 *     ),
 *     @OA\Server(url="http://otc-miniapp.qa.yyimgs.com"),
 * )
 */
