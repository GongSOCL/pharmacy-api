<?php
declare(strict_types=1);

namespace App\Helper;

use App\Constants\Policy;
use App\Repository\CouponRepository;
use App\Repository\ProductCategoryRelRepository;
use App\Repository\ProductRepository;
use Carbon\Carbon;
use Hyperf\Utils\Collection;

class CommonBus
{
    /**
     * 策略合并
     * @param $policies
     * @return \Hyperf\Utils\Collection
     */
    public static function handlePolicy($policies)
    {
        $pList = [];
        $productIds = [];
        foreach ($policies as $policy) {
            $key = md5(implode('|', [$policy['policy_type'],
                $policy['policy_val'],
                $policy['deadline_type'],
                $policy['deadline_val']]));
            if (!collect($pList)->where('key', $key)->count()) {
                $productIds[$key][] = $policy['yy_product_id'];
                if ($policy['deadline_type'] == Policy::DEADLINE_TYPE_DOWN &&
                    is_numeric($policy['deadline_val'])) {
                    /**@var Carbon  $createAt **/
                    $createAt = $policy['created_at'];
                    $deadlineVal = $createAt->addDays((int)$policy['deadline_val']);
                    $policy['deadline_val'] = $deadlineVal->toDateTimeString();
                }
                $pList[] = [
                    'key'=>$key,
                    'store_id'=>$policy['store_id'],
                    'policy_type'=>$policy['policy_type'],
                    'policy_val'=>$policy['policy_val'],
                    'deadline_type'=>$policy['deadline_type'],
                    'deadline_val'=>$policy['deadline_val'],
                ];
            } else {
                $productIds[$key][] = $policy['yy_product_id'];
            }
        }
        $pList = collect($pList)->map(function ($p) use ($productIds) {
            $p['product_ids'] = $productIds[$p['key']];
            return $p;
        });
        return $pList;
    }

    public static function handleSimplePolicies(Collection $policies, $pMap)
    {
        return $policies->map(function ($policy) use ($pMap) {
            $policy->product_name = $pMap[$policy['yy_product_id']]??'';
            if ($policy['deadline_type'] == Policy::DEADLINE_TYPE_DOWN &&
                is_numeric($policy['deadline_val'])) {
                /**@var Carbon  $createAt **/
                $createAt = $policy['created_at'];
                $deadlineVal = $createAt->addDays((int)$policy['deadline_val']);
                $policy['deadline_val'] = $deadlineVal->format('Y-m-d');
            } else {
                $policy['deadline_val'] = date('Y-m-d', strtotime($policy['deadline_val']));
            }
        });
    }

    public static function handleCouponInfo($res)
    {
        $couponIds = collect($res->items())
            ->pluck('coupon_id')
            ->unique();
        $coupons = CouponRepository::getCouponListByIds($couponIds);
        $couponMap = $coupons->pluck(null, 'id');
        $yyProductIds = $coupons->pluck('yy_product_id');
        $productMap = ProductRepository::getListByProductIds($yyProductIds)
            ->pluck(null, 'yy_product_id');
        $catMap = ProductCategoryRelRepository::getProductCategories($yyProductIds)
            ->pluck(null, 'yy_product_id');
        $res->getCollection()->transform(function ($row) use ($couponMap, $productMap, $catMap) {
            $couponInfo = $couponMap[$row['coupon_id']]??null;
            if ($couponInfo) {
                $productInfo = $productMap[$couponInfo->yy_product_id]??null;
                $catInfo = $catMap[$couponInfo->yy_product_id]??null;
                if ($catInfo) {
                    $row->product_name = $catInfo->cat_name;
                } else {
                    $row->product_name = '';
                }
                if (empty($row->product_name)) {
                    if ($productInfo) {
                        $row->product_name = $productInfo->name;
                    } else {
                        $row->product_name = '';
                    }
                }
                $row->policy_type = $couponInfo->policy_type;
                $row->policy_val = $couponInfo->policy_val;
            } else {
                $row->policy_type = 0;
                $row->policy_val = 0;
                $row->product_name = '';
            }
            $row->end_time = $row->deadline_time?
                date('Y-m-d H:i:s', $row->deadline_time):'';
            return $row;
        });
    }
}
