<?php
declare(strict_types=1);

namespace App\Helper;

use Aws\Handler\GuzzleV6\GuzzleHandler;
use Aws\S3\S3Client;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Hyperf\Guzzle\CoroutineHandler;
use League\Flysystem\AwsS3V3\AwsS3V3Adapter;
use League\Flysystem\Filesystem;

class Minio
{
    public static function getMinio(): Filesystem
    {
        $client = self::getS3Client();

        $adapter = new AwsS3V3Adapter(
            $client,
            env('S3_BUCKET'),
            '',
            null,
            null,
            ['override_visibility_on_copy' => true]
        );
        return new Filesystem($adapter);
    }

    /**
     * 获取对应的s3client, 可以支持更多操作
     * @return S3Client
     */
    public static function getS3Client(): S3Client
    {
        $options = [
            'credentials' => [
                'key' => env('S3_KEY'),
                'secret' => env('S3_SECRET'),
            ],
            'region' => env('S3_REGION'),
            'version' => 'latest',
            'bucket_endpoint' => false,
            'use_path_style_endpoint' => true,
            'endpoint' => env('S3_ENDPOINT'),
            'bucket_name' => env('S3_BUCKET'),
        ];
        $handler = new GuzzleHandler(new Client([
            'handler' => HandlerStack::create(new CoroutineHandler()),
        ]));
        $options = array_merge($options, ['http_handler' => $handler]);

        return new S3Client($options);
    }

    /**
     * 创建私有bucket文件对象链接(含过期时间)
     * @param $bucket
     * @param $key
     * @param string $expireTime
     * @param string $name
     * @return string
     */
    public static function createPrivateBucketFile($bucket, $key, $expireTime = "+ 1 days", $name = "minio"): string
    {
        $client = self::getS3Client();
        $cmd = $client->getCommand('GetObject', [
            'Bucket' => $bucket,
            'Key' => $key
        ]);
        return (string)$client->createPresignedRequest($cmd, $expireTime)->getUri();
    }

    /**
     * 创建公共bucket文件对象uri
     * @param $bucket
     * @param $key
     * @param string $baseUri
     * @return string
     */
    public static function createPublicBucketFileLink($bucket, $key, $baseUri = "", $useSsl = false): string
    {
        $baseUri = $baseUri ?: (string)env("MINIO_BASE_URI");
        $path = sprintf("%s/%s", trim($bucket, '/'), trim($key, '/'));
        if ($baseUri) {
            $path = sprintf("%s/%s", rtrim($baseUri, '/'), $path);
        }
        if ($useSsl && substr($path, 0, 6) != 'https:') {
            $path = 'https' . substr($path, 4);
        }

        return $path;
    }

    public static function uploadFile($filePath, $uploadPath)
    {
        self::getS3Client()
            ->putObject([

            ]);
    }
}
