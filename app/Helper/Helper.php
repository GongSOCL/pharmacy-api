<?php
declare(strict_types=1);
namespace App\Helper;

use App\Constants\AppErr;
use App\Constants\Auth;
use App\Constants\DataStatus;
use App\Exception\AuthException;
use App\Exception\BusinessException;
use App\Model\Pharmacy\User;
use App\Model\Pharmacy\UserRole;
use App\Repository\UserRoleRepository;
use Google\Protobuf\Internal\RepeatedField;
use Grpc\Common\CommonPageResponse;
use Grpc\Pharmacy\Common\ShareLinkToken;
use GuzzleHttp\Client;
use Hashids\Hashids;
use Hyperf\Database\Query\Expression;
use Hyperf\DbConnection\Db;
use Hyperf\Guzzle\ClientFactory;
use Hyperf\Logger\LoggerFactory;
use Hyperf\Redis\RedisFactory;
use Hyperf\Redis\RedisProxy;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Context;
use Psr\Log\LoggerInterface;

class Helper
{
    /**
     * 根据id生成hashid
     * @param $id
     * @return string
     */
    public static function generateHashId($id): string
    {
        $hash = new Hashids(env('HASH_ID_SALT'), env('HASH_ID_MIN_LENGTH'));
        return $hash->encode($id);
    }

    /**
     * 根据hashId解码原始值
     * @param string $hashId
     */
    public static function decodeHashId(string $hashId)
    {
        $hash = new Hashids(env('HASH_ID_SALT'), env('HASH_ID_MIN_LENGTH'));
        $hashDecode = $hash->decode($hashId);
        return isset($hashDecode[0]) ? $hashDecode[0] : '';
    }

    /**
     * @return User
     */
    public static function getLoginUser(): User
    {
        $user = Context::get('user');
        if (!($user && $user instanceof User)) {
            throw new BusinessException(Auth::AUTHENTICATION_FAILED);
        }

        return $user;
    }

    /**
     * @return false|string
     */
    public static function getDate()
    {
        return date('Y-m-d H:i:s', time());
    }

    /**
     * @param $dateStr
     * @param string $format
     * @return string
     * @throws \Exception
     */
    public static function formatInputDate($dateStr, $format = 'Y-m-d H:i:s'): string
    {
        return (new \DateTime($dateStr))->format($format);
    }

    /**
     * @param $request
     * @return mixed
     */
    public static function ip($request)
    {
        $res = $request->getHeaders();
        if (isset($res['x-real-ip'])) {
            return $res['x-real-ip'][0];
        } elseif (isset($res['x-forwarded-for'])) {
            return $res['x-forwarded-for'][0];
        } else {
            return $res['remote_addr'];
        }
    }

    /**
     * @param $redisFlag
     * @return RedisProxy|null
     */
    public static function getRedis($name = "default"): ?RedisProxy
    {
        return ApplicationContext::getContainer()->get(RedisFactory::class)->get($name);
    }

    public static function tryGetLoginUserId(): ?User
    {
        $user = Context::get('user');
        return $user && $user instanceof User ? $user : null;
    }


    //二维数组排序
    public static function arraySort($arr, $keys, $type = 'asc')
    {
        $keysvalue = $new_array = array();
        foreach ($arr as $k => $v) {
            $keysvalue[$k] = $v[$keys];
        }
        if ($type == 'asc') {
            asort($keysvalue);
        } else {
            arsort($keysvalue);
        }
        reset($keysvalue);
        foreach ($keysvalue as $k => $v) {
            $new_array[$k] = $arr[$k];
        }
        return $new_array;
    }

    public static function getLogger($name = "default"): LoggerInterface
    {
        return ApplicationContext::getContainer()
            ->get(LoggerFactory::class)
            ->get($name);
    }

    public static function getUserLoginRole(): UserRole
    {
        $roleId = Context::get('role');
        if (!$roleId) {
            throw new AuthException(Auth::ILLEGAL_REQUEST);
        }

        $user = self::getLoginUser();
        $role = UserRoleRepository::getUserRole($user, $roleId);
        if (!$role) {
            throw new AuthException(Auth::ILLEGAL_REQUEST);
        }
        return $role;
    }

    public static function isOnline(): bool
    {
        return env('APP_ENV', 'local') == 'prod';
    }

    /**
     * aes加密
     * @param $str
     * @param $password
     * @param $iv
     * @param $method
     * @return string
     */
    public static function aesEnc(
        $str,
        $password,
        $iv = DataStatus::DEFAULT_AES_IV,
        $method = DataStatus::DEFAULT_AES_METHOD
    ): string {
        $res= base64_encode(openssl_encrypt($str, $method, $password, 0, $iv));
        return strtr($res, '+/', '-_');
    }

    /**
     * aes解密
     * @return string|bool
     */
    public static function aesDec(
        $str,
        $password,
        $iv = DataStatus::DEFAULT_AES_IV,
        $method = DataStatus::DEFAULT_AES_METHOD
    ) {
        return openssl_decrypt(base64_decode($str), $method, $password, 0, $iv);
    }

    public static function buildPageFromCommon(CommonPageResponse $page): array
    {
        return [
            'total' => $page->getTotal(),
            'pages' => $page->getPages(),
            'current' => $page->getCurrent(),
            'size' => $page->getSize(),
        ];
    }

    public static function mapRepeatedField(RepeatedField $field, callable $call, $initial = [])
    {
        $data = $initial;
        foreach ($field as $item) {
            $res = $call($item);
            $data[] = $res;
        }
        return $data;
    }

    public static function buildPharmacyUri($path, array $params = []): string
    {
        $baseUri = "";
        switch (env('APP_ENV')) {
            case DataStatus::ENV_STAGING:
                $baseUri = "https://pharmacy.qa2.yyimgs.com";
                break;
            case DataStatus::ENV_PROD:
                $baseUri = "https://pharmacy.youyao99.com";
                break;
            default:
                $baseUri = "https://pharmacy.qa.yyimgs.com";
                break;
        }

        $uri = sprintf("%/%s", rtrim($baseUri, '/'), ltrim($path, '/'));
        if ($params) {
            $uri = $uri . '?' . http_build_query($params);
        }
        return $uri;
    }

    public static function genShareToken(ShareLinkToken $token): string
    {
        $str = $token->serializeToString();
        [$method, $passphrase, $iv] = self::getShareElem();
        if (strlen($str) % 16) {
            $data = str_pad($str, strlen($str) + 16 - strlen($str) % 16, "\0");
        }

        $res = base64_encode(openssl_encrypt($str, $method, $passphrase, 0, $iv));
        return strtr($res, '+/', '-_');
    }

    public static function decodeShareToken($token): ShareLinkToken
    {
        try {
            [$method, $passphrase, $iv] = self::getShareElem();
            $key = strtr($token, '-_', '+/');
            $raw = openssl_decrypt(base64_decode($key), $method, $passphrase, 0, $iv);

            $share = new ShareLinkToken();
            $share->mergeFromString($raw);

            return $share;
        } catch (\Exception $e) {
            self::getLogger()->error("decode_share_key_error", [
                'msg' => $e->getMessage()
            ]);
            throw new BusinessException(AppErr::BUSINESS_ERROR, "非法请求");
        }
    }

    private static function getShareElem(): array
    {
        $method = env("SHARE_ENC_METHOD", 'aes-256-cbc');
        $iv = "jr2v7*T|}/D[86y=";
        $passphrase = env('SHARE_ENC_PASSPHRASE', '*v-ic}}\+1Kl0|_O');
        return [$method, $passphrase, $iv];
    }

    public static function genRandStr($length)
    {
        $str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $len = strlen($str) - 1;
        $randStr = '';
        for ($i = 0; $i < $length; $i++) {
            $num = mt_rand(0, $len);
            $randStr .= $str[$num];
        }
        return $randStr;
    }

    public static function getHttpClient($uri, $timeout = 5): Client
    {
        return ApplicationContext::getContainer()->get(ClientFactory::class)
            ->create([
                'base_uri' => $uri,
                'timeout' => $timeout
            ]);
    }

    public static function buildOTCUrl(string $path, array $query = []): string
    {
        $base = env('PHARMACY_BASE_URI', '');
        if (!$base) {
            throw new BusinessException(AppErr::BUSINESSERR, "基础url未配置");
        }
        if ($query) {
            $path = $path . "?" . http_build_query($query);
        }

        return rtrim($base, "/") . "/". ltrim($path, '/');
    }

    public static function yyidGenerator(): Expression
    {
        return Db::raw("UPPER(REPLACE(UUID(), '-', ''))");
    }
}
