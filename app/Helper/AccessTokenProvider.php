<?php
declare(strict_types=1);

namespace App\Helper;

use App\Service\Common\TokenRpcService;
use EasyWeChat\Kernel\ServiceContainer;
use EasyWeChat\MiniProgram\Auth\AccessToken;
use Hyperf\Utils\ApplicationContext;

class AccessTokenProvider extends AccessToken
{
    private $channel = "";

    public static function newInstance($app, $channelAlias): AccessTokenProvider
    {
        $o =new self($app);
        $o->channel = $channelAlias;
        return $o;
    }

    public function requestToken(array $credentials, $toArray = false): array
    {
        //调用token server获取token并返回数组
        $token = ApplicationContext::getContainer()->get(TokenRpcService::class)
            ->getToken($this->channel);
        $interval = \DateTime::createFromFormat("Y-m-d H:i:s", $token->getExpire())->getTimestamp() - time();
        Helper::getLogger()->debug("get_wechat_from_token", [
            'token' => $token->getToken(),
            'code' => $token->getCode(),
            'expire' => $token->getExpire(),
            'msg' => $token->getMsg()
        ]);
        return ['access_token' => $token->getToken(), 'expires_in' => $interval];
    }
}
