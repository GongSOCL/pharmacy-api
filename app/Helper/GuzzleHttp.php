<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/admin-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Helper;

use Hyperf\Guzzle\ClientFactory;

class GuzzleHttp
{
    /**
     * @var \Hyperf\Guzzle\ClientFactory
     */
    private $clientFactory;

    public function __construct(ClientFactory $clientFactory)
    {
        $this->clientFactory = $clientFactory;
    }

    /**
     * @param $url
     * @param int $timeout
     * @return \GuzzleHttp\Client
     */
    public function gHttps($url, $timeout = 2)
    {
        $options = [
            'base_uri' => $url,
            'timeout'  => $timeout,
        ];
        $client = $this->clientFactory->create($options);

        return $client;
    }
}
