<?php
declare(strict_types=1);


namespace App\Helper;

use Carbon\Carbon;
use Hyperf\Di\Container;
use Hyperf\Redis\Redis;
use Hyperf\Redis\RedisFactory;

/**
 * 简易锁
 * Class Lock
 * @package App\Helper
 */
class Lock
{
    private $prefix = "pharmacy:lock:";

    /**
     * @var Redis
     */
    private $cache;

    public function __construct(Container $container)
    {
        if (!$this->cache) {
            $this->cache = $container->get(RedisFactory::class)->get('default');
        }
    }

    public function acquire($name, $acquireTimeout = 10, $lockTimeout = 10)
    {
        $identifier = uniqid();
        $lockName = "{$this->prefix}{$name}";

        $a = Carbon::now();
        $a->addSeconds($acquireTimeout);
        while ($a->isAfter(Carbon::now())) {
            $operateResult = $this->cache->set($lockName, $identifier, [
                'nx',
                'ex' => $lockTimeout
            ]);
            if ($operateResult) {
                return $identifier;
            }
            \Swoole\Coroutine::sleep(1);
        }
        return false;
    }

    public function tryLock($name)
    {
        $lockName = "{$this->prefix}{$name}";
        return $this->cache->exists($lockName);
    }

    public function release($name, $identifier): bool
    {
        $lockName = "{$this->prefix}{$name}";

        $cache = $this->cache;
        while (true) {
            try {
                $cache->watch($lockName);
                $current = $cache->get($lockName);
                if ($current == $identifier) {
                    $cache->multi();
                    $cache->del($lockName);
                    $cache->exec();
                    return true;
                }
                $cache->unwatch();
                break;
            } catch (\Exception $e) {
                continue;
            }
        }
        return false;
    }
}
