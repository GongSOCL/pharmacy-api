<?php

namespace App\Helper;

use Aws\Handler\GuzzleV6\GuzzleHandler;
use Aws\S3\S3Client;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Hyperf\Guzzle\CoroutineHandler;
use League\Flysystem\AwsS3V3\AwsS3V3Adapter;
use League\Flysystem\Filesystem;

class S3V3AdapterFactory
{
    public function make(array $options): Filesystem
    {
        $handler = new GuzzleHandler(new Client([
            'handler' => HandlerStack::create(new CoroutineHandler()),
        ]));
        $options = array_merge($options, ['http_handler' => $handler]);
        $client = new S3Client($options);
        $adapter =  new AwsS3V3Adapter(
            $client,
            $options['bucket_name'],
            '',
            null,
            null,
            ['override_visibility_on_copy' => true]
        );
        return new Filesystem($adapter);
    }
}
