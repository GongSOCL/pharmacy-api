<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/sdc-crawler.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Helper;

use App\Constants\AppErr;
use App\Exception\BusinessException;
use App\Service\Common\UploadRpcService;
use Grpc\Upload\BUSINESS_TYPE;
use Grpc\Upload\StorageChannel;
use Grpc\Upload\TokenResponse;
use Grpc\Upload\UPLOAD_SCOPE;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Utils\ApplicationContext;
use Qiniu\Auth;
use Qiniu\Config;
use Qiniu\Http\Error;
use Qiniu\Region;
use Qiniu\Storage\BucketManager;
use Qiniu\Storage\UploadManager;

class UploadHelper
{

    /**
     * @var TokenResponse
     */
    private $token;

    public static function newInstance($business = 'nfx-sales', $type = BUSINESS_TYPE::BUSINESS_TYPE_USER): UploadHelper
    {
        $o = new self();
        $o->token = UploadRpcService::getToken(
            $type,
            UPLOAD_SCOPE::SCOPE_ADMIN,
            $business
        );

        return $o;
    }

    public function upload($path, $fileName): array
    {
        $logger = Helper::getLogger();
        // 使用token上传七牛
        $cfg = new Config(Region::regionHuadong());
        $uploadManager = new UploadManager($cfg);
        $pathPrefix = $this->token->getPath();
        [$ret, $err] = $uploadManager->putFile(
            $this->token->getToken(),
            sprintf('%s%s', $pathPrefix, $fileName),
            $path
        );
        if ($err) {
            if ($err instanceof Error) {
                $logger->error('update_qiniu_http_fail', [
                    'fileName' => $fileName,
                    'response' => $err->getResponse(),
                    'msg' => $err->message(),
                ]);
            }
            throw new BusinessException(AppErr::BUSINESS_ERROR, '七牛文件上传失败');
        }

        $key = $ret['key'] ?? '';
        $url = $this->token->getBaseUri() . '/' . $key;

        // 调用接口记录到数据库, 返回对应id, 用来记录到数据库
        $uploadData = UploadRpcService::addUploadRecord(
            $key,
            $this->token->getBucket(),
            $fileName,
            $url,
            StorageChannel::CHANNEL_QINIU
        );
        return [
            $uploadData->getId(),
            $uploadData->getName(),
            $uploadData->getUrl(),
        ];
    }

    public static function statFiles($bucket, array $paths): array
    {
        $auth = self::getAuth();
        $cfg = self::getConfig();

        $bm = new BucketManager($auth, $cfg);
        $batch = BucketManager::buildBatchStat($bucket, $paths);
        return $bm->batch($batch);
    }

    public static function batchDelete($bucket, array $keys): array
    {
        $auth = self::getAuth();
        $cfg = self::getConfig();

        $bm = new BucketManager($auth, $cfg);
        $batch = BucketManager::buildBatchDelete($bucket, $keys);
        return $bm->batch($batch);
    }

    private static function getConfig(): Config
    {
        return new Config(Region::regionHuadong());
    }

    private static function getAuth(): Auth
    {
        $cfg = ApplicationContext::getContainer()
            ->get(ConfigInterface::class)
            ->get('file.storage.qiniu');
        if (! $cfg) {
            throw new \UnexpectedValueException('七牛配置不存在');
        }
        if (! ($cfg['accessKey'] && $cfg['secretKey'])) {
            throw new \UnexpectedValueException('七牛末配置');
        }

        return new Auth($cfg['accessKey'], $cfg['secretKey']);
    }

    public static function uploadStreamToMinio($fd, $uploadPath, $fileName)
    {
        $fs = Minio::getMinio();
        $fs->writeStream($uploadPath, $fd);

        $bucket = env('S3_BUCKET');
        $uri = Minio::createPublicBucketFileLink(
            $bucket,
            $uploadPath
        );

        $resp  = UploadRpcService::addUploadRecord(
            $uploadPath,
            $bucket,
            $fileName,
            $uri,
            StorageChannel::CHANNEL_MINIO
        );

        return [
            $resp->getId(),
            $resp->getName(),
            $resp->getUrl(),
        ];
    }
}
