<?php
declare(strict_types=1);
namespace App\Handler\Wechat;

use App\Constants\AppErr;
use App\Exception\BusinessException;
use App\Model\Pharmacy\ShareTokenSchema;
use App\Service\Agent\AgentService;
use App\Service\Doctor\DoctorGroupRpcService;
use App\Service\Wechat\WechatService;
use Grpc\Pharmacy\Common\ShareLinkToken;
use Grpc\Pharmacy\Wechat\DoctorQrLinkReply;
use Grpc\Pharmacy\Wechat\DoctorQrLinkRequest;
use Grpc\Pharmacy\Wechat\SchemaReply;
use Grpc\Pharmacy\Wechat\StorePatientTicketPicRequest;
use Grpc\Pharmacy\Wechat\UrlLinkReply;
use Grpc\Pharmacy\Wechat\UrlLinkRequest;
use Grpc\Pharmacy\Wechat\WechatSvcServerHandler;

class WechatHandler extends WechatSvcServerHandler
{
    /**
     * 小程序短链
     * @param UrlLinkRequest $request
     * @return UrlLinkReply
     * @throws \Exception
     */
    public function generateUrlLink(UrlLinkRequest $request): UrlLinkReply
    {
        $params = [];
        if ($request->getParams()) {
            $params = json_decode($request->getParams(), true);
        }
        $isExpireDaysInterval = $request->getIsExpireByDaysInterval();
        if ($isExpireDaysInterval) {
            $expireValue = (int) $request->getExpireDays();
        } else {
            $expireValue = (new \DateTime($request->getExpireEnd()))->getTimestamp();
        }
        $link = WechatService::generateUrlLink(
            $request->getPath(),
            $params,
            $isExpireDaysInterval,
            $expireValue
        );



        $reply = new UrlLinkReply();
        $reply->setLink($link);
        return $reply;
    }

    /**
     * 生成医生的二维码
     * @param DoctorQrLinkRequest $request
     * @return DoctorQrLinkReply
     */
    public function generateDoctorQrLink(DoctorQrLinkRequest $request): DoctorQrLinkReply
    {
        $agentId = $request->getAgentId();
        $doctorId = $request->getDoctorId();
        //生成链接
        $share = new ShareLinkToken();
        $share->setDoctorId($doctorId);
        $qrLink = AgentService::getShareLinkTokenSchema(
            ShareTokenSchema::BIZ_TYPE_DOCTOR_SCHEMA,
            $doctorId,
            'pages/patient/purchase',
            $share
        );
        $reply = new DoctorQrLinkReply();
        $reply->setLink($qrLink);
        return $reply;
    }

    public function generateStorePatientTicketPic(StorePatientTicketPicRequest $request): SchemaReply
    {
        $size = $request->getSize();
        if ($size <= 0) {
            $size = 430;
        }
        return AgentService::generateStorePatientQrcode(
            $request->getStoreId(),
            $request->getAgentId(),
            $size
        );
    }
}
