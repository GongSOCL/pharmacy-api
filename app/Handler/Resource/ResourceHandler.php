<?php
declare(strict_types=1);
namespace App\Handler\Resource;

use App\Service\Resource\ResourceService;
use Grpc\Pharmacy\Resource\ResourceIdsRequest;
use Grpc\Pharmacy\Resource\ResourceListReply;
use Grpc\Pharmacy\Resource\ResourceSvcInterface;
use Grpc\Pharmacy\Resource\ResourceSvcServerHandler;
use Grpc\Pharmacy\Resource\SearchResourceWithLimitRequest;
use Hyperf\Utils\ApplicationContext;

class ResourceHandler extends ResourceSvcServerHandler
{

    /**
     * @inheritDoc
     */
    public function GetResourceByIds(ResourceIdsRequest $request): ResourceListReply
    {
        $ids = [];
        foreach ($request->getId() as $id) {
            $ids[] = (int) $id;
        }
        $ids = array_unique($ids);
       return ApplicationContext::getContainer()
           ->get(ResourceService::class)
            ->getByIds($ids);
    }

    /**
     * @inheritDoc
     */
    public function SearchResource(SearchResourceWithLimitRequest $request): ResourceListReply
    {
        $keywords = $request->getKeywords();
        $limit =$request->getLimit();
        if ( $limit < 1) {
            $limit = 10;
        }
        $type = (int) $request->getType();
        return ApplicationContext::getContainer()
            ->get(ResourceService::class)
            ->searchResourceWithLimit($keywords, $limit, $type);
    }
}