<?php
declare(strict_types=1);
namespace App\Service\HealthTips;

use App\Constants\Auth;
use App\Constants\Clerk;
use App\Constants\InsideMsgTypes;
use App\Constants\Role;
use App\Elasticsearch\QuestionAnswerSearch;
use App\Exception\AuthException;
use App\Helper\Helper;
use App\Model\Pharmacy\PharmacyPointEvent;
use App\Model\Pharmacy\UserClerk;
use App\Repository\HealthTipsRepository;
use App\Repository\Resource\CountRepository;
use App\Repository\UserArticleMonthReadPointRepository;
use App\Repository\UserClerkRepository;
use App\Repository\UserPatientDoctorRepository;
use App\Service\Point\PointRpcService;
use App\Service\Point\PointService;
use App\Service\User\UserClerkService;
use Carbon\Carbon;
use Hyperf\Utils\Context;

class HealthTipsService
{
    private static function checkUser()
    {
        //获取登陆用户clerk
        $user = Helper::getLoginUser();

        $role = Helper::getUserLoginRole();

        if ($role->role == 1) {
            # 患者
            $UserType = '患者';

            $clerk = UserPatientDoctorRepository::getUserClerkByUserId($user->id);
        } else {
            # 店员
            $UserType = '店员';

            $clerk = UserClerkRepository::getUserClerkByUserId($user->id);
        }

        if (!$clerk) {
            throw new AuthException(Auth::USER_FORBIDDEN, "'.$UserType.'帐号不存在或已禁用");
        }

        return $role;
    }

    /**
     * 首页类型
     * @return array
     */
    public static function getResourcesMold()
    {
        self::checkUser();

        # 获取首页类型 【标签目前写死 221114起】
        return HealthTipsRepository::getResourcesMoldList();
    }

    /**
     * 小知识
     * @return array
     */
    public static function getHealthTips($MoldId)
    {
        $RoleId = self::checkUser();

        return HealthTipsRepository::getHealthTipsList($RoleId->role, $MoldId);
    }


    /**
     * 适应症
     * @return array
     */
    public static function getIndication($MoldId)
    {
        $RoleId = self::checkUser();

        return HealthTipsRepository::getIndicationList($RoleId->role, $MoldId);
    }


    /**
     * 资源
     * @return array
     */
    public static function getResourceByID($ResId, $partId, $resourceType)
    {
        self::checkUser();

        $user = Helper::getLoginUser();

        $uid = isset($user->id) ? $user->id : 0;

        # 更新阅读量 $partId 1 首页，2 培训中心。$resourceType 1 普通资源， 2 问卷
//        go(function () use ($ResId, $uid, $partId, $resourceType) {
//            try {
//                CountRepository::recodeUserClick(
//                    $ResId,
//                    $uid,
//                    $partId,
//                    $resourceType
//                );
//            } catch (\Exception $e) {
//                Helper::getLogger()->error("save_reading_volume_log_error", [
//                    'msg' => $e->getMessage()
//                ]);
//            }
//        });

        $data = HealthTipsRepository::getResourceByID($ResId);
        if ($data['type'] == 1 and Context::get('role') == Role::ROLE_CLERK) {
            self::sendClerkReadArticlePoint($ResId);
        }
        return $data;
    }

    public static function isArticleReaded(UserClerk $clerk, $resourceId): bool
    {
        //每天一篇文章只能获得一次积分
        $key = sprintf("pharmay:article:read:clerk:%d:%d", $clerk->id, $resourceId);
        $expire = (new \DateTime())->modify("+1 days")->setTime(0, 0, 0)->getTimestamp();
        return Helper::getRedis()->set($key, true, ['nx', 'ex' => $expire]) === true;
    }

    /**
     * 资源
     * @return array
     */
    public static function getInstructionsByID($DrugId)
    {
//        self::checkUser();
        $manuals = HealthTipsRepository::getInstructionsByID($DrugId);

        $data = [];

        if ($manuals) {
            foreach ($manuals as $item) {
                $data[] = [
                    'bname' => $item['bname'],
                    'content' => $item['content']
                ];
            }

            return ['product_name'=>$manuals[0]['name'], 'manuals'=>$data];
        }

        return [];
    }


    /**
     * 服务指南
     * @return array
     */
    public static function getServiceGuide($ResId)
    {
        self::checkUser();

        return HealthTipsRepository::getServiceGuide($ResId);
    }



    /**
     * 用户反馈
     * @return array
     */
    public static function getQuestionAnswer($ResId, $keywords = '')
    {
        $RoleId = self::checkUser();

        //替换成es
        [$total, $QuestionAnswer] = QuestionAnswerSearch::searchWithHighLight($ResId, $keywords);

        foreach ($QuestionAnswer as &$itm) {
            $itm['fab'] = HealthTipsRepository::getQuestionAnswerFab($itm['id'], $RoleId);

            $self_fab = HealthTipsRepository::getQuestionAnswerFabSelf($itm['id'], $RoleId);

            $itm['self_fab'] = isset($self_fab['fab']) ? true : false;
        }

        return $QuestionAnswer;
    }


    /**
     * 点赞
     * @return array
     */
    public static function setQuestionAnswerFab($QId)
    {
        $RoleId = self::checkUser();

        return HealthTipsRepository::setQuestionAnswerFab($QId, $RoleId);
    }


    /**
     * 店员首页疾病产品
     * @return array
     */
    public static function getDYIndex()
    {
        $RoleId = self::checkUser();
        return HealthTipsRepository::getDYIndex($RoleId->role);
//        return HealthTipsRepository::getDYIndex(2);
    }

    private static function sendClerkReadArticlePoint($resourceId)
    {
        try {
            $clerk = UserClerkService::checkAndGetClerk();
            if (!self::isArticleReaded($clerk, $resourceId)) {
                go(function () use ($clerk) {
                    $checker = ArticleReadLimitCheckService::getInstance(
                        $clerk->id,
                        Carbon::now()
                    );
                    if (!$checker->checkBefore()) {
                        return;
                    }

                    try {
                        PointService::sendClerkPointFromCommonEvent(
                            PharmacyPointEvent::PHARMACY_CLERK_READ_ARTICLE,
                            $clerk,
                            InsideMsgTypes::MSG_TYPE_CLERK_READ_ARTICLE_GAIN_POINT,
                            1,
                            '阅读文章得积分'
                        );
                    } catch (\Exception $e) {
                        $checker->revokeCnt();
                        Helper::getLogger()->error("send_read_article_point_error", [
                            'clerk' => $clerk->id,
                            'msg' => $e->getMessage(),
                            'trace' => $e->getTrace()
                        ]);
                        return;
                    }
                    $checker->checkAfter();
                });
            }
        } catch (\Exception $e) {
            Helper::getLogger()->error("try_send_clerk_read_article_point_error", [
                'msg' => $e->getMessage(),
                'trace' => $e->getTrace()
            ]);
        }
    }
}
