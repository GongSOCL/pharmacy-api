<?php
declare(strict_types=1);
namespace App\Service\HealthTips;

use App\Constants\AppErr;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Helper\Lock;
use App\Repository\UserArticleMonthReadPointRepository;
use Carbon\Carbon;
use Hyperf\Utils\ApplicationContext;

class ArticleReadLimitCheckService
{
    private $userId;

    /**
     * 
     * @var Carbon $time
     */
    private $time;

    private $limit = 0;

    private function __construct()
    {
    }

    public static function getInstance($userId, Carbon $time)
    {
       $o = new self();
       $o->userId = $userId;
       $o->time = $time;
       $o->limit = (int) env('PHARMACY_CLERK_READ_ARTICLE_MONTH_POINT', 0);
       return $o;
    }

    public function checkBefore()
    {
        if ($this->limit) {
            return true;
        }

        $year = $this->time->format('Y');
        $month = $this->time->format('n');
        $key = sprintf("pharmacy:clerk:month:ar:%d:%d", $year, $month);
        $user = sprintf("user:%d", $this->userId);
        $rds = Helper::getRedis();
        if (!$rds->hExists($key, $user)) {
            /**
             * @var Lock $lock
             */
            $lock = ApplicationContext::getContainer()
                ->get(Lock::class);
            $lockKey = sprintf("user:%d:%d:%d", $this->userId, $year, $month);
            $identify = $lock->acquire(
                $lockKey,
                10,
                10
            );
            if ($identify === false) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '刷新阅读数据锁异常');
            }
            if (!$rds->hExists($key, $user)) {
                $cnt = UserArticleMonthReadPointRepository::countUserRead($this->userId, $this->time);
                $rds->hSet($key, $user, $cnt);
                $expire = $this->time->clone()
                    ->modify("+1 month")
                    ->setDay(1)
                    ->setTime(0, 0, 0)
                    ->getTimestamp();
                $rds->expireAt($key, $expire);
            }
            $lock->release($lockKey, $identify);
        }
        return $rds->hIncrBy($key, $user, 1) <= $this->limit;
    }

    public function checkAfter()
    {
        if ($this->limit == 0) {
            return;
        }

        UserArticleMonthReadPointRepository::addArticleRead(
            $this->userId,
            $this->time
        );
    }

    public function revokeCnt()
    {
        $year = $this->time->format('Y');
        $month = $this->time->format('n');
        $key = sprintf("pharmacy:clerk:month:ar:%d:%d", $year, $month);
        $user = sprintf("user:%d", $this->userId);
        Helper::getRedis()
            ->hIncrBy($key, $user, -1);
    }
}