<?php
declare(strict_types=1);

namespace App\Service\Store;

use App\Constants\AppErr;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Pharmacy\BranchStore;
use App\Model\Pharmacy\ShareTokenSchema;
use App\Repository\AreaProvinceRepository;
use App\Repository\BranchStoreGpsRepository;
use App\Repository\BranchStoreRepository;
use App\Repository\CityRepository;
use App\Repository\CouponRepository;
use App\Repository\DepartProductRepository;
use App\Repository\DoctorProductRepository;
use App\Repository\MainStoreRepository;
use App\Repository\ProductRepository;
use App\Repository\UserKeywordRepository;
use App\Service\Agent\AgentService;
use App\Service\Agent\UserRpcService;
use App\Service\Common\AmapService;
use Grpc\Pharmacy\Common\SHARE_TYPE;
use Grpc\Pharmacy\Common\ShareLinkToken;
use Grpc\Upload\StorageChannel;
use Grpc\Wxapi\User\AgentItem;
use Grpc\Wxapi\User\HospitalItem;
use Hyperf\Utils\Arr;

class StoreService
{


    public static function generateStoreCommonSchema($size = 430, $channel = StorageChannel::CHANNEL_QINIU): string
    {
        $storeId = 0;
        $share = new ShareLinkToken();
        $share->setStoreId($storeId);
        $share->setAgentId(1);
        $share->setType(SHARE_TYPE::SHARE_AGENT_CLERK);
        return AgentService::getShareLinkTokenSchema(
            ShareTokenSchema::BIZ_TYPE_CLERK_INVITE,
            $storeId,
            'pages/clerk/registerForm',
            $share,
            $size,
            $channel
        );
    }

    public function provinceList()
    {
        return AreaProvinceRepository::getProvinces();
    }
    public function provinceCityList($proId)
    {
        return CityRepository::getCityListByProId($proId);
    }

    public function cityList($page, $pageSize)
    {
        $res = CityRepository::getCityListByPage($pageSize);
        if ($res->total() <= 0) {
            return [
                'list' => [],
                'total' => 0,
                'page' => $page,
                'page_size' => $pageSize
            ];
        }
        return [
            'list' => $res->items(),
            'total' => $res->total(),
            'page' => $res->currentPage(),
            'page_size' => $pageSize
        ];
    }

    public function mainStoreListByCity($cityId, $page, $pageSize, $keywords = '')
    {
        $res = MainStoreRepository::getMainStoreListByPageAndCityId($cityId, $pageSize, $keywords);
        if ($res->total() <= 0) {
            return [
                'list' => [],
                'total' => 0,
                'page' => $page,
                'page_size' => $pageSize
            ];
        }
        return [
            'list' => $res->items(),
            'total' => $res->total(),
            'page' => $res->currentPage(),
            'page_size' => $pageSize
        ];
    }

    public function storeListByMainStore($mainStoreId, $page, $pageSize, $keywords = '')
    {
        $res =  BranchStoreRepository::getStoreListByPageAndMainId($mainStoreId, $pageSize, $keywords);
        if ($res->total() <= 0) {
            return [
                'list' => [],
                'total' => 0,
                'page' => $page,
                'page_size' => $pageSize
            ];
        }
        return [
            'list' => $res->items(),
            'total' => $res->total(),
            'page' => $res->currentPage(),
            'page_size' => $pageSize
        ];
    }

    public static function getStoreAgents($storeId)
    {
        $storeInfo = BranchStoreRepository::getStoreInfoById($storeId);
        if (empty($storeInfo) || $storeInfo->status != 1) {
            return [];
        }
        $res = UserRpcService::getStoreAgents($storeInfo->yy_store_id);
        $agentIds = [];
        if ($res->getAgents()->count() <= 0) {
            return $agentIds;
        }
        /** @var AgentItem $item */
        foreach ($res->getAgents() as $item) {
            $agentIds[] = $item->getUserId();
        }
        return array_unique($agentIds);
    }

    public static function listAgentStores($token, $keywords = ''): array
    {
        $userId = AgentService::checkAndGetAgent($token);
        // $resp = UserRpcService::getAgentServeHospitals($userId);
        $resp = UserRpcService::getAgentSpecialHospitals($userId, $keywords);
        if ($resp->getHospitals()->count() == 0) {
            return [];
        }
        $ids = [];
        /** @var HospitalItem $item */
        foreach ($resp->getHospitals() as $item) {
            $ids[] = $item->getId();
        }
        $stores = BranchStoreRepository::getStoreInfoByYyStoreIds($ids);
        $list = [];
        /** @var BranchStore $store */
        foreach ($stores as $store) {
            $list[] = [
                'id' => $store->yy_store_id,
                'name' => $store->store_name_alias
            ];
        }
        return $list;
    }

    public static function genStoreQrcode($storeId, $token): string
    {
        $userId = AgentService::checkAndGetAgent($token);

        // 需要检查代表当前是否服务该医院
        if (!UserRpcService::checkUserSpecifyServeHospital($userId, $storeId)) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, "代表未服务该医院或服务已过期解绑");
        }

        //生成医院二维码链接
        $share = new ShareLinkToken();
        $share->setStoreId($storeId);
        $share->setAgentId($userId);
        $share->setType(SHARE_TYPE::SHARE_AGENT_STORE);
        return AgentService::getShareLinkTokenSchema(
            ShareTokenSchema::BIZ_TYPE_STORE_SCHEMA,
            $storeId,
            'pages/patient/scancodeTip',
            $share
        );
    }

    public static function getInviteClerkLink($agentToken, $storeId): string
    {
        $agentId = AgentService::checkAndGetAgent($agentToken);

        // 需要检查代表当前是否服务该医院
        if (!UserRpcService::checkUserSpecifyServeHospital($agentId, $storeId)) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, "代表未服务该医院或服务已过期解绑");
        }
        $share = new ShareLinkToken();
        $share->setStoreId($storeId);
        $share->setAgentId($agentId);
        $share->setType(SHARE_TYPE::SHARE_AGENT_CLERK);
        return AgentService::getShareLinkTokenSchema(
            ShareTokenSchema::BIZ_TYPE_CLERK_INVITE,
            $storeId,
            'pages/clerk/registerForm',
            $share
        );
    }


    /**
     * 患者扫医生码出现的列表
     * @param $tokenId
     * @param $keyword
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function scanDoctorQrStoreList($tokenId, $keyword, $page, $pageSize)
    {
        $share = AgentService::getShareLinkFromShareId($tokenId);
        if (!($share->getType() == SHARE_TYPE::SHARE_AGENT_DOCTOR && $share->getDoctorId() > 0)) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, "二维码信息缺失");
        }
        $doctorId = $share->getDoctorId();
        $productIds = DoctorProductRepository::getProductIdsByDoctorId($doctorId)
            ->pluck('yy_product_id');
        if (empty($productIds)) {
            return [
                'list' => [],
                'total' => 0,
                'page' => $page,
                'page_size' => $pageSize
            ];
        }

        if ($productIds || $keyword) {
            if ($keyword) {
                $keywords = $keyword;
                $type = 0;
            } else {
                $keywords = ProductRepository::getListByProductIds($productIds)
                    ->pluck('name')->all();
                $type = 1;
            }
            $user = Helper::getLoginUser();
            $userId = $user->id;
            self::addKeyword($userId, $keywords, $type);
        }
        return $this->storeList(0, $productIds, $keyword, $pageSize);
    }

    /**
     * 患者直接进入展示的药店列表
     * @param $cityId
     * @param $departId
     * @param $keyword
     * @param $pageSize
     * @return array
     */
    public function searchStoreList($cityId, $departId, $keyword, $pageSize)
    {
        $productIds = [];
        if ($departId) {
            $productIds = DepartProductRepository::getProductIdsByDepartId($departId)
                ->pluck('yy_product_id');
        }

        if ($keyword) {
            $user = Helper::getLoginUser();
            $userId = $user->id;
            self::addKeyword($userId, $keyword);
        }
        return $this->storeList($cityId, $productIds, $keyword, $pageSize);
    }

    /**
     * 药店详情
     * @param $storeId
     * @return mixed
     */
    public function getStoreDetail($storeId)
    {
        $list = BranchStoreRepository::getListByIds(Arr::wrap($storeId));
        if ($list->count()) {
            $productMap = ProductRepository::getListByAll()
                ->pluck('name', 'yy_product_id')
                ->toArray();

            $list = $list->map(function ($row) use ($productMap) {
                $proInfo = AreaProvinceRepository::getProvinceById($row->pro_id);
                $cityInfo = CityRepository::getInfoById($row->city_id);
                $row->province_name = $proInfo->province;
                $row->city_name = $cityInfo->city_name;
                $yyProductIds = [];
                $row->products = $row->products->map(function ($pInfo) use ($productMap, &$yyProductIds) {
                    $pInfo->product_name = $productMap[$pInfo['yy_product_id']]??'';
                    $yyProductIds[] = $pInfo['yy_product_id'];
                });
                $mainStoreId = $row->main_store_id;
                $storeId = $row->store_id;
                // 药店没有设置药品，不分配优惠券
                if (empty($yyProductIds)) {
                    $row->is_policy = CouponRepository::storeHasPolicy($mainStoreId, $storeId, $yyProductIds)?1:0;
                } else {
                    $row->is_policy = 0;
                }

                return $row;
            });
        }
        return $list->first();
    }

    private function storeList($cityId, $productIds, $keyword, $pageSize)
    {
        $pageSize = (int)$pageSize;
        $res = BranchStoreRepository::searchStoreList($cityId, $productIds, $keyword, $pageSize);
        if ($res->total() > 0) {
            $productMap = ProductRepository::getListByAll()
                ->pluck('name', 'yy_product_id')
                ->toArray();
            $mainStoreIds = collect($res->items())
                ->pluck('main_store_id')->unique()
                ->toArray();
            $storeIds = collect($res->items())
                ->pluck('store_id')->unique()
                ->toArray();
            $yyProductIds = [];
            collect($res->items())->each(function ($row) use (&$yyProductIds) {
                collect($row['products'])->each(function ($p) use (&$yyProductIds) {
                    $yyProductIds[] = $p['yy_product_id'];
                });
            });
            // 适用所有门店的相关联的商品优惠券
            $productCouponKv = CouponRepository::getProductCouponKv(array_unique($yyProductIds));
            // 适用连锁门店的相关联的商品优惠券
            $mainStoreCouponKv = CouponRepository::getMainStoreCouponKv(array_unique($mainStoreIds));
            // 适用具体门店的相关联的商品优惠券
            $storeCouponKv = CouponRepository::getStoreCouponKv(array_unique($storeIds));
            $res->getCollection()->transform(function ($row)
 use ($productMap, $productCouponKv, $mainStoreCouponKv, $storeCouponKv) {
                $isPolicy = 0;
                $row->products = $row->products->map(function ($pInfo)
 use ($row, $productMap, $productCouponKv, $mainStoreCouponKv, $storeCouponKv, &$isPolicy) {
                    $pInfo->product_name = $productMap[$pInfo['yy_product_id']]??'';
                    if ($isPolicy == 0 && $productCouponKv
                            ->where('yy_product_id', $pInfo['yy_product_id'])
                            ->count()) {
                        $isPolicy = 1;
                    }
                    if ($isPolicy == 0 && $mainStoreCouponKv
                            ->where('main_store_id', $row['main_store_id'])
                            ->where('yy_product_id', $pInfo['yy_product_id'])
                            ->count()) {
                        $isPolicy = 1;
                    }
                    if ($isPolicy == 0 && $storeCouponKv
                            ->where('store_id', $row['id'])
                            ->where('yy_product_id', $pInfo['yy_product_id'])
                            ->count()) {
                        $isPolicy = 1;
                    }
                });
                $row->is_policy = $isPolicy;
                return $row;
            });
        }
        return [
            'list' => $res->items(),
            'total' => $res->total(),
            'page' => $res->currentPage(),
            'page_size' => $pageSize
        ];
    }

    public static function addKeyword($userId, $keywords, $type = 0)
    {
        if (empty($keywords)) {
            return;
        }
        go(function () use ($userId, $keywords, $type) {
            UserKeywordRepository::batchAddKeyword($userId, $keywords, $type);
        });
    }

    public function getStoreGpsInfo($branchStoreId): array
    {
        $store = BranchStoreRepository::getStoreInfoById($branchStoreId);
        if (!$store) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '药店不存在');
        }

        $gps = BranchStoreGpsRepository::getStoreGps($store);
        if ($gps) {
            return [$gps->latitude, $gps->longitude];
        } else {
            //使用
            return self::getStoreAmapGps($store);
        }
    }

    private static function getStoreAmapGps(BranchStore $store): array
    {
        //获取缓存
        $gpsInfo = StoreCacheService::getStoreGps($store->id);
        if (empty($gpsInfo)) {
            $cityCode = '';
            if ($store->city_id) {
                $city = CityRepository::getInfoById($store->city_id);
                $cityCode = $city ? $city->number : '';
            }
            //调用高德api获取gps信息
            $amap = new AmapService();
            $loc = $amap->geoLocation($store->location, $cityCode);
            if (empty($loc)) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '无法定位药店gps');
            }
            $location = $loc[0]['location'];
            [$longtitude, $latitude] = explode(',', $location);
            StoreCacheService::setStoreGps($store->id, $longtitude, $latitude);
            return [$latitude, $longtitude];
        } else {
            [$longtitude, $latitude] = $gpsInfo;
            return [$latitude, $longtitude];
        }
    }
}
