<?php
declare(strict_types=1);
namespace App\Service\Store;

use App\Helper\Helper;

class StoreCacheService
{
    public static function getStoreGps($storeId)
    {
        $key = "pharmacy:store:local:gps";
        $field = sprintf("store:%d", $storeId);
        $res = Helper::getRedis()->hGet($key, $field);
        if (false === $res) {
            return [];
        } else {
            return json_decode($res, true);
        }
    }

    public static function setStoreGps($storeId, $longitude, $latitude, $ttl = 3600 * 24)
    {
        $key = "pharmacy:store:local:gps";
        $field = sprintf("store:%d", $storeId);
        $redis = Helper::getRedis();
        $redis->hSet(
            $key,
            $field,
            json_encode(
                [$longitude, $latitude],
                JSON_UNESCAPED_SLASHES |JSON_UNESCAPED_UNICODE
            )
        );
        $redis->expire($key, $ttl);
    }
}
