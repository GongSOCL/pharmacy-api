<?php
declare(strict_types=1);

namespace App\Service\Store;

use App\Constants\AppErr;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Repository\BranchStoreGpsRepository;
use App\Repository\BranchStoreProductRepository;
use App\Repository\BranchStoreRepository;
use App\Repository\CityRepository;
use App\Repository\CouponRepository;
use App\Repository\DepartProductRepository;
use App\Repository\ProductRepository;
use App\Service\Agent\AgentService;
use App\Service\Common\AmapService;
use App\Service\Doctor\DoctorGroupRpcService;
use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Collection;

class StoreGpsService
{
    // 店铺地理位置缓存
    const PHARMACY_GPS_KEY = 'pharmacy:branch_store_gps';
    // 城市店铺地理位置缓存
    const PHARMACY_CITY_GPS_KEY = 'pharmacy:branch_store_gps:city:';
    // 城市店铺地理位置缓存
    const PHARMACY_CITY_GPS_CACHE = 'pharmacy:cache:branch_store_gps:city:%s:depart:%s';
    // 当前所在城市
    const PHARMACY_CURRENT_CITY = 'pharmacy:cache:current:user:%s:city:%s';
    // geo radius米单位
    const UNIT = 'm';
    // geo radius千米单位
    const UNIT_KM = 'km';
    private $redis;
    public function __construct()
    {
        $this->redis = ApplicationContext::getContainer()
            ->get(RedisFactory::class)->get('gps');
    }

    /**
     * 获取当前城市或科室信息
     * @param $longitude
     * @param $latitude
     * @param $tokenId
     * @return array
     */
    public function getCurrentGeoPosAndDepart($longitude, $latitude, $tokenId)
    {
        $user = Helper::getLoginUser();
        $currentInfo = [
            'city'=>[],
            'depart'=>[],
            'is_scan'=>0
        ];
        $geoInfo = ApplicationContext::getContainer()
            ->get(AmapService::class)
            ->reverseCoordinate($longitude, $latitude);
        if (!isset($geoInfo['addressComponent'])) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '地理位置错误');
        }
        $addressComponent = $geoInfo['addressComponent'];
        $cityName = $addressComponent['city'];
        if (empty($cityName)) {
            $cityName = $addressComponent['province'];
        }
        if (empty($cityName)) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '地理位置错误');
        }
        $cityInfo = CityRepository::getInfoByName($cityName);
        if ($cityInfo) {
            $currentInfo['city'] = [
                'id'=>$cityInfo->id,
                'name'=>$cityInfo->city_name,
            ];
            $this->redis->setex(
                sprintf(
                    self::PHARMACY_CURRENT_CITY,
                    $user->id,
                    $cityInfo->id
                ),
                86400,
                $cityInfo->id
            );
        }
        if ($tokenId) {
            $share = AgentService::getShareLinkFromShareId($tokenId);
            if (!$share->getDoctorId()) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, "二维码信息缺失");
            }
            $doctorId = $share->getDoctorId();
            $doctorInfo = DoctorGroupRpcService::getDoctorInfo($doctorId);
            if (!$doctorInfo->getDepartId()) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, "科室信息异常");
            }
            $currentInfo['depart'] = [
                'id'=>$doctorInfo->getDepartId(),
                'name'=>$doctorInfo->getDepartName(),
            ];
            $currentInfo['is_scan'] = 1;
        } else {
            $currentInfo['depart'] = [
                'id'=>0,
                'name'=>'全部',
            ];
        }
        return $currentInfo;
    }

    /**
     * 附近的药店
     * @param $longitude
     * @param $latitude
     * @param $range
     * @return array|\Hyperf\Utils\Collection
     */
    public function nearStoreList($longitude, $latitude, $range)
    {
        $key = self::PHARMACY_GPS_KEY;
        $unit = self::UNIT;
        $nearStores = $this->redis->georadius(
            $key,
            (float)$longitude,
            (float)$latitude,
            (float)$range,
            $unit,
        );
        if (empty($nearStores)) {
            return ['list'=>[]];
        }
        $storeIds = collect($nearStores)->map(function ($storeInfo) {
            return explode('-', $storeInfo)[1];
        })->unique();
        $branchStoreList = BranchStoreRepository::getListByIds($storeIds);
        $gspMap = BranchStoreGpsRepository::getListByStoreIds($storeIds)
            ->groupBy('store_id');
        $productMap = ProductRepository::getListByAll()
            ->pluck('name', 'yy_product_id')
            ->toArray();
        $yyProductIds = [];
        $branchStoreList->each(function ($row) use (&$yyProductIds) {
            collect($row['products'])->each(function ($p) use (&$yyProductIds) {
                $yyProductIds[] = $p['yy_product_id'];
            });
        });
        $mainStoreIds = $branchStoreList
            ->pluck('main_store_id')->unique()
            ->toArray();
        $branchStoreIds =$branchStoreList
            ->pluck('id')->unique()
            ->toArray();
        // 适用所有门店的相关联的商品优惠券
        $productCouponKv = CouponRepository::getProductCouponKv(array_unique($yyProductIds));
        // 适用连锁门店的相关联的商品优惠券
        $mainStoreCouponKv = CouponRepository::getMainStoreCouponKv(array_unique($mainStoreIds));
        // 适用具体门店的相关联的商品优惠券
        $storeCouponKv = CouponRepository::getStoreCouponKv(array_unique($branchStoreIds));
        $res = $branchStoreList->map(function ($storeInfo)
 use ($gspMap, $productMap, $longitude, $latitude, $productCouponKv, $mainStoreCouponKv, $storeCouponKv) {
            $gspList = $gspMap[$storeInfo['id']]??[];
            $isPolicy = 0;
            if (empty($gspList)) {
                $storeInfo['dist'] = '999999999';
                $storeInfo['gps'] = [];
            } else {
                $gspInfo = $gspList[0];
                $storeInfo['dist'] = $this->calculateDistance(
                    $longitude,
                    $latitude,
                    $gspInfo['longitude'],
                    $gspInfo['latitude']
                );
                $storeInfo['gps'] = [$gspInfo];
            }
            $storeInfo['products'] = collect($storeInfo['products'])->map(function ($pInfo)
 use ($productMap, $productCouponKv, $mainStoreCouponKv, $storeCouponKv, $storeInfo, &$isPolicy) {
                $pInfo['product_name'] = $productMap[$pInfo['yy_product_id']]??'';
                if ($isPolicy == 0 && $productCouponKv
                        ->where('yy_product_id', $pInfo['yy_product_id'])
                        ->count()) {
                    $isPolicy = 1;
                }
                if ($isPolicy == 0 && $mainStoreCouponKv
                        ->where('main_store_id', $storeInfo['main_store_id'])
                        ->where('yy_product_id', $pInfo['yy_product_id'])
                        ->count()) {
                    $isPolicy = 1;
                }
                if ($isPolicy == 0 && $storeCouponKv
                        ->where('store_id', $storeInfo['id'])
                        ->where('yy_product_id', $pInfo['yy_product_id'])
                        ->count()) {
                    $isPolicy = 1;
                }
                return $pInfo;
            });
            $storeInfo['is_policy'] = $isPolicy;
            return $storeInfo;
        });
        return ['list'=>array_values($res->sortBy('dist')->toArray())];
    }

    /**
     * 直接通过cityId获取所有药店信息列表
     * @param $cityId
     * @param $departId
     * @param $longitude
     * @param $latitude
     * @param $isScan
     * @param $page
     * @param int $pageSize
     * @return array
     */
    public function geoStoreList(
        $cityId,
        $departId,
        $longitude,
        $latitude,
        $isScan,
        $page,
        $pageSize = 20
    ) {
        $user = Helper::getLoginUser();
        $isCurrentGeo = $this->redis->get(
            sprintf(
                self::PHARMACY_CURRENT_CITY,
                $user->id,
                $cityId
            )
        );
        if (!$isCurrentGeo) {
            return [
                'list'=>[],
                'page'=>$page,
                'page_size'=>$pageSize,
                'total'=>0,
                'msg'=>'选择的城市非当前城市'
            ];
        }
        // 如果是扫码进入的departId必填
        $yyProductIds = [];
        if ($isScan) {
            if (empty($departId)) {
                return [
                    'list'=>[],
                    'page'=>$page,
                    'page_size'=>$pageSize,
                    'total'=>0
                ];
            }
            $yyProductIds = DepartProductRepository::getProductIdsByDepartId($departId)
                ->pluck('yy_product_id')->toArray();
            // 扫码科室关联商品必须存在，否则返回
            if (empty($yyProductIds)) {
                return [
                    'list'=>[],
                    'page'=>$page,
                    'page_size'=>$pageSize,
                    'total'=>0
                ];
            }
            // 扫码进入获取产品信息放入搜索历史中
            $keywords = ProductRepository::getListByProductIds($yyProductIds)
                ->pluck('name')->all();
            $type = 1;
            StoreService::addKeyword($user->id, $keywords, $type);
        } else {
            // 患者自行进入药店，如果选择了科室
            if ($departId) {
                $yyProductIds = DepartProductRepository::getProductIdsByDepartId($departId)
                    ->pluck('yy_product_id')->toArray();
            }
        }
        // 分页数据计算
        $start = ($page -1) * $pageSize;
        $end = $start + $pageSize;
        $cacheStoreList = $this->redis->get(sprintf(self::PHARMACY_CITY_GPS_CACHE, $cityId, $departId));
        if ($cacheStoreList) {
            $branchStoreList = json_decode($cacheStoreList, true);
            $branchStoreList = collect($branchStoreList);
        } else {
            $branchStoreList = BranchStoreRepository::geoStoreListByCityIdAndProductIds($cityId, $yyProductIds);
            $this->redis->setex(
                sprintf(self::PHARMACY_CITY_GPS_CACHE, $cityId, $departId),
                1800,
                json_encode($branchStoreList->toArray())
            );
        }

        $total = $branchStoreList->count();
        // 起始点超过总数量
        if (($start > $total) || $total <= 0) {
            return [
                'list'=>[],
                'page'=>$page,
                'page_size'=>$pageSize,
                'total'=>0
            ];
        }
        // 结束点超过总数量
        if ($end > $total) {
            $end = $total;
        }

        $gpsMap = BranchStoreGpsRepository::getListByStoreIds($branchStoreList->pluck('id'))
            ->groupBy('store_id');
        $storeList = $branchStoreList->map(function ($row)
 use ($gpsMap, $longitude, $latitude) {
            $gspList = $gpsMap[$row['id']]??[];
            if (empty($gspList)) {
                $row['dist'] = '999999999';
                $row['gps'] = [];
            } else {
                $gspInfo = $gspList[0];
                $row['dist'] = $this->calculateDistance(
                    $longitude,
                    $latitude,
                    $gspInfo['longitude'],
                    $gspInfo['latitude']
                );
                $row['gps'] = [$gspInfo];
            }
            return $row;
        });
        // 根据距离升序排列
        $storeList = $storeList->sortBy('dist')->values()->toArray();
        unset($branchStoreList);
        // 分页数据
        $pageStoreList = [];
        for ($i = $start; $i < $end; $i++) {
            $pageStoreList[] = $storeList[$i];
        }
        unset($storeList);
        $storeIds = array_column($pageStoreList, 'id');
        $yyProductIds = array_column($pageStoreList, 'yy_product_id');
        $mainStoreIds = array_column($pageStoreList, 'main_store_id');
        // 店铺药品列表
        $storeProductMap = BranchStoreProductRepository::getStoreProductsByStoreIds(array_unique($storeIds))
            ->groupBy('store_id');
        // 适用所有门店的相关联的商品优惠券
        $productCouponKv = CouponRepository::getProductCouponKv(array_unique($yyProductIds));
        // 适用连锁门店的相关联的商品优惠券
        $mainStoreCouponKv = CouponRepository::getMainStoreCouponKv(array_unique($mainStoreIds));
        // 适用具体门店的相关联的商品优惠券
        $storeCouponKv = CouponRepository::getStoreCouponKv(array_unique($storeIds));
        $pageStoreList = collect($pageStoreList)->map(function ($row)
 use ($storeProductMap, $productCouponKv, $mainStoreCouponKv, $storeCouponKv) {
            $isPolicy = 0;
            /** @var Collection $pList */
            $pList = $storeProductMap[$row['id']]??[];
            $row['products'] = $pList;
            // 计算当前药店是否存在礼券优惠政策
            foreach ($pList as $pInfo) {
                if ($isPolicy == 0 && $productCouponKv
                        ->where('yy_product_id', $pInfo['yy_product_id'])
                        ->count()) {
                    $isPolicy = 1;
                }
                if ($isPolicy == 0 && $mainStoreCouponKv
                        ->where('main_store_id', $row['main_store_id'])
                        ->where('yy_product_id', $pInfo['yy_product_id'])
                        ->count()) {
                    $isPolicy = 1;
                }
                if ($isPolicy == 0 && $storeCouponKv
                        ->where('store_id', $row['id'])
                        ->where('yy_product_id', $pInfo['yy_product_id'])
                        ->count()) {
                    $isPolicy = 1;
                }
                if ($isPolicy == 1) {
                    break;
                }
            }
            $row['dist_km'] = $this->distPretty($row['dist']);
            $row['is_policy'] = $isPolicy;
            return $row;
        });
        return [
            'list'=>$pageStoreList,
            'page'=>$page,
            'page_size'=>$pageSize,
            'total'=>$total
        ];
    }

    /**
     * 通过redis获取一定范围的某城市的药店信息列表
     * @param $cityId
     * @param $departId
     * @param $longitude
     * @param $latitude
     * @param $range
     * @param $isScan
     * @param $page
     * @param int $pageSize
     * @return array
     */
    public function geoStoreListByRedis(
        $cityId,
        $departId,
        $longitude,
        $latitude,
        $range,
        $isScan,
        $page,
        $pageSize = 20
    ) {
        $cityKey = self::PHARMACY_CITY_GPS_KEY.$cityId;
        $unit = self::UNIT;
        // 获取定位数据
        $nearStores = $this->redis->georadius(
            $cityKey,
            (float)$longitude,
            (float)$latitude,
            (float)$range,
            $unit,
        );

        if (empty($nearStores)) {
            return [
                'list'=>[],
                'page'=>$page,
                'page_size'=>$pageSize,
                'total'=>0
            ];
        }
        // 如果是扫码进入的departId必填
        $yyProductIds = [];
        if ($isScan) {
            if (empty($departId)) {
                return [
                    'list'=>[],
                    'page'=>$page,
                    'page_size'=>$pageSize,
                    'total'=>0
                ];
            }
            $yyProductIds = DepartProductRepository::getProductIdsByDepartId($departId)
                ->pluck('yy_product_id')->toArray();
            // 扫码科室关联商品必须存在，否则返回
            if (empty($yyProductIds)) {
                return [
                    'list'=>[],
                    'page'=>$page,
                    'page_size'=>$pageSize,
                    'total'=>0
                ];
            }
        } else {
            // 患者自行进入药店，如果选择了科室
            if ($departId) {
                $yyProductIds = DepartProductRepository::getProductIdsByDepartId($departId)
                    ->pluck('yy_product_id')->toArray();
            }
        }
        // 分页数据计算
        $start = ($page -1) * $pageSize;
        $end = $start + $pageSize;

        // 获取当前距离的数据值列表
        $storeIds = collect($nearStores)->map(function ($storeInfo) {
            return explode('-', $storeInfo)[1];
        })->unique();
        $branchStoreList = BranchStoreRepository::geoStoreListByIdsAndProductIds($storeIds, $yyProductIds);
        $total = $branchStoreList->count();
        // 起始点超过总数量
        if (($start > $total) || $total <= 0) {
            return [
                'list'=>[],
                'page'=>$page,
                'page_size'=>$pageSize,
                'total'=>0
            ];
        }
        // 结束点超过总数量
        if ($end > $total) {
            $end = $total;
        }

        $gpsMap = BranchStoreGpsRepository::getListByStoreIds($storeIds)
            ->groupBy('store_id');
        $storeList = $branchStoreList->map(function ($row)
 use ($gpsMap, $longitude, $latitude) {
            $gspList = $gpsMap[$row['id']]??[];
            if (empty($gspList)) {
                $row['dist'] = '999999999';
                $row['gps'] = [];
            } else {
                $gspInfo = $gspList[0];
                $row['dist'] = $this->calculateDistance(
                    $longitude,
                    $latitude,
                    $gspInfo['longitude'],
                    $gspInfo['latitude']
                );
                $row['gps'] = [$gspInfo];
            }
            return $row;
        });
        // 根据距离升序排列
        $storeList = $storeList->sortBy('dist')->values()->toArray();
        unset($branchStoreList);
        // 分页数据
        $pageStoreList = [];
        for ($i = $start; $i < $end; $i++) {
            $pageStoreList[] = $storeList[$i];
        }
        unset($storeList);
        $storeIds = array_column($pageStoreList, 'id');
        $yyProductIds = array_column($pageStoreList, 'yy_product_id');
        $mainStoreIds = array_column($pageStoreList, 'main_store_id');
        // 店铺药品列表
        $storeProductMap = BranchStoreProductRepository::getStoreProductsByStoreIds(array_unique($storeIds))
            ->groupBy('store_id');
        // 适用所有门店的相关联的商品优惠券
        $productCouponKv = CouponRepository::getProductCouponKv(array_unique($yyProductIds));
        // 适用连锁门店的相关联的商品优惠券
        $mainStoreCouponKv = CouponRepository::getMainStoreCouponKv(array_unique($mainStoreIds));
        // 适用具体门店的相关联的商品优惠券
        $storeCouponKv = CouponRepository::getStoreCouponKv(array_unique($storeIds));
        $pageStoreList = collect($pageStoreList)->map(function ($row)
 use ($storeProductMap, $productCouponKv, $mainStoreCouponKv, $storeCouponKv) {
            $isPolicy = 0;
            /** @var Collection $pList */
            $pList = $storeProductMap[$row['id']]??[];
            $row['products'] = $pList;
            // 计算当前药店是否存在礼券优惠政策
            foreach ($pList as $pInfo) {
                if ($isPolicy == 0 && $productCouponKv
                        ->where('yy_product_id', $pInfo['yy_product_id'])
                        ->count()) {
                    $isPolicy = 1;
                }
                if ($isPolicy == 0 && $mainStoreCouponKv
                        ->where('main_store_id', $row['main_store_id'])
                        ->where('yy_product_id', $pInfo['yy_product_id'])
                        ->count()) {
                    $isPolicy = 1;
                }
                if ($isPolicy == 0 && $storeCouponKv
                        ->where('store_id', $row['id'])
                        ->where('yy_product_id', $pInfo['yy_product_id'])
                        ->count()) {
                    $isPolicy = 1;
                }
                if ($isPolicy == 1) {
                    break;
                }
            }
            $row['dist_km'] = $this->distPretty($row['dist']);
            $row['is_policy'] = $isPolicy;
            return $row;
        });
        return [
            'list'=>$pageStoreList,
            'page'=>$page,
            'page_size'=>$pageSize,
            'total'=>$total
        ];
    }



    /**
     * 计算距离
     * @param $lat1
     * @param $lng1
     * @param $lat2
     * @param $lng2
     * @return float
     */
    private function calculateDistance($lat1, $lng1, $lat2, $lng2): float
    {
        $earthRadius = 6367000; //approximate radius of earth in meters

        $lat1 = ($lat1 * pi() ) / 180;
        $lng1 = ($lng1 * pi() ) / 180;

        $lat2 = ($lat2 * pi() ) / 180;
        $lng2 = ($lng2 * pi() ) / 180;

        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);
        $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;
        return round($calculatedDistance);
    }

    private function distPretty($dist)
    {
        return $dist ? round(bcdiv((string)$dist, '1000', 2), 1):0;
    }
}
