<?php
declare(strict_types=1);
namespace App\Service\Wechat;

use App\Constants\AppErr;
use App\Constants\ErrorWechat;
use App\Exception\BusinessException;
use App\Exception\WechatException;
use App\Helper\AccessTokenProvider;
use App\Helper\EasyWechatCache;
use App\Helper\Helper;
use EasyWeChat\Factory;
use EasyWeChat\Kernel\Exceptions\InvalidConfigException;
use EasyWeChat\OfficialAccount\Application;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\HandlerStack;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Di\Container;
use Hyperf\Guzzle\CoroutineHandler;
use Hyperf\HttpMessage\Upload\UploadedFile;
use Hyperf\HttpServer\Contract\RequestInterface;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Request;
use Hyperf\Di\Annotation\Inject;

/**
 * 微信功能类
 * Class WechatObjectService
 * @package App\Service\Wechat
 */
class WechatObjectService
{
    //微信配置，参数参考:https://easywechat.com/5.x/mini-program/index.html
    private $config;

    /**
     * @var Application
     */
    private $officialAccount;

    /**
     * @var \EasyWeChat\MiniProgram\Application
     */
    private $miniApp;

    /**
     * @Inject
     * @var RequestInterface
     */
    private $hyperfRequest;

    private $channelAlias = "";

    public function __construct(Container $container)
    {
        $config = $container->get(ConfigInterface::class);
        $cfg = $config->get('wechat.pharmacy.config');
        if (!($cfg['app_id'] && $cfg['secret'])) {
            throw new WechatException(ErrorWechat::WECHAT_BUSINESS_EXCEPTIN, "微信配置不存在");
        }
        $this->channelAlias = (string) $config->get('wechat.pharmacy.channel');
        $this->config = $cfg;
    }

    private function getMiniApp($rebindRequest = false): \EasyWeChat\MiniProgram\Application
    {
        $originApp = $this->miniApp;
        if (!$originApp) {
            $originApp = $this->initMiniApp();
        }
        $app = clone  $originApp;
        if ($rebindRequest) {
            $app->rebind('request', $this->buildRequestFromHyperf());
        }
        $app->rebind('logger', Helper::getLogger());

        return $app;
    }

    private function buildRequestFromHyperf(): Request
    {
        $get = $this->hyperfRequest->getQueryParams();
        $post = $this->hyperfRequest->getParsedBody();
        $cookie = $this->hyperfRequest->getCookieParams();
        $uploadFiles = $this->hyperfRequest->getUploadedFiles() ?? [];
        $server = $this->hyperfRequest->getServerParams();
        $xml = $this->hyperfRequest->getBody()->getContents();
        $files = [];
        /** @var UploadedFile $v */
        foreach ($uploadFiles as $k => $v) {
            $files[$k] = $v->toArray();
        }
        $request = new Request($get, $post, [], $cookie, $files, $server, $xml);
        $request->headers = new HeaderBag($this->hyperfRequest->getHeaders());

        return $request;
    }

    /**
     * 检查微信调用结果
     * @param $result
     * @param $api
     * @param array $data
     */
    private static function checkResult($result, $api, $data = [])
    {
        if (isset($result['errcode']) && $result['errcode'] && $result['errcode'] != 0) {
            Helper::getLogger()->error("wechatobject.checkResult", [
                'api' => $api,
                'result' => $result,
                'data' => $data
            ]);
            throw WechatException::newFromCode($result['errcode'], $result['errmsg'] ?? '', $api, $data);
        }
    }


    private function initMiniApp(): \EasyWeChat\MiniProgram\Application
    {
        $app = Factory::miniProgram([
            'app_id' => $this->config['app_id'],
            'secret' => $this->config['secret']
        ]);
        $handler = new CoroutineHandler();

        // 设置 HttpClient，部分接口直接使用了 http_client。
        $config = $app['config']->get('http', []);
        $config['handler'] = $stack = HandlerStack::create($handler);
        $app->rebind('http_client', new Client($config));

        // 部分接口在请求数据时，会根据 guzzle_handler 重置 Handler
        $app['guzzle_handler'] = $handler;
        $app->rebind('cache', new EasyWechatCache());
        if ($this->channelAlias) {
            $channelAlias = $this->channelAlias;
            $app['access_token'] = function ($app) use ($channelAlias) {
                return  AccessTokenProvider::newInstance($app, $channelAlias);
            };
        }
        $this->miniApp = $app;

        return $app;
    }

    public function getSessionFromCode($code)
    {
        $res = $this->getMiniApp()->auth->session($code);
        self::checkResult($res, 'code2session', ['code' => $code]);
        return $res;
    }

    public function getAccessToken(): array
    {
        return $this->getMiniApp()->access_token->getToken();
    }

    /**
     * 下载schea码并保存到本地文件
     * @param array $params
     * @param $path
     * @return string
     * @throws InvalidConfigException
     * @throws GuzzleException
     */
    public function getScheme(array $params, $path, $page, $width = 430): string
    {
        $envVersion = env('MINIAPP_ENV_VERSION', 'develop');
        $query = http_build_query($params);
        $token = $this->getAccessToken();
        $data  = [
            'query' => ['access_token' => $token['access_token']],
            'json' => [
                'scene' => $query,
                'page' => $page,
                'check_path' => false,
                'env_version' => $envVersion,
                'width' => $width
            ]
        ];
        $res= $this->getMiniApp()->url_scheme->request('wxa/getwxacodeunlimit', 'POST', $data, true);
        $contentType = $res->getHeader('content-type');
        if (empty($contentType)) {
            throw new WechatException(100000, '微信服务响应异常');
        }
        $contentType = trim($contentType[0]);
        $isJsonResponse = stripos($contentType, 'application/json') !== false;
        if ($res->getStatusCode() == '200') {
            if ($isJsonResponse) {
                //异常
                $body = json_decode($res->getBody()->getContents(), true);
                self::checkResult($body, 'generateunlimitedschema');
            } elseif (stripos($contentType, 'image') !== false) {
                //正常返回文件
                $saveFd = fopen($path, 'wb');
                while (!$res->getBody()->eof()) {
                    fwrite($saveFd, $res->getBody()->read(4096));
                }
                fclose($saveFd);
                return $contentType;
            } else {
                //响应异常
                throw new WechatException(100000, '微信服务响应异常');
            }
            throw new WechatException(10000, '微信接口响应异常');
        }


        if (!$isJsonResponse) {
            throw new WechatException(10000, '微信接口响应异常');
        }

        $body = json_decode($res->getBody()->getContents(), true);
        self::checkResult($body, 'generateunlimitedschema');
        return "";
    }

    public function getUrlLink($path, array $params = [], $isExpireInterval = true, $expireValue = 1)
    {
        $envVersion = env('MINIAPP_ENV_VERSION', 'develop');
        $query = !empty($params) ? http_build_query($params) : '';
        if (strlen($query) > 1024) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '参数超长');
        }
        $token = $this->getAccessToken();
        $req = [
            'path' => $path,
            'query' => $query,
            'env_version' => $envVersion,
            'expire_type' => $isExpireInterval ? 1 : 0,
            'expire_interval' => $expireValue
        ];
        if ($isExpireInterval) {
            $req['expire_interval'] = $expireValue;
            if ($expireValue > 30) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '链接过期时间错误，最多允许30天');
            }
        } else {
            $req['expire_time'] = $expireValue;
            $interval = $expireValue - time();
            if ($interval) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '链接过期时间不能早于当前时间');
            }
            if ($interval > 30 * 24 * 3600) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '链接过期时间错误，最多允许30天');
            }
        }
        $data  = [
            'query' => ['access_token' => $token['access_token']],
            'json' => $req
        ];
        $res= $this->getMiniApp()->url_scheme->request('wxa/generate_urllink', 'POST', $data, false);
        self::checkResult($res, 'generate_url_link');
        return $res['url_link'];
    }
}
