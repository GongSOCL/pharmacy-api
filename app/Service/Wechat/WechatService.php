<?php
declare(strict_types=1);
namespace App\Service\Wechat;

use Hyperf\Utils\ApplicationContext;

class WechatService
{

    public static function generateUrlLink(
        $path = "",
        $params = [],
        $isExpireDaysInterval = true,
        $expireValue = 1
    ): string {
        return ApplicationContext::getContainer()
            ->get(WechatObjectService::class)
            ->getUrlLink($path, $params, $isExpireDaysInterval, $expireValue);
    }
}
