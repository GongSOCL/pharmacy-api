<?php
declare(strict_types=1);

namespace App\Service\Common;

use App\Constants\InsideMsgTypes;
use Google\Protobuf\Internal\Message;
use Grpc\InsideMsg\CountUnReadMsgReply;
use Grpc\InsideMsg\CountUnReadMsgRequest;
use Grpc\InsideMsg\CreateOneMsgReply;
use Grpc\InsideMsg\CreateOneMsgRequest;
use Grpc\InsideMsg\InsideMsgClient;
use Grpc\InsideMsg\InsideMsgListReply;
use Grpc\InsideMsg\InsideMsgListRequest;
use Grpc\InsideMsg\OneMsgDetailReply;
use Grpc\InsideMsg\OneMsgDetailRequest;
use Hyperf\Utils\ApplicationContext;
use Youyao\Framework\Client\CoreClient;

class InsideMsgRpcService extends CoreClient
{
    /**
     * @param CreateOneMsgRequest $argument
     * @return Message
     */
    public function createOneMsg(CreateOneMsgRequest $argument): Message
    {
        return $this->sendRequest($argument, [CreateOneMsgReply::class, 'decode']);
    }

    public function sendInsideMsg(
        $userRoleId,
        $msg,
        $title,
        $type = InsideMsgTypes::MSG_TYPE_CLERK_ORDER_FINISHED,
        $subTitle = '',
        $url = ''
    ) {
        $req = new CreateOneMsgRequest();
        $req->setTitle($title);
        $req->setType($type);
        $req->setSubTitle($subTitle ? $subTitle : $msg);
        $req->setBody($msg);
        $req->setToUserId($userRoleId);
        $req->setToUserType(InsideMsgTypes::USER_TYPE_PHARMACY_USER);
        $req->setLink($url);

        $this->createOneMsg($req);
    }

    public function fetchInsideMsg($userId, $current = 1, $limit = 10): InsideMsgListReply
    {
        $req = new InsideMsgListRequest();
        $req->setUserId($userId);
        $req->setPage($current);
        $req->setPageSize($limit);
        $req->setUserType(InsideMsgTypes::USER_TYPE_PHARMACY_USER);

        $reply = $this->sendRequest($req, [InsideMsgListReply::class, 'decode']);
        if (!$reply instanceof InsideMsgListReply) {
            throw new \Youyao\Framework\Exception\GrpcResponseException("响应异常，请稍后重试");
        }

        return $reply;
    }

    public function fetchOneMsgDetail($userId, $msgId): OneMsgDetailReply
    {
        $req = new OneMsgDetailRequest();
        $req->setUserId($userId);
        $req->setMsgId($msgId);
        $req->setUserType(InsideMsgTypes::USER_TYPE_PHARMACY_USER);
        $req->setPlatform(InsideMsgTypes::PLATFORM_PHARMACY_MINIAPP);

        $reply = $this->sendRequest($req, [OneMsgDetailReply::class, 'decode']);
        if (!$reply instanceof OneMsgDetailReply) {
            throw new \Youyao\Framework\Exception\GrpcResponseException("响应异常,请稍后重试");
        }

        return $reply;
    }

    public function countUnReadMsg($userId): CountUnReadMsgReply
    {
        $req = new CountUnReadMsgRequest();
        $req->setUserId($userId);
        $req->setUserType(InsideMsgTypes::USER_TYPE_PHARMACY_USER);

        $reply = $this->sendRequest($req, [CountUnReadMsgReply::class, 'decode']);
        if (!$reply instanceof CountUnReadMsgReply) {
            throw new \Youyao\Framework\Exception\GrpcResponseException("响应异常,请稍后重试");
        }

        return $reply;
    }
}
