<?php
declare(strict_types=1);
namespace App\Service\Common;

use Grpc\Activity\Task\FinishTaskRequest;
use Grpc\Activity\Task\FrontTaskSvcClient;
use Hyperf\Utils\ApplicationContext;

class TaskRpcService
{
    private static function getClient(): FrontTaskSvcClient
    {
        return ApplicationContext::getContainer()
            ->get(FrontTaskSvcClient::class);
    }

    public static function finishTask($token, $userId, $returnErr = false)
    {
        $req = new FinishTaskRequest();
        $req->setToken($token);
        $req->setUserType(3);
        $req->setUserId($userId);
        $req->setErrorWhenFinished($returnErr);
        self::getClient()->finishTask($req);
    }
}
