<?php
declare(strict_types=1);
namespace App\Service\Common;

use App\Constants\AppErr;
use App\Exception\BusinessException;
use App\Repository\VersionRepository;

class VersionService
{
    public static function getVersionInfo($version): \App\Model\Pharmacy\Version
    {
        $version = VersionRepository::getByVersion($version);
        if (!$version) {
            throw new BusinessException(AppErr::BUSINESSERR, '版本号不存在');
        }

        return $version;
    }
}
