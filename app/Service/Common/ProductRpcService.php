<?php
declare(strict_types=1);
namespace App\Service\Common;

use Grpc\Common\CommonProductClient;
use Grpc\Common\ListProductResponse;
use Grpc\Common\ListShowProduct;
use Grpc\Common\ProductIdRequest;
use Grpc\Common\ProductIdsRequest;
use Grpc\Common\ProductSkuListReply;
use Grpc\Common\SkuIdsRequest;
use Hyperf\Utils\ApplicationContext;

class ProductRpcService
{
    private static function getClient()
    {
        return ApplicationContext::getContainer()
            ->get(CommonProductClient::class);
    }

    public static function listProducts($onlyShow = true): ListProductResponse
    {
        $req = new ListShowProduct();
        $req->setOnlyIsShow($onlyShow);
        return self::getClient()->listShowProduct($req);
    }

    public static function listProductsByIds(array $ids): ListProductResponse
    {
        $req = new ProductIdsRequest();
        $req->setIds(array_unique($ids));
        return self::getClient()->listProductsByIds($req);
    }

    public static function listProductSkus($productId): ProductSkuListReply
    {
        $req = new ProductIdRequest();
        $req->setId($productId);
        return self::getClient()->listProductSkus($req);
    }

    public static function listSkuByIds(array $skuIds): ProductSkuListReply
    {
        $req = new SkuIdsRequest();
        $req->setId($skuIds);
        return self::getClient()->listSkuByIds($req);
    }
}
