<?php
declare(strict_types=1);
namespace App\Service\Common;

use App\Constants\Auth;
use App\Exception\AuthException;
use App\Helper\Helper;
use App\Model\Pharmacy\UserClerk;
use App\Repository\BranchStoreProductRepository;
use App\Repository\UserClerkRepository;
use App\Service\User\UserClerkService;
use Grpc\Common\ProductItem;
use Grpc\Common\ProductSkuItem;

class ProductService
{
    public static function getProducts(): array
    {
        $clerk = UserClerkService::checkAndGetClerk();
        $products = BranchStoreProductRepository::getStoreProducts($clerk->store_id);
        if ($products->isEmpty()) {
            return [];
        }
        $productIds = $products->pluck('yy_product_id')->unique()->all();
        $resp = ProductRpcService::listProductsByIds($productIds);
        return Helper::mapRepeatedField($resp->getProducts(), function ($product) {
            /** @var ProductItem $product */
            return [
                'id' => $product->getId(),
                'name' => $product->getName()
            ];
        }, []);
    }

    public static function listProductSkus($productId): array
    {
        $resp = ProductRpcService::listProductSkus($productId);
        if ($resp->getList()->count() == 0) {
            return [];
        }

        return Helper::mapRepeatedField($resp->getList(), function ($item) {
            /** @var ProductSkuItem $item */
            return [
                'id' => $item->getId(),
                'spec' => $item->getSpec()
            ];
        }, []);
    }
}
