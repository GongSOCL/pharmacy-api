<?php


namespace App\Service\Common;

use Google\Protobuf\GPBEmpty;
use Grpc\Token\GetTokenReply;
use Grpc\Token\GetTokenRequest;
use Grpc\Token\TokenManageSvcClient;
use Hyperf\Utils\ApplicationContext;

class TokenRpcService
{
    private static function getClient(): TokenManageSvcClient
    {
        return ApplicationContext::getContainer()
            ->get(TokenManageSvcClient::class);
    }

    public function getToken($channelAlias): GetTokenReply
    {
        $req = new GetTokenRequest();
        $req->setChannelAlias($channelAlias);
        return self::getClient()->GetToken($req);
    }

    public function generateUniqId(): int
    {
        $resp = self::getClient()->GenerateUniqId(new GPBEmpty());
        return $resp->getId();
    }
}
