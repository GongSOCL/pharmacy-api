<?php
declare(strict_types=1);

namespace App\Service\Common;

use Grpc\Msg\MSG_PROJECT;
use Hyperf\Utils\ApplicationContext;
use Youyao\Framework\Components\Msg\MessageRpcClient;

class SmsRpcService
{
    /**
     * 创建客户端
     * @return mixed|MessageRpcClient
     */
    private static function createClient()
    {
        return ApplicationContext::getContainer()
            ->get(MessageRpcClient::class);
    }

    /**
     * 发送短信验证码
     * @param $phone
     * @param $smsCode
     * @return string
     * @throws \Exception
     */
    public static function sendCode($phone, $smsCode)
    {
        $client = self::createClient();
        return $client->setProject(MSG_PROJECT::PROJECT_DOCTOR_API)
            ->sendCaptureCode($phone, $smsCode, '');
    }
}
