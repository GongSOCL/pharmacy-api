<?php
declare(strict_types=1);


namespace App\Service\Common;

use App\Constants\AppErr;
use App\Exception\BusinessException;
use Google\Protobuf\Any;
use Google\Protobuf\Internal\Message;
use Grpc\Task\Task;
use Hyperf\Nsq\Nsq;

/**
 * 任务的入队及反序列化
 */
class TaskManageService
{
    /**
     * @var Task
     */
    private $task;

    private $params;

    private $topic;

    //创建任务参数
    public static function create($taskId, Message $params, $topic = ""): TaskManageService
    {
        $o = new Task();
        $o->setTaskId($taskId);
        $o->setCount(1);
        $o->setRePush(false);
        $p = new Any();
        $p->setValue($params->serializeToString());
        $o->setParams((new Any())->setValue($params->serializeToString()));

        $t = new self();
        $t->task = $o;
        $t->params = $params;
        $t->topic = $topic;

        return $t;
    }

    //根据现有参数重新投递
    public function rePush(Message $params = null)
    {
        if (!$this->topic) {
            throw new \RuntimeException("topic不存在");
        }
        $this->task->setCount($this->task->getCount() + 1);
        $this->task->setRePush(true);
        if ($params) {
            $this->task->setParams(new Any($params->serializeToString()));
        }

        $this->push();
    }


    /**
     * 解析投递参数
     * @throws \Exception
     */
    public static function decode(string $data, $className = "", $topic = ""): TaskManageService
    {
        $o = new Task();
        $o->mergeFromString(hex2bin($data));

        $params = null;
        if ($className) {
            $params = new $className;
            if ($params instanceof Message) {
                $params->mergeFromString($o->getParams()->getValue());
            }
        }

        $t = new self();
        $t->task = $o;
        $t->params = $params;
        $t->topic = $topic;

        return $t;
    }

    public function getTask(): Task
    {
        return $this->task;
    }

    public function getTaskId()
    {
        return $this->task->getTaskId();
    }

    public function getParams()
    {
        return $this->params;
    }

    public function push()
    {
        $nsq = make(Nsq::class);
        $nsq->publish($this->topic, bin2hex($this->task->serializeToString()));
    }

    /**
     * @param mixed $topic
     */
    public function setTopic($topic): TaskManageService
    {
        $this->topic = $topic;
        return $this;
    }

    public function getCount(): int
    {
        return $this->task->getCount();
    }
}
