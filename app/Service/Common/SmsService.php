<?php
declare(strict_types=1);

namespace App\Service\Common;

use App\Constants\AppErr;
use App\Constants\RedisKey;
use App\Exception\BusinessException;
use App\Helper\Helper;
use Hyperf\Redis\Redis;
use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;

class SmsService
{
    // 1分钟禁止发送
    const PHONE_KEY_EXPIRE = 60;
    // 20分钟有效
    const CODE_KEY_EXPIRE = 1200;
    /**
     * @var Redis
     */
    private $redis;
    public function __construct()
    {
        $this->redis = ApplicationContext::getContainer()
            ->get(RedisFactory::class)->get('default');
    }

    /**
     * 发送验证码
     * @param $phone
     * @return array|string[]
     * @throws \Exception
     */
    public function sendCode($phone)
    {
        if (!$this->setRedisPhone($phone)) {
            throw new BusinessException(
                AppErr::BUSINESS_ERROR,
                '请等待60s之后发送'
            );
        }
        $verifyCode = $this->generateCode();
        if (!Helper::isOnline()) {
            $this->setRedisCode($phone, $verifyCode);
            return ['verify_code'=>$verifyCode];
        } else {
            SmsRpcService::sendCode($phone, $verifyCode);
            $this->setRedisCode($phone, $verifyCode);
            return ['verify_code'=>''];
        }
    }

    /**
     * 验证验证码正确
     * @param $phone
     * @param $code
     * @return int[]
     */
    public function verify($phone, $code)
    {
        $verifyCode = $this->getRedisCode($phone, $code);
        if (empty($verifyCode) ||
            $code != $verifyCode) {
            throw new BusinessException(
                AppErr::BUSINESS_ERROR,
                '验证错误或失效'
            );
        }
        return ['verify'=>1];
    }

    /**
     * 创建验证码
     * @return int
     */
    private function generateCode()
    {
        return mt_rand(100000, 999999);
    }

    /**
     * 设置同一个手机60s后再发送
     * @param $phone
     * @return bool
     */
    private function setRedisPhone($phone):bool
    {
        $phoneSuffix = 'phone:'.$phone;
        $phoneKey = RedisKey::CLERK_PHONE_VERIFY_KEY.$phoneSuffix;
        return $this->redis->set($phoneKey, 1, [
            'nx',
            'ex' => self::PHONE_KEY_EXPIRE
        ]);
    }

    /**
     * 设置验证码code
     * @param $phone
     * @param $code
     * @return bool
     */
    private function setRedisCode($phone, $code)
    {
        $codeSuffix = 'phone:%s:code';
        $codeKey = sprintf(RedisKey::CLERK_PHONE_VERIFY_KEY.$codeSuffix, $phone);
        return $this->redis->set($codeKey, $code, self::CODE_KEY_EXPIRE);
    }

    /**
     * 获取验证码
     * @param $phone
     * @param $code
     * @return bool|mixed|string
     */
    private function getRedisCode($phone, $code)
    {
        $codeSuffix = 'phone:%s:code';
        $codeKey = sprintf(RedisKey::CLERK_PHONE_VERIFY_KEY.$codeSuffix, $phone);
        return $this->redis->get($codeKey);
    }
}
