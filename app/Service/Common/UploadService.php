<?php
declare(strict_types=1);


namespace App\Service\Common;

use App\Constants\AppErr;
use App\Exception\BusinessException;
use EasyWeChat\Kernel\Support\Arr;
use Grpc\Upload\UploadFileItem;

class UploadService
{
    public static function getConfig($type, $scope, $business, $channel = 1): array
    {
        switch ($channel) {
            case 1:
                $resp = UploadRpcService::getToken($type, $scope, $business);
                return [
                    'path' => $resp->getPath(),
                    'token' => $resp->getToken(),
                    'expire_time' => $resp->getExpireTime(),
                    'bucket' => $resp->getBucket(),
                    'base_uri' => $resp->getBaseUri(),
                    'upload_uri' => $resp->getUploadUri(),
                    'channel' => 1
                ];
            case 3:
                $resp = UploadRpcService::getOssToken($type, $scope, $business);
                return [
                    'path' => $resp->getPath(),
                    'token' => $resp->getToken(),
                    'expire_time' => $resp->getExpireTime(),
                    'bucket' => $resp->getBucket(),
                    'base_uri' => $resp->getBaseUri(),
                    'upload_uri' => $resp->getUploadUri(),
                    'key' => $resp->getKey(),
                    'signature' => $resp->getSignature(),
                    'policy' => $resp->getPolicy(),
                    'channel' => 3
                ];
            default:
                throw new BusinessException(AppErr::BUSINESS_ERROR, "不支持该存储方式");
        }
    }

    public static function addUpload($bucket, $key, $name, $url, $channel = 1): array
    {
        $resp = UploadRpcService::addUploadRecord($key, $bucket, $name, $url, $channel);
        $res = json_decode($resp->serializeToJsonString(), true);
        $res['type'] = (int) $resp->getType();
        return $res;
    }

    public static function getUploadUriByIds(array $uploadIds): array
    {
        $resp = UploadRpcService::getByIds($uploadIds);
        $uploadMap = [];
        /** @var UploadFileItem $item */
        foreach ($resp->getList() as $item) {
            $uploadMap[$item->getId()] = $item;
        }

        return $uploadMap;
    }
}
