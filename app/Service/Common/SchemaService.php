<?php
declare(strict_types=1);
namespace App\Service\Common;

use App\Constants\AppErr;
use App\Exception\BusinessException;
use App\Helper\UploadHelper;
use App\Model\Pharmacy\ShareTokenSchema;
use App\Repository\ShareTokenSchemaRepository;
use App\Service\Wechat\WechatObjectService;
use Grpc\Upload\StorageChannel;
use Grpc\Upload\UploadFileItem;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Exception\ParallelExecutionException;
use Hyperf\Utils\Parallel;

/**
 * 业务小程序schema生成/存储/获取/解码
 */
class SchemaService
{
    /**
     * 判断业务schema是否存在
     * @param $bizType
     * @param $bizId
     * @return bool
     */
    public static function isExists($bizType, $bizId, $size = 430): bool
    {
        $resp = ShareTokenSchemaRepository::getBizSchema($bizType, $bizId, $size);
        return $resp && $resp->schema_id > 0;
    }

    /**
     * 获取schema图片
     * @param $bizType
     * @param $bizId
     * @return string
     */
    public static function getSchemaUploadImage($bizType, $bizId, $size = 430): string
    {
        $exists = ShareTokenSchemaRepository::getBizSchema($bizType, $bizId, $size);
        if (!($exists && $exists->schema_id > 0)) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '分享码不存在');
        }

        $uploadId = $exists->schema_id;

        $resp = UploadRpcService::getByIds([$uploadId]);
        if ($resp->getList()->count() == 0) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '上传文件不存在');
        }

        /** @var UploadFileItem $upItem */
        $upItem = $resp->getList()[0];
        return $upItem->getUrl();
    }

    /**
     * 新生成一条schema码记录并返回图片链接
     * @param string $token
     * @param $bizType
     * @param $bizId
     * @param string $page          //小程序跳转页面路径
     * @return string
     */
    public static function newSchemaRecord(
        string $token,
        $bizType,
        $bizId,
        $page,
        $size = 430,
        $channel = StorageChannel::CHANNEL_QINIU
    ): string {
        $schema = ShareTokenSchemaRepository::getBizSchema($bizType, $bizId, $size);
        if (!$schema) {
            $schema = ShareTokenSchemaRepository::newSchema($bizType, $bizId, $token, $size);
        }
        self::generateSchemaFromRecord($schema, $page, $size, $channel);
        $schema->refresh();

        $resp = UploadRpcService::getByIds([$schema->schema_id]);
        if ($resp->getList()->count() == 0) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, 'schema不存在');
        }
        /** @var UploadFileItem $item */
        $item = $resp->getList()[0];
        return $item->getUrl();
    }

    /**
     * 根据分享id获取对应存储序列化的token
     * @param $shareId
     * @return string
     */
    public static function getStoredShareToken($shareId): string
    {
        $exists = ShareTokenSchemaRepository::getById($shareId);
        if (!$exists) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '小程序码不存在');
        }

        return $exists->token;
    }

    private static function generateSchemaFromRecord(
        ShareTokenSchema $exists,
        $page,
        $width = 430,
        $channel = StorageChannel::CHANNEL_QINIU
    ) {
        if ($exists->schema_id) {
            return;
        }
        $o = tmpfile();
        $meta = stream_get_meta_data($o);
        $mime = ApplicationContext::getContainer()
            ->get(WechatObjectService::class)
            ->getScheme(
                [
                    'token_id' => $exists->id
                ],
                $meta['uri'],
                $page,
                $width
            );
        fseek($o, SEEK_SET, 0);

        // 并发处理
        $parallel = new Parallel(2);
        $qr = $logo = null;
        $parallel->add(function () use (&$qr, $meta) {
            $qr = file_get_contents($meta['uri']);
        });
        $parallel->add(function () use (&$logo, $exists) {
            $logo = self::createLogo($exists);
        });
        try {
            $parallel->wait();
        } catch (ParallelExecutionException $e) {
        }
        //合并图片且将图片写入临时文件中
        file_put_contents($meta['uri'], self::qrcodeWithLogo($qr, $logo));
        //上传文件
        if ($channel == StorageChannel::CHANNEL_MINIO) {
            $fileName = sprintf("%s.png", uniqid());
            $uploadPath = sprintf("/ticket/%s/%s", date('Ymd'), $fileName);
            [$upId, $upName, $upUrl] = UploadHelper::uploadStreamToMinio($o, $uploadPath, $uploadPath);
        } else {
            //默认走七牛
            $uploader = UploadHelper::newInstance('schema');
            [$upId, $upName, $upUrl] = $uploader->upload($meta['uri'], sprintf("%s.png", uniqid()));
        }

        fclose($o);
        //更新schema存储上传id
        ShareTokenSchemaRepository::setUpload($exists, $upId);
    }

    public static function createLogo(ShareTokenSchema $schema)
    {
        // 根据类型生成不同logo
        switch ($schema->biz_type) {
            case ShareTokenSchema::BIZ_TYPE_DOCTOR_SCHEMA:
                $logoUrl = 'https://img.youyao99.com/FhmOopX869mfh8QxVdXqb8t53Hdm';
                break;
            case ShareTokenSchema::BIZ_TYPE_STORE_SCHEMA:
                $logoUrl = 'https://img.youyao99.com/FqchvpkTxc_nffPnR0cYGtq7MGQi';
                break;
            case ShareTokenSchema::BIZ_TYPE_CLERK_INVITE:
                $logoUrl = 'https://img.youyao99.com/Fty92kY1hewaK5vOStOO1aYhu6A7';
                break;
            default:
                $logoUrl = 'https://img.youyao99.com/FmW50NxQR4rZFz4WAiZYqj8J4hs5';
        }
        return file_get_contents($logoUrl);
    }

    public static function qrcodeWithLogo($qr, $logo)
    {
        $qr = imagecreatefromstring($qr);
        $logo = imagecreatefromstring($logo);
        //二维码图片宽度
        $qr_width = imagesx($qr);
        //二维码图片高度
        $qr_height = imagesy($qr);
        //logo图片宽度
        $logo_width = imagesx($logo);
        //logo图片高度
        $logo_height = imagesy($logo);
        //组合之后logo的宽度(占二维码的1/2.18)
        $logo_qr_width = $qr_width / 2.18;
        //logo的宽度缩放比(本身宽度/组合后的宽度)
        $scale = $logo_width / $logo_qr_width;
        //组合之后logo的高度
        $logo_qr_height = $logo_height / $scale;
        //组合之后logo左上角所在坐标点
        $from_width = ($qr_width - $logo_qr_width) / 2;
        imagecopyresampled(
            $qr,
            $logo,
            (int)$from_width,
            (int)$from_width,
            0,
            0,
            (int)$logo_qr_width,
            (int)$logo_qr_height,
            (int)$logo_width,
            (int)$logo_height
        );
        ob_start();
        imagepng($qr);
        imagedestroy($qr);
        imagedestroy($logo);
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }
}
