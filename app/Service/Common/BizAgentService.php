<?php

declare(strict_types=1);
namespace App\Service\Common;

use App\Repository\BizAgentRelRepository;
use App\Service\Store\StoreService;

class BizAgentService
{
    public static function recordAgent($bizId, $storeId, $bizType, $agentId = 0)
    {
        if ($agentId) {
            $agentIds = [$agentId];
        } else {
            $agentIds = StoreService::getStoreAgents($storeId);
        }
        BizAgentRelRepository::batchAddBizAgent(
            $bizId,
            $bizType,
            $agentIds
        );
    }
}
