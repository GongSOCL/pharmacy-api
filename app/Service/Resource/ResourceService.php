<?php

declare(strict_types=1);

namespace App\Service\Resource;

use App\Model\Pharmacy\Resource;
use App\Repository\Resource\ResourceRepository;
use Grpc\Pharmacy\Resource\ResourceListReply;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Collection;

class ResourceService
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    public function getByIds(array $ids): ResourceListReply
    {
        $resp = ResourceRepository::getByIds($ids);
        return $this->buildResourceReplyList($resp);
    }

    public function searchResourceWithLimit($keywords = "", $limit = 10, $type = 0): ResourceListReply
    {
        $resp = ResourceRepository::searchWithLimit($keywords, $type, $limit);
        return $this->buildResourceReplyList($resp);
    }

    /**
     * @param Collection $resp
     * @return ResourceListReply
     */
    private function buildResourceReplyList(Collection $resp): ResourceListReply
    {
        $reply = new ResourceListReply();
        if ($resp->isEmpty()) {
            return $reply;
        }
        $items = [];
        /** @var Resource $item */
        foreach ($resp as $item) {
            $o = new \Grpc\Pharmacy\Resource\Resource();
            $o->setId($item->id);
            $o->setName($item->title);
            $items[] = $o;
        }
        $reply->setResource($items);
        return $reply;
    }
}
