<?php
declare(strict_types=1);
namespace App\Service\Exam;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;

class RadioQuestionItemChecker extends QuestionItemChecker
{
    public function check(): bool
    {
        if (empty($this->user_answers)) {
            return false;
        }

        $answers = $this->answers->toArray();
        if (empty($answers)) {
            return false;
        }
        foreach ($answers as $answer) {
            if ($answer['is_right']) {  //找到第一个对的，然后跳过后面的检查
                $this->rightOptions = [$answer[$this->filed]];
                break;
            }
        }

        return $this->rightOptions[0] == $this->user_answers[0];
    }
}
