<?php
declare(strict_types=1);
namespace App\Service\Exam;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;

/**
 * 多选校验器
 */
class MultiQuestionItemChecker extends QuestionItemChecker
{
    public array $rightOptions = [];

    public function check(): bool
    {
        if (empty($this->user_answers)) {
            return false;
        }

        $answers = $this->answers->toArray();
        if (empty($answers)) {
            return false;
        }
        $this->rightOptions = [];
        foreach ($answers as $answer) {
            $answer['is_right'] == 1 && $this->rightOptions[] = $answer[$this->filed];
        }
        return count($this->user_answers) == count($this->rightOptions) &&
            empty(array_diff($this->user_answers, $this->rightOptions));
    }
}
