<?php
declare(strict_types=1);
namespace App\Service\Exam;

use App\Constants\Auth;
use App\Constants\ErrorCode;
use App\Constants\ExamCode;
use App\Constants\Role;
use App\Exception\AuthException;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Helper\TaskManageHelper;
use App\Model\Pharmacy\QuestionnaireAnswer;
use App\Model\Pharmacy\User;
use App\Model\Qa\TComplianceExamAnswersSnapshot;
use App\Model\Qa\TComplianceQuestion;
use App\Model\Qa\TComplianceQuestionAnswer;
use App\Model\Qa\TComplianceQuestionItem;
use App\Model\Qa\TComplianceTestExamFraction;
use App\Model\Qa\TComplianceTestPaper;
use App\Model\Qa\TComplianceTestPaperItem;
use App\Model\Qa\TComplianceUserExam;
use App\Repository\ComplianceExamAnswersSnapshotRepository;
use App\Repository\ComplianceExamResponseRepository;
use App\Repository\ComplianceQuestionAnswerRepository;
use App\Repository\ComplianceQuestionItemsRepository;
use App\Repository\ExamServeScopePRepository;
use App\Repository\UserClerkRepository;
use App\Service\Point\PointService;
use App\Service\User\UserService;
use Carbon\Carbon;
use Grpc\Point\AddActivityPointReq;
use Grpc\Point\PointClient;
use Grpc\Task\ExamToDb;
use Hyperf\DbConnection\Db;
use Hyperf\Paginator\LengthAwarePaginator;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Collection;
use InvalidArgumentException;

class ExamService
{

    private static function checkUser()
    {
        //获取登陆用户clerk
        $user = Helper::getLoginUser();

        $role = Helper::getUserLoginRole();

        if ($role->role == 1) {
            throw new AuthException(Auth::USER_FORBIDDEN, "该账号禁考");
        } else {
            # 店员
            $UserType = '店员';

            $clerk = UserClerkRepository::getUserClerkByUserId($user->id);
        }

        if (!$clerk) {
            throw new AuthException(Auth::USER_FORBIDDEN, "'.$UserType.'帐号不存在或已禁用");
        }

        return $clerk;
    }


    /**
     * 考试列表
     * @param string $keywords
     * @param int $status
     * @param int $current
     * @param int $limit
     * @return array
     * @throws BusinessException
     * @throws InvalidArgumentException
     */
    public static function list(): array
    {
        $UserInfo = self::checkUser();

        $resp = self::getAllExams($UserInfo);  # 获取所有考试

        return $resp;
    }

    /**
     * 获取用户所有考试
     * @param string $keywords
     * @param int $current
     * @param int $limit
     * @return LengthAwarePaginator|null
     */
    private static function getAllExams($UserInfo): ? array
    {
        $resp = ExamServeScopePRepository::getUserServeProducts($UserInfo);

        return $resp;
    }


    /**
     * 获取考试信息
     * @param int $id
     * @return array
     */
    public static function getExamInfo(int $id): array
    {
        $exam = ExamServeScopePRepository::getExamInfoById([$id]);

        if (!$exam) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在或未开放");
        }

        $examInfo = $exam[0];

        $is_pass = 0;

        $now = date('Y-m-d H:i:s');

        $UserInfo = self::checkUser();

        $ExamStatus = QuestionnaireAnswer::where('paper_id', $id)
            ->where('user_id', $UserInfo->user_id)
            ->where('type', 2)
            ->select('paper_id', 'score', 'is_pass', 'exam_num')
            ->first();

        if ($ExamStatus) {
            # 判断考试状态
            if ($ExamStatus->is_pass == 1) {
                $is_pass = $ExamStatus->is_pass;
            } else {
                if ($examInfo['exam_num'] && $examInfo['exam_num'] == $ExamStatus->exam_num) {
                    $is_pass = $ExamStatus->is_pass;
                } else {
                    if ($examInfo['exam_end'] && strtotime($examInfo['exam_end']) <= time()) {
                        $is_pass = ExamCode::OVERDUE;   # 当前考试正巧过期 返回考试列表
                    } else {
                        $is_pass = ExamCode::MAKE_UP_EXAM;  # 补考
                    }
                }
            }
        } else {
            if ($now < $examInfo['exam_start']) {
                $is_pass = ExamCode::NOTSTARTED ;    # 未开始
            } elseif ($now >= $examInfo['exam_start']) {
                $is_pass = ExamCode::CANPART ;      # 待考
            }
        }


        return [
            'name' => $examInfo['name'],
            'desc' => $examInfo['introduce'],
            'is_time_unlimited' => $examInfo['exam_end'] ? 0 : 1,
            'start' => (string)$examInfo['exam_start'],
            'end' => (string)$examInfo['exam_end'],
            'exam_time' => !empty($examInfo['exam_time']) ? ((string)$examInfo['exam_time'] * 60) : '',
            'fraction' => isset($ExamStatus->score) ? $ExamStatus->score : 0,
            'is_pass' => $is_pass,  # 考试状态 0未通过 1通过 2可以参加考试 3未开始 4补考 5过期
            'exam_num' => empty($examInfo['exam_num']) ? 0 : $examInfo['exam_num'],
        ];
    }


    /**
     * 开始考试
     * @param int $id
     * @return array
     */
    public static function startExam(int $id): array
    {
        $user = Helper::getLoginUser();

        $exam = ExamServeScopePRepository::getExamInfoById([$id]);

        if (!$exam) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在");
        }

        $examInfo = $exam[0];

        if (!$examInfo['test_paper_yyid']) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试尚未配置完成，无法参与");
        }

        $now = date('Y-m-d H:i:s');

        if ($examInfo['exam_start'] > $now) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试尚未开始，无法参与");
        }

        # 先清除 nsq 缓存
        $requestId=  $examInfo['yyid'].'-'.$user->id.'-pharmacy';  # 生成固定 字符串

        $cacheRequestId = self::buildCacheRequestId($requestId, $user, $examInfo);

        # 这里是发送到nsq消费的缓存
        $redis = Helper::getRedis('exam');
        $redis->del("pharmacy:exam:answers:".$cacheRequestId.'-nsq');
        $redis->del("pharmacy:exam:questions:".$cacheRequestId.'-nsq');
        $redis->del("pharmacy:exam:rights:".$cacheRequestId.'-nsq');

        //获取最新考试记录
        $last = ExamServeScopePRepository::getExamStatus($user->id, $examInfo);

        if ($last && $last->is_pass == 1) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试已通过，无法重复答题");
        }

        //获取考试第一题目
        $questionItemIds = self::getExamQuestionIds($examInfo, 1);


        //添加超过2小时未答完的记录(垃圾数据)
        go(function () use ($examInfo, $user, $requestId) {
            self::clearOldRequest($examInfo, $user, $requestId);
        });


        # 考试时间 返回单位："秒"
        # exam_time_out 考试时间到【交卷】,unlimited_time 考试时长无限时
        if ($examInfo['exam_time']) {
            $examTime = $examInfo['exam_time'];


            # 获取缓存时间
            $getcCacheExamTime = ExamServeScopePRepository::getCacheExamTime($cacheRequestId);

            if ($getcCacheExamTime) {
                $time = explode('-', $getcCacheExamTime);

                $useTime = $time[0]+($time[1]*60);

                if ($useTime <= time()) {
                   # -1 正常, -2 强制提交考卷
                    $useTimen = '-2';
                } else {
                    $useTimen = ($useTime - time());
                }

                if (is_numeric($useTimen) && $useTimen<=0) {
                    # 全部让前端处理交卷
                    $useTimen = '-2';
                }
            } else {
                $useTimen = $examTime * 60;

                ExamServeScopePRepository::cacheExamTime($examTime, $cacheRequestId);
            }
        } else {
            $useTimen = '-1';
        }

         # 缓存试题
        sort($questionItemIds);
        $info = ExamServeScopePRepository::cacheExamQuestionList($cacheRequestId, $questionItemIds, 7200);

        # 断点续考
        $redis = Helper::getRedis('exam');

        $answerKey = ExamServeScopePRepository::buildExamAnswerKey($cacheRequestId);

        $answerKeys = $redis->hKeys($answerKey);    # 已答题目的所有key

        $itemId = $info[0];

        $preItemId = '';

        $is_sub = 'no'; #

        sort($answerKeys);

        if ($answerKeys) {
            $QuestionKey = ExamServeScopePRepository::buildQuestionCacheKey($cacheRequestId);

            $QuestionData = $redis->zRange($QuestionKey, 0, -1);

            $answerItemId = last($answerKeys);

            $nextQuestion = ExamServeScopePRepository::getNextQuestionId($cacheRequestId, (int) $answerItemId);

            $itemId = isset($nextQuestion[0]) ? $nextQuestion[0] : '';

            $preItemId = $answerItemId;

            $item_id = last($QuestionData);

            if ($item_id == $itemId || !$itemId) {
                $is_sub = 'yes';
            }
        }

        return [$itemId, count($questionItemIds), $requestId, $useTimen, $preItemId, $is_sub];
    }


    private static function clearOldRequest($examInfo, $users, $newRequestID)
    {

        ExamServeScopePRepository::clearOldRequests($examInfo['id'], $users->id, $newRequestID);
    }


    private static function buildCacheRequestId($requestId, $users, $examInfo): string
    {
        return sprintf("%d:%d:%s", $examInfo['id'], $users->id, $requestId);
    }

    /**
     * 根据题库yyid获取所有的试题yyid
     * @param array $qs
     * @return array
     */
    private static function getQuestionItemsByQuestions(array $qs, $examItem): array
    {
        //根据yyid过滤有效题库(处理下架不一致)
        $resp = ExamServeScopePRepository::getQuestionsByYYIDS($qs);
        if ($resp->isEmpty()) {
            return [];
        }
        $qs = $resp->pluck('yyid')->all();

        //根据题库获取所有题目
        $qi  = ExamServeScopePRepository::getItemsByQuestionYYIDS($qs, $examItem);

        $ids = [];

        /** @var TComplianceQuestionItem $item */
        foreach ($qi as $item) {
            array_push($ids, $item['id']);
        }

        return $ids;
    }

    /**
     * 获取试卷所有试题id列表
     * @param TComplianceTestPaper $paper
     * @return array
     */
    public static function getQuestionItemsByTestPaper(TComplianceTestPaper $paper, $testPaperConf): array
    {
        /** @var Collection $items */
        $items = $paper->testPaperItems;

        $ids = [];
        if (!$paper->isTestPaperQuestion()) {
            //题目出题
            $questionYYID = $items->reduce(function ($carry, TComplianceTestPaperItem $item) {

                if ($item->item_yyid) {
                    $carry[] = $item->item_yyid;
                }

                return $carry;
            }, []);

            //根据题目id获取有效题目
            $resp = ExamServeScopePRepository::getItemsByYYIDS($questionYYID);

            /** @var TComplianceQuestionItem $item */
            foreach ($resp as $item) {
                array_push($ids, $item->id);
            }

            # 随机出题 随机指定数量题目
            if (isset($testPaperConf['item_num'])) {
                shuffle($ids);

                $ids = array_slice($ids, 0, $testPaperConf['item_num']);
            }
        } else {
            # 随机出题  题库出题 gdfgd
            if (isset($testPaperConf['examItem'])) {
                $examItem = array_column($testPaperConf['examItem'], null, 'yyid');

                $qs = array_column($testPaperConf['examItem'], 'yyid');

                $ids = self::getQuestionItemsByQuestions($qs, $examItem);
            } else {
                //题库出题
                $qs = $items->reduce(function ($carry, TComplianceTestPaperItem $item) {

                    if ($item->question_yyid) {
                        $carry[] = $item->question_yyid;
                    }

                    return $carry;
                }, []);

                if (!empty($qs)) {
                    $ids = self::getQuestionItemsByQuestions($qs, []);
                }
            }
        }
        return $ids;
    }

    /**
     * 获取考试试题
     * @param int $examId
     * @param int $questionId
     * @param string $requestId
     * @return array
     */
    public static function getQuestion(int $examId, int $questionId, string $requestId): array
    {
        $user = Helper::getLoginUser();

        $exam = ExamServeScopePRepository::getExamInfoById([$examId]);

        if (!$exam) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在");
        }

        $examInfo = $exam[0];

        //检查缓存中试题
        $cacheRequestId = self::buildCacheRequestId($requestId, $user, $examInfo);

        ExamServeScopePRepository::checkRequestAndQuestion($cacheRequestId, $questionId);

        //获取试题及选项
        $questionItem = ExamServeScopePRepository::getItemsById($questionId);

        if (!$questionItem) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "试题不存在或已下架");
        }

        /** @var Collection $answers */
        $answers = $questionItem->answers;
        $options = $answers->reduce(function ($carry, $item) {
            $carry[] = [
                'id' => $item->id,
                'name' => $item->name
            ];
            return $carry;
        }, []);


        $redis = Helper::getRedis('exam');

        $answerKey = ExamServeScopePRepository::buildExamAnswerKey($cacheRequestId);

        $answerKeys = $redis->hKeys($answerKey);    # 已答题目的所有key

        $pre_item_id = '';

        $s_num = 1;

        $nc = '';

        $is_sub = 'no';

        $itemInfo = "";

        sort($answerKeys);

        if ($answerKeys) {
            if (in_array($questionId, $answerKeys)) {
                $Index = array_search($questionId, $answerKeys);

                $preIndex = ($Index==0)?$Index:($Index-1);

                if ($Index > 0) {
                    $s_num += $Index;
                } else {
                    $s_num = 1;
                }

                # 获取已答题的题目选项
                $rank = $redis->hGet($answerKey, (string)$questionId);

                if ($rank) {
                    $rank = json_decode($rank, true);

                    $itemInfo = json_encode($rank['options']);
                }
            } else {
                $Index = array_search(last($answerKeys), $answerKeys);

                $nc = count($answerKeys);   # 当只做了一题时 index=0 不会给出缓存里的题

                $preIndex = ($Index==0)?$Index:($Index);

                $s_num += $nc;
            }

            if ($Index != 0 || $nc == 1) {
                $pre_item_id = $answerKeys[$preIndex];
            }

            $QuestionKey = ExamServeScopePRepository::buildQuestionCacheKey($cacheRequestId);

            $QuestionData = $redis->zRange($QuestionKey, 0, -1);

            $item_id = last($QuestionData);

            if ($item_id == $questionId) {
                $is_sub = 'yes';    # 告知前端 这是最后一题 按钮应改为 "交卷"
            }
        }

        return [
            'item_info' => $itemInfo,
            'is_sub' => $is_sub,
            's_num' => $s_num,
            'pre_item_id' => $pre_item_id,
            'question_id' => $questionItem->id,
            'name' => $questionItem->name,
            'type' => [
                'id' => $questionItem->type,
                'desc' => $questionItem->getTypeDesc()
            ],
            'options' => $options
        ];
    }

    /**
     * 回答考试
     * @param $examId
     * @param $questionId
     * @param string $requestId
     * @param array $optionIds
     * @param string $content
     * @return array
     */
    public static function answerExam(
        $examId,
        $questionId,
        string $requestId,
        array $optionIds = [],
        $content = ""
    ): array {
        $user = Helper::getLoginUser();

        $exam = ExamServeScopePRepository::getExamInfoById([$examId]);

        if (!$exam) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在");
        }

        $examInfo = $exam[0];

        //检查缓存中试题
        $cacheRequestId = self::buildCacheRequestId($requestId, $user, $examInfo);

        ExamServeScopePRepository::checkRequestAndQuestion($cacheRequestId, (int)$questionId);

        //获取试题及选项
        $questionItem = ExamServeScopePRepository::getItemsById($questionId);

        if (!$questionItem) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "试题不存在或已下架");
        }

        //检验答案选项
        /** @var Collection $answer */
        $checker = QuestionItemChecker::getChecker($questionItem);

        if ($questionItem->isWithOptions()) {
            $checker->withOptions($optionIds);
        } else {
            $checker->withContent($content);
        }

        $checkResult = $checker->check();

        ExamServeScopePRepository::cacheExamAnswer(
            $cacheRequestId,
            $questionItem,
            $checkResult,
            $optionIds,
            $content,
            7200,
            $user->open_id ? $user->open_id : ''
        );

        //从缓存获取下一题目id
        $nextQuestion = ExamServeScopePRepository::getNextQuestionId($cacheRequestId, $questionId);

        return [
            'is_question_pass' => $checkResult ? 1 : 0,
            'is_last' => empty($nextQuestion) ? 1 : 0,
            'next_question_id' => !empty($nextQuestion) ? $nextQuestion[0] : '',
            'is_explain' => $examInfo['wrong_remind'] == 0 ? 0 : 1,
            'explain' => $examInfo['wrong_remind'] == 0? '' : $questionItem->explain_content
        ];
    }


    /**
     * @throws \Exception
     */
    public static function finishExam(int $examId, string $requestId, string $is_sub): array
    {
        $user = Helper::getLoginUser();

        $exam = ExamServeScopePRepository::getExamInfoById([$examId]);

        if (!$exam) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在");
        }

        $examInfo = $exam[0];

        //检查缓存中试题
        $cacheRequestId = self::buildCacheRequestId($requestId, $user, $examInfo);

        $isFinished = ExamServeScopePRepository::checkFinished($cacheRequestId);

        # 是否强制交卷
        if ($is_sub == 'yes') {
            self::forceSubmExam($cacheRequestId);
        } else {
            if (!$isFinished) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "答题尚未完成");
            }
        }

        # 检查是否通过
        $examPassed = ExamServeScopePRepository::checkExamPassed(
            $cacheRequestId,
            $examInfo['pass_condition'],
            $examInfo['pass_type'],
            $examInfo['id']
        );

        # Todo 无论通过与否  根据考试id 写入新表 examination_results
        if ($examPassed['status']) {
            $pass = true;
            $status = 2;
        } else {
            $pass = false;
            $status = 4;    # 判断考试是否存在补考 若存在 =3 若不存在 =4【考试不通过】
        }

        ExamServeScopePRepository::updateStatusByexamId($examPassed, $user->id, $examInfo['id'], $status);

        # 积分发放 暂时不发
//        $req = new AddActivityPointReq();
//        $req->setUid(intval($user->id));
//        $req->setKey();
//        $req->setType();
//        $req->setVal(strval());
//        $req->setWechatUserID(0);
//        $req->setUserType(3);
//        $req->setNum($examInfo['integral']); # 积分数
//        self::getClient()->addActivityPoint($req);
        if ($pass) {
            try {
                $role = Helper::getUserLoginRole();
                ExamRpcService::finishExamGainPoint($exam[0]['id'], $role->id);
                if ($role->role == Role::ROLE_CLERK) {
                    $clerk = UserClerkRepository::getUserClerkByUserId($user->id);
                    if ($clerk) {
                        PointService::updateClerkPointRankInfo($clerk, true, false);
                    }
                }
            } catch (\Exception $e) {
                Helper::getLogger()->error("send_user_exam_point_error", [
                    'user_id' => $user->id,
                    'role_id' => isset($role) && $role ? $role->id : 0,
                    'msg' => $e->getMessage()
                ]);
            }
        }


        # 删除当前使用缓存 防止多次考试时拿到的还是上一次考试的数据 看你导致 开始考试就被强制提交
        $redis = Helper::getRedis('exam');
        $redis->hDel("pharmacy:exam:timeh", $cacheRequestId);
        $redis->del("pharmacy:exam:answers:".$cacheRequestId);
        $redis->del("pharmacy:exam:questions:".$cacheRequestId);
        $redis->del("pharmacy:exam:rights:".$cacheRequestId);
        $redis->del("pharmacy:exam:user_exam:start:$examId:$user->id");

        $last = self::writeExamResult($examInfo, $user, $pass, $cacheRequestId.'-nsq');

        return [
            'with_agreement' => $pass && $examInfo['agreement_yyid'] ? 1 : 0,
            'is_exam_pass' => $pass ? 1 : 0,
            'fraction' => $examPassed['fraction'],
//            'last_time' => (string)$last->created_time
        ];
    }


    private static function writeExamResult(
        $examInfo,
        $user,
        bool $examPassed,
        string $cacheRequestId
    ): TComplianceUserExam {
        //检查最后一次提交记录状态
        $last = ExamServeScopePRepository::getExamLast($user->id, $examInfo['yyid']);

        //如果已经通过直接返回false
        if ($last && $last->isPassed()) {
            return $last;
        }

        //重置旧的数据,添加新数据
        Db::beginTransaction();
        try {
            ExamServeScopePRepository::clearUserExams($user->id, $examInfo['yyid']);

            $last = ExamServeScopePRepository::addUserExam(
                $user->id,
                $examInfo['yyid'],
                $examPassed,
                $user->open_id ? $user->open_id : ''
            );

            Db::commit();
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }

        //最后记录未通过记录通过状态
        self::processExamsToDb($cacheRequestId, $examInfo['id'], $user->id);
        return $last;
    }

    private static function processExamsToDb($cacheRequestId, $examId, $userId)
    {
        $p = new ExamToDb();
        $p->setExamId($examId);
        $p->setPlatform(ExamCode::PHARMACY);
        $p->setUserId($userId);
        $p->setRequestId($cacheRequestId);
        $p->setLastTime(date('Y-m-d H:i:s'));

        $tm = TaskManageHelper::create(0, $p, 'nsq', 'exam-to-db');
        $tm->push();
    }


    /**
     * @return mixed|PointClient
     */
    protected static function getClient(): PointClient
    {
        return ApplicationContext::getContainer()->get(PointClient::class);
    }

    private static function forceSubmExam($cacheRequestId)
    {
        $redis = Helper::getRedis('exam');

        $user = Helper::getLoginUser();

        $key = ExamServeScopePRepository::buildQuestionCacheKey($cacheRequestId);

        $answerKey = ExamServeScopePRepository::buildExamAnswerKey($cacheRequestId);

        # 当考试有结束时间时
        $answerKeys = $redis->hKeys($answerKey);    # 已答题目的所有key

        $questionKey = $redis->zRange($key, 0, -1);    # 未答题目的所有

        $keyArrayDiff = array_diff($questionKey, $answerKeys);

        # 手动缓存redis 所有题目答案为空 //缓存当前题目校验结果
        foreach ($keyArrayDiff as $item) {
            $questionItem = ExamServeScopePRepository::getItemsById($item);

            if (!$questionItem) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "试题不存在或已下架");
            }

            ExamServeScopePRepository::cacheExamAnswer(
                $cacheRequestId,
                $questionItem,
                false,
                [],
                '',
                7200,
                $user->open_id ? $user->open_id : ''
            );
        }
        return true;
    }


    /**
     * 获取所有的考试试题
     * @param $examInfo
     * @param $t
     * @return array
     */
    private static function getExamQuestionIds($examInfo, $t): array
    {
        $user = Helper::getLoginUser();

        $testPaper = ExamServeScopePRepository::getByYYID($examInfo['test_paper_yyid']);

        if (!$testPaper) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试尚未配置完成，无法参与");
        }

        if ($testPaper->question_type == 1) { # 随机出题  查 t_compliance_question_items_fraction 配置的题目
            $examItem = $examItemNum = [];

            if ($testPaper->test_paper_type == 0) {    # 题库出题
                $examItem = TComplianceTestExamFraction::where(
                    [
                        ['test_id',$testPaper->id],
                        ['status',1],
                        ['type',2],
                        ['exam_id',$examInfo['id']]
                    ]
                )
                    ->select("question_bank_id", "item_num")
                    ->get()->toArray();

                # 获取 题库yyid ComplianceQuestion
                foreach ($examItem as $k => $item) {
                    $quYYid = TComplianceQuestion::where(
                        'id',
                        $item['question_bank_id']
                    )->select('yyid')->first()->toArray();

                    $examItem[$k]['yyid'] = $quYYid['yyid'];
                }
            } else {
                # 题目出题
                $examItemNum = TComplianceTestExamFraction::where(
                    [
                        ['test_id',$testPaper->id],
                        ['status',1],
                        ['type',3],
                        ['exam_id',$examInfo['id']]
                    ]
                )
                    ->select("item_num")
                    ->get()->toArray();
            }

            # 获取随机题目数量的id
            $questionItemIds = self::getQuestionItemsByTestPaper(
                $testPaper,
                [
                    'item_num'=>isset($examItemNum[0]['item_num'])?$examItemNum[0]['item_num']:[],
                    'examItem' => $examItem
                ]
            );
        } else {  # 顺序出题 【老规矩】
            $questionItemIds = self::getQuestionItemsByTestPaper($testPaper, []);

            if (empty($questionItemIds)) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该考试未配置试题");
            }
        }

        # 写入数据库 回顾时数据尚未落地到数据库【随机题】
        if ($t == 1) {    # 初始化时
            ExamServeScopePRepository::addExamRandItem($examInfo, $user, $testPaper->id, $questionItemIds);
        } else {
            $examRandItem = ExamServeScopePRepository::getExamRandItem($examInfo, $user, $testPaper->id);

            if (!$examRandItem->isEmpty()) {
                $examRandItem = $examRandItem->toArray();

                $examRandItem = array_column($examRandItem, 'item_id');

                return $examRandItem;
            }
        }

        return $questionItemIds;
    }


    /**
     * @param int $examId
     * @return ComplianceAgreement
     */
//    public static function getExamAgreement(int $examId): ComplianceAgreement
//    {
//        $user = Helper::getLoginUser();
//        $exam = ComplianceExamRepository::getById($examId);
//        if (!$exam) {
//            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在");
//        }
//        if (!$exam->agreement_yyid) {
//            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该考试未关联协议");
//        }
//
//        $agree = (new ComplianceAgreementRepository)->getByYYID($exam->agreement_yyid);
//        if (!$agree) {
//            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试协议不存在");
//        }
//
//        return $agree;
//    }






    /**
     * 回顾考试
     * @param int $examId
     * @param string $requestId
     * @return array
     */
    public static function review(int $examId, string $requestId = ""): array
    {
        $user = Helper::getLoginUser();
        $exam = ExamServeScopePRepository::getExamInfoById([$examId]);

        if (!$exam) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在");
        }

        $examInfo = $exam[0];

        //获取最后一次记录，如果未完成答题，没有回顾
        $last = ExamServeScopePRepository::getExamLast($user->id, $examInfo['yyid']);

        if (!$last) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "尚未完成考试，无法回顾");
        }

        $reviews = [];
        //首先通过缓存获取考试回顾列表
        if ($requestId) {
            $reviews = self::getAnswersFromCache($examInfo, $user, $requestId);
        }
        if (!empty($reviews)) {
            return $reviews;
        }

        //其次通过snapshot表获取考试回顾
        $reviews = self::getAnswersFromDb($examInfo, $user);
        if (!empty($reviews)) {
            return $reviews;
        }

        //最后通过错误选项和考试组合成回顾选项
        return self::getAnswersFromErrResponse($examInfo, $user);
    }

    /**
     * 从缓存中获取用户的回顾记录
     * @param $examInfo
     * @param User $user
     * @param $requestId
     * @return array
     */
    private static function getAnswersFromCache($examInfo, User $user, $requestId): array
    {
        $cacheRequestId = self::buildCacheRequestId($requestId, $user, $examInfo);

        //从缓存获取所有的题目id
        $questionIds = ExamServeScopePRepository::getQuestionIds($cacheRequestId);

        if (!$questionIds) {
            return [];
        }

        //获取试题集合
        $userAnswers = ExamServeScopePRepository::getAnswers($cacheRequestId);
        if (!$userAnswers) {
            return [];
        }
        $userAnswers = array_map(function ($val) {
            return json_decode($val, true);
        }, $userAnswers);

        //根据试题id获取题目和选项
        $questions = ComplianceQuestionItemsRepository::getItemsByIds($questionIds);
        if ($questions->isEmpty()) {
            return [];
        }
        $questionYYIDS = $questions->pluck('yyid')->all();

        $answers = ComplianceQuestionAnswerRepository::getByQuestionYYIDS($questionYYIDS);
        $answerMap = $answers->groupBy('question_item_yyid')->all();

        //构建响应数据
        $data = [];
        /** @var TComplianceQuestionItem $question */
        foreach ($questions as $question) {
            $answerItem = $userAnswers[$question->id] ?? [];
            $optionIds = $answerItem['options'] ?? [];

            $options = [];
            isset($answerMap[$question->yyid]) &&
            $options = self::buildReviewOptions(
                $answerMap[$question->yyid],
                $optionIds
            );

            $data[]  = [
                'id' => $question->id,
                'name' => $question->name,
                'type' => [
                    'id' => $question->type,
                    'desc' => $question->getTypeDesc()
                ],
                'options' => $options,
                'is_right' => $answerItem['is_right'] ?? 0,
                'explain' => $question->explain_content,
                'content' => $answerItem['content'] ?? ''
            ];
        }

        return $data;
    }

    private static function buildReviewOptions(Collection $answers, array $answerOptionIds = []): array
    {
        $data = [];
        foreach ($answers as $answer) {
            $data[] = [
                'id' => $answer->id,
                'name' => $answer->name,
                'is_right' => $answer->is_right ? 1 : 0,
                'is_choosed' => in_array($answer->id, $answerOptionIds) ? 1 : 0
            ];
        }

        return $data;
    }


    /**
     * 通过用户答题提交历史快照获取回顾记录(只展示最新记录)
     * @param $examInfo
     * @param User $user
     * @return array
     */
    private static function getAnswersFromDb($examInfo, User $user): array
    {
        //判断快照表是否存在记录
        $snapshots = ComplianceExamAnswersSnapshotRepository::getUserExamSnapshot($user, $examInfo);
        if ($snapshots->isEmpty()) {
            return [];
        }
        $snapMap = $snapshots->keyBy('question_id')->all();

        //存在则从snap
        $questionIds = $snapshots->pluck('question_id')->all();
        $questions = ComplianceQuestionItemsRepository::getItemsByIds($questionIds);
        if ($questions->isEmpty()) {
            return [];
        }

        $answerYYIDS = $questions->pluck('yyid')->all();
        $answers = ComplianceQuestionAnswerRepository::getByQuestionYYIDS($answerYYIDS);
        $answerMap  = $answers->groupBy('question_item_yyid')->all();

        $data = [];
        /** @var TComplianceQuestionItem $question */
        foreach ($questions as $question) {
            if (!isset($snapMap[$question->id])) {
                continue;
            }
            /** @var TComplianceExamAnswersSnapshot $snap */
            $snap = $snapMap[$question->id];

            $options = [];
            isset($answerMap[$question->yyid]) &&
            $options = self::buildReviewOptions($answerMap[$question->yyid], $snap->getOptionIds());

            $data[] = [
                'id' => $question->id,
                'name' => $question->name,
                'type' => [
                    'id' => $question->type,
                    'desc' => $question->getTypeDesc()
                ],
                'options' => $options,
                'is_right' => $snap->is_pass,
                'explain' => $question->explain_content,
                'content' => (string)$snap->content
            ];
        }

        return $data;
    }

    private static function getAnswersFromErrResponse($examInfo, User $user): array
    {
        //获取错误记录
        $response = ComplianceExamResponseRepository::getExamResponse($examInfo, $user);
        $responseMap = $response->keyBy('question_item_yyid')->all();
        $wrongYYIDS = $response->pluck('question_item_yyid')->all();

        //获取考试id
        $questionIds = self::getExamQuestionIds($examInfo, 2);
        if (empty($questionIds)) {
            return [];
        }
        //获取所有考试
        $questions = ComplianceQuestionItemsRepository::getItemsByIds($questionIds);
        if ($questions->isEmpty()) {
            return [];
        }

        //获取所有答案
        $questionYYIDS = $questions->pluck('yyid')->all();


        $answers = ComplianceQuestionAnswerRepository::getByQuestionYYIDS($questionYYIDS);
        $answerQuestionMap = $answers->groupBy('question_item_yyid')->all();

        //构建返回值
        $data = [];
        /** @var TComplianceQuestionItem $question */
        foreach ($questions as $question) {
            $answerYYID = [];

            if (isset($responseMap[$question->yyid])) {
                $answerYYID = $responseMap[$question->yyid]->getAnswerYYIDS();
            }

            $chooseByRight = !isset($responseMap[$question->yyid]);

            $options = [];
            if (isset($answerQuestionMap[$question->yyid])) {
                /** @var TComplianceQuestionAnswer $answer */
                foreach ($answerQuestionMap[$question->yyid] as $answer) {
                    $isChosen = in_array($answer->yyid, $answerYYID) ? 1 : (
                    $chooseByRight && $answer->is_right ? 1 : 0
                    );

                    $options[] = [
                        'id' => $answer->id,
                        'name' => $answer->name,
                        'is_right' => $answer->is_right ? 1 : 0,
                        'is_choosed' => $isChosen
                    ];
                }
            }

            $data[] = [
                'id' => $question->id,
                'name' => $question->name,
                'type' => [
                    'id' => $question->type,
                    'desc' => $question->getTypeDesc()
                ],
                'options' => $options,
                'is_right' => in_array($question->yyid, $wrongYYIDS) ? 0 : 1,
                'explain' => $question->explain_content,
                'content' => $answerItem['content'] ?? ''
            ];
        }

        return $data;
    }
}
