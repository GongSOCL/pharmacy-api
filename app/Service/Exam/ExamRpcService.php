<?php
declare(strict_types=1);
namespace App\Service\Exam;

use Grpc\Exam\ExamClient;
use Grpc\Exam\FinishExamGainPointRequest;
use Hyperf\Utils\ApplicationContext;

class ExamRpcService
{
    public static function getClient(): ExamClient
    {
        return ApplicationContext::getContainer()
            ->get(ExamClient::class);
    }

    public static function finishExamGainPoint($examId, $userId)
    {
        $req = new FinishExamGainPointRequest();
        $req->setExamId($examId);
        $req->setUserId($userId);
        self::getClient()->finishExamGainPoint($req);
    }
}