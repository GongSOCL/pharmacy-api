<?php
declare(strict_types=1);
namespace App\Service\Exam;

use App\Model\qa\TComplianceQuestionItem;

abstract class QuestionItemChecker
{
    protected $questionItem;
    protected $answers;
    public array $rightOptions = [];

    public bool $needCheck = true;
    protected string $content = "";
    protected array $user_answers = [];
    protected string $filed = "id";

    /**
     * @return bool
     */
    abstract public function check(): bool;

    public static function getChecker(TComplianceQuestionItem $questionItem)
    {
        if ($questionItem->isRadioCheck()) {
            $object = new RadioQuestionItemChecker();
        } elseif ($questionItem->isCheckboxCheck()) {
            $object = new MultiQuestionItemChecker();
        } else {
            $object = new InputQuestionItemChecker();
        }

        $object->questionItem = $questionItem;
        $object->answers = $questionItem->answers;

        return $object;
    }

    public function getRightOptions()
    {
        return $this->getRightOptions();
    }

    /**
     * 设置答题内容
     * @param string $content
     */
    public function withContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * 设置选中的答题选项
     * @param array $options
     */
    public function withOptions(array $options)
    {
        $this->user_answers = $options;
    }

    /**
     * 设置对比字段，默认id
     * @param string $field
     */
    public function withField(string $field)
    {
        $this->filed = $field;
    }
}
