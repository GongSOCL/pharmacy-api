<?php
declare(strict_types=1);

namespace App\Service\User;

use App\Constants\AppErr;
use App\Constants\ErrorCode;
use App\Constants\Role;
use App\Constants\Upload;
use App\Controller\Upload\UploadController;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Pharmacy\User;
use App\Model\Qa\TUserIdentity;
use App\Repository\BranchStoreRepository;
use App\Repository\CityRepository;
use App\Repository\ClerkApplyRepository;
use App\Repository\UserRepository;
use App\Repository\UserRoleRepository;
use App\Service\ALiYun\OCRAPIService;
use App\Service\Common\InsideMsgRpcService;
use App\Service\Coupon\UserCouponService;
use App\Service\Point\PointService;
use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Context;
use Hyperf\Utils\Parallel;
use Swoole\Coroutine\Channel;
use function Swoole\Coroutine\Http\get;

class UserService
{

    public static function getAggregationInfo(): array
    {
        $user = Helper::getLoginUser();
        $userRole = Helper::getUserLoginRole();
        var_dump($userRole->toArray());

        $parallel = new Parallel(3);

        //获取待读消息总数量
        $parallel->add(function () use ($userRole) {
            try {
                $resp = ApplicationContext::getContainer()
                    ->get(InsideMsgRpcService::class)
                    ->countUnReadMsg($userRole->id);
                return [
                    'msg_unread_total' => $resp->getCnt()
                ];
            } catch (\Exception $e) {
                Helper::getLogger()->error("fetch_unread_msg_error", [
                    'e' => $e->getMessage()
                ]);
                return [
                    'msg_unread_total' => 0
                ];
            }

        });

        //获取店员待确认收货数量
        $parallel->add(function () use ($userRole, $user) {
            try {
                if ($userRole->role != Role::ROLE_CLERK) {
                    return ['receipt_total' => 0];
                }
                $cnt = PointService::countReceiptToConfirm($userRole);
                return ['receipt_total' =>$cnt];
            } catch (\Throwable $e) {
                Helper::getLogger()->error("count_receipt_error", [
                    'msg' => $e->getMessage(),
                    'trace' => $e->getTrace()
                ]);
                return ['receipt_total' => 0];
            }

        });

        //获取患者待使用优惠券数量
        $parallel->add(function () use($userRole, $user) {
            try {
                if ($userRole->role != Role::ROLE_PATIENT) {
                    return ['coupon_total' => 0];
                }
                Context::set('user', $user);
                Context::set('role', $userRole->role);
                $resp = ApplicationContext::getContainer()
                    ->get(UserCouponService::class)
                    ->getUserCouponNum();
                return ['coupon_total' => isset($resp['num']) ? $resp['num'] : 0];
            } catch (\Throwable $e) {
                Helper::getLogger()->error("count_user_coupon_error", [
                    'msg' => $e->getMessage(),
                    'trace' => $e->getTrace()
                ]);
                return ['coupon_total' => 0];
            }
        });

        $default = [
            'msg_unread_total' => 0,
            'receipt_total' => 0,
            'coupon_total' => 0
        ];
        try {
            $result = $parallel->wait(false);
            $list = array_reduce($result, function ($carry, $item) {
                return array_merge($carry, $item);
            }, []);

            //补全默认字段
            foreach ($default as $k => $v) {
                if (!isset($list[$k])) {
                    $list[$k] = $v;
                }
            }

            return $list;
        } catch (\Exception $e) {
            Helper::getLogger()->error('getAggregationInfo_error', [
                'msg' => $e->getMessage(),
                'trace' => $e->getTrace()
            ]);
            return $default;
        }
    }

    public function getUserInfoByRole($userId, $role)
    {
        $applyInfo = ClerkApplyRepository::getInfoById($userId);
        $profile = UserRepository::getUserProfileById($userId);
        $storeId = 0;
        if ($role == Role::ROLE_CLERK && $applyInfo) {
            $storeId = $applyInfo->store_id;
            $profile->clerk->role = $applyInfo->role;
            $profile->clerk->phone = $applyInfo->phone;
            $profile->clerk->position = $applyInfo->position;
            $profile->clerk->real_name = $applyInfo->real_name;
        } elseif ($role == Role::ROLE_CLERK && !empty($profile['clerk']['store_id'])) {
            $storeId = $profile['clerk']['store_id'];
        }
        if ($storeId) {
            $storeInfo = BranchStoreRepository::getStoreInfoById($storeId);
            if (is_null($storeInfo)) {
                return $profile;
            }
            $cityInfo = CityRepository::getInfoById($storeInfo->city_id);
            $profile->clerk->city_id = $storeInfo->city_id;
            $profile->clerk->city_name = $cityInfo->city_name;
            $profile->clerk->main_store_id = $storeInfo->main_store_id;
            $profile->clerk->store_id = $storeInfo->id;
            $profile->clerk->store_name = $storeInfo->store_name;
            $profile->clerk->store_name_alias =  $storeInfo->store_name_alias;
            $profile->clerk->location = $storeInfo->location;
        }


        # 新增20230222判断是否上传身份证并且是否过期
        # 增加平台选择 1优药 2优佳医 4优能汇
        $validPeriod = TUserIdentity::where('uid', $userId)
            ->where('platform', 4)
            ->orderBy('created_at', 'desc')
            ->first();

        if ($validPeriod) {
            $validTime = strtotime(str_replace('.', '-', $validPeriod->due_date));

            if ($validTime > time()) {
                $is_upload_idcard = 1;
            } else {
                $is_upload_idcard = 2;
            }
        } else {
            $is_upload_idcard = 0;
        }

        $profile->is_upload_idcard = $is_upload_idcard;

        return $profile;
    }

    public function updateUserInfo($nickName = '', $headerUrl = '')
    {
        if ($headerUrl == '' && $nickName == '') {
            return;
        }
        $user = Helper::getLoginUser();
        if ($nickName) {
            $user->nick_name = $nickName;
        }
        if ($headerUrl) {
            $user->header_url = $headerUrl;
        }
        $user->updated_at = Carbon::now();
        $user->save();
    }


    /**
     * @Inject()
     * @var UploadController
     */
    private $upload;

    /**
     * @Inject()
     * @var OCRAPIService
     */
    private $OCRAPIService;

    public function uploadIdcard($file)
    {
        $userInfo = Helper::getLoginUser();

        $fileName = $this->upload->upload(Upload::UPLOAD_URI_IDCARD, $file, $userInfo->id);
//        $fileName = $this->upload->upload(Upload::UPLOAD_URI_IDCARD, $file, 1);
        if (!$fileName) {
            throw new BusinessException(ErrorCode::UPLOAD_ERROR);
        }

        $res = $this->OCRAPIService::main(Upload::UPLOAD_URI_IDCARD.$userInfo->id.'/'.$fileName, $fileName);
//        $uid = 1;
//        $res = $this->OCRAPIService::main(Upload::UPLOAD_URI_IDCARD.$uid.'/'.$fileName, $fileName);

        return ['file_name'=>$fileName,'user_info'=>$res];
    }


    /**
     * 身份证识别-提交
     * @param $front
     * @param $back
     * @param $name
     * @param $sex
     * @param $ethnicity
     * @param $birthDate
     * @param $address
     * @param $idNumber
     * @param $issueAuthority
     * @param $validPeriod
     * @param $mobile
     * @param $is_upload_idcard
     * @return array
     */
    public function idcardSubmit(
        $front,
        $back,
        $name,
        $sex,
        $ethnicity,
        $birthDate,
        $address,
        $idNumber,
        $issueAuthority,
        $validPeriod,
        $mobile,
        $is_upload_idcard
    ) {
        $userInfo = Helper::getLoginUser();

        if ($is_upload_idcard  != 2) {
            //判断是否有数据
            if (!$front || !$back) {
                throw new BusinessException(ErrorCode::USER_NOT_EXISTS);
            }

            //判断是否上传过
            $info = UserRepository::getUserByYYID($userInfo->id);           # 通过用户id

            if (!$info) {
                throw new BusinessException(ErrorCode::UPLOAD_INFO_MISS);
            }
            if ($info->is_upload_idcard == 1) {
                throw new BusinessException(ErrorCode::IDCARD_ALREADY_UPLOADED);
            }
        }


        Db::beginTransaction();
        try {
            //向t_user_identity表插入一条记录
            UserRepository::create(
                $front,
                $back,
                $name,
                $sex,
                $ethnicity,
                $birthDate,
                $address,
                $idNumber,
                $issueAuthority,
                $validPeriod,
                $mobile,
                $userInfo->id
            );

            Db::commit();
            return ['user_id' => $userInfo->id];
        } catch (BusinessException $exception) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::UPLOAD_ERROR);
        }
    }
}
