<?php
declare(strict_types=1);

namespace App\Service\User;

use App\Constants\AppErr;

use App\Constants\Role;
use App\Constants\Wechat;
use App\Constants\WechatMsg;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Helper\Lock;
use App\Model\Pharmacy\User;
use App\Repository\BranchStoreRepository;
use App\Repository\PharmacyPointEventRepository;
use App\Repository\UserClerkRepository;
use App\Repository\UserPatientDoctorRepository;
use App\Repository\UserPatientRepository;
use App\Repository\UserRepository;
use App\Repository\UserRoleRepository;
use App\Service\Agent\AgentService;
use App\Service\Agent\UserRpcService;
use App\Service\Common\SchemaService;
use App\Service\Common\TaskManageService;
use App\Service\Doctor\DoctorCacheService;
use App\Service\Doctor\DoctorGroupRpcService;
use App\Service\Doctor\DoctorInfoRpcService;
use App\Service\Doctor\DoctorService;
use App\Service\Point\PointRpcService;
use Carbon\Carbon;
use Grpc\DoctorInfo\DoctorWechatInfoItem;
use Grpc\Pharmacy\Common\SHARE_TYPE;
use Grpc\Pharmacy\Common\ShareLinkToken;
use Grpc\Point\ActivitySpecialType;
use Grpc\Point\UserType;
use Grpc\Task\GroupDoctorDepartChangeEvent;
use Grpc\Task\SingleDoctorDepartChangeEventParams;
use Grpc\Wxapi\DoctorGroup\DoctorAgentItem;
use Hyperf\DbConnection\Db;
use Hyperf\Nsq\Nsq;
use Hyperf\Utils\ApplicationContext;

class UserRoleService
{
    private static function checkShareTypeRole($shareType, $role)
    {
        //店员能扫邀请码
        //患者能扫医生码和店铺码
        if (!(
        ($role == Role::ROLE_PATIENT && $shareType == SHARE_TYPE::SHARE_AGENT_STORE) ||
        ($role == Role::ROLE_CLERK && $shareType == SHARE_TYPE::SHARE_AGENT_CLERK) ||
        ($role == Role::ROLE_PATIENT && $shareType == SHARE_TYPE::SHARE_AGENT_DOCTOR)
        )) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '当前角色不支持该类型小程序码');
        }
    }

    /**
     * 设置角色
     * @param User $user
     * @param $role
     * @param $tokenId
     * @return bool
     * @throws \Exception
     */
    public function setRole(User $user, $role, $tokenId)
    {
        //解码token，患者可以不通过二维码进入
        $share = null;
        if (!empty($tokenId)) {
            $share = AgentService::getShareLinkFromShareId($tokenId);
            self::checkShareTypeRole($share->getType(), $role);
            if ($share->getType() == SHARE_TYPE::SHARE_AGENT_DOCTOR && $share->getDoctorId() <= 0) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '小程序码参数异常');
            }
        }


        // 创建锁
        $lock = ApplicationContext::getContainer()
            ->get(Lock::class);
        // 加锁
        $lockKey = sprintf("pharmacy:set-role:user:%d", $user->id);
        $identify = $lock->acquire($lockKey, 10, 10);
        if (false === $identify) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, "设置角色，请等待");
        }

        //判断是否先前有扫码记录
        $isFirstScan = false;
        $isPatientScanDoctor = $role == Role::ROLE_PATIENT &&
            $share && $share->getType() == SHARE_TYPE::SHARE_AGENT_DOCTOR && $share->getDoctorId() > 0;
        if ($isPatientScanDoctor) {
            $isFirstScan = !UserPatientDoctorRepository::existPatientDoctor($user->id, $share->getDoctorId());
        }

        // 绑定医生每次都进行
        $this->bindDoctor($user->id, $role, $share);

        // 角色已存在直接切换
        if ($this->isExistRole($user, $role)) {
            $lock->release($lockKey, $identify);
            return true;
        }

        try {
            Db::transaction(function () use ($user, $role, $share) {
                switch ($role) {
                    case Role::ROLE_PATIENT:
                        $this->handlePatient($user->id, $share);
                        break;
                    case Role::ROLE_CLERK:
                        $this->handleClerk($user->id, $share);
                        // 如果是店员自动给店员创建患者角色
                        $this->handlePatient($user->id, $share);
                        break;
                }
            });

            // 释放锁
            $lock->release($lockKey, $identify);
        } catch (\Exception $e) {
            // 释放锁
            $lock->release($lockKey, $identify);
            throw $e;
        }
        if ($isPatientScanDoctor && $isFirstScan) {
            self::sendPatientDoctorScanQrPoint($user->id, $share->getDoctorId());
        }
        return true;
    }

    private static function sendPatientDoctorScanQrPoint($patientId, $doctorId)
    {
        $log = Helper::getLogger();
        $eventKey = 'PATIENT_SCAN_DOCTOR_DOCTOR_POINT';
        $rule = PharmacyPointEventRepository::getByEventKey($eventKey);
        if (!($rule && ($rule->service_point > 0 || $rule->money_point > 0))) {
            $log->warning("patient_scan_doctor_point_not_send", [
                'reason' => '当前没有可用积分规则'
            ]);
            return;
        }

        try {
            //根据医生Id获取医生微信信息
            $doctor = DoctorGroupRpcService::getDoctorInfo($doctorId);
            if (!$doctor->getOpenid()) {
                return;
            }

            //根据微信信息换取优佳医医生用户id
            $resp = DoctorInfoRpcService::getDoctorWechatInfo([$doctor->getOpenid()]);
            if ($resp->getList()->count() == 0) {
                return;
            }
            /** @var DoctorWechatInfoItem $doctorInfoItem */
            $doctorInfoItem = $resp->getList()[0];
            if (!$doctorInfoItem->getDoctorId() > 0) {
                return;
            }

            //获取积分翻倍规则
            $magnification = DoctorService::getDoctorMagnification($doctorId);
            $num = $magnification > 0 ? $magnification : 1;
            //根据用户id发放积分
            $rpc = new PointRpcService();
            $rpc->addActivityPoint(
                $doctorInfoItem->getDoctorId(),
                $eventKey,
                0,
                $num,
                "患者首次扫码奖励金币",
                UserType::USER_TYPE_DOCTOR,
                ActivitySpecialType::CUSTOM,
                $rule->group_id
            );
            $log->info("patient_scan_doctor_send_point", [
                'patient_id' => $patientId,
                'doctor_id' => $doctorId
            ]);
        } catch (\Throwable $e) {
            $log->info("patient_scan_doctor_send_point_error", [
                'patient_id' => $patientId,
                'doctor_id' => $doctorId,
                'msg' => $e->getMessage()
            ]);
        }
    }

    private static function sendDoctorAgentScanQrcodeNotify($doctorId)
    {
        $log = Helper::getLogger();
        try {
            $doctorInfo = DoctorGroupRpcService::GetDoctorInfo($doctorId);

            $agents = DoctorGroupRpcService::getDoctorServeHospitalRelatedAgents($doctorId);
            if ($agents->getAgents()->count() == 0) {
                return;
            }

            /** @var DoctorAgentItem $agent */
            foreach ($agents->getAgents() as $agent) {
                try {
                    UserRpcService::sendUserTemplateMsg(
                        $agent->getId(),
                        WechatMsg::AGENT_PHARMACY_AGENT_NOTIFY,
                        [
                            'first' => '药店关联项目进度实时提醒',
                            'keyword1' => '药店关联',
                            'keyword2' => '客户已扫码',
                            'keyword3' => sprintf("%s已推荐客户扫码成功", $doctorInfo->getName()),
                            'keyword4' => date('m/d H:i:s'),
                            'remark' => '请您注意跟进。谢谢！'
                        ]
                    );
                } catch (\Exception $e) {
                    $log->error('send_doctor_serve_agent_error', [
                        'msg' => $e->getMessage(),
                        'user_id' => $agent->getId()
                    ]);
                }
            }
        } catch (\Exception $e) {
            $log->error('send_doctor_serve_hospital_fail', [
                'msg' => $e->getMessage(),
                'doctor_id' => $doctorId
            ]);
        }
        //获取医生信息

        //获取医生对应代表
    }

    private static function refreshDoctorDepartFromToken($doctorId)
    {
        try {
            $params = new SingleDoctorDepartChangeEventParams();
            $params->setUserId($doctorId);
            $params->setRefrershDepart(true);
            TaskManageService::create(0, $params, 'single-doctor-depart-change-event')
                ->push();
        } catch (\Exception $e) {
            Helper::getLogger()
                ->error('scan_doctor_qrcode_refresh_depart_error', [
                    'doctor_id' => $doctorId,
                    'msg' => $e->getMessage(),
                    'trace' => $e->getTrace()
                ]);
        }
    }

    private function handlePatient($userId, ShareLinkToken $share = null)
    {
        $userPatientInfo = UserPatientRepository::getUserPatientByUserId($userId);
        if ($userPatientInfo) {
            return $userPatientInfo;
        }
        UserPatientRepository::addUserPatient($userId);
        UserRoleRepository::addUserRole($userId, Role::ROLE_PATIENT);
        return true;
    }

    private function handleClerk($userId, ShareLinkToken $share = null)
    {
        $userClerkInfo = UserClerkRepository::getUserClerkByUserId($userId);
        if ($userClerkInfo) {
            return $userClerkInfo;
        }
        if (is_null($share)) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '扫码信息不存在');
        }
        $agentId = 0;
        $storeId = 0;
        if ($share && $share->getStoreId() > 0) {
            $agentId = $share->getAgentId();
            $yyStoreId = $share->getStoreId();
            $storeInfo = BranchStoreRepository::getStoreInfoByYyStoreId($yyStoreId);
            if (!$storeInfo) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '关联药店缺失');
            }
            $storeId = $storeInfo->id;
        }
        UserClerkRepository::addUserClerkRel($userId, $agentId, $storeId);
        UserRoleRepository::addUserRole($userId, Role::ROLE_CLERK);
        UserRepository::updateUserRole($userId, Role::ROLE_CLERK);
        return true;
    }

    public function isExistRole(User $user, $role)
    {
        $roles = UserRoleRepository::getUserRoleByUserId($user->id);
        // 存在且切换
        if ($role && $role != $user->role && $roles->count()>1) {
            UserRepository::updateUserRole($user->id, $role);
            return true;
        }
        if ($role == Role::ROLE_CLERK && $roles->count() == 1) {
            return false;
        }
        if ($role && $roles->count() == 0) {
            return false;
        }
    }

    private function bindDoctor($userId, $role, ShareLinkToken $share = null)
    {
        if ($role == Role::ROLE_PATIENT &&
            $share &&
            $share->getType() == SHARE_TYPE::SHARE_AGENT_DOCTOR &&
            $share->getDoctorId() > 0
        ) {
            $doctorId=  $share->getDoctorId();
            $departId = 0;
            UserPatientDoctorRepository::addPatientDoctorRel($doctorId, $userId, $departId);
            DoctorService::updatePatientDoctorFirstScanInfo($doctorId);
            // 给医生所属代表发送微信模板消息
            go(function () use ($userId, $doctorId) {
                //每天只发一次
                if (UserCacheService::markPatientDoctorMsgSended($userId, $doctorId)) {
                    self::sendDoctorAgentScanQrcodeNotify($doctorId);
                }
            });
            self::refreshDoctorDepartFromToken($doctorId);
        }
    }
}
