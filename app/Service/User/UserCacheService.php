<?php
declare(strict_types=1);
namespace App\Service\User;

use App\Helper\Helper;

class UserCacheService
{

    const SCOPE_DAY = 1;

    const SCOPE_MONTH = 2;

    const SCOPE_QUARTER = 3;

    const SCOPE_YEAR = 4;

    public static function markPatientDoctorMsgSended($userId, $doctorId): bool
    {
        $key = sprintf("pharmacy:p:%d:d:%d:day:%s:msg", $userId, $doctorId, date('Ymd'));
        $redis = Helper::getRedis();
        return $redis->set($key, 1, ['nx', 'ex' => 24 * 3600]) !== false;
    }


    public static function updateUserServicePointRank($userId, $storeId, $servicePoint, $scope = self::SCOPE_DAY)
    {
        $zsetKey = self::getServicePointRankKey($storeId, $scope);
        $user = sprintf('user:%d', $userId);
        Helper::getRedis()->zAdd($zsetKey, $servicePoint, $user);
    }

    public static function updateBranchStoreUserServicePointInfo(
        $userId,
        $branchStoreId,
        $servicePoint,
        $scope = self::SCOPE_DAY
    ) {
        $key = self::buildBranchStoreUserServicePointInfoHashKey($branchStoreId, $scope);
        $field = sprintf('user:%d', $userId);
        Helper::getRedis()->hSet($key, $field, $servicePoint);
    }

    public static function sumBranchStoreServicePoint($branchStoreId, $scope = self::SCOPE_DAY)
    {
        $total = 0.0;
        $hashKey = self::buildBranchStoreUserServicePointInfoHashKey($branchStoreId, $scope);
        $res = Helper::getRedis()->hGetAll($hashKey);
        if (empty($res)) {
            return $total;
        }
        foreach ($res as $item) {
            $total += $item;
        }

        return $total;
    }

    public static function updateBranchStoreServicePointRank($mainStoreId, $storeId, $sum, $scope = self::SCOPE_DAY)
    {
        $zset = self::buildBranchStoreServicePointRankKey($mainStoreId, $scope);
        $store = sprintf('store:%d', $storeId);
        Helper::getRedis()->zAdd($zset, $sum, $store);
    }

    private static function getServicePointRankKey($storeId, $scope)
    {
        switch ($scope) {
            case self::SCOPE_DAY:
                return sprintf('pharmacy:clerk:day:service:point:rank:%d', $storeId);
            case self::SCOPE_MONTH:
                return sprintf('pharmacy:clerk:month:service:point:rank:%d', $storeId);
            case self::SCOPE_QUARTER:
                return sprintf('pharmacy:clerk:quarter:service:point:rank:%d', $storeId);
            case self::SCOPE_YEAR:
                return sprintf('pharmacy:clerk:year:service:point:rank:%d', $storeId);
            default:
                throw new \UnexpectedValueException('类型不正确');
        }
    }

    private static function buildBranchStoreUserServicePointInfoHashKey($branchStoreId, $scope)
    {
        switch ($scope) {
            case self::SCOPE_DAY:
                return sprintf('pharmacy:clerk:day:service:point:branchstore:%d', $branchStoreId);
            case self::SCOPE_MONTH:
                return sprintf('pharmacy:clerk:month:service:point:branchstore:%d', $branchStoreId);
            case self::SCOPE_QUARTER:
                return sprintf('pharmacy:clerk:quarter:service:point:branchstore:%d', $branchStoreId);
            case self::SCOPE_YEAR:
                return sprintf('pharmacy:clerk:year:service:point:branchstore:%d', $branchStoreId);
            default:
                throw new \UnexpectedValueException('类型不正确');
        }
    }

    private static function buildBranchStoreServicePointRankKey($mainStoreId, $scope)
    {
        switch ($scope) {
            case self::SCOPE_DAY:
                return sprintf('pharmacy:store:day:service:point:rank:%d', $mainStoreId);
            case self::SCOPE_MONTH:
                return sprintf('pharmacy:store:month:service:point:rank:%d', $mainStoreId);
            case self::SCOPE_QUARTER:
                return sprintf('pharmacy:store:quarter:service:point:rank:%d', $mainStoreId);
            case self::SCOPE_YEAR:
                return sprintf('pharmacy:store:year:service:point:rank:%d', $mainStoreId);
            default:
                throw new \UnexpectedValueException('类型不正确');
        }
    }

    public static function getUserServicePointRank($mainStoreId, $scope = self::SCOPE_DAY, $limit = 10): array
    {
        $key = self::getServicePointRankKey($mainStoreId, $scope);
        $res = Helper::getRedis()->zRevRange($key, 0, $limit - 1, true);
        if (!$res) {
            return [];
        }

        $list = [];
        foreach ($res as $user => $score) {
            [$prefix, $userID] = explode(":", $user);
            if (!$userID) {
                continue;
            }
            $list[] = [(int) $userID, $score];
        }
        return $list;
    }

    public static function getStoreClerkServicePointRankAndScore($storeId, $userId, $scope = self::SCOPE_DAY): array
    {
        $user = sprintf('user:%d', $userId);
        $key = self::getServicePointRankKey($storeId, $scope);
        $redis = Helper::getRedis();
        $rank = $redis->zRevRank($key, $user);
        $score = $redis->zScore($key, $user);
        return [$rank, $score];
    }

    public static function getStoreServicePointRank($mainStoreId, $scope, $limit = 10)
    {
        $key = self::buildBranchStoreServicePointRankKey($mainStoreId, $scope);
        $res = Helper::getRedis()->zRevRange($key, 0, $limit - 1, true);
        if (!$res) {
            return [];
        }

        $list = [];
        foreach ($res as $user => $score) {
            [$prefix, $storeId] = explode(":", $user);
            if (!$storeId) {
                continue;
            }
            $list[] = [(int) $storeId, $score];
        }
        return $list;
    }

    public static function getBranchStoreServicePointRank($mainStoreId, $storeId, $scope = self::SCOPE_DAY): array
    {
        $key = self::buildBranchStoreServicePointRankKey($mainStoreId, $scope);
        $redis = Helper::getRedis();
        $field = sprintf("store:%d", $storeId);
        $rank = $redis->zRevRank($key, $field);
        $score = $redis->zScore($key, $field);
        return [$rank, $score];
    }

    public static function updateClerkMoneyPoint($mainStoreId, $userId, $productId, $point, $scope = self::SCOPE_DAY)
    {
        $key = self::buildClerkMoneyPointKey($mainStoreId, $productId, $scope);
        $user = sprintf("user:%d", $userId);
        Helper::getRedis()->zAdd($key, $point, $user);
    }

    private static function buildClerkMoneyPointKey($mainStoreId, $productId, $scope = self::SCOPE_DAY)
    {
        switch ($scope) {
            case self::SCOPE_DAY:
                return sprintf('pharmacy:clerk:day:money:point:rank:%d:%d', $productId, $mainStoreId);
            case self::SCOPE_MONTH:
                return sprintf('pharmacy:clerk:month:money:point:rank:%d:%d', $productId, $mainStoreId);
            case self::SCOPE_QUARTER:
                return sprintf('pharmacy:clerk:quarter:money:point:rank:%d:%d', $productId, $mainStoreId);
            case self::SCOPE_YEAR:
                return sprintf('pharmacy:clerk:year:money:point:rank:%d:%d', $productId, $mainStoreId);
            default:
                throw new \UnexpectedValueException('类型不正确');
        }
    }

    public static function saveClerkStoreMoneyPoint($storeId, $userId, $productId, $point, $scope)
    {
        $key = self::buildClerkMoneyPointInfoKey($storeId, $productId, $scope);
        $user = sprintf("user:%d", $userId);
        Helper::getRedis()->hSet($key, $user, $point);
    }

    private static function buildClerkMoneyPointInfoKey($storeId, $productId, $scope)
    {
        switch ($scope) {
            case self::SCOPE_DAY:
                return sprintf('pharmacy:clerk:day:money:point:branchstore:%d:%d', $storeId, $productId);
            case self::SCOPE_MONTH:
                return sprintf('pharmacy:clerk:month:service:point:branchstore:%d:%d', $storeId, $productId);
            case self::SCOPE_QUARTER:
                return sprintf('pharmacy:clerk:quarter:service:point:branchstore:%d:%d', $storeId, $productId);
            case self::SCOPE_YEAR:
                return sprintf('pharmacy:clerk:year:service:point:branchstore:%d:%d', $storeId, $productId);
            default:
                throw new \UnexpectedValueException('类型不正确');
        }
    }

    public static function sumBranchStoreMoneyPoint($storeId, $productId, $scope)
    {
        $key = self::buildClerkMoneyPointInfoKey($storeId, $productId, $scope);
        $res = Helper::getRedis()->hGetAll($key);
        if (!$res) {
            return 0.0;
        }

        $total = 0.0;
        foreach ($res as $point) {
            $total += $point;
        }

        return $total;
    }

    public static function updateBranchStoreMoneyPointRank(
        $mainStoreId,
        $storeId,
        $productId,
        $point,
        $scope = self::SCOPE_DAY
    ) {
        $key = self::buildStoreMoneyPointRankKey($mainStoreId, $productId, $scope);
        $store = sprintf("store:%d", $storeId);
        Helper::getRedis()
            ->zAdd($key, $point, $store);
    }

    private static function buildStoreMoneyPointRankKey($mainStoreId, $productId, $scope)
    {
        switch ($scope) {
            case self::SCOPE_DAY:
                return sprintf('pharmacy:store:day:money:point:rank:%d:%d', $productId, $mainStoreId);
            case self::SCOPE_MONTH:
                return sprintf('pharmacy:store:month:money:point:rank:%d:%d', $productId, $mainStoreId);
            case self::SCOPE_QUARTER:
                return sprintf('pharmacy:store:quarter:money:point:rank:%d:%d', $productId, $mainStoreId);
            case self::SCOPE_YEAR:
                return sprintf('pharmacy:store:year:money:point:rank:%d:%d', $productId, $mainStoreId);
            default:
                throw new \UnexpectedValueException('类型不正确');
        }
    }

    public static function getClerkMoneyPointRank(
        $mainStoreId,
        $productId,
        $scope = self::SCOPE_DAY,
        $limit = 10
    ): array {
        $key = self::buildClerkMoneyPointKey($mainStoreId, $productId, $scope);
        $res = Helper::getRedis()->zRevRange($key, 0, $limit - 1, true);
        if (!$res) {
            return [];
        }

        $list = [];
        foreach ($res as $user => $score) {
            [$prefix, $userId] = explode(':', $user);
            $list[] = [(int) $userId, $score];
        }

        return $list;
    }

    public static function getClerkUserMoneyPointRankAndScore(
        $mainStoreId,
        $productId,
        $clerkId,
        $scope = self::SCOPE_DAY
    ): array {
        $key = self::buildClerkMoneyPointKey($mainStoreId, $productId, $scope);
        $user = sprintf("user:%d", $clerkId);
        $redis = Helper::getRedis();
        $rank = $redis->zRevRank($key, $user);
        $score = $redis->zScore($key, $user);
        return [$rank, $score];
    }

    public static function getStoreMoneyPointRankAndScore(
        $mainStoreId,
        $storeId,
        $productId,
        $scope = self::SCOPE_DAY
    ): array {
        $key = self::buildStoreMoneyPointRankKey($mainStoreId, $productId, $scope);
        $store = sprintf("store:%d", $storeId);
        $redis = Helper::getRedis();
        $rank = $redis->zRevRank($key, $store);
        $score = $redis->zScore($key, $store);
        return [$rank, $score];
    }


    public static function getStoreMoneyPointRank(
        $mainStoreId,
        $productId,
        $scope = self::SCOPE_DAY,
        $limit = 10
    ): array {
        $key = self::buildStoreMoneyPointRankKey($mainStoreId, $productId, $scope);
        $res = Helper::getRedis()->zRevRange($key, 0, $limit - 1, true);
        if (!$res) {
            return [];
        }

        $list = [];
        foreach ($res as $store => $score) {
            [$prefix, $storeId] = explode(':', $store);
            $list[] = [(int) $storeId, $score];
        }

        return $list;
    }

    private static function buildCityServicePointRankKey($mainStoreId, $scope)
    {
        switch ($scope) {
            case self::SCOPE_DAY:
                return sprintf('pharmacy:city:day:service:point:rank:%d', $mainStoreId);
            case self::SCOPE_MONTH:
                return sprintf('pharmacy:city:month:service:point:rank:%d', $mainStoreId);
            case self::SCOPE_QUARTER:
                return sprintf('pharmacy:city:quarter:service:point:rank:%d', $mainStoreId);
            case self::SCOPE_YEAR:
                return sprintf('pharmacy:city:year:service:point:rank:%d', $mainStoreId);
            default:
                throw new \UnexpectedValueException('类型不正确');
        }
    }

    public static function getCityRankList($mainStoreId, $scope, $limit = 10)
    {
        $key = self::buildCityServicePointRankKey($mainStoreId, $scope);
        $res = Helper::getRedis()->zRevRange($key, 0, $limit - 1, true);
        if (!$res) {
            return [];
        }

        $list = [];
        foreach ($res as $city => $score) {
            [$prefix, $cityId] = explode(':', $city);
            $list[] = [(int) $cityId, $score];
        }

        return $list;
    }

    public static function getCityRankAndScore($mainStoreId, $cityId, $scope = self::SCOPE_DAY)
    {
        $key = self::buildCityServicePointRankKey($mainStoreId, $scope);
        $city = sprintf("city:%d", $cityId);

        $rds = Helper::getRedis();
        $rank = $rds->zRevRank($key, $city);
        $score = $rds->zScore($key, $city);
        return [$rank, $score];
    }

    public static function buildCityMoneyPointRankKey($mainStoreId, $productId, $scope)
    {
        switch ($scope) {
            case self::SCOPE_DAY:
                return sprintf('pharmacy:city:day:money:point:rank:%d:%d', $mainStoreId, $productId);
            case self::SCOPE_MONTH:
                return sprintf('pharmacy:city:month:money:point:rank:%d:%d', $mainStoreId, $productId);
            case self::SCOPE_QUARTER:
                return sprintf('pharmacy:city:quarter:money:point:rank:%d:%d', $mainStoreId, $productId);
            case self::SCOPE_YEAR:
                return sprintf('pharmacy:city:year:money:point:rank:%d:%d', $mainStoreId, $productId);
            default:
                throw new \UnexpectedValueException('类型不正确');
        }
    }

    public static function getCityMoneyPointRankList($mainStoreId, $productId, $scope = self::SCOPE_DAY, $limit = 10)
    {
        $key = self::buildCityMoneyPointRankKey($mainStoreId, $productId, $scope);
        var_dump($key);
        $res = Helper::getRedis()->zRevRange($key, 0, $limit - 1, true);
        if (!$res) {
            return [];
        }

        $list = [];
        foreach ($res as $city => $score) {
            [$prefix, $cityId] = explode(':', $city);
            $list[] = [(int) $cityId, $score];
        }

        return $list;
    }

    public static function getCityMoneyPointRankAndScore($mainStoreId, $productId, $cityId, $scope = self::SCOPE_DAY)
    {
        $key = self::buildCityMoneyPointRankKey($mainStoreId, $productId, $scope);
        $city = sprintf("city:%d", $cityId);

        $rds = Helper::getRedis();
        $rank = $rds->zRevRank($key, $city);
        $score = $rds->zScore($key, $city);
        return [$rank, $score];
    }
}
