<?php
declare(strict_types=1);

namespace App\Service\User;

use App\Constants\AppErr;
use App\Constants\Auth;
use App\Constants\BizAgentType;
use App\Constants\Clerk;
use App\Constants\Role;
use App\Exception\AuthException;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Pharmacy\BranchStore;
use App\Model\Pharmacy\City;
use App\Model\Pharmacy\ClerkApply;
use App\Model\Pharmacy\PharmacyPointEvent;
use App\Model\Pharmacy\User;
use App\Model\Pharmacy\UserClerk;
use App\Repository\BizAgentRelRepository;
use App\Repository\BranchStoreRepository;
use App\Repository\CityRepository;
use App\Repository\ClerkApplyRepository;
use App\Repository\OrderRepository;
use App\Repository\UserClerkAgentRepository;
use App\Repository\UserClerkRepository;
use App\Repository\UserRepository;
use App\Service\Common\BizAgentService;
use App\Service\Point\PointService;
use App\Service\Store\StoreService;
use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Context;

class UserClerkService
{

    private static function processClerkAuditPoint(UserClerk $clerk)
    {
        go(function () use ($clerk) {
            try {
                PointService::sendClerkPointFromCommonEvent(
                    "PHARMACY_CLERK_VERIFY",
                    $clerk,
                    '',
                    1,
                    "完成注册认证获得积分奖励"
                );
            } catch (\Exception $e) {
                Helper::getLogger()->error("send_clerk_audit_point_error", [
                    'msg' =>$e->getMessage(),
                    'trace' => $e->getTrace()
                ]);
            }
        });
    }

    public static function getUserServicePointRank($type = UserCacheService::SCOPE_DAY, $limit = 10): array
    {
        $user = self::checkAndGetClerk();
        $store = BranchStoreRepository::getStoreInfoById($user->store_id);
        if (!$store) {
            return [[], []];
        }
        $userScore = UserCacheService::getUserServicePointRank($store->main_store_id, $type, $limit);
        if (empty($userScore)) {
            return [[], []];
        }

        $list = [];
        $userIds = [];
        foreach ($userScore as $us) {
            $userIds[] = $us[0];
        }

        $users = UserClerkRepository::getUserWithStoreByIds($userIds);
        $userMap =$users->keyBy('id')->all();
        foreach ($userScore as $us) {
            [$userId, $score] = $us;
            if (!isset($userMap[$userId])) {
                continue;
            }
            $userInfo = $userMap[$userId];
            $list[] = [
                'id' => $userId,
                'name' => $userInfo ? ($userInfo->real_name ? $userInfo->real_name : $userInfo->nick_name) : '',
                'store' => $userInfo ? $userInfo->store_name : '',
                'point' => $score,
                'cover' => $userInfo ? $userInfo->header_url : ''
            ];
        }

        //获取用户自身信息
        [$userRank, $userScore] = UserCacheService::getStoreClerkServicePointRankAndScore(
            $store->main_store_id,
            $user->id,
            $type
        );
        $mine = [
            'id' => $user->id,
            'name' => $user->real_name ? $user->real_name : '',
            'store' => $store ? $store->store_name : '',
            'point' => false === $userScore ? 0 : $userScore,
            'rank' => false === $userRank ? 0 : $userRank + 1
        ];
        return [$list, $mine];
    }

    public static function checkAndGetClerk(): UserClerk
    {
        $user = Helper::getLoginUser();
        $role = Helper::getUserLoginRole();
        if (!$role->isClerk()) {
            throw new AuthException(Auth::USER_FORBIDDEN);
        }
        $clerk = UserClerkRepository::getUserClerkByUserId($user->id);
        if (!$clerk) {
            throw new AuthException(Auth::USER_FORBIDDEN, "店员帐号不存在或已禁用");
        }
        return $clerk;
    }

    public static function getStoreServicePointRank($scope = UserCacheService::SCOPE_DAY, $limit = 10): array
    {
        $user = self::checkAndGetClerk();
        $store = BranchStoreRepository::getStoreInfoById($user->store_id);
        if (!$store) {
            return [[], []];
        }

        $storeScore = UserCacheService::getStoreServicePointRank($store->main_store_id, $scope, $limit);
        if (empty($storeScore)) {
            return [[], []];
        }

        $storeIds = [];
        foreach ($storeScore as $ss) {
            $storeIds[]  = $ss[0];
        }
        $branchStores = BranchStoreRepository::getStoreByIdsWithCity(array_unique($storeIds));
        $storeMap = $branchStores->keyBy('id')->all();

        $list = [];
        foreach ($storeScore as $ss) {
            [$storeId, $score] = $ss;
            if (!isset($storeMap[$storeId])) {
                continue;
            }
            /** @var BranchStore $branchStore */
            $branchStore = $storeMap[$storeId];
            $list[] = [
                'id' => $branchStore->id,
                'name' => $branchStore->store_name,
                'point' => $score,
                'city' => $branchStore->city_name
            ];
        }

        //获取用户当前所属店铺积分信息
        $cityName = "";
        if ($store->city_id) {
            $city = CityRepository::getInfoById($store->city_id);
            $cityName = $city ? $city->city_name : '';
        }
        [$rank, $score] = UserCacheService::getBranchStoreServicePointRank($store->main_store_id, $store->id);
        $mine = [
            'id' => $store->id,
            'name' => $store->store_name,
            'point' => $score !== false ? $score : '',
            'rank' => $rank === false ? '' : $rank + 1,
            'city' => $cityName
        ];
        return [$list, $mine];
    }

    public static function getUserMoneyPointRank($scope, $productId, $limit = 10): array
    {
        $clerk = self::checkAndGetClerk();
        if (!$clerk->store_id) {
            return [[], []];
        }
        $store = BranchStoreRepository::getStoreInfoById($clerk->store_id);
        if (!$store) {
            return [[], []];
        }

        $rankResp = UserCacheService::getClerkMoneyPointRank($store->main_store_id, $productId, $scope);
        if (empty($rankResp)) {
            return [[], []];
        }

        $clerkIds = [];
        foreach ($rankResp as $item) {
            $clerkIds[] = $item[0];
        }

        $resp = UserClerkRepository::getUserWithStoreByIds($clerkIds);
        $userMap = $resp->keyBy('id')->all();

        $list = [];
        foreach ($rankResp as $item) {
            [$userId, $score] = $item;
            if (!isset($userMap[$userId])) {
                continue;
            }
            $user = $userMap[$userId];
            $list[] = [
                'id' => $user->id,
                'name' => $user->real_name,
                'store' => $user->store_name,
                'point' => $score,
                'cover' => $user->header_url ? $user->header_url : ''
            ];
        }

        //获取用户自身排名
        [$userRank, $userScore] = UserCacheService::getClerkUserMoneyPointRankAndScore(
            $store->main_store_id,
            $productId,
            $clerk->id,
            $scope
        );
        $mine = [
            'id' => $clerk->id,
            'name' => $clerk->real_name,
            'store' => $store->store_name,
            'point' => $userScore === false ? 0 : $userScore,
            'rank' => $userRank === false ?  0 : $userRank + 1
        ];

        return [$list, $mine];
    }

    public static function getStoreMoneyPointRank($scope, $productId, $limit = 10): array
    {
        $clerk = self::checkAndGetClerk();
        if (!$clerk->store_id) {
            return [[], []];
        }
        $store = BranchStoreRepository::getStoreInfoById($clerk->store_id);
        if (!$store) {
            return [[], []];
        }

        $rankResp = UserCacheService::getStoreMoneyPointRank($store->main_store_id, $productId, $scope, $limit);
        if (!$rankResp) {
            return [[], []];
        }

        $storeIds = [];
        foreach ($rankResp as $item) {
            $storeIds[] = $item[0];
        }
        $storeListResp = BranchStoreRepository::getStoreByIdsWithCity(array_unique($storeIds));
        $storeMap = $storeListResp->keyBy('id')->all();

        $list = [];
        foreach ($rankResp as $item) {
            [$storeId, $score] = $item;
            if (!isset($storeMap[$storeId])) {
                continue;
            }

            /** @var BranchStore $storeItem */
            $storeItem = $storeMap[$storeId];
            $list[] = [
                'id' => $storeItem->id,
                'name' => $storeItem->store_name,
                'point' => $score,
                'city' => $storeItem->city_name
            ];
        }

        $cityName = "";
        if ($store->city_id) {
            $city = CityRepository::getInfoById($store->city_id);
            $cityName = $city ? $city->city_name : '';
        }
        [$storeRank, $storeScore] = UserCacheService::getStoreMoneyPointRankAndScore(
            $store->main_store_id,
            $store->id,
            $productId,
            $scope
        );
        $mine = [
            'id' => $store->id,
            'name' => $store->store_name,
            'point' => false !== $storeScore ? $storeScore : 0,
            'rank' => false !== $storeRank ? $storeRank + 1 : 0,
            'city' => $cityName
        ];

        return [$list, $mine];
    }

    /**
     * 店员申请认证
     * @param $userId
     * @param $storeId
     * @param $realName
     * @param $phone
     * @param $role
     * @param $position
     * @return bool
     */
    public function applyClerk($userId, $storeId, $realName, $phone, $role, $position)
    {
        if (ClerkApplyRepository::existApply($userId)) {
            throw new BusinessException(AppErr::CLERK_APPLY_EXISTED);
        } else {
            Db::beginTransaction();
            try {
                $storeInfo = BranchStoreRepository::getStoreInfoById($storeId);
                $clerkApplyModel = new ClerkApply();
                $clerkApplyModel->user_id = $userId;
                $clerkApplyModel->city_id = $storeInfo->city_id;
                $clerkApplyModel->main_store_id = $storeInfo->main_store_id;
                $clerkApplyModel->store_id = $storeId;
                $clerkApplyModel->real_name = $realName;
                $clerkApplyModel->phone = $phone;
                $clerkApplyModel->role = $role;
                $clerkApplyModel->position = $position;
                $clerkApplyModel->status = Clerk::STATUS_AUDITING;
                ClerkApplyRepository::addApply($clerkApplyModel);
                UserClerkRepository::updateStatus($userId, Clerk::STATUS_AUDITING);
                Db::commit();
            } catch (\Exception $e) {
                Db::rollBack();
            }
        }
        return true;
    }

    /**
     * 直接通过认证注册
     * @param $userId
     * @param $storeId
     * @param $realName
     * @param $phone
     * @param $role
     * @param $position
     */
    public function directPassClerk($userId, $storeId, $realName, $phone, $role, $position)
    {
        $clerk = UserClerkService::checkAndGetClerk();
        // 直接审核通过
        if ($clerk->status == Clerk::STATUS_PASS) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '店员已注册');
        }
        Db::beginTransaction();
        try {
            $storeInfo = BranchStoreRepository::getStoreInfoById($storeId);
            $clerkApplyModel = new ClerkApply();
            $clerkApplyModel->user_id = $userId;
            $clerkApplyModel->city_id = $storeInfo->city_id;
            $clerkApplyModel->main_store_id = $storeInfo->main_store_id;
            $clerkApplyModel->store_id = $storeId;
            $clerkApplyModel->real_name = $realName;
            $clerkApplyModel->phone = $phone;
            $clerkApplyModel->role = $role;
            $clerkApplyModel->position = $position;
            $clerkApplyModel->status = Clerk::STATUS_PASS;
            ClerkApplyRepository::addApply($clerkApplyModel);
            $clerk->store_id = $storeId;
            $clerk->role = $role;
            $clerk->phone = $phone;
            $clerk->real_name = $realName;
            $clerk->position = $position;
            $clerk->register_time = time();
            $clerk->status = Clerk::STATUS_PASS;
            $clerk->updated_at = date('Y-m-d H:i:s');
            UserClerkRepository::saveClerk($clerk);
            $user = UserRepository::getUserById($userId);
            $user->real_name = $realName;
            $user->phone = $phone;
            $user->save();
            Db::commit();
            //记录业务关联代表信息
            BizAgentService::recordAgent(
                $clerk->id,
                $storeId,
                BizAgentType::BIZ_TYPE_CLERK,
                $clerk->agent_id
            );
            self::processClerkAuditPoint($clerk);
        } catch (\Exception $e) {
            Db::rollBack();
            Helper::getLogger()->error("clerk_apply_register", [
                'msg' =>$e->getMessage(),
                'trace' => $e->getTrace()
            ]);
        }
    }

    /**
     * 我的店员列表
     * @param $month
     * @param $productId
     * @param $specsId
     * @return array
     */
    public function myClerkList($month, $productId, $specsId)
    {
        $user = Helper::getLoginUser();
        $role = Context::get('role');
        if ($role != Role::ROLE_CLERK) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '非店员操作');
        }
        $clerkInfo = UserClerkRepository::getUserClerkByUserId($user->id);
        if (empty($clerkInfo)) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '非店员操作');
        }
        if ($clerkInfo->role != Clerk::ROLE_MANAGER) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '非店长操作');
        }

        $storeId = $clerkInfo->store_id;
        $list = UserClerkRepository::getMyClerkListByStoreId($storeId);
        $num = 0;
        $amount = 0;
        if ($list->count() > 0) {
            $clerkIds = $list->pluck('user_id');
            $userMap = UserRepository::getListByUserIds($clerkIds)
                ->pluck(null, 'id');
            $saleMap = OrderRepository::totalOrderDataByClerkIds(
                $storeId,
                $clerkIds,
                $month,
                $productId,
                $specsId
            )->pluck(null, 'clerk_user_id');
            $list = $list->map(function ($row)
 use ($saleMap, $userMap, &$num, &$amount) {
                $saleInfo = $saleMap[$row->user_id]??null;
                $userInfo = $userMap[$row->user_id]??null;
                if ($saleInfo) {
                    $row->num = $saleInfo->num;
                    $row->amount = $saleInfo->amount;
                    $num =  $num + $row->num;
                    $amount =  $amount + $row->amount;
                } else {
                    $row->num = 0;
                    $row->amount = 0;
                }
                if ($userInfo) {
                    $row->header_url = $userInfo->header_url??'';
                } else {
                    $row->header_url = '';
                }
                return $row;
            });
        }
        return [
            'list'=>$list,
            'sum'=>[
                'num'=>$num,
                'amount'=>$amount
            ]
        ];
    }

    public function myClerkSaleInfo($clerkId, $month, $productId, $specsId)
    {
        $clerkInfo = UserClerkRepository::getUserClerkByUserId($clerkId);
        if (empty($clerkInfo)) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '店员信息不存在');
        }
        $storeInfo = BranchStoreRepository::getStoreInfoById($clerkInfo->store_id);
        $clerkInfo->store_name = $storeInfo->store_name;
        $clerkInfo->store_name_alias = $storeInfo->store_name_alias;
        $clerkInfo->location = $storeInfo->location;
        $clerkInfo->header_url = Helper::getLoginUser()->header_url;
        $sumInfo = OrderRepository::totalOrderDataByClerk($clerkInfo, $month, $productId, $specsId);
        /** @var Carbon $lastSaleTime */
        $lastSaleTime = $sumInfo->created_at;
        return [
            'store_info'=>$clerkInfo,
            'sum'=>[
                'num'=>$sumInfo->num?:0,
                'amount'=>$sumInfo->amount?:0,
                'last_sale_time'=>$lastSaleTime?$lastSaleTime->format('Y-m-d H:i:s'):''
            ]
        ];
    }

    public static function getCityServicePointRank($scope, $limit = 10): array
    {
        $clerk = self::checkAndGetClerk();
        if (!$clerk->store_id) {
            return [[], []];
        }
        $store = BranchStoreRepository::getStoreInfoById($clerk->store_id);
        if (!($store && $store->city_id)) {
            return [[], []];
        }
        $city = CityRepository::getInfoById($store->city_id);
        if (!$city) {
            return [[], []];
        }

        $scoreRanks = UserCacheService::getCityRankList($store->main_store_id, $scope, $limit);
        if (empty($scoreRanks)) {
            return [[], []];
        }

        $cityIds = [];
        foreach ($scoreRanks as $scoreRank) {
            $cityIds[] =$scoreRank[0];
        }

        $cities = CityRepository::getByIds($cityIds);
        $cityMap = $cities->keyBy('id')->all();
        $list = [];
        foreach ($scoreRanks as $scoreRank) {
            [$cityID, $score] = $scoreRank;
            if (!isset($cityMap[$cityID])) {
                continue;
            }
            /** @var City $cityItem*/
            $cityItem = $cityMap[$cityID];
            $list[] = [
                'id' => $cityItem->id,
                'name' => $cityItem->city_name,
                'point' => $score
            ];
        }


        //获取当前店铺所属城市信息
        [$cityRank, $cityScore] = UserCacheService::getCityRankAndScore($store->main_store_id, $store->city_id, $scope);
        $mine = [
            'id' => $city->id,
            'name' => $city->city_name,
            'point' => false === $cityScore ? 0 : $cityScore,
            'rank' => false === $cityRank ? 0 : $cityRank + 1
        ];

        return [$list, $mine];
    }

    public static function getCityMoneyPointRankInfo($scope, $productId, $limit = 10)
    {
        $clerk = self::checkAndGetClerk();
        if (!$clerk->store_id) {
            return [[], []];
        }
        $store = BranchStoreRepository::getStoreInfoById($clerk->store_id);
        if (!($store && $store->city_id)) {
            return [[], []];
        }
        $city = CityRepository::getInfoById($store->city_id);
        if (!$city) {
            return [[], []];
        }

        $cityMpointRank = UserCacheService::getCityMoneyPointRankList(
            $store->main_store_id,
            $productId,
            $scope,
            $limit
        );
        if (empty($cityMpointRank)) {
            return [[], []];
        }

        $cityIds = [];
        foreach ($cityMpointRank as $scoreRank) {
            $cityIds[] =$scoreRank[0];
        }

        $cities = CityRepository::getByIds($cityIds);
        $cityMap = $cities->keyBy('id')->all();
        $list = [];
        foreach ($cityMpointRank as $scoreRank) {
            [$cityID, $score] = $scoreRank;
            if (!isset($cityMap[$cityID])) {
                continue;
            }
            /** @var City $cityItem*/
            $cityItem = $cityMap[$cityID];
            $list[] = [
                'id' => $cityItem->id,
                'name' => $cityItem->city_name,
                'point' => $score
            ];
        }


        //获取当前店铺所属城市信息
        [$cityRank, $cityScore] = UserCacheService::getCityMoneyPointRankAndScore(
            $store->main_store_id,
            $productId,
            $store->city_id,
            $scope
        );
        $mine = [
            'id' => $city->id,
            'name' => $city->city_name,
            'point' => false === $cityScore ? 0 : $cityScore,
            'rank' => false === $cityRank ? 0 : $cityRank + 1
        ];

        return [$list, $mine];
    }
}
