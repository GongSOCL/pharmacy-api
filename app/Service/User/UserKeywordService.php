<?php
declare(strict_types=1);

namespace App\Service\User;

use App\Helper\Helper;
use App\Repository\UserKeywordRepository;

class UserKeywordService
{
    public function getKeywordList()
    {
        $user = Helper::getLoginUser();
        return UserKeywordRepository::getKeywords($user->id);
    }

    public function deleteKeyword()
    {
        $user = Helper::getLoginUser();
        return UserKeywordRepository::deleteKeyword($user->id);
    }
}
