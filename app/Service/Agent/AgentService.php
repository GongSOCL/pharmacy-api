<?php
declare(strict_types=1);
namespace App\Service\Agent;

use App\Constants\AppErr;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Pharmacy\ShareTokenSchema;
use App\Service\Common\SchemaService;
use App\Service\Doctor\DoctorGroupRpcService;
use Grpc\Pharmacy\Common\SHARE_TYPE;
use Grpc\Pharmacy\Common\ShareLinkToken;
use Grpc\Pharmacy\Wechat\SchemaReply;
use Grpc\Upload\StorageChannel;
use Grpc\Wxapi\DoctorGroup\GroupDoctorInfoReply;
use Grpc\Wxapi\User\AgentDoctorItem;

class AgentService
{

    public static function listAgentDoctors($token, $current = 1, $limit = 10): array
    {
        //解析token,解出代表id
        $userId = self::checkAndGetAgent($token);

        //调用wxapi服务查询医生
        $resp = DoctorGroupRpcService::getAgentGroupDoctors($userId, $current, $limit);
        $page = Helper::buildPageFromCommon($resp->getPage());
        if ($resp->getList()->count() == 0) {
            return [$page, []];
        }

        $data = [];
        /** @var GroupDoctorInfoReply $doctor */
        foreach ($resp->getList() as $doctor) {
            $data[] =  [
                'id' => $doctor->getId(),
                'name' => $doctor->getName(),
                'headimg' => $doctor->getHeadimg(),
                'hospital_name' => $doctor->getHospitalName(),
                'depart_name' => $doctor->getDepartName(),
                'invited' => 0,
                'buyed' => 0
            ];
        }

        return [$page, $data];
    }

    public static function checkAndGetAgent($token): int
    {
        $t = Helper::decodeShareToken($token);
        if (!($t && $t->getAgentId() > 0)) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, "代表不存在");
        }
        return $t->getAgentId();
    }

    public static function getShareLinkTokenSchema(
        $bizType,
        $bizId,
        $page,
        ShareLinkToken $share,
        $size = 430,
        $channel = StorageChannel::CHANNEL_QINIU
    ): string {
        $isExists = SchemaService::isExists($bizType, $bizId, $size);
        if (!$isExists) {
            return SchemaService::newSchemaRecord(
                bin2hex($share->serializeToString()),
                $bizType,
                $bizId,
                $page,
                $size,
                $channel
            );
        } else {
            return SchemaService::getSchemaUploadImage($bizType, $bizId, $size);
        }
    }

    public static function generateDoctorQrLink($token, $doctorId): string
    {
        $agentId = self::checkAndGetAgent($token);

        //检查代表医生关联关系
        if (!DoctorGroupRpcService::checkAgentOwnerDoctor($agentId, $doctorId)) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, "该医生不在群组内");
        }
        //生成链接
        $share = new ShareLinkToken();
        $share->setAgentId($agentId);
        $share->setDoctorId($doctorId);
        $share->setType(SHARE_TYPE::SHARE_AGENT_DOCTOR);
        return self::getShareLinkTokenSchema(
            ShareTokenSchema::BIZ_TYPE_DOCTOR_SCHEMA,
            $doctorId,
            'pages/patient/purchase',
            $share
        );
    }

    public static function getShareLinkFromShareId($tokenId): ShareLinkToken
    {
        $token = SchemaService::getStoredShareToken($tokenId);
        try {
            $share = new ShareLinkToken();
            $share->mergeFromString(hex2bin($token));
            return $share;
        } catch (\Exception $e) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '非法请求，分享码错误');
        }
    }

    public static function generateStorePatientQrcode($storeId, $agentId, $size = 430): SchemaReply
    {
        $share = new ShareLinkToken();
        $share->setAgentId($agentId);
        $share->setStoreId($storeId);
        $share->setType(SHARE_TYPE::SHARE_AGENT_STORE);

        $link = AgentService::getShareLinkTokenSchema(
            ShareTokenSchema::BIZ_TYPE_STORE_SCHEMA,
            $storeId,
            'pages/patient/scancodeTip',
            $share,
            $size,
            StorageChannel::CHANNEL_MINIO
        );
        $reply = new SchemaReply();
        $reply->setUrl($link);
        return $reply;
    }
}
