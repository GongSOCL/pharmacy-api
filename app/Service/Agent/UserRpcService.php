<?php
declare(strict_types=1);
namespace App\Service\Agent;

use Grpc\Common\CommonPageRequest;
use Grpc\Wxapi\User\AgentDoctorGetReply;
use Grpc\Wxapi\User\AgentDoctorGetRequest;
use Grpc\Wxapi\User\AgentServeHospitalsReply;
use Grpc\Wxapi\User\AgentSpecifyHospitalsReply;
use Grpc\Wxapi\User\SendUserTemplateMsgRequest;
use Grpc\Wxapi\User\StoreIdRequest;
use Grpc\Wxapi\User\UserIdRequest;
use Grpc\Wxapi\User\UserServeHospitalCheckRequest;
use Grpc\Wxapi\User\UserSvcClient;
use Hyperf\Utils\ApplicationContext;

class UserRpcService
{
    private static function getClient(): UserSvcClient
    {
        return ApplicationContext::getContainer()
            ->get(UserSvcClient::class);
    }

    public static function getAgentServeHospitalDoctors(
        $userId,
        $current = 1,
        $limit = 10,
        $withWechat = true
    ): AgentDoctorGetReply {
        $req = new AgentDoctorGetRequest();
        $req->setUserId($userId);
        $req->setWithWechat($withWechat);
        $page = new CommonPageRequest();
        $page->setPage($current);
        $page->setSize($limit);
        $req->setPage($page);
        return self::getClient()->getAgentServeHospitalDoctors($req);
    }

    /**
     * 获取代表自己选择的服务药店
     * @param $userId
     * @return AgentServeHospitalsReply
     */
    public static function getAgentServeHospitals($userId): AgentServeHospitalsReply
    {
        $req = new UserIdRequest();
        $req->setUid($userId);
        return self::getClient()->getAgentServeHospitals($req);
    }

    /**
     * 获取代表指定的服务药店
     * @param $userId
     * @return AgentSpecifyHospitalsReply
     */
    public static function getAgentSpecialHospitals($userId, $keywords = ''): AgentSpecifyHospitalsReply
    {
        $req = new UserIdRequest();
        $req->setUid($userId);
        $req->setKeywords((string) $keywords);
        return self::getClient()->getAgentSpecifyHospitals($req);
    }

    public static function checkUserServeHospital($agentId, $hospitalId): bool
    {
        $req = new UserServeHospitalCheckRequest();
        $req->setUserId($agentId);
        $req->setHospitalId($hospitalId);
        return self::getClient()->checkUserServeHospital($req)
            ->getIsPass();
    }

    public static function checkUserSpecifyServeHospital($agentId, $hospitalId): bool
    {
        $req = new UserServeHospitalCheckRequest();
        $req->setUserId($agentId);
        $req->setHospitalId($hospitalId);
        return self::getClient()->checkUserSpecifyServeHospital($req)
            ->getIsPass();
    }

    public static function sendUserTemplateMsg($agentId, $tplAlias, array $params = [], $url = "")
    {
        $req = new SendUserTemplateMsgRequest();
        $req->setUserId($agentId);
        $req->setParams($params);
        $req->setTplAlias($tplAlias);
        $req->setUrl($url);
        self::getClient()->sendUserTemplateMsg($req);
    }

    public static function getStoreAgents($storeId)
    {
        $req = new StoreIdRequest();
        $req->setStoreId($storeId);
        return self::getClient()->getStoreAgents($req);
    }
}
