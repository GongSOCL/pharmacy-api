<?php
declare(strict_types=1);
namespace App\Service\Order;

use App\Constants\AppErr;
use App\Constants\Auth;
use App\Constants\Clerk;
use App\Constants\InsideMsgTypes;
use App\Constants\Role;
use App\Exception\AuthException;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Helper\Lock;
use App\Model\Pharmacy\Order;
use App\Model\Pharmacy\OrderDetail;
use App\Model\Pharmacy\OrderTicket;
use App\Model\Pharmacy\PharmacyPointOrderDrugRule;
use App\Model\Pharmacy\PurchaseNotify;
use App\Model\Pharmacy\UserClerk;
use App\Repository\BranchStoreRepository;
use App\Repository\CouponDetailRepository;
use App\Repository\OrderCouponRepository;
use App\Repository\OrderDetailRepository;
use App\Repository\OrderPointRepository;
use App\Repository\OrderRepository;
use App\Repository\OrderTicketRepository;
use App\Repository\OrderUserRepository;
use App\Repository\PharmacyPointOrderDrugRuleRepository;
use App\Repository\PurchaseNotifyRepository;
use App\Repository\UserClerkRepository;
use App\Repository\UserRepository;
use App\Repository\UserRoleRepository;
use App\Service\Common\InsideMsgRpcService;
use App\Service\Common\ProductRpcService;
use App\Service\Common\TaskManageService;
use App\Service\Common\TokenRpcService;
use App\Service\Common\UploadRpcService;
use App\Service\Point\PointRpcService;
use App\Service\User\UserClerkService;
use Carbon\Carbon;
use Grpc\Common\ProductItem;
use Grpc\Common\ProductSkuItem;
use Grpc\Point\ActivitySpecialType;
use Grpc\Point\UserType;
use Grpc\Task\PharmacyOrderCreatedEvent;
use Grpc\Upload\UploadFileItem;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Collection;
use Hyperf\Utils\Context;

class OrderService
{
    public static function submit(
        $notifyId,
        array $pics,
        array $drugs,
        array $couponNums = [],
        $lon = 0.0,
        $lat = 0.0
    ): Order {
        //获取登陆用户clerk
        $clerk = UserClerkService::checkAndGetClerk();
        if ($clerk->status != Clerk::STATUS_PASS) {
            throw new AuthException(Auth::USER_FORBIDDEN, "店员账号未认证");
        }
        //查询购买通知是否已经占用
        $notify = PurchaseNotifyRepository::getNotifyInfo($notifyId);
        if (!$notify->isPending()) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, "该订单已被处理");
        }
        if ((time() - $notify->created_at->getTimestamp()) >= 3600) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, "该订单已过期");
        }

        $lock = ApplicationContext::getContainer()
            ->get(Lock::class);
        $key = sprintf("order:%d", $notifyId);
        $identify = $lock->acquire($key, 10, 10);
        if (false === $identify) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, "订单抢单失败，请重新提交");
        }

        $notify->refresh();
        try {
            if (!$notify->isPending()) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, "订单已被处理,请返回重试");
            }

            $order = self::processOrder($clerk, $notify, $pics, $drugs, $couponNums, $lon, $lat);

            $lock->release($key, $identify);

            //处理患者订单积分
            self::processOrderPatientPoint($order);

            //投递订单创建事件到队列里面
            $params = new PharmacyOrderCreatedEvent();
            $params->setOrderId($order->id);
            $params->setIsNotify(true);
            TaskManageService::create(0, $params, 'pharmacy-order-created')
                ->push();

            $patientId = $order->patient_user_id;
            $orderId = $order->id;
            go(function () use ($patientId, $orderId) {
                self::notifyPatientFromOrder($patientId, $orderId);
            });

            return $order;
        } catch (\Exception $e) {
            $lock->release($key, $identify);
            //如果订单被其它人抢单成功，上传的图片需要删除
            if ($e instanceof BusinessException && $e->getCode() == AppErr::CLERK_RECEIVE_FAIL) {
                UploadRpcService::deleteFiles($pics, false);
            }
            throw $e;
        }
    }

    private static function notifyPatientFromOrder($patientId, $orderId)
    {
        $logger = Helper::getLogger();
        try {
            //通知患者下单购买成功
            $role = UserRoleRepository::getUserSpecialRole($patientId, Role::ROLE_PATIENT);
            if ($role) {
                ApplicationContext::getContainer()
                    ->get(InsideMsgRpcService::class)
                    ->sendInsideMsg(
                        $role->id,
                        '恭喜你下单购买成功~',
                        '下单成功',
                        InsideMsgTypes::MSG_TYPE_PATIENT_ORDER_FINISHED,
                        '恭喜你下单购买成功~'
                    );
            } else {
                $logger->warning("patient_skip_send_order_finished_msg", [
                    'reason' => 'role not exists'
                ]);
            }
        } catch (\Exception $e) {
            $logger->error("send_patient_order_finished_msg_error", [
                'order_id' => $orderId,
                'msg' => $e->getMessage()
            ]);
        }
    }

    /**
     * 生成订单
     * @param UserClerk $clerk
     * @param PurchaseNotify $notify
     * @param array $pics
     * @param array $drugs
     * @param array $couponNums
     * @param float $lon
     * @param float $lat
     * @return Order
     * @throws \Exception
     */
    public static function processOrder(
        UserClerk $clerk,
        PurchaseNotify $notify,
        array $pics,
        array $drugs,
        array $couponNums = [],
        $lon = 0.0,
        $lat = 0.0
    ) {
        $total = 0;
        $amount = 0.00;
        $skuIds = $productIds = [];
        foreach ($drugs as $k => $drug) {
            if (!isset($drug['sku_id'])) {
                $drugs[$k]['sku_id'] = 0;
            } else {
                $skuIds[] = $drug['sku_id'];
            }
            $productIds[] = $drug['product_id'];
            $total += intval($drug['total']);
            $amount = bcadd((string)$amount, (string)$drug['cash'], 2);
        }
        $coupons = CouponDetailRepository::getCouponDetailListByNums($couponNums);
        $pn = self::getProductNameMapByIds($productIds);
        $sn = self::getProductSpecMapsBySkuIds($skuIds);
        $details = [];
        foreach ($drugs as $drug) {
            if ($drug['product_id'] > 0 && !isset($pn[$drug['product_id']])) {
                //产品不存在
                continue;
            }
            $productName= $pn[$drug['product_id']];
            $skuName = isset($drug['sku_id']) && $drug['sku_id'] > 0 &&
            isset($sn[$drug['sku_id']]) ? $sn[$drug['sku_id']] : '';
            $details[] = [
                'product_id' => $drug['product_id'],
                'product_name' => $productName,
                'sku_id' => $drug['sku_id'],
                'sku_name' => $skuName,
                'total' => $drug['total'],
                'amount' => bcadd((string)$drug['cash'], '0.00', 2)
            ];
        }
        // 订单明细不可为空
        if (empty($details)) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, "订单录入失败，明细为空");
        }

        //生成唯一订单号
        $orderNo = ApplicationContext::getContainer()
            ->get(TokenRpcService::class)
            ->generateUniqId();

        Db::beginTransaction();
        try {
            //修改接单记录
            $res = PurchaseNotifyRepository::receivePurchase($notify, $clerk);
            if (!$res) {
                throw new BusinessException(AppErr::CLERK_RECEIVE_FAIL);
            }

            //添加订单记录表
            $order = OrderRepository::addOrder(
                $clerk,
                $notify,
                $orderNo,
                $amount,
                $total,
                $lon,
                $lat
            );
            //添加添加订单详情记录
            $res = OrderDetailRepository::batchAdd($order, $details);
            if (!$res) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, "订单录入失败，请重试");
            }

            //添加订单小票记录
            $res = OrderTicketRepository::batchAdd($order, $pics);
            if (!$res) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, "订单录入失败，请重试");
            }
            //消费优惠券
            OrderCouponRepository::batchAdd($order->id, $coupons);
            Db::commit();
            return $order;
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }
    }

    private static function getProductNameMapByIds(array $productIds = []): array
    {
        if (empty($productIds)) {
            return [];
        }

        $resp = ProductRpcService::listProductsByIds(array_unique($productIds));
        $list = [];
        /** @var ProductItem $item */
        foreach ($resp->getProducts() as $item) {
            $list[$item->getId()] = $item->getName();
        }

        return $list;
    }

    private static function getProductSpecMapsBySkuIds(array $skuids = []): array
    {
        if (empty($skuids)) {
            return [];
        }
        $resp = ProductRpcService::listSkuByIds(array_unique($skuids));
        $data = [];
        /** @var ProductSkuItem $item */
        foreach ($resp->getList() as $item) {
            $data[$item->getId()] = $item->getSpec();
        }
        return $data;
    }

    /**
     * 销售订单列表数据
     * @param int $current
     * @param int $limit
     * @return array[]
     */
    public static function listFinishOrder($current = 1, $limit = 10): array
    {
        //获取登陆用户clerk
        $clerk = UserClerkService::checkAndGetClerk();

        $resp = OrderRepository::listClerkOrders($clerk, $current, $limit);
        //处理响应订单数据
        return self::responseOrderData($resp, $limit);
    }

    /**
     * 店员个人汇总且列表数据
     * @param $month
     * @param $productId
     * @param $specsId
     * @param int $current
     * @param int $limit
     * @return array[]
     */
    public static function clerkOrderTotalAndOrderList($month, $productId, $specsId, $current = 1, $limit = 10)
    {
        //获取登陆用户clerk
        $clerk = UserClerkService::checkAndGetClerk();

        $resp = OrderRepository::searchClerkOrderList($clerk, $month, $productId, $specsId, $current, $limit);
        //处理响应订单数据
        $result = self::responseOrderData($resp, $limit);
        $sumInfo = OrderRepository::totalOrderDataByClerk($clerk, $month, $productId, $specsId);
        $result[] = $sumInfo->toArray();
        return  $result;
    }

    /**
     * 店员订单列表
     * @param $clerkId
     * @param $month
     * @param $productId
     * @param $specsId
     * @param int $page
     * @param int $pageSize
     */
    public static function clerkOrderList($clerkId, $month, $productId, $specsId, $page = 1, $pageSize = 10)
    {
        $clerk = UserClerkRepository::getUserClerkByUserId($clerkId);
        if (!$clerk) {
            throw new AuthException(Auth::USER_FORBIDDEN, "店员帐号不存在或已禁用");
        }
        $resp = OrderRepository::searchClerkOrderList($clerk, $month, $productId, $specsId, $page, $pageSize);
        //处理响应订单数据
        return self::responseOrderData($resp, $pageSize);
    }


    /**
     * 患者列表数据
     * @param int $current
     * @param int $limit
     * @return array[]
     */
    public static function patientOrderList($current = 1, $limit = 10): array
    {
        $user = Helper::getLoginUser();
        $resp = OrderRepository::listPatientOrders($user->id, $current, $limit);
        // 处理响应订单数据
        return self::responseOrderData($resp, $limit);
    }

    private static function responseOrderData($resp, $limit = 10)
    {
        $page = [
            'total' => $resp->total(),
            'pages' => $resp->lastPage(),
            'current' => $resp->currentPage(),
            'size' => $limit,
        ];
        if ($resp->isEmpty()) {
            return [$page, []];
        }

        $ids = $patientIds = $storeIds = [];
        /** @var Order $item */
        foreach ($resp as $item) {
            $ids[]= $item->id;
            $storeIds[] = $item->store_id;
            if ($item->patient_user_id > 0) {
                $patientIds[] = $item->patient_user_id;
            }
        }

        //获取订单所属店铺
        $storeMap = [];
        if (!empty($storeIds)) {
            $storeResp = BranchStoreRepository::getStoreInfoByIds($storeIds);
            $storeMap = $storeResp->keyBy('id')->all();
        }

        //获取患者信息
        $patientMap = [];
        if (!empty($patientIds)) {
            $patientResp = UserRepository::getListByUserIds($patientIds);
            $patientMap = $patientResp->keyBy('id')->all();
        }

        //获取图片
        $ticketResp = OrderTicketRepository::getTicketsByOrderIds($ids);
        $uploadIds = [];
        /** @var OrderTicket $ticket */
        foreach ($ticketResp as $ticket) {
            $uploadIds[] = $ticket->ticket_id;
        }
        $ticketMap = $ticketResp->groupBy('order_id')->all();
        $uploadResp = UploadRpcService::getByIds(array_unique($uploadIds));
        $uploadMap = [];
        /** @var UploadFileItem $up */
        foreach ($uploadResp->getList() as $up) {
            if (!isset($uploadMap[$up->getId()])) {
                $uploadMap[$up->getId()] = $up;
            }
        }

        //获取详情
        $details = OrderDetailRepository::listDetailByOrderIds($ids);
        $detailMaps = $details->groupBy('order_id')->all();

        //获取订单优惠信息
        $couponMap = self::getOrderCouponList($ids);

        //获取订单积分信息
        $pointMap  = self::getOrderPointList($ids);
        $data = [];
        /** @var Order $order */
        foreach ($resp as $order) {
            //处理小票列表
            $pics = [];
            if (isset($ticketMap[$order->id])) {
                /** @var OrderTicket $ti */
                foreach ($ticketMap[$order->id] as $ti) {
                    if (isset($uploadMap[$ti->ticket_id])) {
                        /** @var UploadFileItem $supload */
                        $supload = $uploadMap[$ti->ticket_id];
                        $pics[] = [
                            'id' => $supload->getId(),
                            'name' => $supload->getName(),
                            'url' => $supload->getUrl(),
                            'type' => (int) $supload->getChannel()
                        ];
                    }
                }
            }

            //处理药品详情
            $drugs = [];
            if (isset($detailMaps[$order->id])) {
                /** @var OrderDetail $detail */
                foreach ($detailMaps[$order->id] as $detail) {
                    $details = $pointMap[$order->id]??null;
                    $score = 0;
                    if ($details) {
                        $scoreInfo = $details->where(
                            'detail_id',
                            $detail->id
                        )->first();
                        if ($scoreInfo) {
                            $score = $scoreInfo->money_point;
                        }
                    }
                    $drugs[] = [
                        'id' => $detail->id,
                        'name' => (string) $detail->product_name,
                        'sku' => (string) $detail->yy_sku_name,
                        'total' => $detail->num,
                        'cash' => $detail->amount,
                        'score'=>$score
                    ];
                }
            }
            //处理优惠券
            /** @var Collection $couponList */
            $coupons = [];
            $couponList = $couponMap[$order->id]??null;
            if ($couponList) {
                $couponList->each(function ($row) use (&$coupons) {
                    $coupons[] = [
                        'policy_type'=>$row->coupon->policy_type,
                        'policy_val'=>$row->coupon->policy_val,
                    ];
                });
            } else {
                $coupons = [];
            }
            // 处理积分
            $score = 0;
            $pointList = $pointMap[$order->id]??null;
            if ($pointList) {
                $pointList->each(function ($row) use (&$score) {
                    $score += $row->money_point;
                });
            }

            $data[] = [
                'id' => $order->id,
                'notify_id'=>$order->notify_id,
                'no' => $order->order_num,
                'date' => (string) $order->created_at,
                'patient' => isset($patientMap[$order->patient_user_id]) ?
                    $patientMap[$order->patient_user_id]->nick_name : '',
                'store' => isset($storeMap[$order->store_id]) ? $storeMap[$order->store_id]->store_name : '',
                'pics' => $pics,
                'drugs' => $drugs,
                'amount' => $order->amount,
                'score'=>$score,
                'coupons'=>$coupons
            ];
        }
        return [$page, $data];
    }

    private static function processOrderPatientPoint(Order $order)
    {
        $log = Helper::getLogger();
        $patientRole = UserRoleRepository::getUserSpecialRole($order->patient_user_id, Role::ROLE_PATIENT);
        if (!$patientRole) {
            $log->warning('patient_ser_role_not_exists', [
                'patient_user_id' => $order->patient_user_id
            ]);
            return;
        }
        try {
            $eventKey = "PATIENT_ORDER_PATIENT_POINT";
            //获取可用积分规则
            $rules = PharmacyPointOrderDrugRuleRepository::getValidRules($order->id, $eventKey);
            if ($rules->isEmpty()) {
                $log->info('order_point_rule_empty', [
                    'order_id' => $order->id
                ]);
                return;
            }

            //遍历规则给患者发积分
            $rpc = new PointRpcService();
            /** @var PharmacyPointOrderDrugRule $rule */
            foreach ($rules as $rule) {
                $num = floor($rule->buy_num / $rule->base_cnt);
                if ($num < 1) {
                    return;
                }

                $rpc->addActivityPoint(
                    $patientRole->id,
                    $eventKey,
                    $rule->id,
                    $num,
                    sprintf("下单购买%s(%s)获得积分", $rule->product_name, $rule->yy_sku_name),
                    UserType::USER_TYPE_PHARMACY,
                    ActivitySpecialType::CUSTOM,
                    $rule->group_id
                );
                $log->info("order_send_patient_point", [
                    'order_id' => $order->id,
                    'patient_id' => $order->patient_user_id,
                    'rule_id' => $rule->id,
                    'num' => $num,
                ]);
            }
        } catch (\Throwable $e) {
            $log->info("order_send_patient_point_error", [
                'order_id' => $order->id,
                'patient_id' => $order->patient_user_id,
                'msg' => $e->getMessage()
            ]);
        }
    }

    /**
     * 获取订单优惠券信息
     * @param $orderIds
     * @return Collection
     */
    private static function getOrderCouponList($orderIds)
    {
        $role = Context::get('role');
        if ($role == Role::ROLE_PATIENT) {
            return new Collection();
        }
        $coupons = OrderCouponRepository::getOrderCoupons($orderIds);
        return $coupons->groupBy('order_id');
    }

    private static function getOrderPointList($orderIds)
    {
        $role = Context::get('role');
        if ($role == Role::ROLE_PATIENT) {
            return new Collection();
        }
        $userIds = OrderUserRepository::getOrderUserClerks($orderIds)
            ->pluck('id');
        $orderPoints = OrderPointRepository::getOrderPoints($userIds, $orderIds);
        return $orderPoints->groupBy('order_id');
    }
}
