<?php


namespace App\Service\Order;

use App\Constants\AppErr;
use App\Constants\BizAgentType;
use App\Constants\Clerk;
use App\Constants\Notify;
use App\Constants\Role;
use App\Helper\Helper;
use App\Model\Pharmacy\User;
use App\Repository\BranchStoreGpsRepository;
use App\Repository\BranchStoreRepository;
use App\Repository\PurchaseNotifyRepository;
use App\Repository\UserClerkRepository;
use App\Repository\UserRepository;
use App\Exception\BusinessException;
use App\Repository\UserRoleRepository;
use App\Service\Agent\AgentService;
use App\Service\Common\BizAgentService;
use App\Service\Common\SchemaService;
use Grpc\Pharmacy\Common\SHARE_TYPE;
use Grpc\Pharmacy\Common\ShareLinkToken;

class PurchaseNotifyService
{
    public function notifyClerk($userId, $tokenId, $lon = 0.0, $lat = 0.0)
    {
        if (!is_null(UserRoleRepository::getUserSpecialRole($userId, Role::ROLE_CLERK))) {
            throw new BusinessException(AppErr::CLERK_DENY_SCAN_PATIENT_QR);
        }
        $share = AgentService::getShareLinkFromShareId($tokenId);
        if (!($share->getType() == SHARE_TYPE::SHARE_AGENT_STORE && $share->getStoreId() > 0)) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, "二维码信息缺失，药店id不存在");
        }
        $yyStoreId = $share->getStoreId();
        $storeInfo = BranchStoreRepository::getStoreInfoByYyStoreId($yyStoreId);
        if (!$storeInfo) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, "非法请求，药店id不存在");
        }
        // 不限制扫码
        $storeId = $storeInfo->id;
        $notifyId = PurchaseNotifyRepository::addNotify($userId, $storeId, $lon, $lat);
        //记录业务关联代表信息
        BizAgentService::recordAgent(
            $notifyId,
            $storeId,
            BizAgentType::BIZ_TYPE_SCAN
        );
        return $storeInfo;
    }

    public function cancelNotify($notifyId, User $user)
    {
        $this->doBefore($notifyId, $user);
        $status = Notify::NOTIFY_CANCEL;
        if (!PurchaseNotifyRepository::updateStatus($notifyId, $user->id, $status)) {
            throw new BusinessException(AppErr::NOTIFY_CANCEL_ERR);
        }
        return true;
    }

    public function receiveNotify($notifyId, User $user)
    {
        $this->doBefore($notifyId, $user);
        $status = Notify::NOTIFY_RECEIVED;
        if (!PurchaseNotifyRepository::updateStatus($notifyId, $user->id, $status)) {
            throw new BusinessException(AppErr::NOTIFY_RECEIVE_ERR);
        }
        return true;
    }

    public function getNotifyListByStoreId($storeId, $userId, $status, $pageSize)
    {
        $res = PurchaseNotifyRepository::getList($storeId, $userId, $status, $pageSize);
        if ($res->total()) {
            $patientUserIds = collect($res->items())->pluck('patient_user_id');
            $patientMap = UserRepository::getListByUserIds($patientUserIds)
                ->pluck('nick_name', 'id');
            $storeInfo = BranchStoreRepository::getStoreInfoById($storeId);
            $res->getCollection()->transform(function ($row) use ($patientMap, $storeInfo) {
                $row['patient_name'] = $patientMap[$row['patient_user_id']]??'';
                $row['store_name'] = $storeInfo->store_name??'';
                $row['notify_id'] = $row['id'];
                return $row;
            });
        }
        return [
            'list' => $res->items(),
            'total' => $res->total(),
            'page' => $res->currentPage(),
            'page_size' => $pageSize
        ];
    }

    private function doBefore($notifyId, User $user)
    {
        if ($user->role != Role::ROLE_CLERK) {
            throw new BusinessException(AppErr::ROLE_NOT_EXIST);
        }
        $notifyInfo = PurchaseNotifyRepository::getNotifyInfo($notifyId);
        if (empty($notifyInfo)) {
            throw new BusinessException(AppErr::REQ_PARAM_EXP);
        }
        $clerkInfo = UserClerkRepository::getUserClerkByUserId($user->id);
        if ($clerkInfo->store_id != $notifyInfo->store_id) {
            throw new BusinessException(AppErr::REQ_PARAM_EXP);
        }
        if ($clerkInfo->status != Clerk::STATUS_PASS) {
            throw new BusinessException(AppErr::CLERK_NOT_PASS);
        }
    }
}
