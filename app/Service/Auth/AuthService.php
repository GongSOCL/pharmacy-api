<?php
declare(strict_types=1);

namespace App\Service\Auth;

use App\Constants\ErrorWechat;
use App\Constants\Role;
use App\Exception\WechatException;
use App\Helper\Helper;
use App\Kernel\Jwt\Jwt;
use App\Repository\UserRepository;
use App\Service\Wechat\WechatObjectService;
use EasyWeChat\Factory;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Utils\ApplicationContext;

class AuthService
{

    private $wechatConfig = null;
    private $jwt = null;
    private $wechatService = null;
    public function __construct()
    {
        $this->wechatService = ApplicationContext::getContainer()
            ->get(WechatObjectService::class);
        $this->jwt = ApplicationContext::getContainer()
            ->get(Jwt::class);
    }

    /**
     * @param $code
     * @return array
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \Exception
     */
    public function login($code)
    {
        // 获取微信登录code的用户信息
        $authInfo = $this->wechatService->getSessionFromCode($code);
        if (isset($authInfo['errcode'])) {
            throw new WechatException(ErrorWechat::USELESS_CODE);
        }
        $uInfo = UserRepository::getUserInfoByOpenId($authInfo['openid']);
        $role = Role::ROLE_PATIENT;
        $openId = $authInfo['openid'];
        $unionId = $authInfo['unionid']??'';
        try {
            if (!$uInfo) {
                $nickName = '用户_'.Helper::genRandStr(8);
                $userId = UserRepository::addUser($unionId, $openId, $nickName, $role);
            } else {
                $userId = $uInfo['id'];
            }
            return [
                'token'=>$this->jwt->generate($userId),
            ];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * 直接获取角色
     * @param $code
     * @return array
     */
    public function getRole($code)
    {
        $authInfo = $this->wechatService->getSessionFromCode($code);
        if (isset($authInfo['errcode'])) {
            throw new WechatException(ErrorWechat::USELESS_CODE);
        }
        $uInfo = UserRepository::getUserInfoByOpenId($authInfo['openid']);
        $role = 0;
        if (!empty($uInfo)) {
            $role = $uInfo->role;
        }
        return  compact('role');
    }
}
