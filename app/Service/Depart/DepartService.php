<?php
declare(strict_types=1);

namespace App\Service\Depart;

use App\Helper\Helper;
use App\Repository\DepartProductRepository;
use Grpc\Wxapi\Depart\DepartItem;

class DepartService
{
    public function departList()
    {
        $departIds = DepartProductRepository::getDeparts()
            ->pluck('depart_id')->toArray();
        $resp = DepartRpcService::getDepartList($departIds);
        if ($resp->getDepart()->count() == 0) {
            return [];
        }
        return Helper::mapRepeatedField($resp->getDepart(), function ($item) {
                /** @var DepartItem $item */
                return [
                    'id'=>$item->getId(),
                    'name'=>$item->getName()
                ];
        });
    }
}
