<?php
declare(strict_types=1);

namespace App\Service\Depart;

use Grpc\Wxapi\Depart\DepartIdsRequest;
use Grpc\Wxapi\Depart\DepartSvcClient;
use Hyperf\Utils\ApplicationContext;

class DepartRpcService
{
    public static function client()
    {
        return ApplicationContext::getContainer()
            ->get(DepartSvcClient::class);
    }

    public static function getDepartList($departIds)
    {
        $request =  new DepartIdsRequest();
        $request->setIds($departIds);
        return self::client()->departList($request);
    }
}
