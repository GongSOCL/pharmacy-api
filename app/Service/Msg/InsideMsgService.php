<?php
declare(strict_types=1);
namespace App\Service\Msg;

use App\Helper\Helper;
use App\Service\Common\InsideMsgRpcService;
use Grpc\InsideMsg\InsideMsgListItem;
use Hyperf\Utils\ApplicationContext;

class InsideMsgService
{
    public static function listMsg($current = 1, $limit = 10): array
    {
        $role = Helper::getUserLoginRole();
        $reply = ApplicationContext::getContainer()
            ->get(InsideMsgRpcService::class)
            ->fetchInsideMsg($role->id, $current, $limit);

        $data = [
            'total' => $reply->getTotal(),
            'unread_total' => $reply->getUnreadTotal(),
            'page' => $reply->getPage(),
            'page_size' => $reply->getPageSize(),
            'list' => []
        ];
        if ($reply->getMap()->count() == 0) {
            return $data;
        }

        $list = [];
        foreach ($reply->getMap()->getIterator() as $v) {
            /** @var InsideMsgListItem $value */
            foreach ($v->getList()->getIterator() as $key => $value) {
                $list[$key] = [
                    'id' => $value->getId(),
                    'title' => $value->getTitle(),
                    'sub_title' => $value->getSubTitle(),
                    'type' => $value->getType(),
                    'link' => $value->getLink(),
                    'paper_yyid' => $value->getPaperYyid(),
                    'created_at' => $value->getCreatedAt(),
                    'is_read' => $value->getIsRead()
                ];
            }
        }
        $data['list'] = $list;
        return $data;
    }

    public static function getMsgDetail($msgId): array
    {
        $role = Helper::getUserLoginRole();
        $reply = ApplicationContext::getContainer()
            ->get(InsideMsgRpcService::class)
            ->fetchOneMsgDetail($role->id, $msgId);

        $data = [];
        $data['id'] = $reply->getId();
        $data['title'] = $reply->getTitle();
        $data['sub_title'] = $reply->getSubTitle();
        $data['type'] = $reply->getType();
        $data['body'] = $reply->getBody();
        $data['link'] = $reply->getLink();
        $data['paper_yyid'] = $reply->getPaperYyid();
        $data['created_at'] = $reply->getCreatedAt();
        return $data;
    }
}
