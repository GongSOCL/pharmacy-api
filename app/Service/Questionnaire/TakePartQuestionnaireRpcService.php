<?php
declare(strict_types=1);

namespace App\Service\Questionnaire;

use Grpc\Questionnaire\AnswerQuestionnaireQuestionRequest;
use Grpc\Questionnaire\AnswerQuestionnaireQuestionResponse;
use Grpc\Questionnaire\CheckQuestionnaireRequest;
use Grpc\Questionnaire\CheckQuestionnaireResponse;
use Grpc\Questionnaire\DirectCheckRequest;
use Grpc\Questionnaire\GetQuestionnaireQuestionRequest;
use Grpc\Questionnaire\GetQuestionnaireQuestionResponse;
use Grpc\Questionnaire\PartQuestionnaireClient;
use Grpc\Questionnaire\QUESTIONNAIRE_PLATFORM;
use Grpc\Questionnaire\StartQuestionnaireRequest;
use Grpc\Questionnaire\StartQuestionnaireResponse;
use Grpc\Questionnaire\SubmitQuestionnaireRequest;
use Grpc\Questionnaire\USER_TYPE;
use Grpc\Questionnaire\WECHAT_TYPE;
use Hyperf\Utils\ApplicationContext;

class TakePartQuestionnaireRpcService
{
    private static function getClient(): PartQuestionnaireClient
    {
        return ApplicationContext::getContainer()
            ->get(PartQuestionnaireClient::class);
    }

    public function checkQuestionnaire($token): CheckQuestionnaireResponse
    {
        $req = new CheckQuestionnaireRequest();
        $req->setShareKey($token);

        return self::getClient()->checkQuestionnaire($req);
    }

    public function startQuestionnaire(
        $openId,
        $wechatType = WECHAT_TYPE::WECHAT_TYPE_DOCTOR,
        $userId = 0,
        $wechatId = 0,
        $token = "",
        $paperId = 0
    ): StartQuestionnaireResponse {
        $req = new StartQuestionnaireRequest();
        $req->setShareKey($token);
        $req->setUserId($userId);
        $req->setOpenid($openId);
        $req->setWechatType($wechatType);
        $req->setUserType(USER_TYPE::USER_TYPE_OTC);
        $req->setWechatId($wechatId);
        $req->setPaperId($paperId);

        return self::getClient()->startQuestionnaire($req);
    }


    public function getQuestionnaireQuestion(
        $requestId,
        $questionId,
        $token = "",
        $paperId = 0
    ): GetQuestionnaireQuestionResponse {
        $req = new GetQuestionnaireQuestionRequest();
        $req->setShareKey($token);
        $req->setRequestId($requestId);
        $req->setQuestionId($questionId);
        $req->setPaperId($paperId);

        return self::getClient()->getQuestionnaireQuestion($req);
    }

    public function answerQuestionnaireQuestion(
        $requestId,
        $questionId,
        array $optionIds = [],
        $content = "",
        $token = "",
        $paperId = 0
    ): AnswerQuestionnaireQuestionResponse {
        $req = new AnswerQuestionnaireQuestionRequest();
        $req->setShareKey($token);
        $req->setRequestId($requestId);
        $req->setQuestionId($questionId);
        $req->setAnswers($optionIds);
        $req->setContent($content);
        $req->setPaperId($paperId);

        return self::getClient()->answerQuestionnaireQuestion($req);
    }

    public function submit($requestId, $pointUrl = "", $token = "", $paperId = 0)
    {
        $req = new SubmitQuestionnaireRequest();
        $req->setShareKey($token);
        $req->setRequestId($requestId);
        $req->setPointUrl($pointUrl);
        $req->setPaperId($paperId);

        self::getClient()->submitQuestionnaire($req);
    }

    public function checkDirectQuestionnaire(
        $paperId,
        $userId,
        $userType = USER_TYPE::USER_TYPE_OTC,
        $openId = "",
        $wechatType = WECHAT_TYPE::WECHAT_TYPE_OTC
    ) {
        $req = new DirectCheckRequest();
        $req->setPaperId($paperId);
        $req->setUid($userId);
        $req->setUserType($userType);
        $req->setOpenid($openId);
        $req->setWechatType($wechatType);
        $req->setPlatform(QUESTIONNAIRE_PLATFORM::PLATFORM_PHARMACY);
//        $req->setPlatform(QUESTIONNAIRE_PLATFORM::PLATFORM_OTC);    # 测试 otc = 130

        self::getClient()->checkDirectQuestionnaire($req);
    }
}
