<?php


namespace App\Service\Questionnaire;

use App\Constants\AppErr;
use App\Constants\BizAgentType;
use App\Constants\Role;
use App\Exception\BusinessException;
use App\Helper\Helper;
//use App\Model\Niferex\UserResourceClick;
use App\Model\Pharmacy\UserClerk;
use App\Repository\Count\CountRepository;
use App\Repository\HealthTipsRepository;
use App\Repository\QuestionnaireAnswerRepository;
use App\Repository\QuestionnaireRepository;
use App\Repository\Resource\ResourceRepository;
use App\Repository\UserRoleRepository;
use App\Service\Common\BizAgentService;
use App\Service\Common\TaskRpcService;
use App\Service\Point\PointRpcService;
use App\Service\Point\PointService;
use App\Service\User\UserClerkService;
use Grpc\Questionnaire\QuestionAnswers;
use Grpc\Questionnaire\USER_TYPE;
use Grpc\Questionnaire\WECHAT_TYPE;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Context;

class QuestionnaireService
{

    # 20221116 问卷只有店员
    public static function start($id): array
    {
        $user = Helper::getLoginUser();

        $role = Context::get('role');
        $loginRole = Helper::getUserLoginRole();

        if ($loginRole->role == 1) {
            throw new BusinessException(AppErr::BUSINESSERR, "非法操作");
        }

        if (! $user->open_id) {
            throw new BusinessException(AppErr::BUSINESSERR, "微信记录不存在");
        }

        $rpc = ApplicationContext::getContainer()
            ->get(TakePartQuestionnaireRpcService::class);

        //调用接口检查问卷是否允许答题
        $rpc->checkDirectQuestionnaire(
            $id,
            $loginRole->id,
            USER_TYPE::USER_TYPE_PHARMACY,
            $user->openid
        );

        try {
            $response = $rpc->startQuestionnaire(
                $user->openid,
                WECHAT_TYPE::WECHAT_TYPE_PHARMACY,
                $loginRole->id,
                $user->id,
                "",
                $id
            );
        } catch (\Exception $e) {
            throw $e;
        }

        return [
            'total' => $response->getTotal(),
            'next_question_id' => $response->getNextQuestionId(),
            'request_id' => $response->getRequestId(),
            'intro' => $response->getIntro(),
            'start_bg' => $response->getStartBg(),
            'backend_bg' => $response->getBackendBg(),
            'answer_bg' => $response->getAnswerBg()
        ];
    }

    public static function getQuestionnaireQuestion($id, string $requestId, int $questionId): array
    {
        $rpc = ApplicationContext::getContainer()->get(TakePartQuestionnaireRpcService::class);
        $resp = $rpc->getQuestionnaireQuestion($requestId, $questionId, '', $id);
        $options  = [];
        /** @var QuestionAnswers $i */
        foreach ($resp->getAnswers() as $i) {
            $options[] = [
                'id' => $i->getAnswerId(),
                'name' => $i->getName()
            ];
        }

        return [
            'name' => $resp->getName(),
            'type' => $resp->getType(),
            'next_question_id' => $resp->getNextQuestionId(),
            'is_skip' => $resp->getIsSkip() ? 1 : 0,
            'options' => $options
        ];
    }

    public static function answerQuestion(
        $id,
        $requestId,
        int $questionId,
        array $options = [],
        string $content = ""
    ): array {
        $rpc = ApplicationContext::getContainer()->get(TakePartQuestionnaireRpcService::class);

        $resp = $rpc->answerQuestionnaireQuestion($requestId, $questionId, $options, $content, '', $id);

        $resource = [];

        if ($resp->getWithResource()) {
            $resource = HealthTipsRepository::getResourceByID($resp->getResourceId());
        }

        $right = [];
        foreach ($resp->getRightOptions() as $i) {
            $right[] = (int)$i;
        }

        return [
            'need_check' => $resp->getNeedCheck() ? 1 : 0,
            'is_right' => $resp->getIsRight() ? 1 : 0,
            'explain' => $resp->getExplain(),
            'next_question_id' => $resp->getNextQuestionId(),
            'right_options' => $right,
            'with_resource' => $resource ? 1 : 0,
            'resource_id' => $resource ? $resource['id'] : 0
        ];
    }

    public static function submit($id, string $requestId, $taskToken)
    {
        //发放积分则跳转到个人中心
        $rpc = ApplicationContext::getContainer()->get(TakePartQuestionnaireRpcService::class);
        $rpc->submit($requestId, '', '', $id);
        $role = Context::get('role');
        if ($role == Role::ROLE_CLERK) {
            $clerk = UserClerkService::checkAndGetClerk();
            // 记录问卷记录
            $answer = QuestionnaireAnswerRepository::addQuestionnaireAnswer(
                $clerk->store_id,
                $clerk->user_id,
                $id
            );
            //记录业务关联代表信息
            BizAgentService::recordAgent(
                $answer->id,
                $clerk->store_id,
                BizAgentType::BIZ_TYPE_QUEST
            );
            // 更新积分记录
            PointService::updateClerkPointRankInfo($clerk, true, false);
        }
        //成功后更新资源计数器
//        try {
//            CountRepository::recodeUserClick(
//                $id,
//                $user->id,
//                3,
//                0,
//                UserResourceClick::RESOURCE_TYPE_QUESTIONNAIRE
//            );
//        } catch (\Exception $e) {
//        }
//
//        if ($taskToken) {
//            TaskRpcService::finishTask($taskToken, $user->id);
//        }
    }

    public static function activityQuestionnaire($page, $pageSize)
    {
        $resp = QuestionnaireRepository::getListByProductIds($page, $pageSize);

        return [
            'list' => $resp->items(),
            'total' => $resp->total(),
            'lastPage' => $resp->lastPage(),
            'page' => $resp->currentPage(),
            'size' => $pageSize,
        ];
    }
}
