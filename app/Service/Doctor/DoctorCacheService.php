<?php
declare(strict_types=1);
namespace App\Service\Doctor;

use App\Helper\Helper;

class DoctorCacheService
{
    /**
     * @param $doctorId
     * @return string|false
     * @throws \RedisException
     */
    public static function getDoctorScanRecord($doctorId)
    {
        $key = sprintf("pharmacy:doctor:point:Magnification:%d", $doctorId);
        $res = Helper::getRedis()->hGet($key, "end");
        if ($res === false) {
            return false;
        }
        return (string) $res;
    }

    public static function updateDoctorFirstScanInfo($doctorId, $end, $magnification = 1, $ttl = 30 * 24 * 3600)
    {
        $key = sprintf("pharmacy:doctor:point:Magnification:%d", $doctorId);
        $pipeline = Helper::getRedis()->pipeline();
        $pipeline->hMSet($key, [
            'end' => $end,
            'magnification' => $magnification
        ]);
        $pipeline->expire($key, $ttl);
        $pipeline->exec();
    }

    public static function addCoinBlock($ttl = 30 * 24 * 3600)
    {
        $key = "pharmacy:doctor:coin:magnification:empty";
        Helper::getRedis()->set($key, true, $ttl);
    }

    public static function checkCoinBlock(): bool
    {
        $key = "pharmacy:doctor:coin:magnification:empty";
        return Helper::getRedis()->get($key) !== false;
    }

    public static function getDoctorMaginification($doctorId): array
    {
        $key = sprintf("pharmacy:doctor:point:Magnification:%d", $doctorId);
        $res = Helper::getRedis()->hMGet($key, ["end", "magnification"]);

        if ($res['end'] === false) {
            return ["", 1];
        }
        return [$res['end'], $res['magnification']];
    }

    public static function clearEmptyBlock()
    {
        $key = "pharmacy:doctor:coin:magnification:empty";
        Helper::getRedis()->del($key);
    }
}
