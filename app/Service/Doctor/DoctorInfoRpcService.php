<?php
declare(strict_types=1);
namespace App\Service\Doctor;

use Grpc\DoctorInfo\DoctorInfoClient;
use Grpc\DoctorInfo\DoctorWechatInfoReply;
use Grpc\DoctorInfo\DoctorWechatInfoRequest;
use Hyperf\Utils\ApplicationContext;

class DoctorInfoRpcService
{
    private static function getClient()
    {
        return ApplicationContext::getContainer()
            ->get(DoctorInfoClient::class);
    }

    public static function getDoctorWechatInfo(array $openIds, $keywords = ""): DoctorWechatInfoReply
    {
        $req = new DoctorWechatInfoRequest();
        $req->setKeywords($keywords);
        $req->setOpenids($openIds);
        return self::getClient()->getDoctorWechatInfo($req);
    }
}
