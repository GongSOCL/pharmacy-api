<?php
declare(strict_types=1);
namespace App\Service\Doctor;

use App\Helper\Helper;
use Grpc\Common\CommonPageRequest;
use Grpc\Wxapi\DoctorGroup\AgentDoctorsListReply;
use Grpc\Wxapi\DoctorGroup\AgentListRely;
use Grpc\Wxapi\DoctorGroup\CheckAgentDoctorRequest;
use Grpc\Wxapi\DoctorGroup\DoctorGroupClient;
use Grpc\Wxapi\DoctorGroup\GroupDoctorIdRequest;
use Grpc\Wxapi\DoctorGroup\GroupDoctorInfoReply;
use Grpc\Wxapi\DoctorGroup\GroupDoctorListRequest;
use Hyperf\Utils\ApplicationContext;

class DoctorGroupRpcService
{
    private static function getClient(): DoctorGroupClient
    {
        return ApplicationContext::getContainer()
            ->get(DoctorGroupClient::class);
    }

    public static function checkAgentOwnerDoctor($agentId, $doctorId): bool
    {
        $req = new CheckAgentDoctorRequest();
        $req->setAgentId($agentId);
        $req->setDoctorId($doctorId);
        return self::getClient()->CheckAgentOwnerDoctor($req)
            ->getIsPass();
    }

    /**
     * 获取群组关联了该医生的代表
     * @param $doctorId
     * @return AgentListRely
     */
    public static function getAgentRelatedWithDoctorInGroup($doctorId): AgentListRely
    {
        $req = new GroupDoctorIdRequest();
        $req->setId($doctorId);
        return self::getClient()->GetAgentRelatedWithDoctorInGroup($req);
    }

    /**
     * 获取服务于医生关联医院的代表
     * @param $doctorId
     * @return AgentListRely
     */
    public static function getDoctorServeHospitalRelatedAgents($doctorId): AgentListRely
    {
        $req = new GroupDoctorIdRequest();
        $req->setId($doctorId);
        return self::getClient()->GetDoctorServeHospitalRelatedAgents($req);
    }

    public static function getDoctorInfo($doctorId): GroupDoctorInfoReply
    {
        $req = new GroupDoctorIdRequest();
        $req->setId($doctorId);
        return self::getClient()->GetDoctorInfo($req);
    }

    /**
     * 获取代表群组医生
     * @param $agentId
     * @param int $current
     * @param int $limit
     * @param bool $onlyWithWechat
     */
    public static function getAgentGroupDoctors(
        $agentId,
        $current = 1,
        $limit = 10,
        $onlyWithWechat = true
    ): AgentDoctorsListReply {
        $page = new CommonPageRequest();
        $page->setPage($current);
        $page->setSize($limit);

        $req = new GroupDoctorListRequest();
        $req->setAgentId($agentId);
        $req->setPage($page);
        $req->setOnlyWithWechat($onlyWithWechat);
        return self::getClient()->GetGroupDoctors($req);
    }
}
