<?php
declare(strict_types=1);
namespace App\Service\Doctor;

use App\Constants\Delete;
use App\Helper\Helper;
use App\Helper\Lock;
use App\Model\Pharmacy\DoctorCoinMagnificationConfig;
use App\Repository\Point\DoctorCoinMagnificationConfigRepository;
use App\Repository\Point\PatientDoctorMagnificationTimelineRepository;
use App\Repository\UserPatientDoctorRepository;
use Carbon\Carbon;
use Hyperf\Utils\ApplicationContext;

class DoctorService
{

    public static function updatePatientDoctorFirstScanInfo($doctorId)
    {
        if (DoctorCacheService::checkCoinBlock()) {
            //没有规则
            return;
        }
        //查询是否有缓存存在
        $res = DoctorCacheService::getDoctorScanRecord($doctorId);
        if ($res !== false) {
            return;
        }
        //记录不存在
        $lock = ApplicationContext::getContainer()
            ->get(Lock::class);
        $name = sprintf("doctor:first:scan:%d", $doctorId);
        $identifier = $lock->acquire($name);
        if (false === $identifier) {
            return;
        }

        try {
            //从时间线表里面获取记录
            $t = PatientDoctorMagnificationTimelineRepository::getDoctorTimeline($doctorId);
            if ($t) {
                //如果有记录则更新缓存记录， 其它的不需要动
                DoctorCacheService::updateDoctorFirstScanInfo($doctorId, $t->magnification_end, $t->magnification);
                return;
            }

            //查找最新记录
            $cfg = DoctorCoinMagnificationConfigRepository::getCurrentValidConfig();
            if (!$cfg) {
                //如果没有配置，最好添加一个缓存key
                DoctorCacheService::addCoinBlock();
                return;
            }

            //获取医生首次扫码时间
            $firstTime = self::getDoctorFirstScan($doctorId);
            if ($firstTime == "") {
                //异常记录
                DoctorCacheService::addCoinBlock();
                return;
            }
            $end = (new \DateTime($firstTime))->modify("+{$cfg->days} days")->format('Y-m-d H:i:s');
            PatientDoctorMagnificationTimelineRepository::addDoctorConfig($doctorId, $firstTime, $end, $cfg);
            DoctorCacheService::updateDoctorFirstScanInfo($doctorId, $end, $cfg->magnification);

            $lock->release($name, $identifier);
        } catch (\Exception $e) {
            $lock->release($name, $identifier);
            Helper::getLogger()->error("update_magnification_error", [
                'doctor_id' => $doctorId,
                'msg' => $e->getMessage()
            ]);
        }
    }

    private static function getDoctorFirstScan($doctorId): string
    {
        $resp = UserPatientDoctorRepository::getDoctorFirstScan($doctorId);
        return $resp ? (string) $resp->created_at : '';
    }

    public static function getDoctorMagnification($doctorId): int
    {
        [$end, $magnification] = DoctorCacheService::getDoctorMaginification($doctorId);
        if ($end == "") {
            //没有缓存信息就先刷新医生翻倍信息
            self::updatePatientDoctorFirstScanInfo($doctorId);
            [$end, $magnification] = DoctorCacheService::getDoctorMaginification($doctorId);
        }
        if ($end && $end > date('Y-m-d H:i:s')) {
            return $magnification;
        }
        return 1;
    }

    public static function updateDoctorMagnification($days, $magnification = 1)
    {
        $cfg = DoctorCoinMagnificationConfigRepository::getCurrentValidConfig();
        if ($cfg) {
            $cfg->days = $days;
            $cfg->magnification = $magnification;
            $cfg->updated_at = Carbon::now();
            $cfg->save();
        } else {
            $o = new DoctorCoinMagnificationConfig();
            $o->magnification = $magnification;
            $o->days = $days;
            $o->is_delete = Delete::UNDELETED;
            $o->created_at = $o->updated_at = Carbon::now();
            $o->save();
        }

        DoctorCacheService::clearEmptyBlock();
    }
}
