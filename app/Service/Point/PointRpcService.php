<?php
declare(strict_types=1);
namespace App\Service\Point;

use Grpc\Point\ActivitySpecialType;
use Grpc\Point\AddActivityPointReq;
use Grpc\Point\AddActivityRequest;
use Grpc\Point\AddActivitySpecialRequest;
use Grpc\Point\AssetsDetail;
use Grpc\Point\DetailListReply;
use Grpc\Point\DIRECT_AUDIT_STATUS;
use Grpc\Point\DirectAuditOrder;
use Grpc\Point\ExchangeGoodsReq;
use Grpc\Point\GetDefaultGroupRequest;
use Grpc\Point\GetDetailListReq;
use Grpc\Point\GroupItemReply;
use Grpc\Point\PointClient;
use Grpc\Point\PointDetail;
use Grpc\Point\PointPlatform;
use Grpc\Point\UidReq;
use Grpc\Point\UserType;
use Hyperf\Utils\ApplicationContext;

class PointRpcService
{
    public static function getClient()
    {
        return ApplicationContext::getContainer()
            ->get(PointClient::class);
    }

    // 添加积分主规则
    public function addPointRule(
        $name,
        $eventKey,
        $pointDesc,
        $servicePoint,
        $moneyPoint,
        $numTotal = 0,
        $numPerDay = 0,
        $start = "",
        $end = "",
        $groupId = 0
    ) {
        $req = new AddActivityRequest();
        $req->setName($name);
        $req->setEventKey($eventKey);
        $req->setPointDesc($pointDesc);
        if ($servicePoint > 0) {
            $req->setServicePoint((int) $servicePoint);
        }
        if ($moneyPoint > 0) {
            $req->setMoneyPoint((int) $moneyPoint);
        }
        $req->setNumPreDay((int) $numPerDay);
        $req->setNumTotal((int) $numTotal);
        $req->setCreateUserId(0);
        if ($start) {
            $req->setStartTime($start);
        }
        if ($end) {
            $req->setEndTime($end);
        }
        $req->setGroupId($groupId);
        self::getClient()->addActivity($req);
    }

    /**
     * 添加积分特例规则
     * @param $eventKey
     * @param $name
     * @param $pkId
     * @param $tableName
     * @param $desc
     * @param $servicePoint
     * @param $moneyPoint
     * @param $numPerDay
     * @param $numTotal
     * @param $start
     * @param $end
     * @param $type
     * @return void
     */
    public function addActivitySpecial(
        $eventKey,
        $name,
        $pkId,
        $tableName,
        $desc,
        $servicePoint,
        $moneyPoint,
        $numPerDay = 0,
        $numTotal = 0,
        $start = "",
        $end = "",
        $type = ActivitySpecialType::CUSTOM,
        $groupId = 0
    ) {
        $req = new AddActivitySpecialRequest();
        $req->setValue($pkId);
        $req->setName($name);
        $req->setPointDesc($desc);
        $req->setEventKey($eventKey);
        $req->setTableName($tableName);
        $req->setServicePoint((int) $servicePoint);
        $req->setMoneyPoint((int) $moneyPoint);
        $req->setNumPreDay($numPerDay);
        $req->setNumTotal($numTotal);
        $req->setGroupId($groupId);
        if ($start) {
            $req->setStartTime($start);
        }
        if ($end) {
            $req->setEndTime($end);
        }
        $req->setType($type);
        self::getClient()->addActivitySpecial($req);
    }

    public function getUserAssets($uid, $groupId = 0):AssetsDetail
    {
        $userType = UserType::USER_TYPE_PHARMACY;
        $request = new UidReq();
        $request->setUid($uid);
        $request->setUserType($userType);
        $request->setGroupId($groupId);

        return self::getClient()->getUserAssets($request);
    }

    public function getUserPointsDetail(
        $uid,
        $page,
        $pageSize,
        int $type,
        $fetchService,
        $userType = UserType::USER_TYPE_PHARMACY,
        $groupId = 0
    ):DetailListReply {
        $req = new GetDetailListReq();
        $req->setUid($uid);
        $req->setPage($page);
        $req->setPageSize($pageSize);
        $req->setType($type);
        $req->setFetchService($fetchService);
        $req->setUserType($userType);
        $req->setGroupId($groupId);

        return self::getClient()->getDetailList($req);
    }

    public function addActivityPoint($uid, $eventKey, $specId = 0, $num = 1, $desc = "", $userType = UserType::USER_TYPE_PHARMACY, $type = ActivitySpecialType::CUSTOM, $groupId = 0): PointDetail
    {
        $req = new AddActivityPointReq();
        $req->setUid($uid);
        $req->setWechatUserID(0);
        $req->setKey($eventKey);
        $req->setVal($specId);
        $req->setNum($num);
        $req->setUserType($userType);
        $req->setDesc($desc);
        $req->setType($type);
        $req->setGroupId($groupId);
        return self::getClient()->addActivityPoint($req);
    }

    public function exchange(
        $userRoleId,
        $goodsYYID,
        $goodsNum,
        $userType = UserType::USER_TYPE_PHARMACY,
        $wechatId = 0
    ): PointDetail {
        $req = new ExchangeGoodsReq();
        $req->setUid($userRoleId);
        $req->setWechatUserID($wechatId);
        $req->setGoodsYyid($goodsYYID);
        $req->setGoodsNum($goodsNum);
        $req->setUserType($userType);

        return self::getClient()->exchangeGoods($req);
    }

    public function directAudit($orderId, $isPass = true, $reason = "")
    {
        $req = new DirectAuditOrder();
        $req->setOrderId($orderId);
        $req->setStatus($isPass ? DIRECT_AUDIT_STATUS::AUDIT_STATUS_OK : DIRECT_AUDIT_STATUS::AUDIT_STATUS_REFUSED);
        if (!$isPass) {
            $req->setReason($reason);
        }
        self::getClient()->directAuditOrder($req);
    }

    public function getDefaultGroup($platform = PointPlatform::PLATFORM_PHARMACY): GroupItemReply
    {
        $req = new GetDefaultGroupRequest();
        $req->setPlatform($platform);
        return self::getClient()->getDefaultGroup($req);
    }
}
