<?php
declare(strict_types=1);
namespace App\Service\Point;

use App\Constants\AppErr;
use App\Exception\BusinessException;
use App\Model\Pharmacy\PointExchange;
use App\Model\Pharmacy\PointGood;
use App\Model\Pharmacy\User;
use App\Model\Pharmacy\UserRole;
use App\Repository\Point\PointExchangeRepository;
use App\Repository\Point\PointGoodRepository;
use App\Service\Point\Exchanger\CashExchanger;
use App\Service\Point\Exchanger\CommonExchanger;
use App\Service\Point\Exchanger\CouponExchanger;
use App\Service\Point\Exchanger\PointExchanger;
use Grpc\Point\PointDetail;
use Hyperf\DbConnection\Db;

class PointExchangeService
{
    public static function cancelPointExchange(PointExchange $exchange, PointGood $good)
    {
        Db::beginTransaction();
        try {
            $res = PointExchangeRepository::rollbackExchange($exchange);
            if (!$res) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '兑换记录撤销失败,请联系管理员');
            }
            $res = PointGoodRepository::cancelFromExchange($exchange);
            if (!$res) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '兑换商品撤销失败,请联系管理员');
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }
    }

    public static function getExchanger(PointGood $goods, User $user, UserRole $role, $num = 1): PointExchanger
    {
        switch ($goods->goods_type) {
            case PointGood::GOODS_TYPE_CASH:
                return (new CashExchanger())
                    ->setGoods($goods, $num)
                    ->setUser($user, $role);
            case PointGood::GOODS_TYPE_VIRTUAL:
            case PointGood::GOODS_TYPE_REAL:
                return (new CommonExchanger())
                    ->setGoods($goods, $num)
                    ->setUser($user, $role);
            case PointGood::GOODS_TYPE_COUPON:
                return (new CouponExchanger())
                    ->setGoods($goods, $num)
                    ->setUser($user, $role);
            default:
                throw new BusinessException(AppErr::BUSINESS_ERROR, '暂不支持该商品类型兑换');
        }
    }

    public static function exchangeOk(PointExchange $exchange, PointDetail $detail)
    {
        PointExchangeRepository::exchangeOk(
            $exchange,
            $detail->getOrderId(),
            $detail->getServicePoint(),
            $detail->getMoneyPoint()
        );
    }

    public static function goodsSended(PointExchange $exchange)
    {
        PointExchangeRepository::exchangeSended($exchange);
        $rpc = new PointRpcService();
        $rpc->directAudit(
            $exchange->point_order_id,
            true,
            'success'
        );
    }

    public static function createExchangeFromGoods(PointGood $goods, User $user, UserRole $role, $num): PointExchange
    {
        Db::beginTransaction();
        try {
            $operateResult = PointGoodRepository::exchangeBefore($goods, $num);
            if (!$operateResult) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '商品数量不足，请刷新重试');
            }
            //
            $exchange = PointExchangeRepository::addExchange($goods, $user, $role, $num);
            Db::commit();
            return $exchange;
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }
    }
}
