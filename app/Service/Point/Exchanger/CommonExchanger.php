<?php
declare(strict_types=1);
namespace App\Service\Point\Exchanger;

use App\Service\Point\PointExchangeService;

/**
 * 通用兑换类
 * 添加兑换记录
 * 调用接口兑换
 * 更新兑换记录状态到完成[直接兑换完成]
 */
class CommonExchanger extends PointExchanger
{

    public function exchange()
    {
        $exchange = PointExchangeService::createExchangeFromGoods(
            $this->goods,
            $this->user,
            $this->role,
            $this->num
        );

        //积分预扣减
        self::preExchangeFromPoint($exchange);
    }

    public function checkBeforeExchange(): PointExchanger
    {
        return $this;
    }
}
