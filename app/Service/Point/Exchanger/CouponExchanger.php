<?php
declare(strict_types=1);
namespace App\Service\Point\Exchanger;

use App\Constants\AppErr;
use App\Constants\Role;
use App\Exception\BusinessException;
use App\Repository\CouponDetailRepository;
use App\Service\Coupon\UserCouponService;
use App\Service\Point\PointExchangeService;
use App\Service\Point\PointRpcService;
use Hyperf\Utils\ApplicationContext;

/**
 * 优惠券兑换类
 * 添加兑换记录并扣减商品数
 * 调用积分接口添加兑换订单
 * 更新兑换状态
 */
class CouponExchanger extends PointExchanger
{

    /**
     * @throws \Exception
     */
    public function exchange()
    {
        //创建兑换记录
        $exchange = PointExchangeService::createExchangeFromGoods(
            $this->goods,
            $this->user,
            $this->role,
            $this->num
        );

        //预扣减积分帐户积分
        self::preExchangeFromPoint($exchange);

        //发放优惠券
        try {
            $res = ApplicationContext::getContainer()
                ->get(UserCouponService::class)
                ->handleGetCoupon($this->goods->external_goods_id, $this->user->id, 1);
            if (!$res['flag']) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, $res['msg']);
            }
            $no = $res['no'];
        } catch (\Exception $e) {
            $this->handlePointDetailRevoke($exchange, $e->getMessage());
            $this->handleExchangeRevoke($exchange);
            throw $e;
        }

        //确认发货
        try {
            $exchange->refresh();
            PointExchangeService::goodsSended($exchange);
        } catch (\Exception $e) {
            //撤销发到用户身上的券
            UserCouponService::revokeCoupon($no);
            //撤销积分扣减
            $this->handlePointDetailRevoke($exchange, $e->getMessage());
            //撤销兑换记录
            $this->handleExchangeRevoke($exchange);
            throw $e;
        }
    }

    public function checkBeforeExchange(): PointExchanger
    {
        if ($this->num > 1) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '优惠券最多允许兑换一份');
        }

        if ($this->role->role != Role::ROLE_PATIENT) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '当前优惠券仅允许患者领取');
        }

        //检查用户之前是否兑换过
        return $this;
    }
}
