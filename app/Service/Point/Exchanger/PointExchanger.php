<?php

namespace App\Service\Point\Exchanger;

use App\Model\Pharmacy\PointExchange;
use App\Model\Pharmacy\PointGood;
use App\Model\Pharmacy\User;
use App\Model\Pharmacy\UserRole;
use App\Service\Point\PointExchangeService;
use App\Service\Point\PointRpcService;

abstract class PointExchanger
{

    /**
     * @var PointGood
     */
    protected $goods;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var UserRole
     */
    protected $role;

    protected $num = 1;

    public function setGoods(PointGood $good, $num): PointExchanger
    {
        $this->goods = $good;
        $this->num = $num;
        return $this;
    }

    public function setUser(User $user, UserRole $role): PointExchanger
    {
        $this->user = $user;
        $this->role = $role;
        return $this;
    }

    //开始兑换
    abstract public function exchange();

    //兑换前检查
    abstract public function checkBeforeExchange(): PointExchanger;

    public function preExchangeFromPoint(PointExchange $exchange)
    {
        //商品扣减速
        try {
            $rpc = new PointRpcService();
            $detail = $rpc->exchange($this->role->id, $this->goods->point_goods_yyid, $this->num);

            //更新兑换记录状态
            PointExchangeService::exchangeOk($exchange, $detail);
        } catch (\Exception $e) {
            //取消兑换
            $this->handleExchangeRevoke($exchange);
            throw $e;
        }
    }

    public function handleExchangeRevoke(PointExchange $exchange)
    {
        PointExchangeService::cancelPointExchange($exchange, $this->goods);
    }

    public function handlePointDetailRevoke(PointExchange $exchange, $msg = "兑换失败")
    {
        $exchange->refresh();
        $rpc = new PointRpcService();
        $rpc->directAudit($exchange->point_order_id, false, $msg);
    }
}
