<?php

namespace App\Service\Point\Exchanger;

use App\Constants\AppErr;
use App\Exception\BusinessException;
use App\Model\Pharmacy\PointExchange;
use App\Model\Pharmacy\PointGood;
use App\Repository\Point\PointExchangeRepository;
use App\Repository\Point\PointGoodRepository;
use App\Service\Point\PointExchangeService;
use App\Service\Point\PointRpcService;
use App\Service\Point\PointService;
use Hyperf\DbConnection\Db;

/**
 *
 * 现金商品兑换类
 *
 * 添加兑换记录
 * 调用积分系统兑换
 * 更新兑换记录
 */
class CashExchanger extends PointExchanger
{

    /**
     * @throws \Exception
     */
    public function exchange()
    {
        //创建兑换记录并更新余量
        $exchange = PointExchangeService::createExchangeFromGoods(
            $this->goods,
            $this->user,
            $this->role,
            $this->num
        );

        //积分预扣减
        self::preExchangeFromPoint($exchange);
    }

    public function checkBeforeExchange(): PointExchanger
    {
        return $this;
    }
}
