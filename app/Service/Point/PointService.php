<?php
declare(strict_types=1);
namespace App\Service\Point;

use App\Constants\AppErr;
use App\Constants\Role;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Pharmacy\BranchStore;
use App\Model\Pharmacy\CouponBranchStore;
use App\Model\Pharmacy\PharmacyPointEvent;
use App\Model\Pharmacy\PharmacyPointOrderDrugRule;
use App\Model\Pharmacy\PointExchange;
use App\Model\Pharmacy\PointGood;
use App\Model\Pharmacy\User;
use App\Model\Pharmacy\UserRole;
use App\Repository\BranchStoreRepository;
use App\Repository\CouponRepository;
use App\Model\Pharmacy\UserClerk;
use App\Repository\PharmacyPointEventRepository;
use App\Repository\PharmacyPointOrderDrugRuleRepository;
use App\Repository\Point\PointExchangeRepository;
use App\Repository\Point\PointGoodRepository;
use App\Repository\ProductCategoryRelRepository;
use App\Repository\UserRoleRepository;
use App\Service\Common\ProductRpcService;
use App\Service\Common\UploadRpcService;
use Grpc\Common\ProductItem;
use App\Service\Common\InsideMsgRpcService;
use App\Service\Common\TaskManageService;
use Grpc\Point\ActivitySpecialType;
use Grpc\Point\GroupItemReply;
use App\Service\Common\UploadService;
use Grpc\Point\PointDetail;
use Grpc\Point\UserType;
use Grpc\Task\ClerkPointChangeEvent;
use Grpc\Upload\UploadFileItem;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Context;

class PointService
{
    /**
     * 用户总资产
     * @return float[]
     */
    public static function getUserAssets($groupId = 0)
    {
        if ($groupId == 0) {
            $groupId = self::getDefaultGroupId()->getInfo()->getId();
        }
        $user = Helper::getLoginUser();
        $points = [
            'service_point' =>  0.0,
            'money_point' => 0.0
        ];
        $role = Context::get('role');
        $userRoles = UserRoleRepository::getUserRoleByUserId($user->id);
        $role = $userRoles->where('role', $role)->first();
        // 没有对应的角色数据返回0
        if (empty($role)) {
            $points['msg'] = 'err:无对应的用户角色ID';
            return $points;
        }
        $resp = ApplicationContext::getContainer()
            ->get(PointRpcService::class)
            ->getUserAssets($role->id, $groupId);
        $servicePoint = $resp->getServicePoint();
        $moneyPoint = $resp->getMoneyPoint();
        return [
            'service_point' => $servicePoint + 0.0,
            'money_point' => $moneyPoint + 0.0
        ];
    }

    private static function getDefaultGroupId(): GroupItemReply
    {
        return ApplicationContext::getContainer()
            ->get(PointRpcService::class)
            ->getDefaultGroup();
    }

    /**
     * 用户资产明细
     * @param $page
     * @param $pageSize
     * @param $pointType
     * @param int $type
     * @return array
     * @throws \Exception
     */
    public static function getUserPointsDetail($page, $pageSize, $pointType, $type = 0, $groupId = 0)
    {
        if ($groupId == 0) {
            $groupId = self::getDefaultGroupId()->getInfo()->getId();
        }
        $user = Helper::getLoginUser();
        $role = Context::get('role');
        $userRoles = UserRoleRepository::getUserRoleByUserId($user->id);
        $role = $userRoles->where('role', $role)->first();
        // 没有对应的角色数据返回0
        if (empty($role)) {
            return [
                'list'=>[],
                'total'=>0,
                'page'=>$page,
                'page_size'=>$pageSize,
                'msg'=>'err:无对应的用户角色ID',
            ];
        }
        $fetchService = $pointType == 1;
        $resp = ApplicationContext::getContainer()
            ->get(PointRpcService::class)
            ->getUserPointsDetail(
                $role->id,
                (int)$page,
                (int)$pageSize,
                (int)$type,
                $fetchService,
                UserType::USER_TYPE_PHARMACY,
                $groupId
            );
        $list = [];
        if ($resp->getTotal() > 0) {
            /** @var PointDetail $item */
            foreach ($resp->getList() as $key => $item) {
                $p = $pointType == 1 ? $item->getServicePoint() : $item->getMoneyPoint();
                $list[] = [
                    'desc' => $item->getDesc(),
                    'status' => $item->getStatus(),
                    'date' => Helper::formatInputDate($item->getCreatedAt(), "Y-m-d"),
                    'points' =>self::getPointDesc($p, $item->getStatus())
                ];
            }
        }
        return [
            'list'=>$list,
            'total'=>$resp->getTotal(),
            'page'=>$resp->getPage(),
            'page_size'=>$resp->getPageSize(),
        ];
    }

    public static function loadPointRules()
    {
        //获取规则
        $events = PharmacyPointEventRepository::getAll();
        if ($events->isEmpty()) {
            return;
        }
        /** @var PharmacyPointEvent $event */
        foreach ($events as $event) {
            switch ($event->event_key) {
                case PharmacyPointEvent::PATIENT_SCAN_DOCTOR_PATIENT_POINT:
                    self::loadPatientScanDoctorPatientPoint($event);
                    break;
                case PharmacyPointEvent::PATIENT_SCAN_DOCTOR_DOCTOR_POINT:
                    self::loadPatientScanDoctorDoctorPoint($event);
                    break;
                case PharmacyPointEvent::PATIENT_ORDER_PATIENT_POINT:
                case PharmacyPointEvent::PATIENT_ORDER_DOCTOR_FIRST_POINT:
                case PharmacyPointEvent::PATIENT_ORDER_CLERK_POINT:
                    self::loadOrderWithRulePoint($event);
                    break;
                case PharmacyPointEvent::PHARMACY_CLERK_READ_ARTICLE:
                    self::loadClerkReadArticleRule($event);
                    break;
                case PharmacyPointEvent::PHARMACY_CLERK_TOPIC_PART:
                    self::loadClerkTopicPartRule($event);
                    break;
                case PharmacyPointEvent::PHARMACY_CLERK_VERIFY:
                    self::loadClerkVerifyRule($event);
                    break;
            }
        }
        //获取规则详情
    }

    /**
     * 加载患者扫医生码患者得积分规则到积分服务
     * @param PharmacyPointEvent $event
     * @return void
     */
    private static function loadPatientScanDoctorPatientPoint(PharmacyPointEvent $event)
    {
        ApplicationContext::getContainer()
            ->get(PointRpcService::class)
            ->addPointRule(
                "患者首次扫医生码患者得积分",
                $event->event_key,
                $event->event_desc,
                $event->service_point,
                $event->money_point,
                $event->num_total,
                $event->num_per_day,
                (string) $event->created_at,
                '',
                $event->group_id
            );
    }

    /**
     * 加载患者扫医生码医生得积分规则到积分服务
     * @param PharmacyPointEvent $event
     * @return void
     */
    private static function loadPatientScanDoctorDoctorPoint(PharmacyPointEvent $event)
    {
        ApplicationContext::getContainer()
            ->get(PointRpcService::class)
            ->addPointRule(
                "患者首次扫医生医生得积分",
                $event->event_key,
                $event->event_desc,
                $event->service_point,
                $event->money_point,
                $event->num_total,
                $event->num_per_day,
                (string) $event->created_at,
                '',
                $event->group_id
            );
    }

    /**
     * 加载下单相送积分规则[和药品情况有关]
     * @param PharmacyPointEvent $event
     * @return void
     */
    private static function loadOrderWithRulePoint(PharmacyPointEvent $event)
    {
        //获取详情
        $name = "完成订单任务获得";
        switch ($event->event_key) {
            case PharmacyPointEvent::PATIENT_ORDER_PATIENT_POINT:
                $name = "患者下单完成获得积分";
                break;
            case PharmacyPointEvent::PATIENT_ORDER_CLERK_POINT:
                $name = "店员接单成功获得积分";
                break;
            case PharmacyPointEvent::PATIENT_ORDER_DOCTOR_FIRST_POINT:
                $name = "医生推荐患者首次购药成功获取积分";
                break;
        }

        $detailResp = PharmacyPointOrderDrugRuleRepository::getEventDetail($event);

        //添加积分
        $rpc = ApplicationContext::getContainer()
            ->get(PointRpcService::class);
        $rpc->addPointRule(
            $name,
            $event->event_key,
            $event->event_desc,
            $event->service_point,
            $event->money_point,
            $event->num_total,
            $event->num_per_day,
            (string) $event->created_at,
            '',
            $event->group_id
        );
        /** @var PharmacyPointOrderDrugRule $detail */
        foreach ($detailResp as $detail) {
            $rpc->addActivitySpecial(
                $event->event_key,
                $name,
                $detail->id,
                'pharmacy_point_order_drug_rule',
                $detail->rule_desc,
                $detail->service_point,
                $detail->money_point,
                $detail->num_per_day,
                $detail->num_total,
                (string) $detail->created_at,
                '',
                ActivitySpecialType::CUSTOM,
                $detail->group_id
            );
        }
    }

    /**
     * 积分描述
     * @param $point
     * @param $status
     * @return string
     */
    private static function getPointDesc($point, $status): string
    {
        switch ($status) {
            case 2:
                return sprintf("+%.2f", $point);
            default:
                return sprintf("-%.2f", $point);
        }
    }

    private static function getLoginUserPointUserType(): int
    {
        $role = Helper::getUserLoginRole();
        return $role->role == Role::ROLE_PATIENT ? PointGood::EXCHANGE_BY_PATIENT : PointGood::EXCHANGE_BY_CLERK;
    }

    public static function getGoodsList($pointType = 1, $current = 1, $limit = 10, $groupId = 0): array
    {
        if ($groupId == 0) {
            $groupId = self::getDefaultGroupId()->getInfo()->getId();
        }
        $goodsUserType = self::getLoginUserPointUserType();
        $resp = PointGoodRepository::listGoodsByPointTypeAndUserType(
            $pointType,
            $goodsUserType,
            $current,
            $limit,
            $groupId
        );

        $page =  [
            'total' => $resp->total(),
            'pages' => $resp->lastPage(),
            'current' => $resp->currentPage(),
            'size' => $limit,
        ];
        if ($resp->isEmpty()) {
            return [$page, []];
        }

        $imageIds = $resp->getCollection()->pluck('cover_id')->unique()->all();
        $imgMap = UploadService::getUploadUriByIds($imageIds);

        $list = [];
        /** @var PointGood $item */
        foreach ($resp->items() as $item) {
            $img = $item->cover_id && isset($imgMap[$item->cover_id]) ? $imgMap[$item->cover_id]->getUrl() : '';
            $list[] = [
                'id' => $item->id,
                'name' => $item->name,
                'cover' => $img,
                'type' => $item->goods_type,
                'rest' => $item->total > 0 ? $item->total - $item->exchanged : -1,
                'service_point' => $pointType == PointGood::POINT_TYPE_SERVICE_POINT ? $item->point : 0,
                'money_point' => $pointType == PointGood::POINT_TYPE_MONEY_POINT ? $item->point : 0
            ];
        }

        return [$page, $list];
    }

    public static function getGoodsDetail($id): array
    {
        $goods = PointGoodRepository::getById($id);
        if (!$goods) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '商品不存在');
        }

        if ($goods->isCouponGoods()) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '无法获取优惠券详情');
        }

        $coverUri = self::getGoodsCover($goods);

        $goodsDesc = PointGoodRepository::getGoodsDesc($goods);

        return [
            'id' => $goods->id,
            'name' => $goods->name,
            'desc' => $goodsDesc ? (string) $goodsDesc->goods_desc : '',
            'cover' => $coverUri,
            'rest' => $goods->total > 0 ? $goods->total - $goods->exchanged : -1,
            'service_point' => $goods->getServicePoint(),
            'money_point' => $goods->getMoneyPoint()
        ];
    }

    public static function getCouponGoodsInfo($id): array
    {
        $goods = PointGoodRepository::getById($id);
        if (!$goods) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '商品不存在');
        }
        if (!$goods->isCouponGoods()) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '商品类型不正确');
        }

        //获取优惠券数据
        $coupon = CouponRepository::getCouponInfoById($goods->external_goods_id);
        if (!$coupon) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, "优惠券不存在或已下架");
        }
        $products = "";
        $storeName = "";
        $total = 0;
        if ($coupon->yy_product_id) {
            $productResp  = ProductRpcService::listProductsByIds([$coupon->yy_product_id]);
            $category = ProductCategoryRelRepository::getProductCategory($coupon->yy_product_id);
            if ($category) {
                $products = $category->cat_name?:'';
            } else {
                $products = $productResp->getProducts()->count() == 1 ? $productResp->getProducts()[0]->getName() : '';
            }


            $scopePage = BranchStoreRepository::couponScopeStoreList(
                $coupon->scope,
                $coupon->id,
                $coupon->yy_product_id,
                1,
                1
            );
            $total = $scopePage->total();
            if ($scopePage->isNotEmpty()) {
                /** @var BranchStore $item */
                $item = $scopePage->items()[0];
                $storeName = $item->store_name;
            }
        }

        //获取商品封面
        $cover = self::getGoodsCover($goods);

        return [
            'id' => $goods->id,
            'name' => $goods->name,
            'coupon_id' => $goods->external_goods_id,
            'cover' => $cover,
            'rest' => $goods->total > 0 ? $goods->total - $goods->exchanged : -1,
            'service_point' => $goods->getServicePoint(),
            'money_point' => $goods->getMoneyPoint(),
            'drugs' => $products,
            'store' => $storeName,
            'store_total' => $total,
            'expire_type' => $coupon->deadline_type == 1 ? 2 : 1,
            'expire_day' => $coupon->deadline_type == 1 ? (int) $coupon->deadline_val : 0,
            'exipre_at' => $coupon->deadline_type == 2 ? (string) $coupon->deadline_val : ''
        ];
    }

    public static function exchangeGods($id, $num)
    {
        $user = Helper::getLoginUser();
        $role = Helper::getUserLoginRole();
        $goods = PointGoodRepository::getById($id);
        if (!$goods) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '商品不存在');
        }

        if ($goods->isExpired()) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '商品已过期，无法兑换');
        }
        if ($goods->quoteExceed()) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '商品已兑完');
        }

        if (!$goods->isValidToExchange($role->role)) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '当前商品类型不允许兑换');
        }

        PointExchangeService::getExchanger($goods, $user, $role, $num)
            ->checkBeforeExchange()
            ->exchange();
    }

    /**
     * @param PointGood $goods
     * @return string
     */
    public static function getGoodsCover(PointGood $goods): string
    {
        $coverUri = "";
        if ($goods->cover_id > 0) {
            $imgResp = UploadRpcService::getByIds([$goods->cover_id]);
            if ($imgResp->getList()->count() == 1) {
                $coverUri = $imgResp->getList()[0]->getUrl();
            }
        }
        return $coverUri;
    }


    /**
     * @param $isUpdateServicePoint
     * @param $isUpdateOrderMoneyPoint
     * @return void
     */
    public static function updateClerkPointRankInfo(
        UserClerk $clerk,
        $isUpdateServicePoint = true,
        $isUpdateOrderMoneyPoint = true
    ) {
        $clerkId = $clerk->id;
        $storeId = $clerk->store_id;
        if ($storeId && ($isUpdateOrderMoneyPoint || $isUpdateServicePoint)) {
            go(function () use ($clerkId, $storeId, $isUpdateServicePoint, $isUpdateOrderMoneyPoint) {
                $params = new ClerkPointChangeEvent();
                $params->setClerkId($clerkId);
                $params->setStoreId($storeId);
                $tm = TaskManageService::create(0, $params);
                if ($isUpdateServicePoint) {
                    $tm->setTopic('pharmacy-clerk-service-point-change')
                        ->push();
                    ;
                }

                if ($isUpdateOrderMoneyPoint) {
                    $tm->setTopic('pharmacy-clerk-order-point-change')
                        ->push();
                }
            });
        }
    }

    /**
     * 从通用积分事件里面向用户发送积分
     * @param $eventKey
     * @return void
     */
    public static function sendClerkPointFromCommonEvent(
        $eventKey,
        UserClerk $userClerk,
        $insideMsgType,
        $num = 1,
        $desc = '',
        $msgUri = ""
    ) {
        $event = PharmacyPointEventRepository::getByEventKey($eventKey);
        if (!$eventKey) {
            return;
        }

        if ($event->is_special == 1) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '该积分事件是特例事件');
        }

        $role = UserRoleRepository::getUserSpecialRole($userClerk->user_id, Role::ROLE_CLERK);
        try {
            $pointRpc = new PointRpcService();
            $detail = $pointRpc->addActivityPoint(
                $role->id,
                $eventKey,
                0,
                $num,
                $desc ? $desc : $event->event_desc,
                UserType::USER_TYPE_PHARMACY,
                ActivitySpecialType::CUSTOM,
                $event->group_id
            );
            if ($detail->getServicePoint() > 0 || $detail->getMoneyPoint() > 0) {
                (new InsideMsgRpcService())
                    ->sendInsideMsg(
                        $role->id,
                        $desc ? $desc : sprintf("获取积分: %s", $event->event_desc),
                        '获取积分',
                        $insideMsgType,
                        $desc ? $desc : $event->event_desc,
                        $msgUri
                    );
            }
            self::updateClerkPointRankInfo($userClerk);
        } catch (\Exception $e) {
            Helper::getLogger()->error("send_clerk_point_error", [
                'event_key' => $eventKey,
                'clerk_id' => $userClerk->id,
                'msg' => $e->getMessage(),
                'trace' => $e->getTrace()
            ]);
        }
    }

    private static function loadClerkReadArticleRule(PharmacyPointEvent $event)
    {
        ApplicationContext::getContainer()
            ->get(PointRpcService::class)
            ->addPointRule(
                "阅读文章获得",
                $event->event_key,
                $event->event_desc,
                $event->service_point,
                $event->money_point,
                $event->num_total,
                $event->num_per_day,
                (string) $event->created_at,
                '',
                $event->group_id
            );
    }

    private static function loadClerkTopicPartRule(PharmacyPointEvent $event)
    {
        //后面会改成单个话题时分配积分
        ApplicationContext::getContainer()
            ->get(PointRpcService::class)
            ->addPointRule(
                "参与话题认证通过获得",
                $event->event_key,
                $event->event_desc,
                $event->service_point,
                $event->money_point,
                $event->num_total,
                $event->num_per_day,
                (string) $event->created_at,
                '',
                $event->group_id
            );
    }

    private static function loadClerkVerifyRule(PharmacyPointEvent $event)
    {
        ApplicationContext::getContainer()
            ->get(PointRpcService::class)
            ->addPointRule(
                "认证通过获得",
                $event->event_key,
                $event->event_desc,
                $event->service_point,
                $event->money_point,
                $event->num_total,
                $event->num_per_day,
                (string) $event->created_at,
                '',
                $event->group_id
            );
    }

    public static function listExchangeRealGoodsReceiptsToConfirm($current = 1, $limit = 10)
    {
        $role = Helper::getUserLoginRole();
        $resp = PointExchangeRepository::listExchangeByGoodsTypeAndState(
            $role,
            PointGood::GOODS_TYPE_REAL,
            PointExchange::STATE_SEND_OK,
            $current,
            $limit
        );

        $page = [
            'total' => $resp->total(),
            'pages' => $resp->lastPage(),
            'current' => $resp->currentPage(),
            'size' => $limit,
        ];
        if ($resp->isEmpty()) {
            return [$page, []];
        }

        $coverIDS = [];
        $col = $resp->getCollection();
       /**
        * @var PointExchang $item
        */
        foreach ($col as $item) {
            if ($item->cover_id > 0) {
                $coverIDS[] = $item->cover_id;
            }
        }
        $coverMap = [];
        if (!empty($coverIDS)) {
            $coverMap = UploadService::getUploadUriByIds(array_unique($coverIDS));
        }

        $data = [];
       /**
        * @var PointExchange $item
        */
        foreach ($resp as $item) {
            $cover = isset($coverMap[$item->cover_id]) ? $coverMap[$item->cover_id]->getUrl() : '';
            $data[] = [
                'id' => $item->id,
                'name' => $item->goods_name,
                'cover' => $cover,
                'num' => $item->num,
                'time' => (string) $item->created_at
            ];
        }

        return [$page, $data];
    }

    public static function goodsReceiptConfirmed($exchangeId)
    {
        $role = Helper::getUserLoginRole();
        $exchange = PointExchangeRepository::getById($exchangeId);
        if (!$exchange) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '兑换记录不存在');
        }
        if (!($exchange->user_id == $role->user_id && $exchange->role_id == $role->id)) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '兑换不存在');
        }
        if (!$exchange->isSended()) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '兑换记录状态不正确，无法确认收货');
        }

        $goods = PointGoodRepository::getExchangeGoods($exchange);
        if (!$goods) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '商品不存在');
        }
        if (!$goods->isRealTypeGoods()) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '非实物类型商品无法确认收货');
        }

        $operateResult =PointExchangeRepository::receiptConfirmed($exchange);
        if (!$operateResult) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '兑换状态更新失败，请刷新重试');
        }

        //积分审核通过
        try {
            /**
             * @var PointRpcService $rpc
             */
            $rpc = ApplicationContext::getContainer()
                ->get(PointRpcService::class);
            $rpc->directAudit($exchange->point_order_id, true);
        } catch (\Exception $e) {
            Helper::getLogger()
                ->error("receipt_confirm_point_audit_error", [
                    'msg' => $e->getMessage(),
                    'exchange_id' => $exchange->id,
                    'trace' => $e->getTrace()
                ]);
        }
    }

    public static function countReceiptToConfirm(UserRole $role): int
    {
        return PointExchangeRepository::countExchangeOrders($role);
    }
}
