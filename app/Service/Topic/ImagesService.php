<?php
declare(strict_types=1);
namespace App\Service\Topic;

use App\Constants\Auth;
use App\Constants\ErrorCode;
use App\Exception\AuthException;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Pharmacy\TopicParticipateRecord;
//use App\Repository\Resource\ResourceRepository;
use App\Model\Pharmacy\TopicParticipateRecordFab;
use App\Model\Pharmacy\TopicParticipateRecordUpload;
use App\Repository\QuestionnaireRepository;
use App\Repository\Topic\TopicParticipateRecordRepository;
use App\Repository\Topic\TopicRepository;
use App\Service\Exam\ExamService;
use App\Service\Upload\UploadCommonService;
use App\Service\Upload\UploadRpcService;

class ImagesService
{

    private static function checkUser()
    {
        $role = Helper::getUserLoginRole();

        if ($role->role == 1) {
            throw new AuthException(Auth::USER_FORBIDDEN, "非法操作");
        }
    }

    public static function listUserImages($state = 0, $current = 1, $limit = 10): array
    {
        self::checkUser();

        $user = Helper::getLoginUser();

        $resp = TopicParticipateRecordRepository::listUserParticipated($user, $state, $current, $limit);

        $page = [
            'total' => $resp->total(),
            'pages' => $resp->lastPage(),
            'current' => $resp->currentPage(),
            'size' => $limit,
        ];

        $data = [];

        if ($resp->isEmpty()) {
            return [$page, $data];
        }

        /** @var TopicParticipateRecord $item */
        foreach ($resp as $item) {
            # 每条记录最新的一张图片/视频
            $UploadId = TopicParticipateRecordUpload::where(['record_id'=>$item->id])->min('upload_id');

            if (!$UploadId) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, '参与记录数据异常 - 资源不存在！');
            }

            # 获取上传图片连接
            $uploadMap = UploadCommonService::getUploadUrlMapByIds([$UploadId]);

            $data[] = [
                'id' => $item->id,
                'tp' => $item->tp,
                'upload_id' => $UploadId,
                'url' => $uploadMap[$UploadId] ?? ''
            ];
        }

        return [$page, $data];
    }

    public static function publishImage(int $id)
    {
        $user = Helper::getLoginUser();
        $role = Helper::getUserLoginRole();
        $part = TopicParticipateRecordRepository::getById($id);
        if (!($part && $part->user_id == $user->id)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '参与记录不存在');
        }
        if (!$part->isDraft()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '已发布');
        }
        TopicParticipateRecordRepository::publishDraft($part);

        $topic = TopicRepository::getById($part->topic_id);
        if ($topic && $part->isAuditPassed()) {
            TopicService::sendTopicAuditPoint($topic, $role);
        }
    }

    public static function delByIds($ids)
    {
        $user = Helper::getLoginUser();

        $resp = TopicParticipateRecordRepository::getByIds($ids, $user);

        $imagesIds = [];

        $ids = [];

        /** @var TopicParticipateRecord $item */
        foreach ($resp as $item) {
            $ids[] = $item->id;

            $imagesIds[] = $item->image_id;
        }

        //删除参与记录
        TopicParticipateRecordRepository::deleteByIds($ids);
        //TODO:是否需要处理资源

        //根据id删除视频资源
        self::deleteQiniu(array_unique($imagesIds));
    }

    public static function deleteQiniu($uploadIds)
    {
        go(function () use ($uploadIds) {
            $rpc = new UploadRpcService();
            $rpc->deleteByIds($uploadIds);
        });
    }

    /**
     * 获取参与记录详情
     * @param int $id
     * @return array
     */
    public static function info($record_id, $action, $order, $source, $state, $topic_id): array
    {

        if ($source == 1 && !$state) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '个人中心详情状态不正确');
        }

        if ($source == 2 && !$order) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '话题参与详情状态不正确');
        }

        self::checkUser();

        $PreLast = $NextLast = false;

        $user = Helper::getLoginUser();

        $Info  = TopicParticipateRecordRepository::getUploadDetailById(
            $record_id,
            $action,
            $order,
            $source,
            $state,
            $user,
            $topic_id
        );

        if (!$Info) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '参与记录不存在');
        }

        # 是否最后一张
        $LastInfo  = TopicParticipateRecordRepository::isLastById(
            $Info->record_id,
            $action,
            $order,
            $source,
            $state,
            $user->id,
            $topic_id
        );

        if ($LastInfo['pre']) {
            $PreLast = true;
        }

        if ($LastInfo['next']) {
            $NextLast = true;
        }

        $likeCount = TopicParticipateRecordRepository::getLikesByResourceId($Info->record_id);

        $imagesMap = UploadCommonService::getUploadUrlMapByIds(explode(',', $Info->upload_ids));

        $fab = TopicParticipateRecordFab::where(['user_id'=>$user->id,'record_id'=>$Info->record_id,'fab'=>1])->first();

        return [
            'id'        => $Info->record_id,
            'pre'       => $PreLast,
            'next'      => $NextLast,
            'tp'        => $Info->tp,
            'url'       => $imagesMap ? array_merge($imagesMap) : [],
            'comment'   => $Info->comment,
            'fab'       => $fab ? 1 : 0,
            'state'     => $Info->state,
            'like_count'    => $likeCount,
            'share_count'   => $Info->share_count,
            'topic_id'      => $Info->topic_id,
            'topic_name'    => $Info->name,
            'reject_reason' => $Info->state == TopicParticipateRecord::STATE_AUDIT_FAILED ? $Info->reject_reason : ''
        ];
    }


    /**
     * 活动中心 - 目前按照 只有一个活动做
     * @param int $id
     * @return array
     */
    public static function activityList(): array
    {
        self::checkUser();

        $data = [];

//        $QuestionnaireRepository = QuestionnaireRepository::getListByProductIds();  # 问卷

        $TopicRepository = TopicRepository::getTopicsList();    # 话题

        $Exam = ExamService::list();

//        if ($QuestionnaireRepository) {
//            foreach ($QuestionnaireRepository as &$item) {
//                $item['type'] = 1;
//            }
//        }


        if ($TopicRepository) {
            foreach ($TopicRepository as &$item2) {
                $item2['type'] = 2;
            }
        }


        # 20230301 首页删除问卷 改 api/questionnaire/activity-question 培训中心接口
        $data[] = array_merge($TopicRepository, $Exam);

        return $data;
    }

    /**
     * 点赞
     * @param int $id
     * @return array
     */
    public static function setTopicLikeImages(int $id)
    {
        self::checkUser();

        $part = TopicParticipateRecordRepository::getById($id);

        if (!$part) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '参与记录不存在');
        }

        $user = Helper::getLoginUser();

        TopicParticipateRecordRepository::setTopicLikeImages($id, $user);
    }

    /**
     * 分享
     * @param int $id
     * @return array
     */
    public static function setTopicShareNum(int $id)
    {
        self::checkUser();

        $part = TopicParticipateRecordRepository::getById($id);

        if (!$part) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '参与记录不存在');
        }

        TopicParticipateRecordRepository::setTopicShareNum($id);
    }
}
