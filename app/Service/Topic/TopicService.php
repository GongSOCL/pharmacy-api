<?php
declare(strict_types=1);
namespace App\Service\Topic;

use App\Constants\Auth;
use App\Constants\BizAgentType;
use App\Constants\Clerk;
use App\Constants\ErrorCode;
use App\Exception\AuthException;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Pharmacy\RecordLike;
use App\Model\Pharmacy\Topic;
use App\Model\Pharmacy\TopicParticipateRecord;
use App\Model\Pharmacy\TopicParticipateRecordFab;
use App\Model\Pharmacy\TopicParticipateRecordUpload;
use App\Model\Pharmacy\UserRole;
use App\Repository\Topic\TopicParticipateRecordRepository;
use App\Repository\Topic\TopicRepository;
use App\Service\Common\BizAgentService;
use App\Service\Common\TaskRpcService;
use App\Service\Point\PointRpcService;
use App\Service\Upload\UploadCommonService;
use App\Service\Upload\UploadRpcService;
use App\Service\User\UserClerkService;
use Grpc\Common\CommonPageResponse;
use Grpc\Otc\Topic\TopicListReply;
use Grpc\Point\ActivitySpecialType;
use Grpc\Point\UserType;
use Grpc\Upload\UploadFileItem;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Collection;

class TopicService
{


    private static function checkUser()
    {
        $role = Helper::getUserLoginRole();

        if ($role->role == 1) {
            throw new AuthException(Auth::USER_FORBIDDEN, "非法操作");
        }
    }

    public static function addTopic($name, $coverId, $detailId, $isPublish = false): \App\Model\Niferex\Topic
    {
        return TopicRepository::addOne(
            $name,
            $coverId,
            $detailId,
            $isPublish
        );
    }

    public static function updateLike(int $resourceId, $count)
    {
        TopicParticipateRecordRepository::updateLike($resourceId, $count);
    }

    public static function getByIds(array $ids): TopicListReply
    {
        $resp = TopicRepository::getByIds(array_unique($ids));
        return self::buildTopicListFromCollection($resp);
    }

    private static function buildTopicListFromCollection(Collection $collection): TopicListReply
    {
        $resp = new TopicListReply();
        if ($collection->isEmpty()) {
            return $resp;
        }

        $data = [];
        /** @var Topic $item */
        foreach ($collection as $item) {
            $o= new \Grpc\Otc\Topic\Topic();
            $o->setId($item->id);
            $o->setName($item->name);
            $data[] = $o;
        }
        $resp->setTopic($data);
        return $resp;
    }

    public static function searchTopics($keywords = "", $limit = 5): TopicListReply
    {
        $resp = TopicRepository::limitSearch($keywords, $limit);
        return self::buildTopicListFromCollection($resp);
    }

    public static function sendTopicAuditPoint(Topic $topic, UserRole $role)
    {
        try {
            ApplicationContext::getContainer()
                ->get(PointRpcService::class)
                ->addActivityPoint(
                    $role->id,
                    'TOPIC_PARTICIPATE',
                    $topic->id,
                    1,
                    sprintf("参与pk'%s'通过审核奖励积分", $topic->name),
                    UserType::USER_TYPE_PHARMACY,
                    ActivitySpecialType::CUSTOM,
                    $topic->group_id
                );
        } catch (\Exception $e) {
            Helper::getLogger()->error("send_topic_point_error", [
                'user_id' => $role->id,
                'topic_id' => $topic->id,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function publish($id)
    {
        $topic = TopicRepository::getById((int) $id);
        if (!$topic) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '话题不存在');
        }
        if ($topic->isPublished()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '话题已经发布，请刷新重试');
        }
        TopicRepository::publishTopic($topic);
    }


    /**
     * 话题详情
     * @param $id
     * @return array
     */
    public static function detail($id): array
    {
        //获取登陆用户clerk
        $clerk = UserClerkService::checkAndGetClerk();
        if ($clerk->status != Clerk::STATUS_PASS) {
            throw new AuthException(Auth::USER_FORBIDDEN, "店员账号未认证");
        }

        $topic = TopicRepository::getById($id);

        if (!$topic) {
            return [
                'name' => '测试话题详情',
                'total' => 2,
                'detail_pic' => 'http://img.daimg.com/uploads/allimg/210114/1-210114151951.jpg'
            ];

            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '话题不存在');
        }
        if (!$topic->isPublished()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '话题不存在');
        }

        //获取详情页图片
//        $rpc = new UploadRpcService();
//
//        $resp = $rpc->getByIds([$topic->detail_id]);
//
//        $url = "";
//        if (isset($resp->getList()[0])) {
//            /** @var UploadFileItem $item */
//            $item = $resp->getList()[0];
//            $url = $item->getUrl();
//        }

        //统计参与人数
        $total = TopicParticipateRecordRepository::countTopicParticipates($topic);

        return [
            'name' => $topic->name,
            'total' => $total,
            'detail_pic' => $topic->detail_url
        ];
    }

    /**
     * 获取参与话题下的图片列表
     * @param $topicId
     * @param bool $isOrderNew
     * @param int $current
     * @param int $limit
     * @return array
     */
    public static function listTopicImages($topicId, bool $isOrderNew, $current = 1, $limit = 10)
    {
//
        //获取登陆用户clerk
        $clerk = UserClerkService::checkAndGetClerk();
        if ($clerk->status != Clerk::STATUS_PASS) {
            throw new AuthException(Auth::USER_FORBIDDEN, "店员账号未认证");
        }

        $user = Helper::getLoginUser();

        $topic = TopicRepository::getById($topicId);

        if (!$topic) {
            $page = [
                'total' => 2,
                'pages' => 2,
                'current' => 2,
                'size' => 2,
            ];
            $data[] = [
                'id' => 1,
                'url' => 'http://img.daimg.com/uploads/allimg/210114/1-210114151951.jpg',
                'is_mine' => 1,
                'total' => 22,
                'status' => 1,
            ];
            $data[] = [
                'id' => 1,
                'url' => 'http://img.daimg.com/uploads/allimg/210114/1-210114151951.jpg',
                'is_mine' => 1,
                'total' => 22,
                'status' => 2,
            ];
            $data[] = [
                'id' => 1,
                'url' => 'http://img.daimg.com/uploads/allimg/210114/1-210114151951.jpg',
                'is_mine' => 1,
                'total' => 22,
                'status' => 3,
            ];
            $data[] = [
                'id' => 1,
                'url' => 'http://img.daimg.com/uploads/allimg/210114/1-210114151951.jpg',
                'is_mine' => 0,
                'total' => 22,
                'status' => 3,
            ];

            return [$page, $data];
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '话题不存在');
        }
        if (!$topic->isPublished()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '话题不存在');
        }

//        $resp = TopicParticipateRecordRepository::getTopicParticipate(
//            2,
//            $topic,
//            $isOrderNew,
//            $current,
//            $limit
//        );

        $resp = TopicParticipateRecordRepository::getTopicParticipate(
            $user,
            $topic,
            $isOrderNew,
            $current,
            $limit
        );


        $page = [
            'total' => $resp->total(),
            'pages' => $resp->lastPage(),
            'current' => $resp->currentPage(),
            'size' => $limit,
        ];

        $data = [];

        if ($resp->isEmpty()) {
            return [$page, $data];
        }

        foreach ($resp as $item) {
            # 是否自己点赞
            $fab = TopicParticipateRecordFab::where(['user_id'=>$user->id,'record_id'=>$item->id,'fab'=>1])->first();

            # 每条记录最新的一张图片/视频
            $UploadId = TopicParticipateRecordUpload::where(['record_id'=>$item->id])->min('upload_id');

            if (!$UploadId) {
                continue;
            }
            # 获取上传图片连接
            $uploadMap = UploadCommonService::getUploadUrlMapByIds([$UploadId]);

            $data[] = [
                'id' => $item->id,
                'upload_id' => $UploadId,
                'tp' => $item->tp,
                'url' => $uploadMap[$UploadId] ?? '',
                'is_fab' => $fab ? 1 : 0,
                'is_mine' => ($item->user_id == $user->id) ? 1 : 0,
                'total' => (int) $item->like_count,
                'status' => $item->state,
            ];
        }

        return [$page, $data];
    }

    public static function participate(
        $topicId,
        $tp,
        $upload_image_ids,
        $upload_video_ids,
        $comment,
        bool $isPublish
    ): TopicParticipateRecord {
        $role = Helper::getUserLoginRole();
        //获取登陆用户clerk
        $clerk = UserClerkService::checkAndGetClerk();
        if ($clerk->status != Clerk::STATUS_PASS) {
            throw new AuthException(Auth::USER_FORBIDDEN, "店员账号未认证");
        }

        //获取登陆用户clerk
        $clerk = UserClerkService::checkAndGetClerk();
        if ($clerk->status != Clerk::STATUS_PASS) {
            throw new AuthException(Auth::USER_FORBIDDEN, "店员账号未认证");
        }

        if (!$tp) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '上传资源类型异常');
        }

        if ($tp == 1 && !$upload_image_ids) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '上传图片资源不可为空');
        }

        if ($tp == 2 && !$upload_video_ids) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '上传视频资源不可为空');
        }


        $user = Helper::getLoginUser();

        $topic = TopicRepository::getById($topicId);

        if (!$topic) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '话题不存在');
        }

        $resp = TopicParticipateRecordRepository::addParticipate(
            $topic,
            $user,
            $tp,
            $upload_image_ids,
            $upload_video_ids,
            $isPublish,
            $comment
        );

        // 通过审核直接进行积分发放
        if ($isPublish) {
            $topic->refresh();
            self::sendTopicAuditPoint($topic, $role);
        }

        //记录业务关联代表信息
        BizAgentService::recordAgent(
            $resp->id,
            $clerk->store_id,
            BizAgentType::BIZ_TYPE_DISPLAY
        );
        return $resp;
    }

    public static function getTopicPartCountByIds(array $topicIds = []): array
    {
        if (empty($topicIds)) {
            return [];
        }
        $count = [];
        $resp = TopicParticipateRecordRepository::countGroupByTopics($topicIds);
        $map = $resp->keyBy('topic_id')->all();
        foreach ($topicIds as $id) {
            if (isset($map[$id])) {
                $count[$id] = (int) $map[$id]->cnt;
            } else {
                $count[$id] = 0;
            }
        }

        return $count;
    }
}
