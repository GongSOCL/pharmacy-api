<?php
declare(strict_types=1);

namespace App\Service\Upload;

use App\Constants\AppErr;
use App\Constants\Upload;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Repository\Ticket\TicketRepository;

class UploadService
{
    /**
     * @param $userId
     * @param $file
     * @return array
     */
    public function uploadTicket($userId, $file)
    {
        $fileName = UploadCommonService::upload(Upload::UPLOAD_URI_TICKET, $file, $userId);
        if (!$fileName) {
            throw new BusinessException(AppErr::FAIL_TO_UPLOAD);
        }

        $newId = TicketRepository::createOneTicket(
            $userId,
            '/tickets/' . $userId . '/' . $fileName,
            explode('.', $fileName)[0]
        );
        if (!$newId) {
            throw new BusinessException(AppErr::FAIL_TO_UPLOAD_TICKET_INFO);
        }

        return [
            'path' => '/tickets/' . $userId . '/' . $fileName,
            'fileName' => $fileName
        ];
    }

    public function addUpload($bucket, $key, $name, $url): array
    {
        $user = Helper::getLoginUser();
        $rpc = new UploadRpcService();
        $resp = $rpc->addUploadRecord(
            $user,
            $name,
            $key,
            $bucket,
            $url
        );
        return [
            'id' => $resp->getId(),
            'name' => $resp->getName(),
            'url' => $resp->getUrl()
        ];
    }

    public function getConfig($type, $business): array
    {
        $rpc = new UploadRpcService();
        $resp = $rpc->getToken($business, $type);
        return [
            'path' => $resp->getPath(),
            'token' => $resp->getToken(),
            'expire_time' => $resp->getExpireTime(),
            'bucket' => $resp->getBucket(),
            'base_uri' => $resp->getBaseUri(),
            'upload_uri' => $resp->getUploadUri()
        ];
    }
}
