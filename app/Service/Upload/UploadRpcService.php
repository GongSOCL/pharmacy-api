<?php
declare(strict_types=1);
namespace App\Service\Upload;

use App\Model\Pharmacy\User;
use Google\Protobuf\GPBEmpty;
use Grpc\Upload\AddUploadRequest;
use Grpc\Upload\BUSINESS_TYPE;
use Grpc\Upload\CommonIdsRequest;
use Grpc\Upload\DeleteUploadRequest;
use Grpc\Upload\OssStsResponse;
use Grpc\Upload\TokenRequest;
use Grpc\Upload\TokenResponse;
use Grpc\Upload\UPLOAD_SCOPE;
use Grpc\Upload\UploadClient;
use Grpc\Upload\UploadFileList;
use Hyperf\Utils\ApplicationContext;

class UploadRpcService
{
    private static function getClient(): UploadClient
    {
        return ApplicationContext::getContainer()
            ->get(UploadClient::class);
    }

    public function getByIds(array $ids): UploadFileList
    {
        $req = new CommonIdsRequest();

        $req->setId($ids);

        return self::getClient()->GetByIds($req);
    }

    public function deleteByIds(array $ids, bool $retainFile = false)
    {
        $req = new DeleteUploadRequest();
        $req->setUploadId($ids);
        $req->setRetainFile($retainFile);
        self::getClient()->DeleteByIds($req);
    }

    public function getToken($business, $type): TokenResponse
    {
        $req = new TokenRequest();
        $req->setBusiness($business);
        $req->setScope(UPLOAD_SCOPE::SCOPE_OTC);
        $req->setType($type);
        return self::getClient()->GetToken($req);
    }

    public function addUploadRecord(User $user, $name, $key, $bucket, $url): \Grpc\Upload\AddUploadResponse
    {
        $req = new AddUploadRequest();
        $req->setUid($user->id);
        $req->setKey($key);
        $req->setBucket($bucket);
        $req->setUrl($url);
        $req->setName($name);
        return self::getClient()->AddUploadRecord($req);
    }

    public static function getOssSts(): OssStsResponse
    {
        return self::getClient()->GetOssSts(new GPBEmpty());
    }
}
