<?php
declare(strict_types=1);

namespace App\Service\Upload;

use Grpc\Upload\UploadFileItem;

class UploadCommonService
{
    /**
     * @param string $url
     * @param object $file
     * @param $id
     * @return string
     */
    public static function upload(string $url, object $file, $id)
    {
        $extension = $file->getExtension();
        $dir = $url . $id . '/';
        $fileName = $id . '_' . time() . rand(10000000, 99999999) . '.' . $extension;
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $path = $dir.$fileName;
        $file->moveTo($path);
        if (!$file->isMoved()) {
            return '';
        }
        return $fileName;
    }

    public static function getUploadUrlMapByIds(array $uploadIds): array
    {
        $rpc = new UploadRpcService();
        $resp = $rpc->getByIds($uploadIds);
        $map = [];
        /** @var UploadFileItem $item */
        foreach ($resp->getList() as $item) {
            $map[$item->getId()] = $item->getUrl();
        }
        return $map;
    }
}
