<?php
declare(strict_types=1);

namespace App\Service\Coupon;

use App\Constants\AppErr;
use App\Constants\CouponConstant;
use App\Constants\Role;
use App\Exception\BusinessException;
use App\Helper\CommonBus;
use App\Helper\Helper;
use App\Helper\Lock;
use App\Repository\BranchStoreRepository;
use App\Repository\CouponDetailRepository;
use App\Repository\CouponRepository;
use App\Repository\ProductRepository;
use App\Repository\PurchaseNotifyRepository;
use App\Service\User\UserClerkService;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Context;

class UserCouponService
{
    /**
     * 患者优惠券列表
     * @param $type
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getUserCouponList($type, $page, $pageSize)
    {
        $resp = [
            'list' => [],
            'total' => 0,
            'page' => $page,
            'page_size' => $pageSize
        ];
        $user = Helper::getLoginUser();
        $role = Context::get('role');
        if ($role != Role::ROLE_PATIENT) {
            $resp['msg'] = '非法角色访问';
            return $resp;
        }
        $res = CouponDetailRepository::getUserCouponList($type, $user->id, $pageSize);
        if ($res->isNotEmpty()) {
            CommonBus::handleCouponInfo($res);
        }
        $resp = [
            'list' => $res->items(),
            'total' => $res->total(),
            'page' => $page,
            'page_size' => $pageSize
        ];
        return  $resp;
    }

    /**
     * 店员下单患者可用优惠券列表
     * @param $notifyId
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getUserUseCouponList($notifyId, $page, $pageSize)
    {
        $notifyInfo = PurchaseNotifyRepository::getNotifyInfo($notifyId);
        $resp = [
            'list' => [],
            'total' => 0,
            'page' => $page,
            'page_size' => $pageSize
        ];
        if (!$notifyInfo) {
            $resp['msg'] = '待处理的待接单消息不存在';
            return $resp;
        }
        try {
            $clerkInfo = UserClerkService::checkAndGetClerk();
            $storeInfo = BranchStoreRepository::getStoreInfoById($clerkInfo->store_id);
            $patientUserId = $notifyInfo->patient_user_id;
            $storeId = $storeInfo->id;
            $mainStoreId = $storeInfo->main_store_id;
            $res = CouponDetailRepository::getCanUseCouponList(
                $patientUserId,
                $mainStoreId,
                $storeId,
                $pageSize
            );
            if ($res->isNotEmpty()) {
                CommonBus::handleCouponInfo($res);
            }
            $resp = [
                'list' => $res->items(),
                'total' => $res->total(),
                'page' => $page,
                'page_size' => $pageSize
            ];
            return $resp;
        } catch (\Exception $e) {
            $resp['msg'] = $e->getMessage();
            return $resp;
        }
    }

    /**
     * 患者可用优惠券数量
     * @return int[]
     */
    public function getUserCouponNum()
    {
        $resp = [
            'num'=>0
        ];
        $user = Helper::getLoginUser();
        $role = Context::get('role');
        if ($role != Role::ROLE_PATIENT) {
            $resp['msg'] = '非法角色访问';
            return $resp;
        }
        $num = CouponDetailRepository::getUserCouponNum($user->id);
        $resp['num'] = $num;
        return $resp;
    }

    /**
     * 下单可用数量
     * @param $notifyId
     * @return int[]
     */
    public function getCanUserCouponNum($notifyId)
    {
        $notifyInfo = PurchaseNotifyRepository::getNotifyInfo($notifyId);
        $resp = [
            'num'=>0
        ];
        if (!$notifyInfo) {
            $resp['msg'] = '待处理的待接单消息不存在';
            return $resp;
        }
        try {
            $clerkInfo = UserClerkService::checkAndGetClerk();
            $storeInfo = BranchStoreRepository::getStoreInfoById($clerkInfo->store_id);
            $patientUserId = $notifyInfo->patient_user_id;
            $storeId = $storeInfo->id;
            $mainStoreId = $storeInfo->main_store_id;
            $num = CouponDetailRepository::getCanUseCouponNum($patientUserId, $mainStoreId, $storeId);
            $resp['num'] = $num;
        } catch (\Exception $e) {
            $resp['msg'] = $e->getMessage();
        }
        return $resp;
    }

    /**
     * 免费领取优惠券
     * @param $couponIds
     * @throws \Exception
     */
    public function gainCoupon($couponIds)
    {
        $user = Helper::getLoginUser();
        $role = Context::get('role');
        if ($role != Role::ROLE_PATIENT) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '非法角色访问');
        }
        if (empty($couponIds)) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '领取礼券ID不可为空');
        }
        $userId = $user->id;
        $lockKey = sprintf("pharmacy:get-coupon:user:%d", $userId);
        // 创建锁
        $lock = ApplicationContext::getContainer()
            ->get(Lock::class);
        $identify = $lock->acquire($lockKey, 10, 10);
        if (false === $identify) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, "正在处理礼券，请等待");
        }
        try {
            collect($couponIds)->each(function ($couponId) use ($userId) {
                $this->handleGetCoupon($couponId, $userId);
            });
            // 释放锁
            $lock->release($lockKey, $identify);
        } catch (\Exception $e) {
            // 释放锁
            $lock->release($lockKey, $identify);
            throw $e;
        }
    }

    /**
     *
     * @param $goodsId
     * @throws \Throwable
     */
    public function exchangeCoupon($goodsId)
    {
        $user = Helper::getLoginUser();
        $role = Context::get('role');
        if ($role != Role::ROLE_PATIENT) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '非法角色访问');
        }
        $userId = $user->id;
        $lockKey = sprintf("pharmacy:exchange-coupon:user:%d", $userId);
        // 创建锁
        $lock = ApplicationContext::getContainer()
            ->get(Lock::class);
        $identify = $lock->acquire($lockKey, 10, 10);
        if (false === $identify) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, "正在处理礼券，请等待");
        }
        try {
            $couponId = 1;
            $isExchange = 1;
            [$flag, $msg] = $this->handleGetCoupon($couponId, $user->id, $isExchange);
            if ($flag) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, $msg);
            }
            // 释放锁
            $lock->release($lockKey, $identify);
        } catch (\Throwable $e) {
            // 释放锁
            $lock->release($lockKey, $identify);
            throw $e;
        }
    }
    /**
     * @param $couponId
     * @param $userId
     * @param int $isExchange 0:免费领取 1:需要积分兑换
     * @return array
     */
    public function handleGetCoupon($couponId, $userId, $isExchange = 0)
    {
        try {
            // 免费领取限制某种券一人一张
            if ($isExchange == 0 && CouponDetailRepository::existGetCoupon($userId, $couponId)) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '当前用户已领取:'.$userId);
            }
            // 优惠券信息不存在无法领取
            $couponInfo = CouponRepository::getCouponInfoById($couponId);
            if (empty($couponInfo)) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '优惠券信息不存在无法:'.$couponId);
            }
            // 未上架无法领取
            if ($couponInfo->status != CouponConstant::COUPON_STATUS_YES) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '优惠券未发布:'.$couponId);
            }
            // 非免费领取无法领取
            if ($isExchange == 0 && $couponInfo->is_exchange != CouponConstant::COUPON_EXCHANGE_NO) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '优惠券非免费领取:'.$couponId);
            }
            // 兑换优惠券判断
            if ($isExchange == 1 && $couponInfo->is_exchange != CouponConstant::COUPON_EXCHANGE_YES) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '兑换优惠券类型有误:'.$couponId);
            }

            // 超过领取总量无法领取
            if ($couponInfo->is_limit == CouponConstant::COUPON_LIMIT_YES &&
                $couponInfo->total_num <= $couponInfo->receive_num) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '优惠券超出领取数量:'.$couponId);
            }
            // 过期无法领取
            $time = date('Y-m-d H:i:s', time());
            if ($couponInfo->deadline_type == CouponConstant::COUPON_DEADLINE_TYPE_SPEC &&
                $couponInfo->deadline_val < $time) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '优惠券已过期:'.$couponId);
            }
        } catch (\Exception $e) {
            Helper::getLogger()->error(
                "pharmacy.getCoupon.error",
                [
                    'user_id'=>$userId,
                    'coupon_id'=>$couponId,
                    'msg'=>$e->getMessage()
                ]
            );
            return ['flag'=>false, 'msg'=>$e->getMessage(), 'no' => ''];
        }
        Db::beginTransaction();
        try {
            $couponNum = 'CN'.date('YdmHis').mt_rand(100000, 999999);
            $deadlineTime = $this->handleDeadlineTime(
                $couponInfo->deadline_type,
                $couponInfo->deadline_val
            );
            $incFlag = CouponRepository::incCouponReceiveNum($couponId);
            if (!$incFlag) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '优惠券已领取完:'.$couponId);
            }
            CouponDetailRepository::getCoupon($couponId, $couponNum, $userId, $deadlineTime);
            Db::commit();
        } catch (\Exception $e) {
            Db::rollBack();
            Helper::getLogger()->error(
                "pharmacy.getCoupon.error",
                [
                    'user_id'=>$userId,
                    'coupon_id'=>$couponId,
                    'msg'=>$e->getMessage()
                ]
            );
            return ['flag'=>false, 'msg'=>$e->getMessage(), 'no' => ''];
        }
        return ['flag'=>true, 'msg'=>'success', 'no' => $couponNum];
    }

    private function handleDeadlineTime($type, $val)
    {
        $time = 0;
        switch ($type) {
            case 1:
                $time = strtotime(date(
                    'Y-m-d',
                    time() + $val*24*60*60
                ).' 23:59:59');
                break;
            case 2:
                $time = strtotime(date(
                    'Y-m-d',
                    strtotime($val)
                ).' 23:59:59');
                break;
        }
        return $time;
    }

    public static function revokeCoupon($couponNo)
    {
        $couponDetail = CouponDetailRepository::getCouponByNo($couponNo);
        if (!$couponDetail) {
            return;
        }

        $coupon = CouponRepository::getCouponInfoById($couponDetail->coupon_id);

        Db::beginTransaction();
        try {
            //撤销详情
            $res = CouponDetailRepository::revokeCoupon($couponDetail);
            if (!$res) {
                throw new BusinessException(AppErr::BUSINESS_ERROR, '用户优惠券撤回失败，请联系平台');
            }
            if ($coupon && $coupon->is_exchange) {
                CouponRepository::decrCouponReceiveNum($coupon->id);
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }
    }
}
