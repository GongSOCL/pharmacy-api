<?php
declare(strict_types=1);

namespace App\Service\Coupon;

use App\Constants\AppErr;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Repository\BranchStoreProductRepository;
use App\Repository\BranchStoreRepository;
use App\Repository\CouponDetailRepository;
use App\Repository\CouponRepository;
use App\Repository\ProductCategoryRelRepository;
use App\Repository\ProductRepository;

class CouponService
{

    /**
     * 礼券适用范围列表
     * @param $couponId
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getCouponScopeList($couponId, $page, $pageSize)
    {
        $couponInfo = CouponRepository::getCouponInfoById($couponId);
        if (!$couponInfo) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '当前优惠券信息不存在');
        }
        $scope = $couponInfo->scope;
        $yyProductId = $couponInfo->yy_product_id;
        $res = BranchStoreRepository::couponScopeStoreList($scope, $couponId, $yyProductId, $pageSize, $page);
        return [
            'list' => $res->items(),
            'total' => $res->total(),
            'page' => $page,
            'page_size' => $pageSize
        ];
    }

    public function storeCouponList($storeId)
    {
        $storeInfo = BranchStoreRepository::getStoreInfoById($storeId);
        if (!$storeInfo) {
            throw new BusinessException(AppErr::BUSINESS_ERROR, '店铺消息不存在');
        }
        $user = Helper::getLoginUser();
        $storeId = $storeInfo->id;
        $mainStoreId = $storeInfo->main_store_id;
        $storeProductIds = BranchStoreProductRepository::getStoreProductsByStoreId($storeId)
            ->pluck('yy_product_id')->toArray();
        $list = CouponRepository::getStoreCouponList($mainStoreId, $storeId, $storeProductIds);
        $isGet = 1;
        if ($list->count() > 0) {
            $yyProductIds = $list->pluck('yy_product_id')->unique();
            $productMap = ProductRepository::getListByProductIds($yyProductIds)
                ->pluck(null, 'yy_product_id');
            $catMap = ProductCategoryRelRepository::getProductCategories($yyProductIds)
                ->pluck(null, 'yy_product_id');
            $couponIds = $list->pluck('id')->unique();
            $gainCouponMap = CouponDetailRepository::getCouponListByUserIdAndCouponIds($user->id, $couponIds)
                ->pluck('id', 'coupon_id')->toArray();
            $list->map(function ($row) use ($catMap, $productMap, $gainCouponMap, &$isGet) {
                $productInfo = $productMap[$row->yy_product_id]??null;
                $catInfo = $catMap[$row->yy_product_id]??null;
                if ($catInfo) {
                    $row->product_name = $catInfo['cat_name'];
                } else {
                    $row->product_name = '';
                }
                if (empty($row->product_name)) {
                    if ($productInfo) {
                        $row->product_name = $productInfo->name;
                    } else {
                        $row->product_name = '';
                    }
                }
                if (array_key_exists($row->id, $gainCouponMap)) {
                    $row->is_get = 1;
                } else {
                    $row->is_get = 0;
                    $isGet = 0;
                }
                return $row;
            });
        }
        return [
            'list'=>$list,
            'store'=>$storeInfo,
            'is_get'=>$isGet
        ];
    }
}
