<?php
declare(strict_types=1);
namespace App\Service\Report;

use App\Helper\Minio;

class ReportService
{
    public static function uploadFileToMinio(
        $filePath,
        $uploadPath,
        $fileName,
        $contentType = "application/vnd.ms-excel"
    ) {
        $fs = Minio::getMinio();
        if ($fs->fileExists($uploadPath)) {
            $fs->delete($uploadPath);
        }
        $stream = fopen($filePath, 'r+');
        $fs->writeStream($uploadPath, $stream, [
            'ContentType' => $contentType,
            'ContentDisposition' => "attachment; filename=\"$fileName\""
        ]);
        fclose($stream);
    }
}
