<?php


namespace App\Service\DynamicInformation;

use App\Constants\Lable;
use App\Repository\DynamicInformationRepository;
use Hyperf\Utils\Collection;

class DynamicInformationService
{
    public static function getList($page, $pageSize)
    {
        $resp = DynamicInformationRepository::getList($page, $pageSize);

        return [
                'list' => $resp->items(),
                'total' => $resp->total(),
                'lastPage' => $resp->lastPage(),
                'page' => $resp->currentPage(),
                'size' => $pageSize,
            ];
    }

    public static function getDetailById($id)
    {
        return DynamicInformationRepository::getDetailById($id);
    }

}
