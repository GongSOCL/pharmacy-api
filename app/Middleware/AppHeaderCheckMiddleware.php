<?php
declare(strict_types=1);
namespace App\Middleware;

use App\Constants\Auth;
use App\Constants\Role;
use App\Exception\AuthException;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\Context;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AppHeaderCheckMiddleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @inheritDoc
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $req = $this->container->get(RequestInterface::class);
        $role = (int) $req->header('app-role');
        if ($role && !in_array($role, [Role::ROLE_PATIENT, Role::ROLE_CLERK])) {
            throw new AuthException(Auth::ILLEGAL_REQUEST);
        }
        if (!$role) {
            //默认设置店员角色
            $role = Role::ROLE_PATIENT;
        }

        Context::set('role', $role);

        return $handler->handle($request);
    }
}
