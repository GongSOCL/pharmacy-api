<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/admin-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */

namespace App\Middleware;

use Hyperf\Utils\Context;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class CorsMiddleware implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $value = ['SIG','DNT','X-Mx-ReqToken','Keep-Alive','User-Agent','X-Requested-With',
            'If-Modified-Since','Cache-Control','Content-Type','Accept-Language','Origin,Accept-Encoding',
            'Authorization','Access-Control-Allow-Origin'];
        $response = Context::get(ResponseInterface::class);
        $response = $response->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, DELETE, PUT, PATCH')
            ->withHeader(
                'Access-Control-Allow-Headers',
                implode(',', $value)
            );

        Context::set(ResponseInterface::class, $response);

        if ($request->getMethod() == 'OPTIONS') {
            return $response;
        }

        return $handler->handle($request);
    }
}
