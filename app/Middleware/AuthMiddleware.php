<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Middleware;

use App\Constants\Auth;
use App\Constants\Status;
use App\Exception\AuthException;
use App\Kernel\Jwt\Jwt;
use App\Repository\UserRepository;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\Arr;
use Hyperf\Utils\Context;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AuthMiddleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @Inject()
     * @var Jwt
     */
    private $jwt;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $req = $this->container->get(RequestInterface::class);
        $token = $req->header('Authorization');
        if (!$token) {
            throw new AuthException(Auth::TOKEN_MISSING);
        }
        if (substr($token, 0, 7) == "Bearer ") {
            $token = substr($token, 7);
        }
        $payload = $this->jwt->decode($token);
        $userId = Arr::get($payload, 'sub');
        if (!$userId) {
            throw new AuthException(Auth::AUTHENTICATION_FAILED);
        }
        $user = UserRepository::getUserById($userId);
        if (!$user) {
            throw new AuthException(Auth::ILLEGAL_USER_ID);
        } elseif (!$user->isValid()) {
            throw new AuthException(Auth::USER_FORBIDDEN);
        }

        Context::set('user', $user);

        return $handler->handle($request);
    }
}
