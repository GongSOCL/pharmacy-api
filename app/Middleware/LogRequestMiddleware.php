<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Helper\Helper;
use Hyperf\Logger\LoggerFactory;
use Hyperf\Utils\ApplicationContext;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerInterface;

class LogRequestMiddleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $log = ApplicationContext::getContainer()
            ->get(LoggerFactory::class)
            ->get('default');
        $log->info("log_request", [
            'path' => $request->getUri()->getPath(),
            'method' => $request->getMethod(),
            'params' => $request->getParsedBody(),
            'header' => $request->getHeaders(),
            'query' => $request->getQueryParams()
        ]);
        return $handler->handle($request);
    }
}
