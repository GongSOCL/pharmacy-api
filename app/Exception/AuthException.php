<?php

declare(strict_types=1);

namespace App\Exception;

use App\Constants\Auth;
use Throwable;

class AuthException extends BusinessException
{
    public function __construct(int $code = 0, string $message = null, Throwable $previous = null)
    {
        if (is_null($message)) {
            $message = Auth::info($code);
        }

        parent::__construct($code, $message, $previous);
    }
}
