<?php

declare(strict_types=1);

namespace App\Exception;

use App\Constants\ErrorCode;
use App\Constants\ErrorWechat;
use App\Helper\Helper;
use Throwable;

class WechatException extends BusinessException
{
    public function __construct(int $code = 0, string $message = null, Throwable $previous = null)
    {
        if (is_null($message)) {
            $message = ErrorWechat::info($code);
        }

        parent::__construct($code, $message, $previous);
    }

    public $api;
    public $data;
    public $errCode;
    public $errMessage;

    public static function newFromCode($code, $message, $api, $data = []): WechatException
    {

        if (Helper::isOnline()) {
            $o = new self(ErrorWechat::WECHAT_BUSINESS_EXCEPTIN);
        } else {
            $o = new self($code, $message);
        }
        $o->api = $api;
        $o->data = $data;
        $o->errCode= $code;
        $o->errMessage = $message;

        return $o;
    }
}
