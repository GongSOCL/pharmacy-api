<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/admin-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Kernel\Jwt;

use Firebase\JWT\JWT as JwtSdk;
use Firebase\JWT\Key;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Di\Annotation\Inject;
use Youyao\Framework\Logger\LoggerApp;

class Jwt
{
    /**
     * @Inject
     * @var ConfigInterface
     */
    private $config;

    public function generate($userId)
    {
        $payload = [
            'iss' => 'otc-service',
            'aud' => 'otc-users',
            'iat' => time(),
            'exp' => time() + $this->config->get('jwt.token_expires'),
            'nbf' => time(),
            'sub' => $userId,
        ];
        try {
            return JwtSdk::encode(
                $payload,
                $this->config->get('jwt.key'),
                $this->config->get('jwt.alg')
            );
        } catch (\Exception $e) {
            LoggerApp::error($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    public function decode(string $token)
    {
        try {
            return (array) JwtSdk::decode(
                $token,
                new Key(
                    $this->config->get('jwt.key'),
                    $this->config->get('jwt.alg')
                )
            );
        } catch (\Exception $e) {
            LoggerApp::error($e->getMessage(), $e->getTrace());
            return false;
        }
    }
}
