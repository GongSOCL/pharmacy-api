<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */

namespace App\Request\User;

use Youyao\Framework\FormRequest;

class IdcardUploadRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'photo' => 'required|image'
        ];
    }

    public function messages(): array
    {
        return [
            'photo.required' => 'photo不能为空',
            'photo.image' => 'photo类型错误'
        ];
    }
}
