<?php
declare(strict_types=1);

namespace App\Request\User;

use Hyperf\Validation\Request\FormRequest;

class UpdateUserInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'nick_name' => 'nullable|string',
            'header_url' => 'nullable|string|url',
        ];
    }

    public function messages(): array
    {
        return [
            'nick_name.*' => '昵称不能为空',
            'header_url.*' => '头像不能为空',
        ];
    }
}
