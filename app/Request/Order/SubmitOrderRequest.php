<?php

declare(strict_types=1);

namespace App\Request\Order;

use Hyperf\Validation\Request\FormRequest;

class SubmitOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'notify_id' => 'required|integer|gt:0',
            'pics' => 'required|array',
            'pics.*' => 'required|integer|gt:0',
            'drugs' => 'required|array',
            'drugs.*.product_id' => 'required|integer|gt:0',
            'drugs.*.sku_id' => 'nullable|integer|gte:0',
            'drugs.*.total' => 'required|integer|gt:0',
            'drugs.*.cash' => 'required|numeric|gt:0',
            'coupon_nums' => 'nullable|array',
            'longitude' => 'nullable|numeric',
            'latitude' => 'nullable|numeric',
        ];
    }

    public function messages(): array
    {
        return [
            'notify_id.required' => '接单不能为空',
            'notify_id.integer' => '接单不能为空',
            'notify_id.gt' => '接单不能为空',
            'pics.required' => '请至少上传一张小票',
            'pics.array' => '请至少上传一张小票',
            'pics.*.integer' => '小票参数格式不正确',
            'pics.*.gt' => '小票参数格式不正确',
            'drugs.required' => '请至少填写一个购买产品',
            'drugs.array' => '请至少填写一个购买产品',
            'drugs.*.product_id.required' => '产品药品不能为空',
            'drugs.*.product_id.integer' => '药品id参数格式不正确',
            'drugs.*.product_id.gt' => '药品id参数格式不正确',
            'drugs.*.sku_id.integer' => '药品规格参数格式不正确',
            'drugs.*.sku_id.gt' => '药品规格参数格式不正确',
            'drugs.*.total.required' => '药品数量不能为空',
            'drugs.*.total.integer' => '药品数量参数格式不正确',
            'drugs.*.total.gt' => '药品数量参数格式不正确',
            'drugs.*.cash.required' => '药品金额不能为空',
            'drugs.*.cash.integer' => '药品金额参数格式不正确',
            'drugs.*.cash.gt' => '药品金额参数格式不正确',
            'coupon_nums.*'=> '优惠券入参格式不对',
            'longitude.*' => '经度数据格式不对',
            'latitude.*' => '维度数据格式不对',
        ];
    }
}
