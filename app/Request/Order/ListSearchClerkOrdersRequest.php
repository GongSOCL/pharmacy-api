<?php
declare(strict_types=1);

namespace App\Request\Order;

use Hyperf\Validation\Request\FormRequest;

class ListSearchClerkOrdersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'year_month' => 'nullable|string',
            'product_id' => 'nullable|integer|gt:0',
            'specs_id' => 'nullable|integer|gt:0',
            'current' => 'nullable|integer|gt:0',
            'limit' => 'nullable|integer|gt:0'
        ];
    }

    public function messages(): array
    {
        return [
            'year_month.*'=>'年月日期格式不对',
            'product_id.*'=>'商品id格式不对',
            'specs_id.*'=>'商品规格id格式不对',
            'current.integer' => '当前页参数格式错误',
            'current.gt' => '当前页参数格式错误',
            'limit.integer' => '返回条数参数格式错误',
            'limit.gt' => '返回条数参数格式错误',
        ];
    }
}
