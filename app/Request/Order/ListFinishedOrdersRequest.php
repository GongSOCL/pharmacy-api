<?php

declare(strict_types=1);

namespace App\Request\Order;

use Hyperf\Validation\Request\FormRequest;

class ListFinishedOrdersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'current' => 'nullable|integer|gt:0',
            'limit' => 'nullable|integer|gt:0'
        ];
    }

    public function messages(): array
    {
        return [
            'current.integer' => '当前页参数格式错误',
            'current.gt' => '当前页参数格式错误',
            'limit.integer' => '返回条数参数格式错误',
            'limit.gt' => '返回条数参数格式错误',
        ];
    }
}
