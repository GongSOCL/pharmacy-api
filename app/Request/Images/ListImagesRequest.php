<?php

declare(strict_types=1);

namespace App\Request\Images;

use Hyperf\Validation\Request\FormRequest;

class ListImagesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'status' => 'nullable|integer|gte:0',
            'current' => 'nullable|integer|gt:0',
            'limit' => 'nullable|integer|gt:0'
        ];
    }
}
