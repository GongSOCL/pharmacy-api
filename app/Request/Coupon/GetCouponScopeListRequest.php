<?php


namespace App\Request\Coupon;

use Hyperf\Validation\Request\FormRequest;

class GetCouponScopeListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'coupon_id' => 'required|integer|gt:0',
            'page'=>'required|integer|gt:0',
            'page_size'=>'nullable|integer|gt:0',
        ];
    }

    public function messages(): array
    {
        return [
            'coupon_id.*' => '礼券ID不能为空',
            'page.*' => '页码不能为空',
            'page_size.*' => '页面大小不能为空',
        ];
    }
}
