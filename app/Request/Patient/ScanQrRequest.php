<?php
declare(strict_types=1);

namespace App\Request\Patient;

use Hyperf\Validation\Request\FormRequest;

class ScanQrRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'token_id' => 'required|integer',
            'longitude' => 'nullable|numeric',
            'latitude' => 'nullable|numeric',
        ];
    }

    public function messages(): array
    {
        return [
            'token_id.*' => '扫码信息不能为空',
            'longitude.*' => '经度数据格式不对',
            'latitude.*' => '维度数据格式不对',
        ];
    }
}
