<?php

declare(strict_types=1);

namespace App\Request\Topic;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class ParticipateTopicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'tp' => 'required|integer|gt:0',
            'comment' => 'required|string|max:100',
            'is_published' => [
                'required',
                'integer',
                Rule::in([0, 1])
            ],
            'task_token' => 'nullable|string'
        ];
    }
}
