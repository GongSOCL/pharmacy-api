<?php

declare(strict_types=1);

namespace App\Request\Topic;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class ListTopicImagesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'order' => [
                'nullable',
                'integer',
                Rule::in([1,2])
            ],
            'current' => 'nullable|integer|gt:0',
            'limit' => 'nullable|integer|gt:0'
        ];
    }
}
