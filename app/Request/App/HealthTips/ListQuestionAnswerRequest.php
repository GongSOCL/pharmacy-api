<?php

declare(strict_types=1);

namespace App\Request\App\HealthTips;

use Hyperf\Validation\Request\FormRequest;

class ListQuestionAnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'keywords' => 'nullable|string|max:32'
        ];
    }
}
