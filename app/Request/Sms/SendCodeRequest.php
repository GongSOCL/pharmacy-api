<?php
declare(strict_types=1);

namespace App\Request\Sms;

use Hyperf\Validation\Request\FormRequest;

class SendCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'phone' => 'required|regex:/^[1]([3-9])[0-9]{9}$/',
        ];
    }

    public function messages(): array
    {
        return [
            'phone.required' => '手机号不能为空',
            'phone.regex' => '手机号格式不对',
        ];
    }
}
