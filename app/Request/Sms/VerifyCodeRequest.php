<?php
declare(strict_types=1);

namespace App\Request\Sms;

use Hyperf\Validation\Request\FormRequest;

class VerifyCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'phone' => 'required|regex:/^[1]([3-9])[0-9]{9}$/',
            'verify_code' => 'required|string|min:6|max:6',
        ];
    }

    public function messages(): array
    {
        return [
            'phone.required' => '手机号不能为空',
            'phone.regex' => '手机号格式不对',
            'verify_code.required' => '手机号格式不对',
            'verify_code.string' => '验证码必须为字符串',
            'verify_code.min' => '验证码必须6位',
            'verify_code.max' => '验证码必须6位',
        ];
    }
}
