<?php
declare(strict_types=1);

namespace App\Request\Clerk;

use Hyperf\Validation\Request\FormRequest;

class CancelNotifyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'notify_id' => 'required|integer|gt:0',
        ];
    }

    public function messages(): array
    {
        return [
            'notify_id.*' => '接单ID不能为空',
        ];
    }
}
