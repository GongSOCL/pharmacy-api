<?php
declare(strict_types=1);

namespace App\Request\Clerk;

use Hyperf\Validation\Request\FormRequest;

class ApplyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'real_name' => 'required|string',
            'branch_store_id' => 'required|integer|gt:0',
            'phone' => 'required|regex:/^[1]([3-9])[0-9]{9}$/',
            'role' => 'required|integer|in:1,2',
            'position' => 'required|integer|in:1,2',
        ];
    }

    public function messages(): array
    {
        return [
            'real_name.*' => '姓名不能为空',
            'branch_store_id.*' => '药店ID不能为空',
            'phone.required' => '手机号不能为空',
            'phone.regex' => '手机号不能为空',
            'role.*' => '店员角色不能为空或不在范围',
            'position.*' => '职位不能为空或不在范围',
        ];
    }
}
