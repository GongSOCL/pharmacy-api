<?php
declare(strict_types=1);

namespace App\Request\Clerk;

use Hyperf\Validation\Request\FormRequest;

class NotifyListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'store_id' => 'required|integer|gt:0',
            'status' => 'required|integer|in:0,1,2',
            'page'=>'required|integer|gt:0',
        ];
    }

    public function messages(): array
    {
        return [
            'store_id.*' => '药店ID不能为空',
            'status.required' => 'Tag不能为空',
            'status.integer' => 'Tag类型不对',
            'status.in' => 'Tag不在范围内',
            'page.*' => '页码不能为空',
        ];
    }
}
