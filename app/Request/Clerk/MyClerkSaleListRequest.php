<?php


namespace App\Request\Clerk;

use Hyperf\Validation\Request\FormRequest;

class MyClerkSaleListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'clerk_id' => 'required|integer|gt:0',
            'year_month' => 'nullable|string',
            'product_id' => 'nullable|integer|gt:0',
            'specs_id' => 'nullable|integer|gt:0',
            'page' => 'nullable|integer|gt:0',
            'page_size' => 'nullable|integer|gt:0'
        ];
    }

    public function messages(): array
    {
        return [
            'clerk_id.*' => '店员ID不能为空',
            'year_month.*' => 'year_month数据格式不对',
            'product_id.*' => 'product_id数据格式不对',
            'specs_id.*' => 'specs_id数据格式不对',
            'page.*' => 'page数据格式不对',
            'page_size.*' => 'page_size数据格式不对',
        ];
    }
}
