<?php
declare(strict_types=1);

namespace App\Request\Store;

use Hyperf\Validation\Request\FormRequest;

class ScanStoreListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'token_id' => 'required|integer',
            'page' => 'nullable|integer|gt:0',
            'page_size' => 'nullable|integer|gt:0',
        ];
    }

    public function messages(): array
    {
        return [
            'token_id.required' => '扫码信息不能为空',
            'token_id.string' => '扫码信息不能为空',
            'page.*'=> '数据格式不对',
            'page_size.*'=> '数据格式不对',
        ];
    }
}
