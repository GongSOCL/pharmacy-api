<?php


namespace App\Request\Store;

use Hyperf\Validation\Request\FormRequest;

class GetCurrentGeoPosAndDepartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'longitude' => 'required|numeric',
            'latitude' => 'required|numeric',
            'depart_id' => 'nullable|integer',

        ];
    }

    public function messages(): array
    {
        return [
            'longitude.*' => '经度不能为空',
            'latitude.*' => '经度不能为空',
            'depart_id.*'=> 'depart_id格式不对',
        ];
    }
}
