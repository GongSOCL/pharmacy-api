<?php
declare(strict_types=1);

namespace App\Request\Store;

use Hyperf\Validation\Request\FormRequest;

class BranchStoreListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'main_store_id' => 'required|integer|gt:0',
            'keywords' => 'nullable|string|max:32'
        ];
    }

    public function messages(): array
    {
        return [
            'main_store_id.required' => '连锁店ID不能为空',
            'main_store_id.integer' => '连锁店ID不能为空',
            'main_store_id.gt' => '连锁店ID不能为空',
            'keywords.string' => '搜索关键字格式不正确',
            'keywords.max' => '搜索关键字最多允许32个字'
        ];
    }
}
