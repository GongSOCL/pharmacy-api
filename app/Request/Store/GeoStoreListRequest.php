<?php


namespace App\Request\Store;

use Hyperf\Validation\Request\FormRequest;

class GeoStoreListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'longitude' => 'required|numeric',
            'latitude' => 'required|numeric',
            'is_scan' => 'required|integer|in:0,1',
            'depart_id' => 'nullable|integer',
            'city_id' => 'nullable|integer|gt:0',
            'page' => 'nullable|integer|gt:0',
            'page_size' => 'nullable|integer|gt:0',
        ];
    }

    public function messages(): array
    {
        return [
            'longitude.*' => '经度不能为空',
            'latitude.*' => '经度不能为空',
            'is_scan.*'=> 'is_scan扫码标记不能为空',
            'depart_id.*'=> 'depart_id格式不对',
            'city_id.*'=> 'city_id城市ID不能为空',
            'page.*'=> 'page的格式不对',
            'page_size.*'=> 'page_size的格式不对',
        ];
    }
}
