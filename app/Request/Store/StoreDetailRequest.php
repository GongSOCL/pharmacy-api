<?php
declare(strict_types=1);

namespace App\Request\Store;

use Hyperf\Validation\Request\FormRequest;

class StoreDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'store_id' => 'required|integer|gt:0',
        ];
    }

    public function messages(): array
    {
        return [
            'store_id.*' => '药店ID不能为空',
        ];
    }
}
