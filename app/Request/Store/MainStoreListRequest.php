<?php
declare(strict_types=1);

namespace App\Request\Store;

use Hyperf\Validation\Request\FormRequest;

class MainStoreListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'city_id' => 'required|integer|gt:0',
            'keywords' => 'nullable|string|max:32'
        ];
    }

    public function messages(): array
    {
        return [
            'city_id.required' => '城市ID不能为空',
            'city_id.integer' => '城市ID不能为空',
            'city_id.gt' => '城市ID不能为空',
            'keywords.string' => '搜索关键字格式不正确',
            'keywords.max' => '搜索关键字最多允许32个字符'
        ];
    }
}
