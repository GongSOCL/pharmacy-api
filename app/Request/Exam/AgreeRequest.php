<?php

declare(strict_types=1);

namespace App\Request\Exam;

use Hyperf\Validation\Request\FormRequest;

class AgreeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
                'sign_img' => 'required|string'
        ];
    }

    public function messages(): array
    {
        return [
            'sign_img.required' => '签名图片不能为空',
            'sign_img.string' => '签名图片格式不正确'
        ];
    }
}
