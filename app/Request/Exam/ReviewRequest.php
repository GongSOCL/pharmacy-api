<?php

declare(strict_types=1);

namespace App\Request\Exam;

use Hyperf\Validation\Request\FormRequest;

class ReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
                'request_id' => 'string'
        ];
    }

    public function messages(): array
    {
        return [];
    }
}
