<?php

declare(strict_types=1);

namespace App\Request\Exam;

use Hyperf\Validation\Request\FormRequest;

class GetQuestion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'question_id' => 'required|integer|gt:0',
            'request_id' => 'required|string'
        ];
    }

    public function messages(): array
    {
        return [
            'question_id.required' => '试题id不能为空',
            'question_id.integer' => '试题不存在或格式不正确',
            'question_id.gt' => '试题不存在或格式不正确'
        ];
    }
}
