<?php

declare(strict_types=1);

namespace App\Request\DynamicInformation;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class DynamicInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'page' => 'required|integer|gt:0',
        ];
    }

    public function messages(): array
    {
        return [
            'page.required' => '页码不能为空',
        ];
    }
}
