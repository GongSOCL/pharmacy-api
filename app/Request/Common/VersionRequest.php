<?php

declare(strict_types=1);

namespace App\Request\Common;

use Hyperf\Validation\Request\FormRequest;

class VersionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'version' => 'required|string'
        ];
    }

    public function messages(): array
    {
        return [
            'version.required' => '版本号不能为空',
            'version.string' => '版本号格式不正确'
        ];
    }
}
