<?php

declare(strict_types=1);

namespace App\Request\Questionnaire;

use Hyperf\Validation\Request\FormRequest;

class AnswerQuestionnaireQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'paper_id' => 'required|integer|gt:0',
            'request_id' => 'required|string',
            'options' => 'nullable|array',
            'options.*' => 'integer|gt:0',
            'content' => 'nullable|string'
        ];
    }

    public function messages(): array
    {
        return [
            'paper_id.required' => '问卷不能为空',
            'paper_id.integer' => '问卷格式不正确',
            'paper_id.gt' => '问卷格式不正确',
            'request_id.required' => '非法请求',
            'request_id.string' => '非法请求',
            'options.array' => '选择格式不正确',
            'options.*.integer' => '选择格式不正确',
            'options.*.gt' => '选项格式不正确',
            'content.string' => '输入答案格式不正确'
        ];
    }
}
