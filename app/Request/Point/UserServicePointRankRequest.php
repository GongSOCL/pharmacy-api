<?php

declare(strict_types=1);

namespace App\Request\Point;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class UserServicePointRankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'type' => [
                'required',
                'integer',
                Rule::in([1, 2, 3, 4])
            ],
            'limit' => 'nullable|integer|gt:0'
        ];
    }
}
