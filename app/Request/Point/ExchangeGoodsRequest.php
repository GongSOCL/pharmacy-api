<?php

declare(strict_types=1);

namespace App\Request\Point;

use Hyperf\Validation\Request\FormRequest;

class ExchangeGoodsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'num' => 'required|integer|gt:0'
        ];
    }

    public function messages(): array
    {
        return [
            'num.required' => '兑换份数不能为空',
            'num.integer' => '兑换份数格式不正确',
            'num.gt' => '至少需要兑换一份'
        ];
    }
}
