<?php

declare(strict_types=1);

namespace App\Request\Point;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class GetGoodsListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'current' => 'nullable|integer|gt:0',
            'limit' => 'nullable|integer|gt:0',
            'type' => [
                'nullable',
                'integer',
                Rule::in([1, 2])
            ],
            'group_id' => 'nullable|integer|gte:0'
        ];
    }

    public function messages(): array
    {
        return [
            'current.integer' => '当前页码格式不正确',
            'current.gt' => '当前页码格式错误',
            'limit.integer' => '每页返回数量格式不正确',
            'limit.gt' => '每页返回数量格式不正确',
            'type.integer' => '积分类型格式不正确',
            'type.in' => '积分类型不正确或不存在'
        ];
    }
}
