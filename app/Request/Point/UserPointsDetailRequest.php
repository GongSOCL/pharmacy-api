<?php
declare(strict_types=1);

namespace App\Request\Point;

use Hyperf\Validation\Request\FormRequest;

class UserPointsDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'page' => 'required|int|gt:0',
            'page_size' => 'required|int|gt:0',
            'point_type' => 'required|int|in:1,2',
            'group_id' => 'nullable|integer|gte:0'
        ];
    }

    public function messages(): array
    {
        return [
            'page.*' => '当前页码格式不对',
            'page_size.*' => '分页大小格式不对',
            'point_type.*' => '积分类型范围不对',
        ];
    }
}
