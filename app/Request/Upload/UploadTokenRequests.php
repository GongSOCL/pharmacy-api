<?php

declare(strict_types=1);

namespace App\Request\Upload;

use Grpc\Upload\BUSINESS_TYPE;
use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class UploadTokenRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'type' => [
                'required',
                Rule::in([
                    BUSINESS_TYPE::BUSINESS_TYPE_USER,
                    BUSINESS_TYPE::BUSINESS_TYPE_STATIC,
                    BUSINESS_TYPE::BUSINESS_TYPE_VIDEO
                ])
            ],
            'business' => 'required|string',
            'channel' => [
                'nullable',
                'integer',
                Rule::in([1, 3])
            ]
        ];
    }

    public function messages(): array
    {
        return [
            'type.required' => '业务类型不能为空',
            'type.in' => '业务类型不正确',
            'business.required' => '业务描述不能为空',
            'business.string' => '业务描述格式不正确'
        ];
    }
}
