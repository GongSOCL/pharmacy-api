<?php

declare(strict_types=1);

namespace App\Request\Upload;

use Hyperf\Validation\Request\FormRequest;

class RecordUploadRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'bucket' => 'required|string',
            'key' => 'required|string|max:256',
            'name' => 'required|string|max:64',
            'url' => 'required|string|max:256'
        ];
    }

    public function messages(): array
    {
        return [
            'bucket.required' => '存储位置不能为空',
            'key.required' => '存储key不能为空',
            'key.string' => '存储key格式不正确',
            'key.max' => '存储key超长，最长允许128个字符',
            'name.required' => '文件名不能为空',
            'name.string' => '文件名格式不正确',
            'name.max' => '文件名超长，最长允许128个字符',
            'url.required' => '文件url不能为空',
            'url.string' => '文件url格式不正确',
            'url.max' => '文件url超长，最长允许128个字符',
        ];
    }
}
