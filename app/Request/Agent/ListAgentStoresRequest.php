<?php

declare(strict_types=1);

namespace App\Request\Agent;

use Hyperf\Validation\Request\FormRequest;

class ListAgentStoresRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'token' => 'required|string',
            'keywords' => 'nullable|string|max:32'
        ];
    }
}
