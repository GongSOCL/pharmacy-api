<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\UserPatient;
use App\Model\Pharmacy\UserPatientDoctor;
use Carbon\Carbon;

class UserPatientDoctorRepository
{
    public static function addPatientDoctorRel($yyDockerId, $patientUserId, $departId = 0)
    {
        $data = [
            'yy_doctor_id'=>$yyDockerId,
            'patient_user_id'=>$patientUserId,
            'depart_id'=>$departId,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s'),
        ];
        return UserPatientDoctor::query()->insert($data);
    }

    public static function updateDoctorDepart($doctorId, $departId): int
    {
        return UserPatientDoctor::query()
            ->where('yy_doctor_id', $doctorId)
            ->where('is_delete', Delete::UNDELETED)
            ->update([
                'depart_id' => $departId,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function existPatientDoctor($patientId, $doctorId): bool
    {
        return UserPatientDoctor::query()
            ->where('yy_doctor_id', $doctorId)
            ->where('patient_user_id', $patientId)
            ->where('is_delete', Delete::UNDELETED)
            ->limit(1)
            ->exists();
    }

    /**
     * @param $userId
     * @return \Hyperf\Database\Model\Model|\Hyperf\Database\Query\Builder|object|null
     */
    public static function getUserClerkByUserId($userId)
    {
        return UserPatient::select('id')
            ->where('user_id', $userId)
            ->first();
    }

    public static function getDoctorFirstScan($doctorId): ?UserPatientDoctor
    {
        return UserPatientDoctor::query()
            ->where('yy_doctor_id', $doctorId)
            ->where('is_delete', Delete::UNDELETED)
            ->orderBy('id')
            ->first();
    }
}
