<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Constants\Status;
use App\Model\Pharmacy\DynamicInformation;

class DynamicInformationRepository
{
    public static function getList($page, $pageSize)
    {
        return DynamicInformation::query()
            ->select(['id', 'cover_url', 'title', 'content', 'lable_id', 'created_at'])
            ->where('status', Status::REGULAR)
            ->where('is_deleted', Delete::UNDELETED)
            ->orderBy('created_at', 'desc')
            ->paginate($pageSize, ['*'], '', $page);
    }

    public static function getDetailById($id)
    {
        return DynamicInformation::query()
            ->select(['id', 'title', 'content', 'lable_id', 'created_at'])
            ->where('status', Status::REGULAR)
            ->where('is_deleted', Delete::UNDELETED)
            ->where('id', $id)
            ->first();
    }
}
