<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\ShareTokenSchema;
use Carbon\Carbon;

class ShareTokenSchemaRepository
{
    public static function getBizSchema($bizType, $bizId, $size = 430): ?ShareTokenSchema
    {
        return ShareTokenSchema::query()
            ->where('biz_type', $bizType)
            ->where('biz_id', $bizId)
            ->where('size', $size)
            ->where('is_delete', Delete::UNDELETED)
            ->first();
    }

    public static function exists($bizType, $bizId, $size = 430): bool
    {
        return ShareTokenSchema::query()
            ->where('biz_type', $bizType)
            ->where('biz_id', $bizId)
            ->where('size', $size)
            ->where('is_delete', Delete::UNDELETED)
            ->exists();
    }

    public static function setUpload(ShareTokenSchema $schema, $uploadId)
    {
        $schema->schema_id = $uploadId;
        $schema->updated_at = Carbon::now();
        $schema->save();
    }

    public static function newSchema($bizType, $bizId, string $token, $size = 430): ShareTokenSchema
    {
        $o = new ShareTokenSchema();
        $o->token = $token;
        $o->schema_id = 0;
        $o->biz_id = $bizId;
        $o->biz_type = $bizType;
        $o->is_delete = Delete::UNDELETED;
        $o->size = $size;
        $o->created_at = $o->updated_at = Carbon::now();

        $o->save();
        return $o;
    }

    public static function getById($shareId): ?ShareTokenSchema
    {
        return ShareTokenSchema::query()
            ->where('id', $shareId)
            ->where('is_delete', Delete::UNDELETED)
            ->first();
    }

    public static function getInfoBySchemaId($schemaId, $size = 430): ?ShareTokenSchema
    {
        return ShareTokenSchema::query()
            ->where('schema_id', $schemaId)
            ->where('size', $size)
            ->first();
    }
}
