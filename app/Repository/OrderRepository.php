<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\Order;
use App\Model\Pharmacy\PurchaseNotify;
use App\Model\Pharmacy\UserClerk;
use Carbon\Carbon;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\Database\Query\Builder;
use Hyperf\DbConnection\Db;

class OrderRepository
{

    public static function addOrder(
        UserClerk $user,
        PurchaseNotify $notify,
        $orderNo,
        $amount,
        $num,
        $lon = 0.0,
        $lat = 0.0
    ): Order {
        $o = new Order();
        $o->notify_id = $notify->id;
        $o->order_num = $orderNo;
        $o->clerk_user_id = $user->user_id;
        $o->patient_user_id = $notify->patient_user_id;
        $o->store_id = $notify->store_id;
        $o->amount = $amount;
        $o->num = $num;
        $o->status = Order::STATUS_PAIED;
        $o->remark = "";
        $o->longitude = $lon;
        $o->latitude = $lat;
        $o->is_delete = Delete::UNDELETED;
        $o->created_at = $o->updated_at = Carbon::now();
        $o->save();

        return $o;
    }


    public static function listClerkOrders(UserClerk $clerk, $current = 1, $limit = 10): LengthAwarePaginatorInterface
    {
        return Order::query()
            ->where('store_id', $clerk->store_id)
            ->where('clerk_user_id', $clerk->user_id)
            ->where('is_delete', Delete::UNDELETED)
            ->orderByDesc('id')
            ->paginate($limit, ['*'], '', $current);
    }

    public static function listPatientOrders($userId, $current = 1, $limit = 10)
    {
        return Order::query()
            ->where('patient_user_id', $userId)
            ->where('is_delete', Delete::UNDELETED)
            ->orderByDesc('id')
            ->paginate($limit, ['*'], '', $current);
    }

    public static function searchClerkOrderList(
        UserClerk $clerk,
        $month,
        $productId,
        $specsId,
        $current = 1,
        $limit = 10
    ) {

        return Order::query()
            ->where('store_id', $clerk->store_id)
            ->where('clerk_user_id', $clerk->user_id)
            ->where('is_delete', Delete::UNDELETED)
            ->when($month, function ($query) use ($month) {
                /** @var Builder $query */
                 [$y, $m] = explode('/', $month);
                 $startDate = Carbon::create((int)$y, (int)$m)->startOfMonth()->format('Y-m-d');
                 $endDate = Carbon::create((int)$y, (int)$m)->lastOfMonth()->format('Y-m-d').' 23:59:59';
                 $query->whereBetween('created_at', [$startDate, $endDate]);
            })
            ->when($productId, function ($query) use ($productId) {
                /** @var Builder $query */
                $query->whereIn('id', function ($query) use ($productId) {
                    /** @var Builder $query */
                    $query->from('order_detail')->select(['order_id'])
                        ->where('yy_product_id', $productId);
                });
            })
            ->when($specsId, function ($query) use ($specsId) {
                /** @var Builder $query */
                $query->whereIn('id', function ($query) use ($specsId) {
                    /** @var Builder $query */
                    $query->from('order_detail')->select(['order_id'])
                        ->where('yy_sku_id', $specsId);
                });
            })
            ->orderByDesc('id')
            ->paginate($limit, ['*'], '', $current);
    }

    public static function totalOrderDataByClerk(UserClerk $clerk, $month, $productId, $specsId)
    {
        return Order::query()
            ->from('order as a')
            ->select([
                Db::raw('MAX(a.created_at) as created_at'),
                Db::raw('IFNULL(count(distinct a.id), 0) as num'),
                Db::raw('IFNULL(sum(b.amount), 0) as amount')
            ])
            ->join('order_detail as b', 'a.id', '=', 'b.order_id')
            ->where('a.store_id', $clerk->store_id)
            ->where('a.clerk_user_id', $clerk->user_id)
            ->where('a.is_delete', Delete::UNDELETED)
            ->when($month, function ($query) use ($month) {
                /** @var Builder $query */
                [$y, $m] = explode('/', $month);
                $startDate = Carbon::create((int)$y, (int)$m)->startOfMonth()->format('Y-m-d');
                $endDate = Carbon::create((int)$y, (int)$m)->lastOfMonth()->format('Y-m-d').' 23:59:59';
                $query->whereBetween('a.created_at', [$startDate, $endDate]);
            })
            ->when($productId, function ($query) use ($productId) {
                /** @var Builder $query */
                $query->where('b.yy_product_id', $productId);
            })
            ->when($specsId, function ($query) use ($specsId) {
                /** @var Builder $query */
                $query->where('b.yy_sku_id', $specsId);
            })->first();
    }

    public static function totalOrderDataByClerkIds($storeId, $clerkIds, $month, $productId, $specsId)
    {
        return Order::query()
            ->from('order as a')
            ->select(['clerk_user_id',
                    Db::raw('MAX(a.created_at) as created_at'),
                    Db::raw('IFNULL(count(distinct a.id), 0) as num'),
                    Db::raw('IFNULL(sum(b.amount), 0) as amount')
            ])
            ->join('order_detail as b', 'a.id', '=', 'b.order_id')
            ->where('a.store_id', $storeId)
            ->whereIn('a.clerk_user_id', $clerkIds)
            ->where('a.is_delete', Delete::UNDELETED)
            ->when($month, function ($query) use ($month) {
                /** @var Builder $query */
                [$y, $m] = explode('/', $month);
                $startDate = Carbon::create((int)$y, (int)$m)->startOfMonth()->format('Y-m-d');
                $endDate = Carbon::create((int)$y, (int)$m)->lastOfMonth()->format('Y-m-d').' 23:59:59';
                $query->whereBetween('a.created_at', [$startDate, $endDate]);
            })
            ->when($productId, function ($query) use ($productId) {
                /** @var Builder $query */
                $query->where('b.yy_product_id', $productId);
            })
            ->when($specsId, function ($query) use ($specsId) {
                /** @var Builder $query */
                $query->where('b.yy_sku_id', $specsId);
            })->groupBy(['clerk_user_id'])
            ->get();
    }
}
