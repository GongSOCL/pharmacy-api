<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\CouponConstant;
use App\Constants\Delete;
use App\Model\Pharmacy\Coupon;
use Hyperf\Database\Query\Builder;
use Hyperf\DbConnection\Db;

class CouponRepository
{

    const COUPON_FIELDS = [
        'id',
        'title',
        'yy_product_id',
        'policy_type',
        'policy_val',
        'deadline_type',
        'deadline_val',
        'scope',
        'is_exchange',
        'is_limit',
        'total_num',
        'receive_num',
        'status',
    ];

    public static function getCouponListByIds($couponIds)
    {
        return Coupon::query()
            ->select(self::COUPON_FIELDS)
            ->whereIn('id', $couponIds)
            ->get();
    }

    public static function getCouponInfoById($couponId):?Coupon
    {
        return Coupon::query()
            ->select(self::COUPON_FIELDS)
            ->where('id', $couponId)
            ->first();
    }

    public static function incCouponReceiveNum($couponId)
    {
        return Coupon::query()
            ->where('id', $couponId)
            ->where(function ($query) {
                /** @var Builder $query */
                $query->where('is_limit', CouponConstant::COUPON_LIMIT_NO)
                    ->orWhere(function ($query) {
                        /** @var Builder $query */
                        $query->where('is_limit', CouponConstant::COUPON_LIMIT_YES)
                            ->whereRaw('total_num > receive_num');
                    });
            })
            ->increment('receive_num');
    }

    public static function storeHasPolicy($mainStoreId, $storeId, array $yyProductIds)
    {
        if (count($yyProductIds) > 0 && self::firstCouponBuilder($yyProductIds)->exists()) {
            return true;
        }
        if (self::secondCouponBuilder($mainStoreId, $yyProductIds)->exists()) {
            return true;
        }
        if (self::thirdCouponBuilder($storeId, $yyProductIds)->exists()) {
            return true;
        }
        return false;
    }

    public static function getProductCouponKv(array $yyProductIds)
    {
        $firstBuilder =  self::firstCouponBuilder($yyProductIds);
        return $firstBuilder->get();
    }

    public static function getMainStoreCouponKv(array $mainStoreIds)
    {
        $defineSelectFields = [
            'b.main_store_id',
            'a.id',
            'a.title',
            'a.yy_product_id',
            'a.policy_type',
            'a.policy_val',
            'a.deadline_type',
            'a.deadline_val',
        ];
        $secondBuilder = self::secondCouponBuilder(
            $mainStoreIds,
            [],
            $defineSelectFields
        );
        return $secondBuilder->get();
    }

    public static function getStoreCouponKv(array $storeIds)
    {
        $defineSelectFields = [
            'b.store_id',
            'a.id',
            'a.title',
            'a.yy_product_id',
            'a.policy_type',
            'a.policy_val',
            'a.deadline_type',
            'a.deadline_val',
        ];
        $secondBuilder = self::thirdCouponBuilder(
            $storeIds,
            [],
            $defineSelectFields
        );
        return $secondBuilder->get();
    }


    public static function getStoreCouponList($mainStoreId, $storeId, $yyProductIds)
    {
        $bindings = self::getStoreBuilder($mainStoreId, $storeId, $yyProductIds)->getQuery();
        $sql = self::getStoreBuilder($mainStoreId, $storeId, $yyProductIds)->toSql();
        return Coupon::query()
            ->mergeBindings($bindings)
            ->from(Db::raw("($sql) as a"))
            ->orderByDesc('a.id')
            ->get();
    }

    private static function getStoreBuilder($mainStoreId, $storeId, $yyProductIds)
    {
        $firstBuilder =  self::firstCouponBuilder($yyProductIds);
        $secondBuilder = self::secondCouponBuilder($mainStoreId, $yyProductIds);
        $thirdBuilder = self::thirdCouponBuilder($storeId, $yyProductIds);
        return $firstBuilder->union($secondBuilder)->union($thirdBuilder);
    }

    private static function firstCouponBuilder($yyProductIds)
    {
        $time = date('Y-m-d H:i:s');
        $selectFields = [
            'a.id',
            'a.title',
            'a.yy_product_id',
            'a.policy_type',
            'a.policy_val',
            'a.deadline_type',
            'a.deadline_val',
        ];
        return Coupon::query()->from('coupon as a')
            ->select($selectFields)
            ->where('a.scope', CouponConstant::COUPON_SCOPE_ALL)
            ->where('a.is_exchange', CouponConstant::COUPON_EXCHANGE_NO)
            ->where('a.is_delete', Delete::UNDELETED)
            ->where('a.status', CouponConstant::COUPON_STATUS_YES)
            ->when($yyProductIds, function ($query) use ($yyProductIds) {
                /** @var Builder $query */
                $query->whereIn('a.yy_product_id', $yyProductIds);
            })
            ->where(function ($query) use ($time) {
                /** @var Builder $query */
                $query->where('a.deadline_type', CouponConstant::COUPON_DEADLINE_TYPE_DOWN)
                    ->orWhere(function ($query) use ($time) {
                        /** @var Builder $query */
                        $query->where('a.deadline_type', CouponConstant::COUPON_DEADLINE_TYPE_SPEC)
                            ->where('a.deadline_val', '>=', $time);
                    });
            })->where(function ($query) {
                /** @var Builder $query */
                $query->where('a.is_limit', CouponConstant::COUPON_LIMIT_NO)
                    ->orWhere(function ($query) {
                        /** @var Builder $query */
                        $query->where('a.is_limit', CouponConstant::COUPON_LIMIT_YES)
                            ->whereRaw('a.total_num > a.receive_num');
                    });
            });
    }


    private static function secondCouponBuilder($mainStoreId, $yyProductIds, $defineSelectFields = [])
    {
        $time = date('Y-m-d H:i:s');
        if (empty($defineSelectFields)) {
            $selectFields = [
                'a.id',
                'a.title',
                'a.yy_product_id',
                'a.policy_type',
                'a.policy_val',
                'a.deadline_type',
                'a.deadline_val',
            ];
        } else {
            $selectFields = $defineSelectFields;
        }

        return Coupon::query()->from('coupon as a')
            ->select($selectFields)
            ->join('coupon_main_store as b', 'a.id', '=', 'b.coupon_id')
            ->when(is_array($mainStoreId), function ($query) use ($mainStoreId) {
                $query ->whereIn('b.main_store_id', $mainStoreId);
            })
            ->when(is_numeric($mainStoreId), function ($query) use ($mainStoreId) {
                $query ->where('b.main_store_id', $mainStoreId);
            })
            ->where('a.scope', CouponConstant::COUPON_SCOPE_MAIN)
            ->where('a.is_exchange', CouponConstant::COUPON_EXCHANGE_NO)
            ->where('a.is_delete', Delete::UNDELETED)
            ->where('a.status', CouponConstant::COUPON_STATUS_YES)
            ->when($yyProductIds, function ($query) use ($yyProductIds) {
                /** @var Builder $query */
                $query->whereIn('a.yy_product_id', $yyProductIds);
            })
            ->where(function ($query) use ($time) {
                /** @var Builder $query */
                $query->where('a.deadline_type', CouponConstant::COUPON_DEADLINE_TYPE_DOWN)
                    ->orWhere(function ($query) use ($time) {
                        /** @var Builder $query */
                        $query->where('a.deadline_type', CouponConstant::COUPON_DEADLINE_TYPE_SPEC)
                            ->where('a.deadline_val', '>=', $time);
                    });
            })->where(function ($query) {
                /** @var Builder $query */
                $query->where('a.is_limit', CouponConstant::COUPON_LIMIT_NO)
                    ->orWhere(function ($query) {
                        /** @var Builder $query */
                        $query->where('a.is_limit', CouponConstant::COUPON_LIMIT_YES)
                            ->whereRaw('a.total_num > a.receive_num');
                    });
            });
    }

    private static function thirdCouponBuilder($storeId, $yyProductIds, $defineSelectFields = [])
    {
        $time = date('Y-m-d H:i:s');
        if (empty($defineSelectFields)) {
            $selectFields = [
                'a.id',
                'a.title',
                'a.yy_product_id',
                'a.policy_type',
                'a.policy_val',
                'a.deadline_type',
                'a.deadline_val',
            ];
        } else {
            $selectFields = $defineSelectFields;
        }
        return Coupon::query()->from('coupon as a')
            ->select($selectFields)
            ->join('coupon_branch_store as b', 'a.id', '=', 'b.coupon_id')
            ->when(is_array($storeId), function ($query) use ($storeId) {
                $query ->whereIn('b.store_id', $storeId);
            })
            ->when(is_numeric($storeId), function ($query) use ($storeId) {
                $query ->where('b.store_id', $storeId);
            })
            ->where('a.scope', CouponConstant::COUPON_SCOPE_STORE)
            ->where('a.is_exchange', CouponConstant::COUPON_EXCHANGE_NO)
            ->where('a.is_delete', Delete::UNDELETED)
            ->where('a.status', CouponConstant::COUPON_STATUS_YES)
            ->when($yyProductIds, function ($query) use ($yyProductIds) {
                /** @var Builder $query */
                $query->whereIn('a.yy_product_id', $yyProductIds);
            })
            ->where(function ($query) use ($time) {
                /** @var Builder $query */
                $query->where('a.deadline_type', CouponConstant::COUPON_DEADLINE_TYPE_DOWN)
                    ->orWhere(function ($query) use ($time) {
                        /** @var Builder $query */
                        $query->where('a.deadline_type', CouponConstant::COUPON_DEADLINE_TYPE_SPEC)
                            ->where('a.deadline_val', '>=', $time);
                    });
            })->where(function ($query) {
                /** @var Builder $query */
                $query->where('a.is_limit', CouponConstant::COUPON_LIMIT_NO)
                    ->orWhere(function ($query) {
                        /** @var Builder $query */
                        $query->where('a.is_limit', CouponConstant::COUPON_LIMIT_YES)
                            ->whereRaw('a.total_num > a.receive_num');
                    });
            });
    }

    public static function decrCouponReceiveNum($couponId)
    {
        return Coupon::query()
            ->where('id', $couponId)
            ->where(function ($query) {
                /** @var Builder $query */
                $query->where('is_limit', CouponConstant::COUPON_LIMIT_NO)
                    ->orWhere(function ($query) {
                        /** @var Builder $query */
                        $query->where('is_limit', CouponConstant::COUPON_LIMIT_YES)
                            ->whereRaw('total_num > receive_num');
                    });
            })
            ->decrement('receive_num');
    }
}
