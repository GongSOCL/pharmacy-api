<?php

declare(strict_types=1);
namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\ProductCategoryRel;

class ProductCategoryRelRepository
{
    public static function getProductCategories($productIds)
    {
        return ProductCategoryRel::query()
            ->from('product_category_rel as rel')
            ->join('product_category as cat', 'cat.id', '=', 'rel.cat_id')
            ->select(['rel.yy_product_id', 'cat.cat_name'])
            ->whereIn('rel.yy_product_id', $productIds)
            ->where('rel.is_delete', Delete::UNDELETED)
            ->where('cat.is_delete', Delete::UNDELETED)
            ->get();
    }

    public static function getProductCategory($productId)
    {
        return ProductCategoryRel::query()
            ->from('product_category_rel as rel')
            ->join('product_category as cat', 'cat.id', '=', 'rel.cat_id')
            ->select(['rel.yy_product_id', 'cat.cat_name'])
            ->where('rel.yy_product_id', $productId)
            ->where('rel.is_delete', Delete::UNDELETED)
            ->where('cat.is_delete', Delete::UNDELETED)
            ->first();
    }
}
