<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\Product;

class ProductRepository
{
    public static function getListByProductIds($productIds)
    {
        return Product::query()
            ->select(['id', 'yy_product_id', 'name'])
            ->whereIn('yy_product_id', $productIds)
            ->get();
    }

    public static function getListByAll()
    {
        return Product::query()
            ->select(['id', 'yy_product_id', 'name'])
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }
}
