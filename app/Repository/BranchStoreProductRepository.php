<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\BranchStoreProduct;
use App\Model\Pharmacy\Product;
use Hyperf\Utils\Collection;

class BranchStoreProductRepository
{

    public static function getStoreProducts($storeId): Collection
    {
        return BranchStoreProduct::query()
            ->join('product as p', 'p.yy_product_id', '=', 'branch_store_product.yy_product_id')
            ->where('branch_store_product.store_id', $storeId)
            ->where('branch_store_product.is_delete', Delete::UNDELETED)
            ->where('p.is_delete', Delete::UNDELETED)
            ->select(['p.*'])
            ->get();
    }

    public static function getStoreProductsByStoreId($storeId)
    {
        return BranchStoreProduct::query()
            ->select(['id','store_id','yy_product_id'])
            ->where('store_id', $storeId)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }

    public static function getStoreProductsByStoreIds($storeIds): Collection
    {
        return BranchStoreProduct::query()
            ->from('branch_store_product as s')
            ->select(['s.store_id','p.yy_product_id', 'p.name as product_name'])
            ->join('product as p', 'p.yy_product_id', '=', 's.yy_product_id')
            ->whereIn('s.store_id', $storeIds)
            ->where('s.is_delete', Delete::UNDELETED)
            ->where('p.is_delete', Delete::UNDELETED)
            ->get();
    }
}
