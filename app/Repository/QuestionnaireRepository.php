<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\Questionnaire;
use App\Model\Pharmacy\Resource;

class QuestionnaireRepository
{
    public static function getListByProductIds($page, $pageSize)
    {
        return Questionnaire::query()
            ->select(['id', 'quest_id', 'title', 'img', 'resources_id', 'type'])
            ->where('status', 2)
            ->where('is_deleted', Delete::UNDELETED)
            ->orderBy('created_at', 'desc')
            ->paginate($pageSize, ['*'], '', $page);
    }
}
