<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\OrderCoupon;
use Hyperf\Database\Query\Builder;
use Hyperf\Utils\Collection;

class OrderCouponRepository
{
    public static function batchAdd($orderId, Collection $coupons)
    {
        if ($coupons->count() == 0) {
            return true;
        }
        $addList = [];
        $couponDetailIds = [];
        $coupons->each(function ($row) use ($orderId, &$addList, &$couponDetailIds) {
            $addList[] = [
                'order_id'=>$orderId,
                'coupon_id'=>$row->coupon_id,
                'coupon_detail_id'=>$row->id,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
            ];
            $couponDetailIds[] = $row->id;
        });
        if ($addList) {
            // 优惠券关联订单
            OrderCoupon::insert($addList);
            // 核销礼券
            CouponDetailRepository::updateCouponUseStatus($couponDetailIds);
        }
        return true;
    }

    public static function getOrderCoupons($orderIds)
    {
        return OrderCoupon::query()
            ->select(['order_id','coupon_id'])
            ->with(['coupon'=>function ($query) {
                /** @var Builder $query */
                $query->select([
                    'id',
                    'policy_type',
                    'policy_val',
                    'deadline_type',
                    'deadline_val'
                ]);
            }])
            ->whereIn('order_id', $orderIds)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }
}
