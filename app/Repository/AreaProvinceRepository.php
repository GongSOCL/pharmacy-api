<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\AreaProvince;

class AreaProvinceRepository
{
    const PRO_FIELDS = [
        'id',
        'area_id',
        'province',
    ];
    public static function getProvinceById($proId)
    {
        return AreaProvince::query()
            ->select(self::PRO_FIELDS)
            ->where('id', $proId)
            ->first();
    }

    public static function getProvinces()
    {
        return AreaProvince::query()
            ->select(['id', 'province as name'])
            ->get();
    }
}
