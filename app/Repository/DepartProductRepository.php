<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\DepartProduct;

class DepartProductRepository
{
    public static function getProductIdsByDepartId($departId)
    {
        return DepartProduct::query()
            ->select(['id', 'depart_id', 'yy_product_id'])
            ->where('depart_id', $departId)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }

    public static function getDeparts()
    {
        return DepartProduct::query()
            ->select(['depart_id'])
            ->distinct()
            ->where(
                'is_delete',
                Delete::UNDELETED
            )
            ->get();
    }
}
