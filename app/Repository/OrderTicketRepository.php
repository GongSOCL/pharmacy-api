<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\Order;
use App\Model\Pharmacy\OrderTicket;
use Hyperf\Utils\Collection;

class OrderTicketRepository
{

    public static function batchAdd(Order $order, array $pics): bool
    {
        $data = [];
        $now = date('Y-m-d H:i:s');
        foreach ($pics as $pic) {
            $data[] = [
                'order_id' => $order->id,
                'ticket_id' => $pic,
                'is_delete' => Delete::UNDELETED,
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        return OrderTicket::insert($data);
    }

    public static function getTicketsByOrderIds(array $orderIds): Collection
    {
        return OrderTicket::query()
            ->whereIn('order_id', $orderIds)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }
}
