<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\Delete;
use App\Constants\Status;
use App\Model\Pharmacy\User;
use App\Model\Qa\TUserIdentity;

class UserRepository
{

    const USER_PROFILE_FIELDS = [
        'id',
        'union_id',
        'open_id',
        'nick_name',
        'real_name',
        'header_url',
        'store_role',
        'role',
    ];

    public static function getUserById($userId): ?User
    {
        return User::query()
            ->where('id', $userId)
            ->first();
    }

    public static function getUserInfoByOpenId($openId): ?User
    {
        return User::select([
            'id',
            'store_role',
            'role'
        ])
        ->where('open_id', $openId)
        ->first();
    }


    public static function addUser($unionId, $openId, $nickName, $role)
    {
        $data = [
            'union_id'=>$unionId,
            'open_id'=>$openId,
            'role'=>$role,
            'nick_name'=>$nickName,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s'),
        ];
        return User::insertGetId($data);
    }

    public static function addUserByModel(User $user)
    {
        return $user->save();
    }

    public static function updateUserRole($userId, $role)
    {
        return User::where('id', $userId)
            ->update(['role'=>$role]);
    }

    public static function getListByUserIds($userIds)
    {
        return User::select(['id', 'nick_name', 'real_name', 'header_url'])
            ->whereIn('id', $userIds)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }


    public static function getUserProfileById($userId)
    {
        return User::query()
            ->select(self::USER_PROFILE_FIELDS)
            ->with(['clerk'=>function ($query) {
                $query->select([
                    'user_id',
                    'store_id',
                    'real_name',
                    'role',
                    'phone',
                    'position',
                    'status' ]);
            }])
            ->with(['roles'=>function ($query) {
                $query->select(['user_id', 'role']);
            }])
            ->where('id', $userId)
            ->first();
    }

    public static function updateUserInfo($userId, $nickName, $headerUrl)
    {
        if (!empty($headerUrl)) {
            User::query()
                ->where('id', $userId)
                ->update([
                    'nick_name'=>$nickName,
                    'header_url'=>$headerUrl,
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
        } else {
            User::query()
                ->where('id', $userId)
                ->update([
                    'nick_name'=>$nickName,
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
        }
        return true;
    }


    /**
     * 获取身份证识别信息
     * @param $uid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getUserByYYID($uid)
    {
        return TUserIdentity::where('uid', $uid)
            ->where('platform', 4)
            ->first();
    }


    public static function create(
        $front,
        $back,
        $name,
        $sex,
        $ethnicity,
        $birthDate,
        $address,
        $idNumber,
        $issueAuthority,
        $validPeriod,
        $mobile,
        $uid
    ) {

        $data = [
            'uid'           => $uid,
            'idcard_front_location' => $front,
            'idcard_back_location'  => $back,
            'realname'  => $name,
            'id_code'   => $idNumber,
            'gender'    => $sex,
            'nationality'   => $ethnicity,
            'birth_date'    => $birthDate,
            'address'   => $address,
            'mobile'    => $mobile,
            'platform'  => 4,   # 平台选择 1优药 2优佳医 4优能汇
            'due_date'  => trim(strstr($validPeriod, '-'), '-'),
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ];
        TUserIdentity::insert($data);
    }
}
