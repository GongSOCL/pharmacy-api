<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\MainStore;

class MainStoreRepository
{
    const MAIN_STORE_FIELDS = [
        'id',
        'city_id',
        'store_name'
    ];

    public static function getMainStoreListByCityId($cityId)
    {
        return MainStore::query()
            ->select(self::MAIN_STORE_FIELDS)
            ->where('city_id', $cityId)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }

    public static function getMainStoreListByPageAndCityId($cityId, $pageSize = 20, $keywords = '')
    {
        return MainStore::query()
            ->select(self::MAIN_STORE_FIELDS)
            ->where('city_id', $cityId)
            ->where('is_delete', Delete::UNDELETED)
            ->when($keywords != '', function ($q) use ($keywords) {
                $q->where('store_name', 'like', "%{$keywords}%");
            })->paginate($pageSize);
    }

    public static function getMainInfo($mainId)
    {
        return MainStore::query()
            ->select(self::MAIN_STORE_FIELDS)
            ->where('id', $mainId)
            ->where('is_delete', Delete::UNDELETED)
            ->first();
    }
}
