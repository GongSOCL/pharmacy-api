<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Constants\Status;
use App\Model\Pharmacy\HealthTip;
use App\Model\Pharmacy\Indication;
use App\Model\Pharmacy\QuestionAnswer;
use App\Model\Pharmacy\QuestionAnswerFab;
use App\Model\Pharmacy\Resource;
use App\Model\Pharmacy\ResourcesMold;
use App\Model\Pharmacy\ServiceGuide;
use App\Model\qa\TDrugProduct;

class HealthTipsRepository
{
    public static function getResourcesMoldList(): array
    {
        $Molds = ResourcesMold::where('is_deleted', Status::NOTDELETED)->select('id', 'name')->get();

        return $Molds->isNotEmpty() ? $Molds->toArray() : [];
    }


    /**
     * 健康小知识
     * @return array
     */
    public static function getHealthTipsList($RoleId, $MoldId): array
    {
        $Data = HealthTip::from('health_tips as hp')
            ->join('resources as b', 'b.id', 'hp.resource_id')
            ->where('hp.is_deleted', Status::NOTDELETED)
            ->whereIn('b.use', [$RoleId,0])
            ->where('b.mold_id', $MoldId)
            ->orderBy('hp.rank', 'desc')
            ->orderBy('hp.created_at', 'desc')
            ->select('hp.id', 'hp.title', 'hp.img', 'hp.resource_id')
            ->get();

        return $Data->isNotEmpty() ? $Data->toArray() : [];
    }

    /**
     * 常见适应症
     * @return array
     */
    public static function getIndicationList($RoleId, $MoldId): array
    {
        $Data = Indication::from('indication as ind')
            ->join('resources as b', 'b.id', 'ind.resource_id')
            ->where('ind.is_deleted', Status::NOTDELETED)
            ->whereIn('b.use', [$RoleId,0])
            ->where('b.mold_id', $MoldId)
            ->orderBy('ind.rank', 'desc')
            ->orderBy('ind.created_at', 'desc')
            ->select('ind.id', 'ind.title', 'ind.img', 'ind.resource_id')
//            ->limit(2)
            ->get();

        return $Data->isNotEmpty() ? $Data->toArray() : [];
    }

    /**
     * 资源
     * @return array
     */
    public static function getResourceByID($ResId): array
    {

        $Data = Resource::where('id', $ResId)
            ->where('is_deleted', 0)
            ->orderBy('created_at', 'desc')
            ->first();


        if ($Data && $Data->is_pdf == 1) {
            $Data->PDFInfoByID;
        }

        if ($Data&& $Data->drug_id > 0) {
            $Data->DrugINfoByID;
        }

        return $Data ? $Data->toArray() : [];
    }

    /**
     * 说明书
     * @return array
     */
    public static function getInstructionsByID($DrugId): array
    {

        $Data = TDrugProduct::from('t_drug_product as a')
            ->join('t_drug_manual as b', 'a.yyid', 'b.product_yyid')
            ->where('a.id', $DrugId)
            ->select('b.name as bname', 'b.content', 'a.name')
            ->get();

        return $Data->isNotEmpty() ? $Data->toArray() : [];
    }

    /**
     * 服务指南
     * @return array
     */
    public static function getServiceGuide($ResId): array
    {
        $Data = ServiceGuide::from('service_guide as a')
            ->join('resources as b', 'a.resource_id', 'b.drug_id')
            ->where('b.drug_id', $ResId)
            ->select('a.id', 'a.content')
            ->first();

        return $Data ? $Data->toArray() : ['id'=>'固定测试ID','content'=>'固定测试内容'];
    }

    /**
     * 用户反馈
     * @return array
     */
    public static function getQuestionAnswer($ResId): array
    {
        $Data = QuestionAnswer::where('resource_id', $ResId)
            ->select('id', 'quest', 'answer')
            ->get();

        return $Data ? $Data->toArray() : [['quest'=>'固定测试问题','answer'=>'固定测试答案','id'=>2]];
    }

    /**
     * @return array
     */
    public static function getQuestionAnswerFab($QId, $RoleId)
    {
        return QuestionAnswerFab::where('quest_id', $QId)
            ->where('fab', 1)
            ->where('role_id', $RoleId->role)
            ->groupBy('quest_id', 'role_id')
            ->sum('fab');
    }

    /**
     * @return array
     */
    public static function getQuestionAnswerFabSelf($QId, $RoleId)
    {
        # role_id = 角色类型
        $self = QuestionAnswerFab::where('quest_id', $QId)
            ->where('user_id', $RoleId->id)->where('role_id', $RoleId->role)
            ->select('user_id', 'fab')
            ->first();

        return $self ? $self->toArray() : false;
    }

    /**
     * 点赞
     * @return array
     */
    public static function setQuestionAnswerFab($QId, $RoleId)
    {

        $sf = self::getQuestionAnswerFabSelf($QId, $RoleId);

        if (isset($sf['fab']) && $sf['fab'] == 0) {
            return QuestionAnswerFab::where('quest_id', $QId)
               ->where('user_id', $RoleId->id)
               ->where('role_id', $RoleId->role)
               ->where('fab', 0)
               ->update(['fab'=>1]);
        } elseif (isset($sf['fab']) && $sf['fab'] == 1) {
            return QuestionAnswerFab::where('quest_id', $QId)
               ->where('user_id', $RoleId->id)
               ->where('role_id', $RoleId->role)
               ->where('fab', 1)
               ->update(['fab'=>0]);
        } else {
            return QuestionAnswerFab::insert(
                [
                    'quest_id'=>$QId,
                    'user_id'=>$RoleId->id,
                    'role_id'=>$RoleId->role,
                    'fab'=>1
                ]
            );
        }
    }

    /**
     * 店员首页疾病产品
     * @return array
     */
    public static function getDYIndex($RoleId)
    {

        # 适应症
//        $Indication = Indication::from('indication as ind')
//            ->join('resources as b','b.id','ind.resource_id')
//            ->where('ind.is_deleted',Status::NOTDELETED)
//            ->where('b.use',$RoleId)
//            ->orderBy('ind.rank','desc')
//            ->select('ind.id','ind.title','ind.img','ind.resource_id')
//            ->get();


        # 小知识
//        $HealthTip = HealthTip::from('health_tips as hp')
//            ->join('resources as b','b.id','hp.resource_id')
//            ->where('hp.is_deleted',Status::NOTDELETED)
//            ->where('b.use',$RoleId)
//            ->orderBy('hp.rank','desc')
//            ->select('hp.id','hp.title','hp.img','hp.resource_id')
//            ->get();

        $list = Resource::where('is_index', 1)
            ->select('id as resource_id', 'img', 'title')
            ->where('is_deleted', Delete::UNDELETED)
            ->whereIn('use', [0,2])
            ->get();


        $list['list'] = $list->isNotEmpty()?$list->toArray():[];

        return $list;
    }
}
