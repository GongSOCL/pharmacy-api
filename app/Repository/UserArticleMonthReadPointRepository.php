<?php
declare(strict_types=1);

namespace App\Repository;

use App\Model\Pharmacy\UserArticleMonthReadPoint;
use Carbon\Carbon;

class UserArticleMonthReadPointRepository
{
    public static function addArticleRead($userId, Carbon $time)
    {
       $o = new UserArticleMonthReadPoint();
       $o->user_id = $userId;
       $o->point_time = $time->format('Y-m-d H:i:s');
       $o->year = $time->format('Y');
       $o->month = $time->format('n');
       $o->created_at = $o->updated_at = Carbon::now();
       $o->save();
    }
 
    public static function countUserRead($userId, Carbon $time = null): int
    {
        if (!$time) {
            $time = Carbon::now();
        }
       return UserArticleMonthReadPoint::query()
        ->where('user_id', $userId)
        ->where('year', $time->format('Y'))
        ->where('month', $time->format('n'))
        ->count();
    }
}