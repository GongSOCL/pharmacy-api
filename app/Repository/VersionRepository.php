<?php
declare(strict_types=1);
namespace App\Repository;

use App\Model\Pharmacy\Version;

class VersionRepository
{
    public static function getByVersion($version): ?Version
    {
        return Version::query()
            ->where('version', $version)
            ->where('status', 1)
            ->first();
    }
}
