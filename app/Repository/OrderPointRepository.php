<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\OrderPoint;
use Hyperf\Database\Query\Builder;

class OrderPointRepository
{
    public static function getOrderPoints($orderUserIds, $orderIds)
    {
        return OrderPoint::query()
            ->select(
                ['id',
                'order_id',
                'detail_id',
                'order_user_id',
                'product_id',
                'sku_id',
                'service_point',
                'money_point',
                ]
            )
            ->whereIn('order_id', $orderIds)
            ->whereIn('order_user_id', $orderUserIds)
            ->get();
    }
}
