<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\ClerkOrderStatistic;
use Carbon\Carbon;
use Hyperf\DbConnection\Db;

class ClerkOrderStatisticRepository
{
    public static function sumClerkSaleByMonth($clerkIds, $month)
    {
        [$y, $m] = explode('/', $month);
        $startDate = Carbon::create((int)$y, (int)$m)->startOfMonth()->format('Y-m-d');
        $endDate = Carbon::create((int)$y, (int)$m)->lastOfMonth()->format('Y-m-d').' 23:59:59';
        return ClerkOrderStatistic::query()
            ->select([
                'clerk_id',
                Db::raw('sum(num) as num'),
                Db::raw('sum(amount) as num')
            ])
            ->whereIn('clerk_id', $clerkIds)
            ->whereBetween('order_date', [$startDate, $endDate])
            ->where('is_delete', Delete::UNDELETED)
            ->groupBy('clerk_id')
            ->get();
    }
}
