<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\DataStatus;
use App\Constants\Delete;
use App\Constants\Notify;
use App\Model\Pharmacy\PurchaseNotify;
use App\Model\Pharmacy\UserClerk;
use Carbon\Carbon;
use Hyperf\Database\Query\Builder;

class PurchaseNotifyRepository
{

    public static function existNotifyInfoByStatus($userId, $storeId, $status)
    {
        return PurchaseNotify::query()
            ->where('store_id', $storeId)
            ->where('patient_user_id', $userId)
            ->where('status', $status)
            ->count();
    }

    public static function getNotifyInfo($notifyId):?PurchaseNotify
    {
        return PurchaseNotify::query()
            ->select([
                'id',
                'store_id',
                'patient_user_id',
                'clerk_user_id',
                'status',
                'created_at',
            ])
            ->where('id', $notifyId)
            ->first();
    }

    public static function addNotify($userId, $storeId, $lon = 0.0, $lat = 0.0)
    {
        $data = [
            'patient_user_id'=>$userId,
            'store_id'=>$storeId,
            'longitude'=>$lon,
            'latitude'=>$lat,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s'),
        ];
        return PurchaseNotify::query()
            ->insertGetId($data);
    }

    public static function getList($storeId, $userId, $status, $pageSize)
    {
        return PurchaseNotify::query()
            ->select(['id','patient_user_id','status', 'store_id', 'created_at'])
            ->where('store_id', $storeId)
            ->where('status', 0)
            ->when($status == Notify::NOTIFY_CANCEL, function ($query) {
                /** @var Builder $query */
                $query->whereRaw('TIMESTAMPDIFF(HOUR, created_at, now())>=1');
            })
            ->when($status == Notify::NOTIFY_PENDING, function ($query) {
                $query->whereRaw('TIMESTAMPDIFF(HOUR, created_at, now()) < 1');
            })
            ->orderByDesc('id')
            ->paginate($pageSize);
    }

    public static function updateStatus($notifyId, $userId, $status)
    {
        return PurchaseNotify::query()
            ->where('id', $notifyId)
            ->where('status', Notify::NOTIFY_PENDING)
            ->update([
                'clerk_user_id'=>$userId,
                'status'=>$status
            ]);
    }

    public static function receivePurchase(PurchaseNotify $notify, UserClerk $clerk): int
    {
        return PurchaseNotify::query()
            ->where('id', $notify->id)
            ->where('clerk_user_id', 0)
            ->where('status', Notify::NOTIFY_PENDING)
            ->update([
                'clerk_user_id' => $clerk->user_id,
                'status' => Notify::NOTIFY_RECEIVED,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function handlePurchaseRelAgentChunk($func, $pageSize = 50)
    {
        return PurchaseNotify::query()
            ->where('is_delete', Delete::UNDELETED)
            ->orderBy('id')
            ->chunk($pageSize, $func);
    }
}
