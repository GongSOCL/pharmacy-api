<?php

declare(strict_types=1);

namespace App\Repository\Count;

use App\Helper\Helper;
use App\Model\Niferex\UserResourceClick;

class CountRepository
{
    /**
     * @param $resourceId
     * @param $userId
     * @param $partId
     * @param $inviteId
     * @param int $resourceType
     */
    public static function recodeUserClick(
        $resourceId,
        $userId,
        $partId,
        $inviteId,
        $resourceType = UserResourceClick::RESOURCE_TYPE_NORMAL
    ) {
        UserResourceClick::insert([
            'resource_id' => $resourceId,
            'user_id' => $userId,
            'part' => $partId,
            'created_at' => Helper::getDate(),
            'resource_type' => $resourceType,
            'invite_id' => $inviteId
        ]);
    }

    /**
     * @param $resourceId
     * @return int
     */
    public static function getResourceClickCount($resourceId)
    {
        return UserResourceClick::where([
            'resource_id' => $resourceId,
            'resource_type' => UserResourceClick::TYPE_1
        ])->count();
    }

    /**
     * @param $resourceId
     * @return int
     */
    public static function getQuestClickCount($resourceId)
    {
        return UserResourceClick::where([
            'resource_id' => $resourceId,
            'resource_type' => UserResourceClick::TYPE_2
        ])->count();
    }
}
