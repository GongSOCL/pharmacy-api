<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Clerk;
use App\Constants\Delete;
use App\Constants\Store;
use App\Model\Pharmacy\BranchStore;
use App\Model\Pharmacy\User;
use App\Model\Pharmacy\UserClerk;
use Hyperf\Utils\Collection;

class UserClerkRepository
{
    const CLERK_FIELDS = [
        'id',
        'agent_id',
        'user_id',
        'real_name',
        'store_id',
        'role',
        'phone',
        'position',
        'status'
    ];
    const ROLE_LIST = [
        1=>'店员',
        2=>'店长',
    ];
    public static function getUserClerkByUserId($userId):?UserClerk
    {
        return UserClerk::select(self::CLERK_FIELDS)
        ->where('user_id', $userId)
        ->first();
    }

    public static function addUserClerkRel($userId, $agentId, $storeId)
    {
        $data =[
            'user_id'=>$userId,
            'agent_id'=>$agentId,
            'store_id'=>$storeId,
            'role'=>Clerk::ROLE_STAFF,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s'),
        ];
        return UserClerk::insert($data);
    }

    public static function updateStatus($userId, $status)
    {
        return UserClerk::query()
            ->where('user_id', $userId)
            ->update(['status'=>$status]);
    }

    public static function getMyClerkListByStoreId($storeId)
    {
        return UserClerk::query()
            ->select(self::CLERK_FIELDS)
            ->where('store_id', $storeId)
            ->where('status', Clerk::STATUS_PASS)
            ->where('role', Clerk::ROLE_STAFF)
            ->where('is_delete', Delete::UNDELETED)
            ->orderByDesc('id')
            ->limit(100)
            ->get();
    }

    public static function saveClerk(UserClerk $model)
    {
        return $model->save();
    }

    public static function handleClerkRelAgentChunk($func, $pageSize = 30)
    {
        return UserClerk::query()
            ->whereIn('status', [2, 4])
            ->orderBy('id')
            ->chunk($pageSize, $func);
    }

    public static function getUserWithStoreByIds(array $userIds): Collection
    {
        return UserClerk::query()
            ->join('branch_store as bs', 'bs.id', '=', 'user_clerk.store_id')
            ->join('user as u', 'u.id', '=', 'user_clerk.user_id')
            ->whereIn('user_clerk.id', $userIds)
            ->where('user_clerk.is_delete', Delete::UNDELETED)
            ->select(['user_clerk.*', 'bs.store_name', 'u.header_url'])
            ->get();
    }
}
