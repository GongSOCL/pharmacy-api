<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\Order;
use App\Model\Pharmacy\OrderDetail;
use Hyperf\Utils\Collection;

class OrderDetailRepository
{

    public static function batchAdd(Order $order, array $details): bool
    {
        $data = [];
        foreach ($details as $detail) {
            $data[] = [
                'order_id' => $order->id,
                'yy_product_id' => $detail['product_id'],
                'yy_sku_id' => $detail['sku_id'],
                'yy_sku_name' => $detail['sku_name'],
                'product_name' => $detail['product_name'],
                'num' => $detail['total'],
                'amount' => $detail['amount'],
                'remark' => '',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
            ];
        }

        return OrderDetail::insert($data);
    }

    public static function listDetailByOrderIds(array $orderIds): Collection
    {
        return OrderDetail::query()
            ->whereIn('order_id', $orderIds)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }
}
