<?php

namespace App\Repository\Topic;

use App\Constants\Auth;
use App\Constants\DataStatus;
use App\Constants\Delete;
use App\Constants\Status;
use App\Exception\AuthException;
use App\Helper\Helper;
use App\Model\Pharmacy\Topic;
use App\Model\Pharmacy\TopicParticipateRecord;
use App\Service\Upload\UploadCommonService;
use Carbon\Carbon;
use Hyperf\Utils\Collection;

class TopicRepository
{
    private static function checkUser()
    {
        $role = Helper::getUserLoginRole();

        if ($role->role == 1) {
            throw new AuthException(Auth::USER_FORBIDDEN, "非法操作");
        }
    }

    /**
     * 话题首页
     * @return array
     */
    public static function getTopicsList()
    {
        self::checkUser();

        $Topic = Topic::query()
            ->where('status', Status::REGULAR)
            ->where('is_publish', Status::REGULAR)
            ->orderBy('created_at', 'desc')
            ->select('name as title', 'id', 'cover_url as img')
            ->get();


        return $Topic->isNotEmpty() ? $Topic->toArray() : [];
    }

    public static function getById($id): ? Topic
    {
        self::checkUser();

        return Topic::query()
            ->where('id', $id)
            ->where('status', Status::REGULAR)
            ->first();
    }

    public static function addOne($name, $coverId, $detailId, $isPublish = false): Topic
    {
        $o = new Topic();
        $o->name = $name;
        $o->cover_id = $coverId;
        $o->detail_id = $detailId;
        $o->is_publish = $isPublish ? 1 : 0;
        $o->total = 0;
        $o->status = Status::REGULAR;
        $o->created_at = $o->updated_at = Carbon::now();
        $o->save();
        $o->refresh();

        return $o;
    }

    public static function publishTopic(Topic $topic): int
    {
            return Topic::query()
                ->where('id', $topic->id)
                ->where('status', Status::REGULAR)
                ->where('is_publish', 0)
                ->update([
                    'is_publish' => 1,
                    'updated_at' => Carbon::now()
                ]);
    }

    public static function getByIds(array $ids): Collection
    {
        return Topic::query()
            ->whereIn('id', $ids)
            ->where('status', Status::REGULAR)
            ->get();
    }

    public static function limitSearch($keywords, $limit): Collection
    {
        return Topic::query()
            ->where('status', Status::REGULAR)
            ->when($keywords != "", function ($query) use ($keywords) {
                $query->where('name', 'like', "%{$keywords}%");
            })->limit($limit)
            ->get();
    }
}
