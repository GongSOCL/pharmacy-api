<?php
declare(strict_types=1);
namespace App\Repository\Topic;

use App\Constants\ErrorCode;
use App\Constants\Status;
use App\Exception\BusinessException;
use App\Model\Pharmacy\BranchStore;
use App\Model\Pharmacy\City;
use App\Model\Pharmacy\MainStore;
use App\Model\Pharmacy\Topic;
use App\Model\Pharmacy\TopicParticipateRecord;
use App\Model\Pharmacy\TopicParticipateRecordFab;
use App\Model\Pharmacy\TopicParticipateRecordUpload;
use App\Model\Pharmacy\User;
use App\Model\Pharmacy\UserClerk;
use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class TopicParticipateRecordRepository
{

    /**
     * 获取话题参与人数
     * @param Topic $topic
     * @return int
     */
    public static function countTopicParticipates(Topic $topic): int
    {
        return TopicParticipateRecord::query()
            ->where('topic_id', $topic->id)
            ->where('status', Status::REGULAR)
            ->select(Db::raw("distinct(user_id) as user_id"))
            ->count();
    }


    /**
     * 获取参与话题下的图片列表
     * @param User $user
     * @param Topic $topic
     * @param bool $isOrderNew
     * @param int $current
     * @param int $limit
     * @return \Hyperf\Contract\LengthAwarePaginatorInterface
     */
    public static function getTopicParticipate(
        User $user,
        Topic $topic,
        bool $isOrderNew,
        $current = 1,
        $limit = 10
    ): \Hyperf\Contract\LengthAwarePaginatorInterface {
        $sb = TopicParticipateRecord::query()
            ->where('topic_id', $topic->id)
            ->where('state', TopicParticipateRecord::STATE_AUDIT_SUCCESS)
            ->where('status', Status::REGULAR);

        if ($isOrderNew) {
            //最新(关联返回用户所有视频)
            $sub = TopicParticipateRecord::query()
                ->where('topic_id', $topic->id)
//                ->where('user_id', 2)
                ->where('user_id', $user->id)
                ->whereIn('state', [
                    TopicParticipateRecord::STATE_AUDITING,
                ])->where('status', Status::REGULAR)
                ->select(['id as sid']);
            $sb->union($sub);
        } else {
            //最热
            $sb->where('like_count', '>', 0)
                ->orderByDesc('like_count');
        }
        $sb = $sb->select(['id as sid']);

        return TopicParticipateRecord::query()
            ->joinSub($sb, 'sb', 'topic_participate_record.id', '=', 'sb.sid')
            ->orderByDesc('topic_participate_record.created_at')
            ->paginate($limit, ['topic_participate_record.*'], '', $current);
    }

    public static function addParticipate(
        Topic $topic,
        User $user,
        $tp,
        $upload_image_ids,
        $upload_video_ids,
        bool $isPublish,
        $comment = ""
    ): TopicParticipateRecord {

        # 获取店员所属药店
        $UserClerk = UserClerk::where('user_id', $user->id)->first();

        if (!$UserClerk || !$UserClerk->store_id) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '店员所属药店信息有误！');
        }

        # 获取所属店
        $where[] = ['id', $UserClerk->store_id];
        $BranchStore = BranchStore::where($where)->select(['main_store_id'])->first();

        # 获取所属连锁店
        if (!$BranchStore || !$BranchStore->main_store_id) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '连锁店铺信息有误！');
        }
        $MainStore = MainStore::where('id', $BranchStore->main_store_id)->select(['city_id'])->first();


        # 获取所属城市
        if (!$MainStore || !$MainStore->city_id) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '连锁店铺所属城市信息有误！');
        }
        $CityInfo = City::where('id', $MainStore->city_id)->select(['province_id'])->first();

        # 获取所属省份
        if (!$CityInfo || !$CityInfo->province_id) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '所属省份信息有误！');
        }

        Db::beginTransaction();
        try {
            $o = new TopicParticipateRecord();
            $o->topic_id    = $topic->id;
            $o->tp          = $tp;
            $o->user_id     = $user->id;
            $o->store_id    = $UserClerk->store_id;
            $o->main_store_id   = $BranchStore->main_store_id;
            $o->city_id     = $MainStore->city_id;
            $o->province_id = $CityInfo->province_id;
            $o->state       = $isPublish ? TopicParticipateRecord::STATE_AUDIT_SUCCESS : TopicParticipateRecord::STATE_DRAFT;
            $o->comment     = $comment;
            $o->reject_reason = "";
            $o->status      = Status::REGULAR;
            $o->created_at  = $o->updated_at = Carbon::now();
            $o->save();

            if ($o->id) {
                if ($tp == 1) {
                    foreach ($upload_image_ids as $k => $upload_image_id) {
                        $insertData[$k]['record_id'] = $o->id;

                        $insertData[$k]['upload_id'] = $upload_image_id;

                        $insertData[$k]['created_at'] = date('Y-m-d H:i:s');
                    }
                } else {
                    foreach ($upload_video_ids as $k => $upload_video_id) {
                        $insertData[$k]['record_id'] = $o->id;

                        $insertData[$k]['upload_id'] = $upload_video_id;

                        $insertData[$k]['created_at'] = date('Y-m-d H:i:s');
                    }
                }

                TopicParticipateRecordUpload::insert($insertData);
            }

            Db::commit();

            return $o;
        } catch (\Exception $e) {
            Db::rollBack();

            throw $e;
        }
    }

    public static function listUserParticipated(
        User $user,
        $state = 0,
        $current = 1,
        $limit = 10
    ): \Hyperf\Contract\LengthAwarePaginatorInterface {
        return TopicParticipateRecord::query()
            ->where('user_id', $user->id)
            ->where('status', Status::REGULAR)
            ->when($state > 0, function ($query) use ($state) {
                $query->where('state', $state);
            })->paginate($limit, ['*'], '', $current);
    }

    public static function getById(int $id): ?TopicParticipateRecord
    {
        return TopicParticipateRecord::query()
            ->where('id', $id)
            ->first();
    }


    /**
     * 记录资源详情
     * @param int $id
     * @return TopicParticipateRecord|null
     */
    public static function getUploadDetailById($id, $action, $order, $source, $state, $user, $topic_id)
    {
        $order = $order == 1 ? 'created_at' : 'like_count';

        if ($source == 1) {
            $order = 'created_at';
        }

        $operator = '';

        $dir = 'asc';
        switch ($action) {
            case 1: //前一个
                $operator = $source == 1 ? '<' : '>';
                $dir = $source == 1 ? 'desc' : 'asc';
                break;
            case 2: //后一个
                $operator = $source == 1 ? '>' : '<';
                $dir = $source == 1 ? 'asc' : 'desc';
                break;
            case 3:
                $operator = '=';
                break;
        }

        $query = TopicParticipateRecordUpload::from('topic_participate_record_upload as a')
            ->join('topic_participate_record as b', function ($query) {
                $query->on('a.record_id', '=', 'b.id')
                    ->where('b.status', '=', 1);
            })
            ->join('topic as c', 'c.id', 'b.topic_id')
            ->where('b.id', $operator, $id);


        if ($topic_id) {
            $query->where('b.topic_id', $topic_id);
        }
        if ($source != 1) {
            $state = 3;
            $query->whereRaw('(state = '.$state.' or (user_id = '.$user->id.' and state = 2))');
        } else {
            $query->whereRaw("(state = ? and user_id=?)", [$state, $user->id]);
        }

        return $query->orderBy('b.'.$order, $dir)
            ->select(
                [
                    Db::raw("GROUP_CONCAT(a.upload_id SEPARATOR ',') as upload_ids"),
                    'a.record_id',
                    'b.comment',
                    'b.state',
                    'b.share_count',
                    'b.topic_id',
                    'b.reject_reason',
                    'b.tp',
                    'c.name'
                ]
            )
            ->groupBy('b.id')
            ->first();



//        $query = TopicParticipateRecordUpload::from('topic_participate_record_upload as a')
//            ->join('topic_participate_record as b', function($query) {
//                $query->on('a.record_id', '=', 'b.id')
//                    ->where('b.status', '=', 1);
//            })
//            ->join('topic as c','c.id','b.topic_id')
//            ->where('a.upload_id',$operator, $id);
//
//        if($source == 1)    $query->where('b.state',$state);
//
//        return $query->orderBy('b.'.$order, 'desc')
//            ->select(['a.upload_id','a.record_id','b.comment','b.state','b.share_count','b.topic_id','b.reject_reason','b.tp','c.name'])
//            ->first();
    }


    public static function isLastById($id, $action, $order, $source, $state, $user_id, $topic_id)
    {
        $order = $order == 1 ? 'created_at' : 'like_count';

        if ($source == 1) {
            $order = 'created_at';
        }

        # 上一个
        $PreQuery = TopicParticipateRecord::query();

        if ($topic_id) {
            $PreQuery->where('topic_id', $topic_id);
        }
        if ($source == 1) {
            //个人作品前一个id小于当前id
            $PreQuery->where('id', '<', $id)
                ->where('state', $state);
            $PreQuery->where('user_id', $user_id);
        } else {
            //最新和最热前一个id大于当前id,所有数据都是倒排的
            $PreQuery->where('id', '>', $id)
                ->whereRaw('(state = 3 or (user_id = '.$user_id.' and state = 2))');
        }

        $PreQueryData =  $PreQuery->select('id')
            ->first();

        # 下一个
        $NextQuery = TopicParticipateRecord::query();
        if ($topic_id) {
            $NextQuery->where('topic_id', $topic_id);
        }
        if ($source == 1) {
            $NextQuery->where('id', '<', $id)
                ->where('state', $state);
            $NextQuery->where('user_id', $user_id);
        } else {
            $NextQuery->where('id', '>', $id)
                ->whereRaw('(state = 3 or (user_id = '.$user_id.' and state = 2))');
        }

        $NextQueryData =  $NextQuery->select('id')
            ->first();

        return ['pre'=>$PreQueryData,'next'=>$NextQueryData];
    }


    /**
     * 点赞
     * @param int $id
     * @return TopicParticipateRecord|null
     */
    public static function setTopicLikeImages(int $id, User $user)
    {

        $sf = TopicParticipateRecordFab::query()
            ->where('record_id', $id)
            ->where('user_id', $user->id)
            ->first();

        if (isset($sf['fab']) && $sf['fab'] == 0) {
            Db::table('topic_participate_record')->where('id', $id)->increment('like_count');

            return TopicParticipateRecordFab::where('record_id', $id)
                ->where('user_id', $user->id)
                ->where('fab', 0)
                ->update(['fab'=>1]);
        } elseif (isset($sf['fab']) && $sf['fab'] == 1) {
            Db::table('topic_participate_record')->where('id', $id)->decrement('like_count');

            return TopicParticipateRecordFab::where('record_id', $id)
                ->where('user_id', $user->id)
                ->where('fab', 1)
                ->update(['fab'=>0]);
        } else {
            Db::table('topic_participate_record')->where('id', $id)->increment('like_count');

            return TopicParticipateRecordFab::insert(['record_id'=>$id,'user_id'=>$user->id,'fab'=>1]);
        }
    }

    /**
     * 获取点赞
     * @param int $id
     * @return TopicParticipateRecord|null
     */
    public static function getLikesByResourceId(int $id)
    {

        return TopicParticipateRecordFab::query()
            ->where('record_id', $id)
            ->where('fab', 1)
            ->sum('fab');
    }


    /**
     * 分享
     * @param int $id
     * @return TopicParticipateRecord|null
     */
    public static function setTopicShareNum(int $id)
    {
        Db::select('update topic_participate_record set share_count = share_count + 1 where id = '.$id);
    }


    public static function publishDraft(TopicParticipateRecord $part): int
    {
        return TopicParticipateRecord::query()
            ->where('id', $part->id)
            ->where('state', TopicParticipateRecord::STATE_DRAFT)
            ->where('status', Status::REGULAR)
            ->update([
                'state' => TopicParticipateRecord::STATE_AUDITING,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function getByIds($ids, User $user = null): Collection
    {
        return TopicParticipateRecord::query()
            ->whereIn('id', $ids)
            ->where('status', Status::REGULAR)
            ->when(!is_null($user), function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->get();
    }

    public static function deleteByIds(array $ids): int
    {
        return TopicParticipateRecord::query()
            ->whereIn('id', $ids)
            ->where('status', Status::REGULAR)
            ->update([
               'status' => 0,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function countGroupByTopics(array $topicIds): Collection
    {
        return TopicParticipateRecord::query()
            ->whereIn('topic_id', $topicIds)
            ->where('status', Status::REGULAR)
            ->select(['topic_id', Db::raw('count(*) as cnt')])
            ->groupBy(['topic_id'])
            ->get();
    }

    public static function updateLike(int $resourceId, $count): int
    {
        return TopicParticipateRecord::query()
            ->where('resource_id', $resourceId)
            ->where('status', Status::REGULAR)
            ->update([
                'like_count' => $count,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function handleTopicRelAgentChunk($func, $pageSize = 30)
    {
        return TopicParticipateRecord::query()
            ->where('status', 1)
            ->orderBy('id')
            ->chunk($pageSize, $func);
    }
}
