<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\CouponMainStore;

class CouponMainStoreRepository
{
    public static function getMainStoreListByCouponId($couponId)
    {
        return CouponMainStore::query()
            ->select(['main_store_id'])
            ->where('coupon_id', $couponId)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }
}
