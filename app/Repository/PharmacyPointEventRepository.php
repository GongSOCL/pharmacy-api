<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\PharmacyPointEvent;
use Hyperf\Utils\Collection;

class PharmacyPointEventRepository
{
    public static function getAll(): Collection
    {
        return PharmacyPointEvent::query()
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }

    public static function getByEventKey($eventKey): ?PharmacyPointEvent
    {
        return PharmacyPointEvent::query()
            ->where('event_key', $eventKey)
            ->where('is_delete', Delete::UNDELETED)
            ->first();
    }
}
