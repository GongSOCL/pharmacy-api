<?php
declare(strict_types=1);
namespace App\Repository\Point;

use App\Constants\DataStatus;
use App\Constants\Delete;
use App\Model\Pharmacy\PointExchange;
use App\Model\Pharmacy\PointGood;
use App\Model\Pharmacy\PointGoodsDesc;
use Carbon\Carbon;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\DbConnection\Db;

class PointGoodRepository
{
    public static function getById($id): ?PointGood
    {
        return PointGood::query()
            ->where('id', $id)
            ->where('is_delete', Delete::UNDELETED)
            ->first();
    }

    public static function listGoodsByPointTypeAndUserType(
        $pointType = PointGood::POINT_TYPE_SERVICE_POINT,
        $goodsUserType = PointGood::EXCHANGE_BY_PATIENT,
        $current = 1,
        $limit = 10
    , $groupId = 0): LengthAwarePaginatorInterface {
        return PointGood::query()
            ->when($goodsUserType > 0, function ($query) use ($goodsUserType) {
                $query->where('exchange_user_type', $goodsUserType);
            })->when($pointType > 0, function ($query) use ($pointType) {
                $query->where('point_type', $pointType);
            })->when($groupId > 0, function ($query) use($groupId) {
                $query->where('group_id', $groupId);
            })->where('status', 1)
            ->where('is_delete', Delete::UNDELETED)
            ->paginate($limit, ['*'], '', $current);
    }

    public static function getGoodsDesc(PointGood $goods): ?PointGoodsDesc
    {
        return PointGoodsDesc::query()
            ->where('goods_id', $goods->id)
            ->where('is_delete', Delete::UNDELETED)
            ->first();
    }

    public static function exchangeBefore(PointGood $goods, $num): int
    {
        return PointGood::query()
            ->where('id', $goods->id)
            ->where('is_delete', Delete::UNDELETED)
            ->where(function ($query) use ($num) {
                $query->where('total', 0)
                ->orWhere('exchanged', '<=', Db::raw("total - {$num}"));
            })->update([
                'exchanged' => Db::raw("exchanged +  {$num}"),
                'updated_at' => Carbon::now()
            ]);
    }

    public static function cancelFromExchange(PointExchange $exchange): int
    {
        return PointGood::query()
            ->where('id', $exchange->goods_id)
            ->where('is_delete', Delete::UNDELETED)
            ->update([
                'exchanged' => Db::raw("exchanged - {$exchange->num}"),
                'updated_at' => Carbon::now()
            ]);
    }

    public static function getExchangeGoods(PointExchange $exchange): ?PointGood
    {
        return PointGood::query()
            ->where('id', $exchange->goods_id)
            ->first();
    }
}
