<?php
declare(strict_types=1);
namespace App\Repository\Point;

use App\Constants\Delete;
use App\Model\Pharmacy\DoctorCoinMagnificationConfig;

class DoctorCoinMagnificationConfigRepository
{

    public static function getCurrentValidConfig(): ?DoctorCoinMagnificationConfig
    {
        return DoctorCoinMagnificationConfig::query()
            ->where('is_delete', Delete::UNDELETED)
            ->orderByDesc('id')
            ->first();
    }
}
