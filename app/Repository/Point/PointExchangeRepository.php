<?php
declare(strict_types=1);
namespace App\Repository\Point;

use App\Constants\Delete;
use App\Model\Pharmacy\PointExchange;
use App\Model\Pharmacy\PointGood;
use App\Model\Pharmacy\User;
use App\Model\Pharmacy\UserRole;
use Carbon\Carbon;
use Hyperf\Paginator\LengthAwarePaginator;
use Hyperf\Utils\Collection;
use PhpParser\ErrorHandler\Collecting;

class PointExchangeRepository
{

    public static function addExchange(PointGood $goods, User $user, UserRole $role, $num): PointExchange
    {
        $o = new PointExchange();
        $o->user_id = $user->id;
        $o->role_id = $role->id;
        $o->goods_id = $goods->id;
        $o->service_point = $o->money_point = 0;
        $o->num = $num;
        $o->point_order_id = 0;
        $o->state = PointExchange::STATE_NEW;
        $o->is_delete = Delete::UNDELETED;
        $o->created_at = $o->updated_at = Carbon::now();

        $o->save();
        return $o;
    }

    public static function updateExchangePointInfo(PointExchange $exchange, $orderId, $servicePoint, $moneyPoint)
    {
        PointExchange::query()
            ->where('id', $exchange->id)
            ->where('is_delete', Delete::UNDELETED)
            ->where('state', PointExchange::STATE_NEW)
            ->update([
                'point_order_id' => $orderId,
                'service_point' => $servicePoint,
                'money_point' => $moneyPoint,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function cancelExchange(PointExchange $exchange): int
    {
        return PointExchange::query()
            ->where('id', $exchange->id)
            ->update([
                'state' => PointExchange::STATE_CANCEL,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function finishExchange(PointExchange $exchange)
    {
        PointExchange::query()
            ->where('id', $exchange->id)
            ->update([
                'state' => PointExchange::STATE_EXCHANGE_OK,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function exchangeOk(PointExchange $exchange, $orderId, $servicePoint, $moneyPoint): int
    {
        return PointExchange::query()
            ->where('id', $exchange->id)
            ->where('state', PointExchange::STATE_NEW)
            ->update([
                'state' => PointExchange::STATE_EXCHANGE_OK,
                'point_order_id' => $orderId,
                'service_point' => $servicePoint,
                'money_point' => $moneyPoint,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function exchangeSended(PointExchange $exchange): int
    {
        return PointExchange::query()
            ->where('id', $exchange->id)
            ->update([
                'state' => PointExchange::STATE_SEND_OK,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function listExchangeByGoodsTypeAndState(
        UserRole $role,
        $goodsType,
        $exchangeState,
        $current = 1,
        $limit = 10
    ): LengthAwarePaginator {
        return PointExchange::query()
            ->join('t_point_goods as pg', 'pg.id', '=', 't_point_exchange.goods_id')
            ->where('t_point_exchange.user_id', $role->user_id)
            ->where('t_point_exchange.role_id', $role->id)
            ->where('t_point_exchange.state', $exchangeState)
            ->where('pg.goods_type', $goodsType)
            ->where('t_point_exchange.is_delete', Delete::UNDELETED)
            ->orderBy('t_point_exchange.id', 'asc')
            ->select([
                'pg.cover_id',
                'pg.name as goods_name',
                't_point_exchange.*'
            ])->paginate($limit, ['*'], '', $current);
    }

    public static function getById($id): ?PointExchange
    {
        return PointExchange::query()
            ->where('id', $id)
            ->where('is_delete', Delete::UNDELETED)
            ->first();
    }

    public static function receiptConfirmed(PointExchange $exchang): int
    {
        return PointExchange::query()
            ->where('id', $exchang->id)
            ->where('state', PointExchange::STATE_SEND_OK)
            ->where('is_delete', Delete::UNDELETED)
            ->update([
                'state' => PointExchange::STATE_RECEIPT_CONFIRMD,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function countExchangeOrders(UserRole $role, $exchangeState = PointExchange::STATE_SEND_OK, $goodsType = PointGood::GOODS_TYPE_REAL): int
    {
        return PointExchange::query()
            ->join('t_point_goods as pg', 'pg.id', '=', 't_point_exchange.goods_id')
            ->where('t_point_exchange.user_id', $role->user_id)
            ->where('t_point_exchange.role_id', $role->id)
            ->where('t_point_exchange.state', $exchangeState)
            ->where('pg.goods_type', $goodsType)
            ->where('t_point_exchange.is_delete', Delete::UNDELETED)
            ->count();
    }

    public static function rollbackExchange(PointExchange $exchange): int
    {
        return PointExchange::query()
            ->where('id', $exchange->id)
            ->update([
                'state' => PointExchange::STATE_FAIL_ROLLBACK,
                'updated_at' => Carbon::now()
            ]);
    }
}
