<?php
declare(strict_types=1);
namespace App\Repository\Point;

use App\Constants\Delete;
use App\Model\Pharmacy\DoctorCoinMagnificationConfig;
use App\Model\Pharmacy\PatientDoctorMagnificationTimeline;
use Carbon\Carbon;

class PatientDoctorMagnificationTimelineRepository
{
    public static function getDoctorTimeline($doctorId): ?PatientDoctorMagnificationTimeline
    {
        return PatientDoctorMagnificationTimeline::query()
            ->where('yy_doctor_id', $doctorId)
            ->where('is_delete', Delete::UNDELETED)
            ->first();
    }

    public static function addDoctorConfig(
        $doctorId,
        $firstTime,
        $end,
        DoctorCoinMagnificationConfig $cfg
    ): PatientDoctorMagnificationTimeline {
        $o = new PatientDoctorMagnificationTimeline();
        $o->yy_doctor_id = $doctorId;
        $o->first_scan = $firstTime;
        $o->magnification = $cfg->magnification;
        $o->magnification_end = $end;
        $o->is_delete = Delete::UNDELETED;
        $o->created_at = $o->updated_at = Carbon::now();
        $o->save();
        return $o;
    }
}
