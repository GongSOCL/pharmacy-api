<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Auth;
use App\Constants\ErrorCode;
use App\Constants\ExamCode;
use App\Constants\Status;
use App\Exception\AuthException;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Pharmacy\BranchStore;
use App\Model\Pharmacy\BranchStoreProduct;
use App\Model\Pharmacy\Exam;
use App\Model\Pharmacy\ExamRandItem;
use App\Model\Pharmacy\QuestionnaireAnswer;
use App\Model\Pharmacy\UserClerk;
use App\Model\Qa\DrugProduct;
use App\Model\Qa\RepsServeScopeP;
use App\Model\Qa\RepsServeScopeS;
use App\Model\Qa\TComplianceExam;
use App\Model\Qa\TComplianceQuestion;
use App\Model\Qa\TComplianceQuestionItem;
use App\Model\Qa\TComplianceTestExamFraction;
use App\Model\Qa\TComplianceTestPaper;
use App\Model\Qa\TComplianceUserExam;
use App\Model\Qa\Users;
use App\Model\Qa\pharmacyHospital;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;
use InvalidArgumentException;
use RuntimeException;

class ExamServeScopePRepository
{
    public static function getUserServeProducts($UserInfo): array
    {
        # 店铺
        $Branch = Exam::from('exam as em')
            ->join('exam_branch_store as bs', 'bs.exam_id', '=', 'em.id')
            ->where([
                'em.status' => Status::REGULAR,
                'bs.status' => Status::REGULAR,
                'bs.branch_store_id' => $UserInfo->store_id,
            ])
            ->select(['em.exam_id'])
            ->get();


        # 获取用户关联门店的连锁店id
        $MainStoreId = BranchStore::where('id', $UserInfo->store_id)->select('main_store_id')->first();

        # 连锁
        $Main = Exam::from('exam as em')
            ->join('exam_main_store as ms', 'ms.exam_id', '=', 'em.id')
            ->where([
                'em.status' => Status::REGULAR,
                'ms.status' => Status::REGULAR,
                'ms.main_store_id' => $MainStoreId->main_store_id,
            ])
            ->orWhere('ms.main_store_id', 0) # 全部连锁店
            ->select(['em.exam_id'])
            ->get();

        # 获取用户门店关联的产品
        $YYProductId = BranchStoreProduct::where('store_id', $UserInfo->store_id)
            ->select('yy_product_id')->get();

        $YYProductIds = $YYProductId->isNotEmpty() ?
            array_unique(array_column($YYProductId->toArray(), 'yy_product_id')) : [];

        # 产品
        $Product = Exam::from('exam as em')
            ->join('exam_product as mp', 'mp.exam_id', '=', 'em.id')
            ->where([
                'em.status' => Status::REGULAR
            ])
            ->whereIn('product_id', $YYProductIds)
            ->select(['em.exam_id'])
            ->get();

        # 合并所有考试id - 并去重
        $BranchExamids  = $Branch ->isNotEmpty() ? array_column($Branch->toArray(), 'exam_id') : [];

        $MainExamids    = $Main ->isNotEmpty() ? array_column($Main->toArray(), 'exam_id') : [];

        $ProductExamids = $Product ->isNotEmpty() ? array_column($Product->toArray(), 'exam_id') : [];

        $Examids = array_unique(array_merge($BranchExamids, $MainExamids, $ProductExamids));

        return self::getUserExams($Examids, $UserInfo);
    }


    /**
     * 获取用户考试状态 及 考试的具体信息
     * @param array $ExamIds
     * @param $UserInfo
     */
    private static function getUserExams(array $ExamIds, $UserInfo)
    {
        $ExamData = self::getExamInfoById($ExamIds);

        # 用户考试状态
        $ExamStatus = QuestionnaireAnswer::whereIn('paper_id', $ExamIds)
            ->where('user_id', $UserInfo->user_id)
            ->where('type', 2)
            ->select('paper_id', 'score', 'is_pass', 'exam_num')
            ->get();

        $ExamStatus = $ExamStatus->isNotEmpty() ?
            array_column($ExamStatus->toArray(), null, 'paper_id'): [];

        # 考试列表图片
        $ExamImage = Exam::whereIn('exam_id', $ExamIds)
            ->select('exam_id', 'image')
            ->get();

        $ExamImage = $ExamImage->isNotEmpty() ?
            array_column($ExamImage->toArray(), null, 'exam_id'): [];

        foreach ($ExamData as $k => $examDatum) {
            # 判断考试结束时间
            if ($examDatum['exam_end'] && strtotime($examDatum['exam_end']) <= time()) {
                unset($ExamData[$k]);
                continue;
            }

            $ExamData[$k]['type'] = 3;

            $ExamData[$k]['title'] = $ExamData[$k]['name'];

            if (isset($ExamImage[$examDatum['id']])) {
                $ExamData[$k]['img'] = $ExamImage[$examDatum['id']]['image'];
            }

            if (isset($ExamStatus[$examDatum['id']])) {
                if ($ExamStatus[$examDatum['id']]['is_pass'] == ExamCode::REGULAR) {
                    $ExamData[$k]['exam_status']['is_pass'] = ExamCode::REGULAR;
                    $ExamData[$k]['exam_status']['score'] = $ExamStatus[$examDatum['id']]['score'];
                    continue;
                }
                # 判断考试次数是否为真 则实际次数等于考试次数，若不未真则表示只能考一次
                if ($ExamStatus[$examDatum['id']]['exam_num'] == $examDatum['exam_num']) {
                    # 考试状态 0未通过 1通过 2可以参加考试 3未开始 4补考
                    $ExamData[$k]['exam_status'] = $ExamStatus[$examDatum['id']];
                } else {
                    # 已考次数 小于 考试次数
                    if ($ExamStatus[$examDatum['id']]['is_pass'] == ExamCode::NOTPASS) { # 未通过可以再次考试
                        $ExamData[$k]['exam_status']['is_pass'] = ExamCode::MAKE_UP_EXAM;
                        $ExamData[$k]['exam_status']['score'] = $ExamStatus[$examDatum['id']]['score'];
                    }
                }
            } else {
                # 没有考试状态 可以参加考试
                $ExamData[$k]['exam_status']['is_pass'] = ExamCode::CANPART;
                $ExamData[$k]['exam_status']['score'] = '0.00';
            }
        }
        return $ExamData;
    }


    /**
     * 获取考试信息
     * @param $id
     * @return array
     */
    public static function getExamInfoById($id)
    {
        # 考试信息
        $field = [
            'id',
            'yyid',
            'exam_start',
            'exam_num',
            'exam_end',
            'name',
            'introduce',
            'integral',
            'pass_condition',
            'pass_type',
            'agreement_yyid',
            'exam_time',
            'test_paper_yyid',
            'wrong_remind'
        ];

        $ExamData = TComplianceExam::whereIn('id', $id)
            ->where('status', Status::REGULAR)
            ->where('exam_type', 5)
            ->select($field)
            ->get();

        if ($ExamData->isNotEmpty()) {
            $ExamData = $ExamData->toArray();
        } else {
            $ExamData = [];
        }

        return $ExamData;
    }


    /**
     * 获取考试信息
     * @param $id
     * @return array
     */
    public static function getExamStatus($uid, $examInfo)
    {
        return QuestionnaireAnswer::where('paper_id', $examInfo['id'])
            ->where('user_id', $uid)
            ->where('type', 2)
            ->select('is_pass')
            ->first();
    }

    /**
     * @param string $yyid
     * @return TComplianceTestPaper|null
     */
    public static function getByYYID(string $yyid): ?TComplianceTestPaper
    {
        return TComplianceTestPaper::where('yyid', $yyid)
            ->where('status', Status::REGULAR)
            ->first();
    }


    /**
     * 通过yyid获取问题列表
     * @param array $YYIDS
     * @return \Hyperf\Utils\Collection
     */
    public static function getItemsByYYIDS(array $YYIDS)
    {
        return TComplianceQuestionItem::whereIn('yyid', $YYIDS)
            ->where('status', Status::REGULAR)
            ->get();
    }




    /**
     * 第一次进入 设置题目
     * @param $exam
     * @param $user
     * @param $test_id
     * @param $questionItemId
     */
    public static function addExamRandItem($examInfo, $user, $test_id, $questionItemId)
    {
        $randItem = ExamRandItem::where([
            ['status','=',Status::REGULAR],
            ['test_id','=',$test_id],
            ['exam_id','=',$examInfo['id']],
            ['uid','=',$user->id],
            ['platform','=',ExamCode::PHARMACY],
        ])->first();

        if (!$randItem) {
            $insertData = [];
            foreach ($questionItemId as $k => $item_id) {
                $insertData[$k]['uid'] = $user->id;
                $insertData[$k]['exam_id'] = $examInfo['id'];
                $insertData[$k]['test_id'] = $test_id;
                $insertData[$k]['item_id'] = $item_id;
                $insertData[$k]['platform'] = ExamCode::PHARMACY;
                $insertData[$k]['created_at'] = date('Y-m-d H:i:s');
            }
            ExamRandItem::insert($insertData);
        }
    }

    /**
     * 获取题目
     * @param $exam
     * @param $user
     * @param $test_id
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getExamRandItem($examInfo, $user, $test_id)
    {
        return ExamRandItem::where([
            ['status','=',Status::REGULAR],
            ['test_id','=',$test_id],
            ['exam_id','=',$examInfo['id']],
            ['uid','=',$user->id],
        ])->get();
    }


    public static function clearOldRequests($examId, $userId, $newRequestID, $ttl = 14400, $clearInterval = 7200)
    {
        $keyOff = sprintf("%s:%s", $examId, $userId);

        $key = self::buildUserExamParticipateKey($keyOff);

        $redis = Helper::getRedis('exam');

        $now = time();

        //添加新记录(重置过期时间)
        $redis->zAdd($key, $now, (string)$newRequestID);

        $redis->expire($key, $ttl);

        //移除超过一定时限的key
        $list = $redis->zRangeByScore($key, '-inf', strval($now - $clearInterval));

        if (!empty($list)) {
            $remKey = array_reduce($list, function ($carry, $item) use ($keyOff) {

                $keyItem = sprintf("%s:%s", $keyOff, $item);

                $carry[] = self::buildQuestionCacheKey($keyItem);

                return $carry;
            }, []);

            array_unshift($list, $key);

            //删除数据并移除入口key
            $pipeline = $redis->pipeline();

            $pipeline->del($remKey);

            call_user_func_array([$pipeline, 'zRem'], $list);

            $pipeline->exec();
        }
    }


    private static function buildUserExamParticipateKey($key): string
    {

        return sprintf("pharmacy:exam:user_exam:start:%s", $key);
    }


    public static function buildQuestionCacheKey($requestId): string
    {
        return "pharmacy:exam:questions:{$requestId}";
    }


    public static function buildExamAnswerKey($requestId): string
    {
        return "pharmacy:exam:answers:{$requestId}";
    }


    private static function buildExamRightKey($requestId): string
    {
        return "pharmacy:exam:rights:{$requestId}";
    }


    public static function cacheExamTime($examTime, $requestId, $ttl = 86400)
    {
        $key = "pharmacy:exam:timeh";

        $redis = Helper::getRedis('exam');
        $redis->hSet($key, $requestId, time().'-'.$examTime);
        $redis->expire($key, $ttl);
    }


    public static function getCacheExamTime($cacheRequestId)
    {
        $key = "pharmacy:exam:timeh";
        $redis = Helper::getRedis('exam');
        $rank = $redis->hGet($key, $cacheRequestId);
        return $rank;
    }

    /**
     * 批量缓存考试试题列表
     * @param $requestId
     * @param array $questionItemIds
     * @param int $ttl
     */
    public static function cacheExamQuestionList($requestId, array $questionItemIds, $ttl = 3600)
    {
        $redis = Helper::getRedis('exam');

        $key = self::buildQuestionCacheKey($requestId);

        $keyNsq = self::buildQuestionCacheKey($requestId.'-nsq');

        $cacheData = $redis->zCard($key);

        if ($cacheData < 1) {
            $pipeline = $redis->pipeline();

            foreach ($questionItemIds as $k => $id) {
                $pipeline->zAdd($key, $k + 1, $id);
            }

            $pipeline->expire($key, $ttl);

            $pipeline->exec();
        }

        $cacheData = $redis->zCard($keyNsq);

        if ($cacheData < 1) {
            $pipeline = $redis->pipeline();

            foreach ($questionItemIds as $k => $id) {
                $pipeline->zAdd($keyNsq, $k + 1, $id);
            }

            $pipeline->expire($keyNsq, $ttl);

            $pipeline->exec();
        }

        return $redis->zRange($key, 0, -1);
    }


    public static function getNextQuestionId(string $cacheRequestId, int $questionId): array
    {
        $redis = Helper::getRedis('exam');

        $key = self::buildQuestionCacheKey($cacheRequestId);

        if (!$redis->exists($key)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试记录不存在");
        }

        $score = $redis->zScore($key, $questionId);

        if ($score == false) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试题目不存在");
        }

        $score = (int)$score;

        $end = strval($score + 1);

        return $redis->zRangeByScore($key, "($score", $end, ['limit' => 1]);
    }


    /**
     * 根据yyid获取所有的题库
     * @param array $YYIDS
     */
    public static function getQuestionsByYYIDS(array $YYIDS)
    {
        return TComplianceQuestion::whereIn('yyid', $YYIDS)
            ->where('status', Status::REGULAR)
            ->get();
    }

    /**
     * 根据题库yyid获取问题列表
     *
     * @param array $questionYYIDS
     */
    public static function getItemsByQuestionYYIDS(array $questionYYIDS, $examItem)
    {
        if ($examItem) {
            # 随机出题 题库出题
            $qi = [];
            foreach ($examItem as $k => $item) {
                # 随机取N题
                $qi[] =  TComplianceQuestionItem::where('question_yyid', $item['yyid'])
                    ->inRandomOrder()
                    ->limit($item['item_num'])
                    ->get()
                    ->toArray();
            }

            # 拼二维
            $qis = [];
            foreach ($qi as $items) {
                foreach ($items as $itm) {
                    array_push($qis, $itm);
                }
            }
            return $qis;
        } else {
            return TComplianceQuestionItem::whereIn('question_yyid', $questionYYIDS)
                ->where('status', Status::REGULAR)
                ->orderBy('sort')
                ->get()
                ->toArray();
        }
    }


    public static function checkFinished(string $cacheRequestId): bool
    {
        $redis = Helper::getRedis('exam');

        $key = self::buildQuestionCacheKey($cacheRequestId);

        if (!$redis->exists($key)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试记录不存在");
        }

        //获取最后一条记录
        $item = $redis->zRevRange($key, 0, 0);
        if (empty($item)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试记录不存在");
        }
        $answerKey = self::buildExamAnswerKey($cacheRequestId);
        $lastId = current($item);

        return $redis->hExists($answerKey, $lastId);
    }


    public static function checkExamPassed(string $requestId, int $pass_condition, int $type, int $exid): array
    {
        $rightKey = self::buildExamRightKey($requestId);

        $redis = Helper::getRedis('exam');

        $nums = (int)$redis->sCard($rightKey);

        $rightData = $redis->sMembers($rightKey);

        # Todo 【pass_type=1】 如果缓存中答对的题目数量大于考试配置数量 则通过
        if ($type == 1) {
            if ($nums >= $pass_condition) {
                return ['status'=>true,'fraction'=>0,'type'=>$type, 'nums'=>$nums];
            } else {
                return ['status'=>false,'fraction'=>0,'type'=>$type, 'nums'=>$nums];
            }
        } else {
            # Todo  【pass_type=2】 如果缓存中答对的题目总分大于考试配置总分则通过
            # 根据考试id 题目id 查询该提分数
            # 试卷 考试 动态分数表
            $fractionData = self::getTestExamFraction($exid);

            if ($fractionData) {
                $fractionData = json_decode(json_encode($fractionData), true);
            }

            # 获取所有答对题
            #   --- 1顺序出题 题库题目出题   随机出题 题库出题
            #   --- 2 随机出题 题库出题
            #   --- 3 随机出题 题目出题
            $fraction = 0;

            if ($fractionData[0]['type'] == 1) {
                foreach ($fractionData as $item) {
                    if (in_array($item['item_id'], $rightData)) {
                        $fraction += $item['fraction'];
                    }
                }
            } elseif ($fractionData[0]['type'] == 2) {    # 题库+随机
                # 查看 答对的每一道题属于某题库
                $fractionData = self::getTestPaperQuestionItemFraction($exid, $rightData);

                $fractionData = $fractionData->toArray();

                $fractionData = array_column($fractionData, 'fraction');

                $fraction = array_sum($fractionData);
            } else {  # 题目+随机
                # 获取每题分数
                $fractionData = self::getTestExamFraction($exid);

                $fractionData = $fractionData->toArray();

                $fraction = count($rightData) * $fractionData['fraction'];
            }

            # 若考试设定为0分可通过 则直接通过
            if ($pass_condition == 0) {
                return ['status' => true, 'fraction' => $fraction, 'type' => $type, 'nums'=>$nums];
            } else {  # 否则 判断是否通过分数线
                # 获取该考试下所有缓存
                if ($rightData) {
                    if ($fraction >= $pass_condition) {
                        return ['status' => true, 'fraction' => $fraction, 'type' => $type, 'nums'=>$nums];
                    } else {
                        return ['status' => false, 'fraction' => $fraction, 'type' => $type, 'nums'=>$nums];
                    }
                } else {
                    return ['status' => false, 'fraction' => 0, 'type' => $type, 'nums'=>$nums];
                }
            }
        }
    }



    /**
     * 获取指定考试下所有题目的总分
     * @param $examid
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function updateStatusByexamId($examPassed, $uid, $examid, $status)
    {
        $is_pass = 0;

        if ($status == 2) {
            $is_pass = 1;
        }

        # 获取用户关联门店
        $UserStoreID = UserClerk::where('user_id', $uid)->select('store_id')->first();

        # 查询该考试是否有考过
        $find = QuestionnaireAnswer::where([['user_id', $uid],['paper_id', $examid]])->first();

        if ($find) {
            QuestionnaireAnswer::where([['user_id', $uid],['paper_id', $examid]])
                ->update(['exam_num'=>$find->exam_num + 1, 'score'=>$examPassed['fraction'], 'is_pass'=>$is_pass]);
        } else {
            # 更新通过状态
            return QuestionnaireAnswer::insertGetId([
                'store_id' => $UserStoreID ? $UserStoreID->store_id : 0,
                'user_id' => $uid,
                'paper_id' => $examid,
                'score' => $examPassed['fraction'],
                'is_pass' => $is_pass,
                'exam_num' => 1,
                'type' => QuestionnaireAnswer::EXAM_TYPE,
                'created_at' => date('Y-m-d H:i:s', time())
            ]);
        }
    }


    /**
     * 获取每道题的分数
     * @param $examid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getTestExamFraction($examid)
    {
        return TComplianceTestExamFraction::where(['status'=>Status::REGULAR,'exam_id'=>$examid])
            ->select(["item_id","question_bank_id","fraction","type"])->get();
    }



    /**
     * 获取每道题的分数
     * @param $examid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getTestPaperQuestionItemFraction($examid, $rightData)
    {
        $DB = self::getDBName();

        return Db::table($DB.'.t_compliance_question_items as a')
            ->join($DB.".t_compliance_question as b", "a.question_yyid", "=", "b.yyid")
            ->join(
                $DB.".t_compliance_test_exam_fraction as c",
                "b.id",
                "=",
                "c.question_bank_id"
            )
            ->whereIn('a.id', $rightData)
            ->where('c.exam_id', $examid)
            ->select('c.fraction')
            ->get();
    }


    private static function getDBName()
    {

        $Db = 'youyao_qa';

        switch (env('APP_ENV')) {
            case 'testing':
                $Db = 'youyao_qa';
                break;
            case 'staging':
                $Db = 'youyao_prod';
                break;
            case 'prod':
                $Db = 'youyao_prod';
                break;
        }

        return $Db;
    }


    public static function checkRequestAndQuestion(string $requestId, int $questionId)
    {
        $redis = Helper::getRedis('exam');

        $key = self::buildQuestionCacheKey($requestId);

        if (!$redis->exists($key)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试记录不存在");
        }

        # 有考试时间才自动提交
        $rank = $redis->zRank($key, $questionId);
        if ($rank === false) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试题目不存在");
        }
    }


    public static function getItemsById($questionId): ?TComplianceQuestionItem
    {
        return TComplianceQuestionItem::where('id', $questionId)
            ->where('status', Status::REGULAR)
            ->first();
    }


    /**
     * 缓存考试题目
     * @param string $cacheRequestId
     * @param TComplianceQuestionItem $question
     * @param bool $isRight
     * @param array $optionIds
     * @param string $content
     * @param int $ttl
     */
    public static function cacheExamAnswer(
        string $cacheRequestId,
        TComplianceQuestionItem $question,
        bool $isRight,
        array $optionIds = [],
        string $content = "",
        int $ttl = 3600,
        $openId = ""
    ) {
        $redis = Helper::getRedis('exam');

        //缓存答题选项和内容
        $answerKey = self::buildExamAnswerKey($cacheRequestId);

        $answerKeyNsq = self::buildExamAnswerKey($cacheRequestId.'-nsq');

        $content = [
            'options' => $optionIds,
            'content' => (string)$content,
            'is_right' => $isRight ? 1 : 0,
            'type' => $question->type,
            'openid' => (string) $openId
        ];
        $redis->hSet(
            $answerKey,
            (string)$question->id,
            json_encode($content, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
        );

        $redis->expire($answerKey, $ttl);

        $redis->hSet(
            $answerKeyNsq,
            (string)$question->id,
            json_encode($content, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
        );
        $redis->expire($answerKeyNsq, $ttl);

        //处理正确数量统计列表
        $rightKey = self::buildExamRightKey($cacheRequestId);

        $rightKeyNsq = self::buildExamRightKey($cacheRequestId.'-nsq');
        $redis->sRem($rightKey);
        if ($isRight) {
            $redis->sAdd($rightKey, $question->id);
        } else {
            if ($redis->sIsMember($rightKey, $question->id)) {
                $redis->sRem($rightKey, $question->id);
            }
        }
        $redis->expire($rightKey, $ttl);

        $redis->sRem($rightKeyNsq);
        if ($isRight) {
            $redis->sAdd($rightKeyNsq, $question->id);
        } else {
            if ($redis->sIsMember($rightKeyNsq, $question->id)) {
                $redis->sRem($rightKeyNsq, $question->id);
            }
        }
        $redis->expire($rightKeyNsq, $ttl);
    }



    /**
     * 获取用户考试参与最终记录
     * @param TComplianceExam $exam
     * @return TComplianceUserExam|null
     */
    public static function getExamLast($uid, $exam_yyid): ?TComplianceUserExam
    {
        return TComplianceUserExam::where([
            'uid' => $uid,
            'platform' => ExamCode::PHARMACY,
            'exam_yyid' => $exam_yyid
        ])->orderByDesc('id')
            ->limit(1)
            ->first();
    }

    public static function clearUserExams($uid, $exam_yyid)
    {
        TComplianceUserExam::where([
            'uid' => $uid,
            'exam_yyid' =>$exam_yyid,
            'platform' => ExamCode::PHARMACY
        ])->update([
            'status' => ExamCode::NOTPASS,
            'modify_time' => date('Y-m-d H:i:s')
        ]);
    }


    public static function addUserExam($uid, $exam_yyid, bool $isPassed = true, $openId = ''): TComplianceUserExam
    {
        $now = date('Y-m-d H:i:s');
        $o = new TComplianceUserExam();
        $o->exam_yyid = $exam_yyid;
        $o->uid = $uid;
        $o->platform = ExamCode::PHARMACY;
        $o->openid = $openId ? $openId :  Db::raw("null");
        $o->yyid = Helper::yyidGenerator();
        $o->status = $isPassed ? ExamCode::REGULAR : ExamCode::NOTPASS;
        $o->pass_time = $now;
        $o->created_time = $o->modify_time = $now;
        $o->save();

        return $o;
    }


    public static function getQuestionIds(string $cacheRequestId): array
    {
        $key = self::buildQuestionCacheKey($cacheRequestId);
        $redis = Helper::getRedis('exam');
        return $redis->zRangeByScore($key, '-inf', 'inf');
    }


    public static function getAnswers($requestId): array
    {
        $key = self::buildExamAnswerKey($requestId);
        $redis = Helper::getRedis('exam');
        return $redis->hGetAll($key);
    }
}
