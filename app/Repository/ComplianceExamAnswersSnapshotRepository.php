<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Constants\ExamCode;
use App\Model\Pharmacy\User;
use App\Model\Qa\TComplianceExamAnswersSnapshot;
use Hyperf\Utils\Collection;

class ComplianceExamAnswersSnapshotRepository
{
    /**
     * 获取用户考试回顾记录
     * @param Users $users
     * @return Collection
     */
    public static function getUserExamSnapshot(User $users, $exam): Collection
    {
        return TComplianceExamAnswersSnapshot::where('exam_id', $exam['id'])
            ->where('user_id', $users->id)
            ->where('platform', ExamCode::PHARMACY)
            ->get();
    }

    public static function clearExam($exam, User $user)
    {
        return TComplianceExamAnswersSnapshot::where('exam_id', $exam->id)
            ->where('user_id', $user->uid)
            ->where('platform', ExamCode::PHARMACY)
            ->delete();
    }
}
