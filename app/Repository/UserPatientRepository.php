<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Constants\Role;
use App\Model\Pharmacy\User;
use App\Model\Pharmacy\UserPatient;
use App\Model\Pharmacy\UserRole;
use Hyperf\Utils\Collection;

class UserPatientRepository
{
    public static function getUserPatientByUserId($userId)
    {
        return UserPatient::select(['nick_name', 'header_url'])
            ->where('user_id', $userId)
            ->first();
    }

    public static function addUserPatient($userId)
    {
        $data = [
            'user_id'=>$userId,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s'),
        ];
        return UserPatient::insert($data);
    }

    public static function addPatientByModel(UserPatient $patient)
    {
        return $patient->save();
    }

    public static function getUserPatientByUserIds(array $patientIds): Collection
    {
        return UserPatient::query()
            ->join('user as u', 'u.id', '=', 'user_patient.user_id')
            ->whereIn('user_patient.id', $patientIds)
            ->where('user_patient.is_delete', Delete::UNDELETED)
            ->where('u.is_delete', Delete::UNDELETED)
            ->select(['user_patient.id as patient_id', 'u.*'])
            ->get();
    }

    public static function getPatientUserRole($patientId)
    {
        return UserPatient::query()
            ->join('user_role as ur', 'ur.user_id', '=', 'user_patient.user_id')
            ->where('user_patient.id', $patientId)
            ->where('user_patient.is_delete', Delete::UNDELETED)
            ->where('ur.role', Role::ROLE_PATIENT)
            ->where('ur.is_delete', Delete::UNDELETED)
            ->select(['ur.*'])
            ->first();
    }
}
