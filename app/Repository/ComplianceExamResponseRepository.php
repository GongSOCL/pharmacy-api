<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Constants\ExamCode;
use App\Constants\Status;
use App\Model\Pharmacy\User;
use App\Model\Qa\TComplianceExamResponse;
use Hyperf\Utils\Collection;

class ComplianceExamResponseRepository
{

    public static function getExamResponse($examInfo, User $user): Collection
    {
        return TComplianceExamResponse::where('exam_yyid', $examInfo['yyid'])
            ->where('uid', $user->id)
            ->where('platform', ExamCode::PHARMACY)
            ->where('status', Status::REGULAR)
            ->get();
    }
}
