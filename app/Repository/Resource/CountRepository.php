<?php

declare(strict_types=1);

namespace App\Repository\Resource;

use App\Helper\Helper;
use App\Model\Pharmacy\UserResourceClick;

class CountRepository
{
    /**
     * @param $resourceId
     * @param $userId
     * @param $partId   1 首页，2 培训中心
     * @param $inviteId
     * @param int $resourceType 1 普通资源， 2 问卷
     */
    public static function recodeUserClick($resourceId, $userId, $partId, $resourceType = UserResourceClick::RESOURCE_TYPE_NORMAL)
    {
        UserResourceClick::insert([
            'resource_id' => $resourceId,
            'user_id' => $userId,
            'part' => $partId,
            'created_at' => Helper::getDate(),
            'resource_type' => $resourceType
        ]);
    }

    /**
     * @param $resourceId
     * @return int
     */
    public static function getResourceClickCount($resourceId)
    {
        return UserResourceClick::where([
            'resource_id' => $resourceId,
            'resource_type' => UserResourceClick::PART_1
        ])->count();
    }

    /**
     * @param $resourceId
     * @return int
     */
    public static function getQuestClickCount($resourceId)
    {
        return UserResourceClick::where([
            'resource_id' => $resourceId,
            'resource_type' => UserResourceClick::PART_2
        ])->count();
    }
}
