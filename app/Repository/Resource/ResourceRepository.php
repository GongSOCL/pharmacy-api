<?php

declare(strict_types=1);

namespace App\Repository\Resource;

use App\Constants\Delete;
use App\Model\Pharmacy\Resource;
use Hyperf\Utils\Collection;

class ResourceRepository
{
    public static function getByIds(array $ids): Collection
    {
        return Resource::query()
            ->whereIn('id', $ids)
            ->where('is_deleted', Delete::UNDELETED)
            ->get();
    }

    public static function searchWithLimit($keywords = "", $type = 0, $limit = 10): Collection
    {
        return Resource::query()
            ->when($keywords != '', function ($query) use($keywords) {
                $query->where('title', 'like', "%{$keywords}%");
            })->when($type > 0, function ($query) use($type) {
                $query->where('type', $type);
            })->where('is_deleted', Delete::UNDELETED)
            ->limit($limit)
            ->get();
    }

}
