<?php

declare(strict_types=1);

namespace App\Repository;

use App\Model\Pharmacy\UserClerkAgent;
use Carbon\Carbon;

class UserClerkAgentRepository
{
    public static function addClerkAgent($userId, $clerkId, $agentId)
    {
        $userClerkModel = new UserClerkAgent();
        $userClerkModel->setAttribute('user_id', $userId);
        $userClerkModel->setAttribute('user_clerk_id', $clerkId);
        $userClerkModel->setAttribute('agent_id', $agentId);
        $userClerkModel->setAttribute('created_at', Carbon::now());
        $userClerkModel->setAttribute('updated_at', Carbon::now());
        $userClerkModel->save();
    }

    public static function batchAddClerkAgent($userId, $clerkId, $agentIds)
    {
        if (empty($agentIds)) {
            return true;
        }
        foreach ($agentIds as $agentId) {
            $add = [
                'user_id'=>$userId,
                'user_clerk_id'=>$clerkId,
                'agent_id'=>$agentId,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ];
            return UserClerkAgent::query()->updateOrInsert([
                'user_clerk_id'=>$clerkId,
                'agent_id'=>$agentId,
            ], $add);
        }
    }
}
