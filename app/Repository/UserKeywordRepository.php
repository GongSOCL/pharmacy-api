<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\UserKeyword;

class UserKeywordRepository
{
    const KEYWORD_FIELDS = [
        'keyword'
    ];

    public static function getKeywords($userId, $limit = 10)
    {
        return UserKeyword::query()
            ->select(self::KEYWORD_FIELDS)
            ->where('user_id', $userId)
            ->where('is_delete', Delete::UNDELETED)
            ->orderByDesc('updated_at')
            ->limit($limit)
            ->get();
    }

    public static function batchAddKeyword($userId, $keywords, $type = 0)
    {
        $addList = [];
        if (is_string($keywords)) {
            $keywords = trim($keywords);
            if (!empty($keywords)) {
                $addList[] = [
                    'user_id'=>$userId,
                    'keyword'=>$keywords,
                    'type'=>$type,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'),
                ];
            }
        } else {
            foreach ($keywords as $keyword) {
                $addList[] = [
                    'user_id'=>$userId,
                    'keyword'=>$keyword,
                    'type'=>$type,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'),
                ];
            }
        }
        $userKeywordModel = UserKeyword::query();
        foreach ($addList as $add) {
            $attributes = [
                'user_id'=>$add['user_id'],
                'keyword'=>$add['keyword'],
                'is_delete'=>Delete::UNDELETED
            ];
            if (!$userKeywordModel->where($attributes)->exists()) {
                return $userKeywordModel->insert(array_merge($attributes, $add));
            }
            unset($add['created_at']);
            return (bool) $userKeywordModel->take(1)->update($add);
        }
    }

    public static function deleteKeyword($userId)
    {
        UserKeyword::where('user_id', $userId)
            ->where('is_delete', Delete::UNDELETED)
            ->update([
                'is_delete'=>Delete::DELETED
            ]);
    }
}
