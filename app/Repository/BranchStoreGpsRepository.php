<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\BranchStore;
use App\Model\Pharmacy\BranchStoreGps;

class BranchStoreGpsRepository
{
    const STORE_GPS_FIELDS =[
        'id',
        'store_id',
        'longitude',
        'latitude',
    ];
    public static function getListByStoreIds($storeIds)
    {
        return BranchStoreGps::query()
            ->select(self::STORE_GPS_FIELDS)
            ->where('status', 1)
            ->whereIn('store_id', $storeIds)
            ->orderBy('id', 'desc')
            ->get();
    }


    public static function getStoreGps(BranchStore $store): ?BranchStoreGps
    {
        return BranchStoreGps::query()
            ->where('store_id', $store->id)
            ->where('status', 1)
            ->first();
    }
}
