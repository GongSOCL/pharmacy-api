<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\OrderUser;

class OrderUserRepository
{
    public static function getOrderUserClerks($orderIds)
    {
        return OrderUser::query()
            ->select(['id', 'order_id', 'user_type'])
            ->whereIn('order_id', $orderIds)
            ->where('user_type', 4)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }
}
