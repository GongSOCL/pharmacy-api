<?php

declare(strict_types=1);
namespace App\Repository;

use App\Model\Pharmacy\BizAgentRel;
use Carbon\Carbon;

class BizAgentRelRepository
{
    public static function addBizAgent($bizId, $bizType, $agentId)
    {
        $bizExist = BizAgentRel::query()
            ->where('biz_id', $bizId)
            ->where('biz_type', $bizType)
            ->where('agent_id', $agentId)
            ->exists();
        if (!$bizExist) {
            $bizObj = new BizAgentRel();
            $bizObj->setAttribute('biz_id', $bizId);
            $bizObj->setAttribute('biz_type', $bizType);
            $bizObj->setAttribute('agent_id', $agentId);
            $bizObj->setAttribute('created_at', Carbon::now());
            $bizObj->setAttribute('updated_at', Carbon::now());
            $bizObj->save();
        }
    }

    public static function batchAddBizAgent($bizId, $bizType, $agentIds)
    {
        if (empty($agentIds)) {
            return true;
        }
        foreach ($agentIds as $agentId) {
            if (empty($agentId)) {
                continue;
            }
            self::addBizAgent($bizId, $bizType, $agentId);
        }
        return true;
    }
}
