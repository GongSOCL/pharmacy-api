<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\DoctorProduct;

class DoctorProductRepository
{

    public static function getProductIdsByDoctorId($doctorId)
    {
        return DoctorProduct::query()
            ->select(['id', 'yy_doctor_id', 'yy_product_id'])
            ->where('yy_doctor_id', $doctorId)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }
}
