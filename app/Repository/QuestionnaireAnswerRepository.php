<?php

declare (strict_types=1);
namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\QuestionnaireAnswer;
use Carbon\Carbon;

class QuestionnaireAnswerRepository
{
    public static function addQuestionnaireAnswer($storeId, $userId, $paperId)
    {
        $questionnaireModel = new QuestionnaireAnswer();
        $questionnaireModel->setAttribute('store_id', $storeId);
        $questionnaireModel->setAttribute('user_id', $userId);
        $questionnaireModel->setAttribute('paper_id', $paperId);
        $questionnaireModel->setAttribute('created_at', Carbon::now());
        $questionnaireModel->setAttribute('updated_at', Carbon::now());
        $questionnaireModel->save();
        return $questionnaireModel;
    }

    public static function handleQuestRelAgentChunk($func, $pageSize = 30)
    {
        return QuestionnaireAnswer::query()
            ->where('is_delete', Delete::UNDELETED)
            ->orderBy('id')
            ->chunk($pageSize, $func);
    }
}
