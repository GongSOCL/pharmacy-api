<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Constants\Store;
use App\Model\Pharmacy\BranchStore;
use App\Model\Pharmacy\BranchStoreProduct;
use App\Model\Pharmacy\City;
use Hyperf\Database\Query\Builder;
use Hyperf\Utils\Collection;
use function _HumbugBoxda2413717501\React\Promise\Stream\first;

class BranchStoreRepository
{

    const SEARCH_STORE_FIELDS = [
        'id',
        'yy_store_id',
        'area_id',
        'pro_id',
        'city_id',
        'main_store_id',
        'store_name_alias',
        'store_name',
        'location',
        'phone',
    ];


    public static function getListByIds($ids)
    {
        return BranchStore::query()
            ->select(self::SEARCH_STORE_FIELDS)
            ->with(['products'=>function ($query) {
                $query->select(['store_id', 'yy_product_id'])
                    ->where('is_delete', Delete::UNDELETED);
            }])
            ->whereIn('id', $ids)
            ->where('status', Store::STATUS_NORM)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }

    public static function getStoreListByMainId($mainId)
    {
        return BranchStore::query()
            ->select(self::SEARCH_STORE_FIELDS)
            ->where('main_store_id', $mainId)
            ->where('is_delete', Delete::UNDELETED)
            ->where('status', Store::STATUS_NORM)
            ->get();
    }

    public static function getStoreListByPageAndMainId($mainId, $pageSize, $keywords = '')
    {
        return BranchStore::query()
            ->select(self::SEARCH_STORE_FIELDS)
            ->where('main_store_id', $mainId)
            ->where('is_delete', Delete::UNDELETED)
            ->where('status', Store::STATUS_NORM)
            ->when($keywords != '', function ($q) use ($keywords) {
                $q->where('store_name', 'like', "%{$keywords}%");
            })->paginate($pageSize);
    }

    public static function getStoreInfoById($storeId):?BranchStore
    {
        return BranchStore::query()
            ->select([
                'id',
                'yy_store_id',
                'city_id',
                'main_store_id',
                'store_name',
                'store_name_alias',
                'location',
                'phone',
                'status',
            ])
            ->where('id', $storeId)
            ->first();
    }

    public static function getStoreInfoByYyStoreId($yyStoreId):?BranchStore
    {
        return BranchStore::query()
            ->select([
                'id',
                'yy_store_id',
                'main_store_id',
                'store_name',
                'store_name_alias',
                'location'
            ])
            ->where('yy_store_id', $yyStoreId)
            ->where('is_delete', Delete::UNDELETED)
            ->first();
    }


    public static function geoStoreListByIdsAndProductIds($storeIds, $productIds)
    {
        return BranchStore::query()
            ->select(self::SEARCH_STORE_FIELDS)
            ->when($productIds, function ($query) use ($productIds) {
                $query->whereIn('id', function ($query) use ($productIds) {
                    $query->select('store_id')
                        ->from('branch_store_product')
                        ->whereIn('yy_product_id', $productIds)
                        ->where('is_delete', Delete::UNDELETED);
                });
            })
            ->whereIn('id', $storeIds)
            ->where('status', Store::STATUS_NORM)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }

    public static function geoStoreListByCityIdAndProductIds($cityId, $productIds)
    {
        return BranchStore::query()
            ->from('branch_store as s')
            ->select(['s.id',
                's.yy_store_id',
                's.area_id',
                's.pro_id',
                's.city_id',
                's.main_store_id',
                's.store_name_alias',
                's.store_name',
                's.location',
                's.phone',])
            ->join('branch_store_gps as g', 's.id', '=', 'g.store_id')
            ->distinct()
            ->when($productIds, function ($query) use ($productIds) {
                $query->whereIn('s.id', function ($query) use ($productIds) {
                    $query->select('store_id')
                        ->from('branch_store_product')
                        ->whereIn('yy_product_id', $productIds)
                        ->where('is_delete', Delete::UNDELETED);
                });
            })
            ->where('s.city_id', $cityId)
            ->where('s.status', Store::STATUS_NORM)
            ->where('s.is_delete', Delete::UNDELETED)
            ->where('g.status', 1)
            ->get();
    }

    public static function searchStoreList($cityId, $productIds, $keyword, $pageSize)
    {
        return BranchStore::query()
            ->select(self::SEARCH_STORE_FIELDS)
            ->with(['products'=>function ($query) {
                $query->select(['store_id', 'yy_product_id'])
                    ->where('is_delete', Delete::UNDELETED);
            }])
            ->when($cityId, function ($query) use ($cityId) {
                $query->where('city_id', $cityId);
            })
            ->when($productIds, function ($query) use ($productIds) {
                $query->whereIn("id", function ($query) use ($productIds) {
                    $query->select('store_id')
                        ->from('branch_store_product')
                        ->whereIn('yy_product_id', $productIds)
                        ->where('is_delete', Delete::UNDELETED);
                });
            })
            ->when($keyword, function ($query) use ($keyword) {
                $query->where('store_name_alias', 'like', '%'.$keyword.'%')
                    ->orWhere('store_name', 'like', '%'.$keyword.'%')
                    ->orWhere('location', 'like', '%'.$keyword.'%');
            })
            ->where('status', Store::STATUS_NORM)
            ->where('is_delete', Delete::UNDELETED)
            ->paginate($pageSize);
    }

    public static function getStoreInfoByIds(array $storeIds): Collection
    {
        return BranchStore::query()
            ->whereIn('id', $storeIds)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }

    public static function getStoreInfoByYyStoreIds(array $ids)
    {
        return BranchStore::query()
            ->whereIn('yy_store_id', $ids)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }


    public static function couponScopeStoreList($scope, $couponId, $yyProductId, $pageSize = 10, $currentPage = 1)
    {
        $res = [];
        switch ($scope) {
            case 0:
                $res = BranchStore::query()
                    ->select(self::SEARCH_STORE_FIELDS)
                    ->where('is_delete', Delete::UNDELETED)
                    ->where('status', Store::STATUS_NORM)
                    ->whereIn('id', function ($query) use ($yyProductId) {
                        /** @var Builder $query */
                        $query->from('branch_store_product')
                            ->select(['store_id'])
                            ->where('yy_product_id', $yyProductId)
                            ->where('is_delete', Delete::UNDELETED);
                    })
                    ->paginate($pageSize, ['*'], '', $currentPage);
                break;
            case 1:
                $mainStoreIds = CouponMainStoreRepository::getMainStoreListByCouponId($couponId)
                    ->pluck('main_store_id');
                $res = BranchStore::query()
                    ->select(self::SEARCH_STORE_FIELDS)
                    ->whereIn('main_store_id', $mainStoreIds)
                    ->where('status', Store::STATUS_NORM)
                    ->where('is_delete', Delete::UNDELETED)
                    ->whereIn('id', function ($query) use ($yyProductId) {
                        /** @var Builder $query */
                        $query->from('branch_store_product')
                            ->select(['store_id'])
                            ->where('yy_product_id', $yyProductId)
                            ->where('is_delete', Delete::UNDELETED);
                    })
                    ->paginate($pageSize, ['*'], '', $currentPage);
                break;
            case 2:
                $res = BranchStore::query()
                    ->select(self::SEARCH_STORE_FIELDS)
                    ->where('status', Store::STATUS_NORM)
                    ->where('is_delete', Delete::UNDELETED)
                    ->whereIn('id', function ($query) use ($couponId) {
                        /** @var Builder $query */
                        $query->from('coupon_branch_store')
                            ->select(['store_id'])
                            ->where('coupon_id', $couponId)
                            ->where('is_delete', Delete::UNDELETED);
                    })
                    ->whereIn('id', function ($query) use ($yyProductId) {
                        /** @var Builder $query */
                        $query->from('branch_store_product')
                            ->select(['store_id'])
                            ->where('yy_product_id', $yyProductId)
                            ->where('is_delete', Delete::UNDELETED);
                    })
                    ->paginate($pageSize, ['*'], '', $currentPage);
                break;
        }
        return $res;
    }

    public static function getStoreByIdsWithCity(array $ids)
    {
        return BranchStore::query()
            ->leftJoin('city as c', 'c.id', '=', 'branch_store.city_id')
            ->whereIn('branch_store.id', $ids)
            ->where('branch_store.is_delete', Delete::UNDELETED)
            ->select(['branch_store.*', 'c.city_name'])
            ->get();
    }
}
