<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Clerk;
use App\Model\Pharmacy\ClerkApply;

class ClerkApplyRepository
{
    const APPLY_FIELDS = [
        'id',
        'user_id',
        'city_id',
        'main_store_id',
        'store_id',
        'real_name',
        'phone',
        'role',
        'position',
        'status',
        'sex',
    ];
    public static function existApply($userId)
    {
        $status = Clerk::STATUS_AUDITING;
        return ClerkApply::query()
            ->where('user_id', $userId)
            ->where('status', $status)
            ->exists();
    }

    public static function addApply(ClerkApply $apply)
    {
        return $apply->save();
    }

    public static function getInfoById($userId):?ClerkApply
    {
        $status = Clerk::STATUS_AUDITING;
        return ClerkApply::query()
            ->select(self::APPLY_FIELDS)
            ->where('user_id', $userId)
            ->where('status', $status)
            ->first();
    }
}
