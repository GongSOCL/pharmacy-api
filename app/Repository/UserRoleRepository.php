<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\User;
use App\Model\Pharmacy\UserRole;

class UserRoleRepository
{
    public static function getUserRole(User $user, $roleId): ?UserRole
    {
        return UserRole::query()
            ->where('user_id', $user->id)
            ->where('role', $roleId)
            ->where('is_delete', Delete::UNDELETED)
            ->first();
    }

    public static function getUserRoleByUserId($userId)
    {
        return UserRole::where('user_id', $userId)
            ->select(['id', 'user_id', 'role'])
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }

    public static function addUserRole($userId, $role)
    {
        if (!UserRole::where('user_id', $userId)
            ->where('role', $role)->exists()) {
            $data = [
                'user_id'=>$userId,
                'role'=>$role,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
            ];
            return UserRole::insert($data);
        }
        return true;
    }

    public static function getUserSpecialRole($userId, $roleId): ?UserRole
    {
        return UserRole::query()
            ->where('user_id', $userId)
            ->where('role', $roleId)
            ->where('is_delete', Delete::UNDELETED)
            ->first();
    }


    /**
     * @param $UserId
     * @param $RoleTypeId
     * @return int
     */
    public static function getUserRolePID($UserId, $RoleTypeId): int
    {
        $info = UserRole::query()
            ->where('user_id', $UserId)
            ->where('role', $RoleTypeId)
            ->where('is_delete', Delete::UNDELETED)
            ->select('id')
            ->first();
        return $info ? $info->id : 0;
    }
}
