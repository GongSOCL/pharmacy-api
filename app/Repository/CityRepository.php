<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\City;
use Hyperf\Utils\Collection;

class CityRepository
{
    const CITY_FIELDS =[
        'id',
        'city_name'
    ];
    public static function getCityList()
    {
        return City::query()
            ->select(self::CITY_FIELDS)
            ->get();
    }

    public static function getCityListByProId($proId)
    {
        return City::query()
            ->select(['id', 'city_name as name'])
            ->where('province_id', $proId)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }

    public static function getCityListByPage($pageSize = 20)
    {
        return City::query()
            ->select(self::CITY_FIELDS)
            ->paginate($pageSize);
    }

    public static function getInfoById($cityId): ?City
    {
        return City::query()
            ->select(self::CITY_FIELDS)
            ->where('id', $cityId)
            ->where('is_delete', Delete::UNDELETED)
            ->first();
    }

    public static function getInfoByName($cityName):?City
    {
        $cityNameAlias = mb_substr($cityName, 0, -1);
        return City::query()
            ->where('city_name', $cityName)
            ->orWhere('city_name', $cityNameAlias)
            ->first();
    }


    public static function getByIds(array $ids): Collection
    {
        return City::query()
        ->select(self::CITY_FIELDS)
        ->whereIn('id', $ids)
        ->where('is_delete', Delete::UNDELETED)
        ->get();
    }
}
