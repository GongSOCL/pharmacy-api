<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\CouponConstant;
use App\Constants\Delete;
use App\Model\Pharmacy\Coupon;
use App\Model\Pharmacy\CouponDetail;
use Carbon\Carbon;
use Hyperf\Database\Query\Builder;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class CouponDetailRepository
{
    const COUPON_FIELDS = [
        'id',
        'coupon_id',
        'coupon_num',
        'is_use',
        'get_time',
        'deadline_time',
    ];

    public static function getCoupon(
        $couponId,
        $couponNum,
        $patientUserId,
        $deadlineTime
    ) {
        $time = date('Y-m-d H:i:s', time());
        $model = new CouponDetail();
        $model->coupon_id = $couponId;
        $model->coupon_num = $couponNum;
        $model->patient_user_id = $patientUserId;
        $model->get_time = time();
        $model->deadline_time = $deadlineTime;
        $model->created_at = $time;
        $model->updated_at = $time;
        $model->save();
    }

    public static function getCouponDetailListByNums($couponNums)
    {
        if (empty($couponNums)) {
            return new Collection();
        }
        return CouponDetail::query()
            ->select(self::COUPON_FIELDS)
            ->where('is_use', CouponConstant::COUPON_USE_NO)
            ->where('deadline_time', '>=', time())
            ->whereIn('coupon_num', $couponNums)
            ->get();
    }

    public static function getCouponDetailById($detailId):?CouponDetail
    {
        return CouponDetail::query()
            ->select(self::COUPON_FIELDS)
            ->where('id', $detailId)
            ->first();
    }
    public static function existGetCoupon($userId, $couponId)
    {
        return CouponDetail::query()
            ->where('patient_user_id', $userId)
            ->where('coupon_id', $couponId)
            ->exists();
    }

    public static function getCouponListByUserIdAndCouponIds($userId, $couponIds)
    {
        return CouponDetail::query()
            ->select(self::COUPON_FIELDS)
            ->where('patient_user_id', $userId)
            ->whereIn('coupon_id', $couponIds)
            ->get();
    }

    public static function getUserCouponList($type, $patientUserId, $pageSize)
    {
        $list = [];
        $time = strtotime(date('Y-m-d H:i:s'));
        switch ($type) {
            case 0:
                $list = CouponDetail::query()
                    ->select(self::COUPON_FIELDS)
                    ->where('patient_user_id', $patientUserId)
                    ->where('is_use', CouponConstant::COUPON_USE_NO)
                    ->where('deadline_time', '>=', $time)
                    ->where('is_delete', Delete::UNDELETED)
                    ->orderBy('deadline_time')
                    ->paginate($pageSize);
                break;
            case 1:
                $list = CouponDetail::query()
                    ->select(self::COUPON_FIELDS)
                    ->where('patient_user_id', $patientUserId)
                    ->where('is_use', CouponConstant::COUPON_USE_YES)
                    ->where('is_delete', Delete::UNDELETED)
                    ->orderBy('deadline_time')
                    ->paginate($pageSize);
                break;
            case 2:
                $list = CouponDetail::query()
                    ->select(self::COUPON_FIELDS)
                    ->where('patient_user_id', $patientUserId)
                    ->where('is_use', CouponConstant::COUPON_USE_NO)
                    ->where('deadline_time', '<', $time)
                    ->where('is_delete', Delete::UNDELETED)
                    ->orderBy('deadline_time')
                    ->paginate($pageSize);
                break;
        }
        return $list;
    }

    public static function getCanUseCouponList($patientUserId, $main_store_id, $store_id, $pageSize)
    {
        $bindings = self::getBuilder($patientUserId, $main_store_id, $store_id)->getQuery();
        $sql = self::getBuilder($patientUserId, $main_store_id, $store_id)->toSql();
        return CouponDetail::query()
            ->select(['a.*'])
            ->mergeBindings($bindings)
            ->from(Db::raw("($sql) as a"))
            ->orderBy('a.deadline_time')
            ->paginate($pageSize);
    }

    public static function getUserCouponNum($patientUserId)
    {
        $time = strtotime(date('Y-m-d H:i:s'));
        return CouponDetail::query()
            ->where('patient_user_id', $patientUserId)
            ->where('is_use', CouponConstant::COUPON_USE_NO)
            ->where('deadline_time', '>=', $time)
            ->where('is_delete', Delete::UNDELETED)
            ->count();
    }

    public static function getCanUseCouponNum($patientUserId, $mainStoreId, $storeId)
    {
        $bindings = self::getBuilder($patientUserId, $mainStoreId, $storeId)->getQuery();
        $sql = self::getBuilder($patientUserId, $mainStoreId, $storeId)->toSql();
        return CouponDetail::query()
            ->mergeBindings($bindings)
            ->from(Db::raw("($sql) as a"))
            ->count();
    }


    public static function updateCouponUseStatus($ids)
    {
        if (empty($ids)) {
            return false;
        }
        return CouponDetail::query()
            ->whereIn('id', $ids)
            ->where('is_use', CouponConstant::COUPON_USE_NO)
            ->where('is_delete', Delete::UNDELETED)
            ->update([
                'use_time'=>Carbon::now()->getTimestamp(),
                'is_use'=>CouponConstant::COUPON_USE_YES,
                'updated_at'=>Carbon::now()
            ]);
    }

    private static function getBuilder($userId, $mainStoreId, $storeId)
    {
        $time = strtotime(date('Y-m-d H:i:s'));
        $selectFields = [
            'a.id', 'a.coupon_id', 'a.coupon_num', 'a.deadline_time'
        ];
        $firstBuilder = CouponDetail::query()->from('coupon_detail as a')
            ->select($selectFields)
            ->join('coupon as b', 'a.coupon_id', '=', 'b.id')
            ->where('b.scope', CouponConstant::COUPON_SCOPE_ALL)
            ->where('a.is_use', CouponConstant::COUPON_USE_NO)
            ->where('a.patient_user_id', $userId)
            ->where('a.deadline_time', '>=', $time)
            ->where('a.is_delete', Delete::UNDELETED);
        $secondBuilder = CouponDetail::query()->from('coupon_detail as a')
            ->select($selectFields)
            ->join('coupon_branch_store as b', 'a.coupon_id', '=', 'b.coupon_id')
            ->where('b.store_id', $storeId)
            ->where('a.patient_user_id', $userId)
            ->where('a.is_use', CouponConstant::COUPON_USE_NO)
            ->where('a.deadline_time', '>=', $time)
            ->where('a.is_delete', Delete::UNDELETED);
        $thirdBuilder = CouponDetail::query()->from('coupon_detail as a')
            ->select($selectFields)
            ->join('coupon_main_store as b', 'a.coupon_id', '=', 'b.coupon_id')
            ->where('b.main_store_id', $mainStoreId)
            ->where('a.is_use', CouponConstant::COUPON_USE_NO)
            ->where('a.patient_user_id', $userId)
            ->where('a.deadline_time', '>=', $time)
            ->where('a.is_delete', Delete::UNDELETED);
        return $firstBuilder->union($secondBuilder)->union($thirdBuilder);
    }

    public static function getCouponByNo($couponNo): ?CouponDetail
    {
        return CouponDetail::query()
            ->where('coupon_num', $couponNo)
            ->where('is_delete', Delete::UNDELETED)
            ->first();
    }

    public static function revokeCoupon(CouponDetail $couponDetail): int
    {
        return CouponDetail::query()
            ->where('id', $couponDetail->id)
            ->where('is_use', CouponConstant::COUPON_USE_NO)
            ->where('is_delete', Delete::UNDELETED)
            ->update([
                'is_delete' => Delete::DELETED,
                'updated_at' => Carbon::now()
            ]);
    }
}
