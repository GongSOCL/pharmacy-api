<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Constants\Status;
use App\Model\Qa\ComplianceQuestionAnswer;
use App\Model\Qa\TComplianceQuestionAnswer;

class ComplianceQuestionAnswerRepository
{

    public static function getByQuestionYYIDS(array $questionIds)
    {
        return TComplianceQuestionAnswer::whereIn('question_item_yyid', $questionIds)
            ->where('status', Status::REGULAR)
            ->orderBy('question_item_yyid')
            ->get();
    }
}
