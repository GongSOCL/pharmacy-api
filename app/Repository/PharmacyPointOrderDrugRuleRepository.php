<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\Delete;
use App\Model\Pharmacy\OrderDetail;
use App\Model\Pharmacy\PharmacyPointEvent;
use App\Model\Pharmacy\PharmacyPointOrderDrugRule;
use Hyperf\Utils\Collection;
use function Swoole\Coroutine\Http\get;

class PharmacyPointOrderDrugRuleRepository
{
    public static function getEventDetail(PharmacyPointEvent $event): Collection
    {
        return PharmacyPointOrderDrugRule::query()
            ->where('event_id', $event->id)
            ->where('is_delete', Delete::UNDELETED)
            ->get();
    }

    public static function getValidRules($orderId, $eventKey): Collection
    {
        return PharmacyPointOrderDrugRule::query()
            ->join('order_detail as od', function ($query) {
                $query->on('od.yy_product_id', '=', 'pharmacy_point_order_drug_rule.product_id')
                    ->on('od.yy_sku_id', '=', 'pharmacy_point_order_drug_rule.sku_id');
            })->join('pharmacy_point_event as ppe', 'ppe.id', '=', 'pharmacy_point_order_drug_rule.event_id')
            ->where('od.order_id', $orderId)
            ->where('ppe.event_key', $eventKey)
            ->select(['od.product_name', 'od.yy_sku_name', 'od.num as buy_num', 'pharmacy_point_order_drug_rule.*'])
            ->get();
    }
}
