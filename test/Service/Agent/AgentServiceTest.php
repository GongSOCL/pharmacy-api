<?php
declare(strict_types=1);
namespace HyperfTest\Service\Agent;

use App\Model\Pharmacy\ShareTokenSchema;
use App\Service\Agent\AgentService;
use Grpc\Pharmacy\Common\SHARE_TYPE;
use Grpc\Pharmacy\Common\ShareLinkToken;
use PHPUnit\Framework\TestCase;

class AgentServiceTest extends TestCase
{

    public function testGetShareLinkFromShareId()
    {
        $token = AgentService::getShareLinkFromShareId(3);
        $this->assertNotNull($token);
        var_dump($token->serializeToJsonString());
    }

    public function testGetShareLinkTokenSchema()
    {
        $share = new ShareLinkToken();
        $share->setStoreId(0);
        $share->setAgentId(1);
        $share->setType(SHARE_TYPE::SHARE_AGENT_CLERK);
        $link = AgentService::getShareLinkTokenSchema(
            ShareTokenSchema::BIZ_TYPE_CLERK_INVITE,
            0,
            'pages/clerk/registerForm',
            $share
        );
        $this->assertNotEmpty($link);
        echo $link . PHP_EOL;
    }
}
