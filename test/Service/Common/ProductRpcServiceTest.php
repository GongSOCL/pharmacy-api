<?php

namespace HyperfTest\Service\Common;

use App\Service\Common\ProductRpcService;
use PHPUnit\Framework\TestCase;

class ProductRpcServiceTest extends TestCase
{

    public function testListProductsByIds()
    {
        $resp = ProductRpcService::listProductsByIds([11]);
        $this->assertGreaterThan(0, $resp->getProducts()->count());
        var_dump($resp->serializeToJsonString());
    }
}
