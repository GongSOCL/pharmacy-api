<?php
declare(strict_types=1);
namespace HyperfTest\Service\Common;

use App\Model\Pharmacy\ShareTokenSchema;
use App\Repository\ShareTokenSchemaRepository;
use App\Service\Common\SchemaService;
use Grpc\Pharmacy\Common\ShareLinkToken;
use PHPUnit\Framework\TestCase;

class SchemaServiceTest extends TestCase
{

    public function testIsExists()
    {
        $bizType = ShareTokenSchema::BIZ_TYPE_DOCTOR_SCHEMA;
        $bizId = 1;
        $share = new ShareLinkToken();
        $isExists = SchemaService::isExists($bizType, $bizId);
        if (!$isExists) {
            $share->setStoreId(0);
            $share->setDoctorId(1);
            $link= SchemaService::newSchemaRecord($share->serializeToString(), $bizType, $bizId, 'pages/patient/purchase');
        } else {
            $link = SchemaService::getSchemaUploadImage($bizType, $bizId);
        }
        $this->assertNotEmpty($link);
        var_dump($link);
    }

    public function test1()
    {
        $bizType = ShareTokenSchema::BIZ_TYPE_DOCTOR_SCHEMA;
        $bizId = 1;
        $schema = ShareTokenSchemaRepository::getBizSchema($bizType, $bizId);
        $this->assertNotNull($schema);
        $token = SchemaService::getStoredShareToken($schema->id);
        $this->assertNotNull($token);
        $link = new ShareLinkToken();
        $link->mergeFromString($token);
        $this->assertGreaterThan(0, $link->getDoctorId());
        var_dump($link->getDoctorId());
    }
}
