<?php

namespace HyperfTest\Service\Order;

use App\Repository\PurchaseNotifyRepository;
use App\Repository\UserClerkRepository;
use App\Service\Order\OrderService;
use PHPUnit\Framework\TestCase;

class OrderServiceTest extends TestCase
{

    public function testProcessOrder()
    {
        $clerk = UserClerkRepository::getUserClerkByUserId(2);
        $this->assertNotNull($clerk);
        $notify = PurchaseNotifyRepository::getNotifyInfo(7);
        OrderService::processOrder(
            $clerk,
            $notify,
            [2476],
            [
                [
                    "product_id"=> 11,
                    "sku_id"=> 55,
                    "total"=>"3",
                    "cash"=> "45"
                ],
                [
                    "product_id"=> 11,
                    "sku_id" => 21,
                    "total" => "5",
                    "cash" => "66"
                ]
            ]
        );
        $this->assertTrue(true);
    }
}
