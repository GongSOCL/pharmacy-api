<?php

namespace HyperfTest\Service\Wechat;

use App\Helper\Helper;
use App\Service\Wechat\WechatObjectService;
use Grpc\Pharmacy\Common\ShareLinkToken;
use Hyperf\Utils\ApplicationContext;
use PHPUnit\Framework\TestCase;

class WechatObjectServiceTest extends TestCase
{

    public function testGetSessionFromCode()
    {
        $wechat = ApplicationContext::getContainer()
            ->get(WechatObjectService::class);
        $this->assertNotNull($wechat);

        $res = $wechat->getSessionFromCode('053J27200FgBNO1ob6100DxWHn2J2728');
        print_r($res);
        $this->assertNotNull($res);
    }

    public function testGetAccessToken()
    {
        $res = ApplicationContext::getContainer()
            ->get(WechatObjectService::class)
            ->getAccessToken();
        $this->assertNotEmpty($res);
        $this->assertArrayHasKey('access_token', $res);
        print_r($res);
    }

    public function test1()
    {
        $token = new ShareLinkToken();
        $token->setAgentId(5358);
        $t = Helper::genShareToken($token);

        $res = ApplicationContext::getContainer()
            ->get(WechatObjectService::class)
            ->getScheme([
                't' => 'abc123'
            ], '/tmp/test4.png', '/');
        $this->assertNotEmpty($res);
        echo($res);
    }

    public function testGetUrlLink()
    {
        $link = ApplicationContext::getContainer()
            ->get(WechatObjectService::class)
            ->getUrlLink('/', [
                'token' => 'abc'
            ], true, 3600);
        $this->assertTrue(true);
        var_dump($link);
    }
}
