<?php
declare(strict_types=1);
namespace HyperfTest\Service\User;

use App\Constants\Role;
use App\Repository\UserRepository;
use App\Service\User\UserCacheService;
use App\Service\User\UserClerkService;
use Hyperf\Utils\Context;
use PHPUnit\Framework\TestCase;

class UserClerkServiceTest extends TestCase
{

    protected function setUp(): void
    {
        $user = UserRepository::getUserById(9);
        $this->assertNotNull($user);
        Context::set('user', $user);
        Context::set('role', Role::ROLE_CLERK);
    }

    public function testGetUserServicePointRank()
    {
        [$list, $mine] = UserClerkService::getUserServicePointRank(UserCacheService::SCOPE_DAY, 10);
        $this->assertNotEmpty($list);
        print_r($list);
    }

    public function testGetStoreServicePointRank()
    {
        [$list, $mine] = UserClerkService::getStoreServicePointRank(UserCacheService::SCOPE_DAY, 10);
        $this->assertNotEmpty($list);
        print_r($list);
        print_r($mine);
    }

    public function testGetStoreMoneyPointRank()
    {
        [$list, $mine ]  = UserClerkService::getStoreMoneyPointRank(
            4,
            8,
            10
        );
        $this->assertNotEmpty($list);
        print_r($list);
        print_r($mine);
    }

    public function testGetUserMoneyPointRank()
    {
        [$list, $mine ]  = UserClerkService::getUserMoneyPointRank(
            2,
            8,
            10
        );
        $this->assertNotEmpty($list);
        print_r($list);
        print_r($mine);
    }

    public function testGetCityPointRank()
    {
        [$list, $mine] = UserClerkService::getCityServicePointRank(UserCacheService::SCOPE_DAY, 10);
        $this->assertTrue(true);
        print_r($list);
        print_r($mine);
    }


    public function testGetCityMoneyPointRankInfo()
    {
        [$list, $mine] = UserClerkService::getCityMoneyPointRankInfo(UserCacheService::SCOPE_YEAR, 8);
        $this->assertTrue(true);
        print_r($list);
        print_r($mine);
    }

}
