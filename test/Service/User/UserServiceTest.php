<?php
declare(strict_types=1);
namespace HyperfTest\Service\User;

use App\Constants\Role;
use App\Repository\UserRepository;
use App\Service\User\UserService;
use Hyperf\Utils\Context;
use PHPUnit\Framework\TestCase;

class UserServiceTest extends TestCase
{
    protected function setUp(): void
    {
        $user = UserRepository::getUserById(6);
        $this->assertNotNull($user);
        Context::set('user', $user);
        Context::set('role', Role::ROLE_PATIENT);
    }

    public function testGetAggregationInfo()
    {
        $resp = UserService::getAggregationInfo();
        $this->assertNotEmpty($resp);
        print_r($resp);
    }
}
