<?php
declare(strict_types=1);
namespace HyperfTest\Service\User;

use App\Constants\Role;
use App\Helper\Helper;
use App\Model\Pharmacy\User;
use App\Repository\UserRepository;
use App\Repository\UserRoleRepository;
use App\Service\User\UserRoleService;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Context;
use PHPUnit\Framework\TestCase;

class UserRoleServiceTest extends TestCase
{

    protected function setUp(): void
    {
        $user = UserRepository::getUserById(4);
        $this->assertNotNull($user);
        Context::set('user', $user);
        Context::set('role', Role::ROLE_PATIENT);
        Db::beginTransaction();
    }

    protected function tearDown(): void
    {
        Db::rollBack();
    }

    public function testSetRole()
    {
        $user = Helper::getLoginUser();
        $this->assertNotNull($user);
        $this->assertInstanceOf(User::class, $user);
        (new UserRoleService())->setRole($user, Role::ROLE_PATIENT, 3);
        $this->assertTrue(true);
    }
}
