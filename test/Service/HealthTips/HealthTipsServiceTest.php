<?php
declare(strict_types=1);
namespace HyperfTest\Service\HealthTips;

use App\Constants\Role;
use App\Repository\UserRepository;
use App\Service\HealthTips\HealthTipsService;
use Hyperf\Utils\Context;
use PHPUnit\Framework\TestCase;

class HealthTipsServiceTest extends TestCase
{

    protected function setUp(): void
    {
        $user = UserRepository::getUserById(9);
        $this->assertNotNull($user);
        Context::set('user', $user);
        Context::set('role', Role::ROLE_CLERK);
    }

    public function getResourceByID()
    {
        $data = HealthTipsService::getResourceByID(2);
        $this->assertNotNull($data);
    }

    public function testGetQuestionAnswer()
    {
        $data = HealthTipsService::getQuestionAnswer(8, '治疗');
        $this->assertTrue(true);
        print_r($data);
    }
}
