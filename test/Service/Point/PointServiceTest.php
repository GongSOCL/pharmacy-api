<?php
declare(strict_types=1);
namespace Service\Point;

use App\Constants\InsideMsgTypes;
use App\Model\Pharmacy\PharmacyPointEvent;
use App\Repository\UserClerkRepository;
use App\Service\Point\PointService;
use PHPUnit\Framework\TestCase;

class PointServiceTest extends TestCase
{

    public function testSendClerkPointFromCommonEvent()
    {
        $userClerk = UserClerkRepository::getUserClerkByUserId(6);
        $this->assertNotNull($userClerk);
        PointService::sendClerkPointFromCommonEvent(
            PharmacyPointEvent::PHARMACY_CLERK_READ_ARTICLE,
            $userClerk,
            InsideMsgTypes::MSG_TYPE_CLERK_READ_ARTICLE_GAIN_POINT,
            1,
            '阅读文章得积分'
        );
    }
}