<?php
declare(strict_types=1);
namespace HyperfTest\Helper;

use App\Helper\Minio;
use PHPUnit\Framework\TestCase;

class MinioTest extends TestCase
{

    public function testGetMinio()
    {
        $path = sprintf("/test/%s/test.txt", date('Ymd'));
        $fs = Minio::getMinio();
        if ($fs->fileExists($path)) {
            $fs->delete($path);
        }
        $fs->write($path, "Hello world");
        $this->assertTrue(true);
    }
}
