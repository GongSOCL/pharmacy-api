<?php
declare(strict_types=1);
namespace HyperfTest\Helper;

use App\Helper\Helper;
use Grpc\Pharmacy\Common\ShareLinkToken;
use PHPUnit\Framework\TestCase;

class HelperTest extends TestCase
{

    public function testGenShareToken()
    {
        $token = new ShareLinkToken();
        $token->setDoctorId(1);
        $str = Helper::genShareToken($token);
        $this->assertNotNull($str);

        $t = Helper::decodeShareToken($str);
        $this->assertInstanceOf(ShareLinkToken::class, $t);
        $this->assertEquals(1, $t->getDoctorId());

        $token = new ShareLinkToken();
        $token->setAgentId(5383);
        $res = Helper::genShareToken($token);
        $this->assertNotNull($res);
        echo $res . PHP_EOL;
    }

    public function test1()
    {
        $resp = Helper::decodeShareToken("VVBIZXRoTkdzcDFDb2pUZ1V6eVRLQT09");
        $this->assertGreaterThan(0, $resp->getAgentId());
        var_dump($resp->getAgentId());
    }

    public function test2()
    {
        $share = new ShareLinkToken();
        $share->setAgentId(5383);
        $share->setStoreId(348468);
        $a = $share->serializeToString();
        $b = bin2hex($a);
        var_dump($a, bin2hex($a));
        $c = new ShareLinkToken();
        $c->mergeFromString(hex2bin($b));
        var_dump($c->getAgentId(), $c->getStoreId());
    }
}
