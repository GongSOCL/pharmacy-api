<?php
declare(strict_types=1);
namespace HyperfTest\Repository\Point;

use App\Repository\Point\PointGoodRepository;
use Hyperf\DbConnection\Db;
use PHPUnit\Framework\TestCase;

class PointGoodRepositoryTest extends TestCase
{
    protected function setUp(): void
    {
        Db::beginTransaction();
    }

    protected function tearDown(): void
    {
        Db::rollBack();
    }

    public function testExchangeBefore()
    {
        $goods = PointGoodRepository::getById(3);
        $this->assertNotNull($goods);
        $res = PointGoodRepository::exchangeBefore($goods, 1);
        $this->assertEquals(1, $res);
    }
}
