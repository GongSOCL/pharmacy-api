<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/doctor-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
return [
    'ak' => env('QINIU_ACCESS_KEY'),
    'sk' => env('QINIU_SECRET_KEY'),
    'bucket' => [
        'static' => [
            'name' => env('QINIU_STATIC_BUCKET'),
            'domain' => env('QINIU_STATIC_DOMAIN'),
        ],
        'user' => [
            'name' => env('QINIU_USER_BUCKET'),
            'domain' => env('QINIU_USER_DOMAIN'),
        ],
    ],
];
