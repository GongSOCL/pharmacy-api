<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/doctor-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
return [
    'point' => [
        'host' => env('POINTS_SERVICE_HOST', 'point:8080')
    ],
    'core' => [
        'host' => env('SERVICE_CORE_HOST', 'core:9501'),
    ],
    'message' => [
        'host' => env('SERVICE_MESSAGE_HOST', 'msg:8080'),
    ],
    'activity' => [
        'host' => env('SERVICE_ACTIVITY_HOST', 'activity:9501'),
    ],
    'token' => [
        'host' => env('SERVICE_TOKEN_HOST', 'token:8080')
    ],
    'wxapi' => [
        'host' => env('SERVICE_WXAPI_HOST', 'wxapi:9502')
    ],
    'doctor' => [
        'host' => env('SERVICE_DOCTOR_HOST', 'doctor-newapi:9502')
    ]
];
