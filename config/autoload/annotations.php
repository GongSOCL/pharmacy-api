<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/doctor-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
return [
    'scan' => [
        'paths' => [
            BASE_PATH . '/app',
        ],
        'ignore_annotations' => [
            'mixin',
            'OA\Post',
            'OA\RequestBody',
            'OA\MediaType',
            'OA\Schema',
            'OA\Property',
            'OA\Response',
            'OA\Items',
            'param',
            'return',
            'throws',
            'OA\Put',
            'OA\Parameter',
            'OA\Patch',
            'OA\Get'
        ],
    ],
];
