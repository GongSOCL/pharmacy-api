<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/doctor-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
return [
    'handler' => [
        'http' => [
            \Youyao\Framework\Exception\Handler\GrpcResponseExceptionHandler::class,
            \App\Exception\Handler\BusinessExceptionHandler::class,
            \Youyao\Framework\Exception\Handler\ValidationExceptionHandler::class,
            \App\Exception\Handler\AppExceptionHandler::class,
        ],
    ],
];
