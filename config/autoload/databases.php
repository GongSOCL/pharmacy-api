<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/doctor-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
return [
    'default' => [
        'driver' => env('DB_DRIVER', 'mysql'),
        'host' => env('DB_HOST', 'localhost'),
        'database' => env('DB_DATABASE', 'youyao_pharmacy_qa'),
        'port' => env('DB_PORT', 3306),
        'username' => env('DB_USERNAME', 'root'),
        'password' => env('DB_PASSWORD', 'root'),
        'charset' => env('DB_CHARSET', 'utf8mb4'),
        'collation' => env('DB_COLLATION', 'utf8mb4_general_ci'),
        'prefix' => env('DB_PREFIX', ''),
        'pool' => [
            'min_connections' => 1,
            'max_connections' => 100,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float) env('DB_MAX_IDLE_TIME', 60),
        ],
        'commands' => [
            'gen:model' => [
                'path' => 'app/Model/Pharmacy',
                'force_casts' => true,
                'inheritance' => 'Model'
            ],
        ],
    ],

    'qa' => [
        'driver' => env('DB_DRIVER', 'mysql'),
        'host' => env('DB_QA_HOST', 'localhost'),
        'database' => env('DB_QA_DATABASE', 'youyao_qa'),
        'port' => env('DB_QA_PORT', 3306),
        'username' => env('DB_QA_USERNAME', 'root'),
        'password' => env('DB_QA_PASSWORD', 'root'),
        'charset' => env('DB_CHARSET', 'utf8mb4'),
        'collation' => env('DB_COLLATION', 'utf8mb4_general_ci'),
        'prefix' => env('DB_PREFIX', ''),
        'pool' => [
            'min_connections' => 1,
            'max_connections' => 10,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float) env('DB_MAX_IDLE_TIME', 60),
        ],
        'commands' => [
            'gen:model' => [
                'path' => 'app/Model/Qa',
                'force_casts' => true,
                'inheritance' => 'Model',
            ],
        ],
    ],
];
