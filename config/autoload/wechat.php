<?php
return [
    'pharmacy' => [
        'config' => [
            'app_id' => env('WECHAT_APP_ID'),
            'secret' => env('WECHAT_SECRET'),
            'token' => env('WECHAT_TOKEN', ''),
            'response_type' => 'array',
        ],
        'channel' => env('TOKEN_CHANNEL_ALIAS', 'pharmacy_miniapp'),
        'templates' => [],
    ]
];
