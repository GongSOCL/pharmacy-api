<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/admin-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
return [
    'token_expires' => 60 * 15 * 11000,
    'key' => 'X{l|<r)OE.JIL"`})5`Zn#`WTq^{?k|&',
    'alg' => env('JWT_ALG', 'HS256')
];
